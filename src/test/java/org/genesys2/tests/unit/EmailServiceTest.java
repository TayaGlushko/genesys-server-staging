/**
 * Copyright 2015 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
package org.genesys2.tests.unit;

import static org.junit.Assert.assertTrue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.MimeMessagePreparator;

public class EmailServiceTest extends AbstractServicesTest {

	private final Log LOG = LogFactory.getLog(EmailServiceTest.class);

	@Value("${mail.user.from}")
	String emailFrom;

	@Value("${mail.requests.to}")
	String emailTo;

	@After
	public void teardown() {
		Mockito.reset(mailSender);
	}

	@Test
	public void toEmailsTest() {
		LOG.info("Start test-method toEmailsTest");

		String[] emails = eMailService.toEmails("salexandrbasov@gmail.com,test@mail.ru;mail@gmail.com;alex@mail.com");
		assertTrue(emails != null);
		assertTrue(emails.length == 4);
		assertTrue(emails[0].equals("salexandrbasov@gmail.com"));
		assertTrue(emails[1].equals("test@mail.ru"));
		assertTrue(emails[2].equals("mail@gmail.com"));
		assertTrue(emails[3].equals("alex@mail.com"));

		LOG.info("Test toEmailsTest passed!");
	}

	@Test
	public void toEmailsEmptyTest() {
		LOG.info("Start test-method toEmailsEmptyTest");

		String[] emails = eMailService.toEmails("");
		assertTrue(emails == null);

		LOG.info("Test toEmailsEmptyTest passed!");
	}

	@Test
	public void sendMailTest() {
		LOG.info("Start test-method sendMailTest");

		String mailSubject = "mailSubject";
		String mailBody = "mail body";

		ArgumentCaptor<MimeMessagePreparator> argumentCaptor = ArgumentCaptor.forClass(MimeMessagePreparator.class);
		// Mockito.doNothing().when(mailSender).send(isA(MimeMessagePreparator.class));

		eMailService.sendMail(mailSubject, mailBody, "test@localhost");

		Mockito.verify(mailSender).send(argumentCaptor.capture());

		LOG.info("Test sendMailTest passed!");
	}
}
