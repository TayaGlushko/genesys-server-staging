/**
 * Copyright 2015 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
package org.genesys2.tests.unit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.isA;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.exception.UserException;
import org.genesys2.server.model.UserRole;
import org.genesys2.server.model.impl.Article;
import org.genesys2.server.model.impl.User;
import org.genesys2.server.model.impl.VerificationToken;
import org.genesys2.server.security.AuthUserDetails;
import org.genesys2.server.service.ContentService;
import org.genesys2.server.service.PasswordPolicy.PasswordPolicyException;
import org.genesys2.server.service.TokenVerificationService;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

public class EmailVerificationServiceTest extends AbstractServicesTest {
	private final Log LOG = LogFactory.getLog(EmailVerificationServiceTest.class);

	@Value("${mail.user.from}")
	String emailFrom;

	@Value("${mail.requests.to}")
	String emailTo;
    @Autowired
    public ContentService contentService;
	@Before
	public void setUp() throws PasswordPolicyException {
		LOG.info("set up");

		try {
			User user = new User();
			user.setName("Name 1");
			user.setPassword("password of u1!ser");
			user.setEmail("salexandrbasov@gmail.com");
			user.setUuid("Uuid");

			userService.addUser(user);

			Article oneArticle = new Article();
			oneArticle.setTitle("title for oneArticle");
			oneArticle.setBody("<h1>Tho BOdy of oneArticle</h1");
			oneArticle.setSlug("smtp.email-verification");
			oneArticle.setLang("en");

			Article twoArticle = new Article();
			twoArticle.setTitle("title for twoArticle");
			twoArticle.setBody("<h1>Tho BOdy of twoArticle</h1");
			twoArticle.setSlug("smtp.email-password");
			twoArticle.setLang("en");

			oneArticle.setClassPk(contentService.ensureClassPK(Article.class));

			twoArticle.setClassPk(contentService.ensureClassPK(Article.class));

			List<Article> articleList = new ArrayList<>();
			articleList.add(oneArticle);
			articleList.add(twoArticle);
			contentService.save(articleList);

		} catch (UserException e) {
			e.printStackTrace();
		}
		LOG.info("finish set up");
	}

	@After
	public void teardown() {
		try {
			for (User user : userService.listUsers(new PageRequest(0, 10)).getContent()) {
				userService.removeUser(user);
			}
			articleRepository.deleteAll();
			Mockito.reset(mailSender);
		} catch (UserException e) {
			e.printStackTrace();
		}
	}

	private void doAuthentication(User user) {
		List<GrantedAuthority> authorities = new ArrayList<>();
		GrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(UserRole.ADMINISTRATOR.getName());
		authorities.add(simpleGrantedAuthority);

		AuthUserDetails authUserDetails = new AuthUserDetails(user.getName(), user.getPassword(), authorities);

		// set actual DB user
		authUserDetails.setUser(user);

		UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(authUserDetails, authUserDetails, authorities);

		SecurityContextHolder.getContext().setAuthentication(authToken);
	}

	@Test
	public void sendVerificationEmailTest() throws InterruptedException {
		LOG.info("Start test-method sendVerificationEmailTest");

		Mockito.doNothing().when(mailSender).send(isA(MimeMessagePreparator.class));
		emailVerificationService.sendVerificationEmail(userService.listUsers(new PageRequest(0, 1)).getContent().get(0));
		Thread.sleep(100);
		Mockito.verify(mailSender).send(isA(MimeMessagePreparator.class));

		LOG.info("Test sendVerificationEmailTest passed!");
	}

	@Test
	public void sendPasswordResetEmailTest() throws InterruptedException {
		LOG.info("Start test-method sendPasswordResetEmailTest");
		User user = userService.listUsers(new PageRequest(0, 1)).getContent().get(0);
		Mockito.doNothing().when(mailSender).send(isA(MimeMessagePreparator.class));
		emailVerificationService.sendPasswordResetEmail(user);
		Thread.sleep(100);
		Mockito.verify(mailSender).send(isA(MimeMessagePreparator.class));

		LOG.info("Test sendPasswordResetEmailTest passed!");
	}

	// FIXME Does not consider existing data
	@Ignore
	@Test
	public void cancelValidationTest() throws TokenVerificationService.NoSuchVerificationTokenException {
		LOG.info("Start test-method cancelValidationTest");

		assertTrue(!verificationTokenRepository.findAll().isEmpty());
        int size1 = verificationTokenRepository.findAll().size();
		emailVerificationService.cancelValidation(verificationTokenRepository.findAll().get(0).getUuid());
        int size2 = verificationTokenRepository.findAll().size();
		assertTrue(size1 != size2);

		LOG.info("Test cancelValidationTest passed!");
	}

	// FIXME Transactional?
	@Ignore
	@Test
	public void validateEMailTest() {
		LOG.info("Start test-method validateEMailTest");

		try {
			VerificationToken verificationToken = verificationTokenRepository.findAll().get(0);

			User user = userService.listUsers(new PageRequest(0, 1)).getContent().get(0);

			doAuthentication(user);

			assertFalse(userService.getUserByEmail("salexandrbasov@gmail.com").getRoles().contains(UserRole.VALIDATEDUSER));

			assertTrue(!verificationTokenRepository.findAll().isEmpty());

			emailVerificationService.validateEMail(verificationToken.getUuid(), verificationToken.getKey());

			assertTrue(userService.getUserByEmail("salexandrbasov@gmail.com").getRoles().contains(UserRole.VALIDATEDUSER));

			assertTrue(verificationTokenRepository.findAll().isEmpty());

		} catch (TokenVerificationService.NoSuchVerificationTokenException e) {
			e.printStackTrace();
		}

		LOG.info("Test validateEMailTest passed!");
	}

	// FIXME Transactional?
	@Ignore
	@Test
	public void changePasswordTest() throws PasswordPolicyException {
		LOG.info("Start test-method changePasswordTest");

		try {
			User user = userService.listUsers(new PageRequest(0, 1)).getContent().get(0);

			assertTrue(user.getPassword().equals("password of user"));

			VerificationToken verificationToken = tokenVerificationService.generateToken("email-password", user.getUuid());

			emailVerificationService.changePassword(verificationToken.getUuid(), verificationToken.getKey(), "1341@#!$!@#!new password for user");

			assertTrue(userService.listUsers(new PageRequest(0, 1)).getContent().get(0).getPassword().equals("new password for user"));
		} catch (TokenVerificationService.NoSuchVerificationTokenException e) {
			e.printStackTrace();
		}
		LOG.info("Test changePasswordTest passed!");
	}
}
