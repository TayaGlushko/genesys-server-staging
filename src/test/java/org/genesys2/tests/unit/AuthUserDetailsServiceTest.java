/**
 * Copyright 2015 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
package org.genesys2.tests.unit;

import static org.junit.Assert.assertTrue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.exception.UserException;
import org.genesys2.server.model.impl.User;
import org.genesys2.server.service.PasswordPolicy.PasswordPolicyException;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class AuthUserDetailsServiceTest extends AbstractServicesTest {

	private final Log LOG = LogFactory.getLog(AuthUserDetailsServiceTest.class);

	// FIXME Transactional?
	@Ignore
	@Test
	public void loadUserByUsernameTest() throws PasswordPolicyException {
		LOG.info("Start test-method loadUserByUsernameTest");

		User user = new User();
		user.setEmail("email@mail.com");
		user.setName("UserName");
		user.setPassword("User1Password!");

		try {
			userService.addUser(user);
			UserDetails userDetails = userDetailsService.loadUserByUsername(user.getEmail());
			assertTrue(userDetails != null);
			assertTrue(userDetails.getPassword().equals(user.getPassword()));
			userService.removeUser(user);

		} catch (UserException e) {
			org.junit.Assert.fail(e.getMessage());
		}

		LOG.info("Test loadUserByUsernameTest passed!");
	}

	@Test(expected = UsernameNotFoundException.class)
	public void loadUserByUsernameTestUserNotFound() {
		LOG.info("Start test-method loadUserByUsernameTestUserNotFound");

		assertTrue(null == userDetailsService.loadUserByUsername("WrongMail"));

		LOG.info("Test loadUserByUsernameTestUserNotFound passed!");
	}
}
