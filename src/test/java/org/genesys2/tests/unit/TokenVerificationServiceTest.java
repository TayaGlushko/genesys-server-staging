/**
 * Copyright 2015 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
package org.genesys2.tests.unit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.impl.VerificationToken;
import org.genesys2.server.service.TokenVerificationService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TokenVerificationServiceTest extends AbstractServicesTest {

	private static final Log LOG = LogFactory.getLog(TokenVerificationServiceTest.class);

	String tokenPurpose;

	String data;

	String key;

	@Before
	public void setUp() {

		tokenPurpose = "Token Purpose";
		data = "data";
		key = "key";
	}

	@After
	public void teardown() {

		if (isAnyTokensExists()) {
			tokenRepository.deleteAll();
		}
	}

	private boolean isAnyTokensExists() {
		return !tokenRepository.findAll().isEmpty();
	}

	@Test
	public void generateTokenTest() {
		LOG.info("Start test-method generateTokenTest");
        tokenRepository.deleteAll();
		assertTrue(!isAnyTokensExists());

		VerificationToken token = tokenVerificationService.generateToken(tokenPurpose, data);

		assertTrue(isAnyTokensExists());
		assertTrue(token != null);
		assertTrue(token.getId() != null);
		assertTrue(token.getData().equals(data));
		assertTrue(token.getPurpose().equals(tokenPurpose));
		assertTrue(token.getKey() != null);

		LOG.info("Test generateTokenTest passed!");
	}

	@Test
	public void cancelTest() {
		LOG.info("Start test-method cancelTest");

		try {
			assertFalse(isAnyTokensExists());

			VerificationToken token = tokenVerificationService.generateToken(tokenPurpose, data);

			assertTrue(token != null);
			assertTrue(isAnyTokensExists());

			tokenVerificationService.cancel(token.getUuid());

			assertTrue(!isAnyTokensExists());

		} catch (TokenVerificationService.NoSuchVerificationTokenException e) {
			fail(e.getMessage());
		}
		LOG.info("Test cancelTest passed!");
	}

	@Test
	public void consumeTokenTest() {
		LOG.info("Start test-method consumeTokenTest");

		try {
			assertFalse(isAnyTokensExists());

			VerificationToken token = tokenVerificationService.generateToken(tokenPurpose, data);

			assertTrue(token != null);
			assertTrue(isAnyTokensExists());

			token = tokenVerificationService.consumeToken(token.getPurpose(), token.getUuid(), token.getKey());

			assertTrue(token != null);
			assertTrue(!isAnyTokensExists());

		} catch (TokenVerificationService.NoSuchVerificationTokenException e) {
			fail(e.getMessage());
		}

		LOG.info("Test consumeTokenTest passed!");
	}

}
