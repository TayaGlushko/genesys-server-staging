/**
 * Copyright 2015 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
package org.genesys2.tests.unit;


import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.impl.ActivityPost;
import org.genesys2.server.model.impl.Article;
import org.genesys2.server.service.ContentService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;

public class ContentServiceTest extends AbstractServicesTest {

	private final Log LOG = LogFactory.getLog(ContentServiceTest.class);
    @Autowired
    public ContentService contentService;
	@Before
	public void setUp() {
		// create activity post for test
		contentService.createActivityPost("title of activity post", "body of activity post");

		// create article for test
		List<Article> articleList = new ArrayList<>();
		Article article = new Article();
		article.setLang("en");
		article.setSlug("smtp.email-verification");
		article.setTitle("title of article");
		article.setBody("<h3>body of article</h3");
		article.setClassPk(contentService.ensureClassPK(Article.class));
		articleList.add(article);
		contentService.save(articleList);

	}

	@After
	public void teardown() {
		// delete activity post
		postRepository.deleteAll();

		// delete articles
		articleRepository.deleteAll();
	}

	@Test
	public void getDefaultLocaleTest() {
		LOG.info("Start test-method getDefaultLocaleTest");

		assertNotNull(contentService.getDefaultLocale());

		LOG.info("Test getDefaultLocaleTest is passed!");
	}

	@Test
	public void lastNewsTest() {
		LOG.info("Start test-method lastNewsTest");

		List<ActivityPost> postList = contentService.lastNews();

		assertTrue(postList.size() == 1);
		assertTrue(postList.get(0).getTitle().equals("title of activity post"));
		assertTrue(postList.get(0).getBody().equals("body of activity post"));

		LOG.info("Test lastNewsTest is passed!");
	}

	@Test
	public void listArticlesTest() {
		LOG.info("Start test-method listArticlesTest");

		List<Article> articleList = contentService.listArticles(new PageRequest(0, 6)).getContent();
		assertNotNull("ArticleList must be not null", articleList);
		assertTrue(!articleList.isEmpty());

		LOG.info("Test listArticlesTest is passed!");
	}

	@Test
	public void listArticlesByLangTest() {
		LOG.info("Start test-method listArticlesByLangTest");

		List<Article> articleList = contentService.listArticlesByLang("ru", new PageRequest(0, 6)).getContent();
		assertTrue(articleList.isEmpty());

		articleList = contentService.listArticlesByLang("en", new PageRequest(0, 6)).getContent();
		assertTrue(!articleList.isEmpty());

		LOG.info("Test listArticlesByLangTest is passed!");
	}

	@Test
	public void saveTest() {
		LOG.info("Start test-method saveTest");

		List<Article> articleList = contentService.listArticles(new PageRequest(0, 6)).getContent();
		assertTrue(articleList.size() == 1);

		List<Article> articleListForSave = new ArrayList<>();
		Article article = new Article();
		article.setLang("en");
		article.setSlug("smtp.email-verification");
		article.setClassPk(contentService.ensureClassPK(Article.class));
		articleListForSave.add(article);
		contentService.save(articleListForSave);

		assertTrue(contentService.listArticles(new PageRequest(0, 6)).getContent().size() == 2);

		LOG.info("Test saveTest is passed!");
	}

	@Test
	public void getGlobalArticleTets() {
		LOG.info("Start test-method getGlobalArticleTets");

		assertNotNull(contentService.getGlobalArticle("smtp.email-verification", Locale.ENGLISH));

		LOG.info("Test getGlobalArticleTets is passed!");
	}

	@Test
	public void getArticleTest() {
		LOG.info("Start test-method getArticleTest");

		String slug = contentService.listArticles(new PageRequest(0, 6)).getContent().get(0).getSlug();

		Article article = contentService.getArticle(Article.class, null, slug, Locale.ENGLISH, true);

		assertNotNull(article);

		LOG.info("Test getArticleTest is passed!");
	}

	@Test
	public void getArticleBySlugAndLangTest() {
		LOG.info("Start test-method getArticleBySlugAndLangTest");

		Article article = contentService.getArticleBySlugAndLang("smtp.email-verification", "en");
		assertNotNull(article);

		LOG.info("Test getArticleBySlugAndLangTest is passed!");
	}

	@Test
	public void updateArticleTest() {
		LOG.info("Start test-method updateArticleTest");

		Article beforeUpdateArticle = contentService.listArticles(new PageRequest(0, 6)).getContent().get(0);

		contentService.updateArticle(beforeUpdateArticle.getId(), "new slug for update test", "title for update test", "<h2>Body for update test</h2", null);

		Article afterUpdateArticle = contentService.listArticles(new PageRequest(0, 6)).getContent().get(0);

		assertThat("Article body not updated", beforeUpdateArticle.getBody(), equalTo(afterUpdateArticle.getBody()));
		assertThat("Article title not updated", beforeUpdateArticle.getTitle(), equalTo(afterUpdateArticle.getTitle()));
		assertThat("Article slug not updated", beforeUpdateArticle.getSlug(), equalTo(afterUpdateArticle.getSlug()));

		LOG.info("Test updateArticleTest is passed!");
	}

	@Test
	public void createGlobalArticleTest() {
		LOG.info("Start test-method createGlobalArticleTest");

		String slug = "create_global_article";
		Locale locale = Locale.ENGLISH;
		String title = "Create Global Article";
		String body = "<h1>Create Global Article</h1>";
		String summary=null;

		Article globalArticle = contentService.updateGlobalArticle(slug, locale, title, body, summary);

		assertNotNull(globalArticle);
		assertTrue(contentService.listArticles(new PageRequest(0, 6)).getContent().size() == 2);

		LOG.info("Test createGlobalArticleTest is passed!");
	}

	@Test
	public void updateGlobalArticleTest() {
		LOG.info("Start test-method updateGlobalArticleTest");

		Locale locale = Locale.ENGLISH;
		String title = "Update Global Article Title";
		String body = "<h1>Update Global Article Body</h1>";

		Article globalArticle = contentService.updateGlobalArticle("smtp.email-verification", locale, title, body, null);

		assertThat("Articles list size mismatch", contentService.listArticles(new PageRequest(0, 6)).getContent().size(), is(1));
		assertThat("Article body not updated", globalArticle.getBody(), equalTo("<h1>Update Global Article Body</h1>"));
		assertThat("Article title must be updated", globalArticle.getTitle(), equalTo("Update Global Article Title"));

		LOG.info("Test updateGlobalArticleTest is passed!");
	}

	@Test
	public void ensureClassPKInternalTest() {
		LOG.info("Start test-method ensureClassPKInternalTest");

		assertNotNull(contentService.ensureClassPK(Article.class));

		LOG.info("Test ensureClassPKInternalTest is passed!");
	}

	@Test
	public void getActivityPostTest() {
		LOG.info("Start test-method getActivityPostTest");

		ActivityPost activityPost = postRepository.findAll().get(0);

		assertNotNull(contentService.getActivityPost(activityPost.getId()));

		LOG.info("Test getActivityPostTest is passed!");
	}

	@Test
	public void createActivityPostTest() {
		LOG.info("Start test-method createActivityPostTest");

		assertNotNull(contentService.createActivityPost("title for new activity post", "body of new activity post"));
		assertTrue(postRepository.findAll().size() == 2);

		LOG.info("Test createActivityPostTest is passed!");
	}

	@Test
	public void updateActivityPostTest() {
		LOG.info("Start test-method updateActivityPostTest");

		ActivityPost activityPost = postRepository.findAll().get(0);
		assertNotNull(contentService.updateActivityPost(activityPost.getId(), "update title", "update body"));
		assertTrue(postRepository.findAll().size() == 1);
		assertEquals(postRepository.findAll().get(0).getTitle(), "update title");

		LOG.info("Test updateActivityPostTest is passed!");
	}

	@Test
	public void deleteActivityPostTest() {
		LOG.info("Start test-method deleteActivityPostTest");

		ActivityPost activityPost = postRepository.findAll().get(0);
		contentService.deleteActivityPost(activityPost.getId());

		assertTrue(postRepository.findAll().isEmpty());

		LOG.info("Test deleteActivityPostTest is passed!");
	}

	@Test
	public void processTemplateTest() {
		LOG.info("Start test-method processTemplateTest");

		// Create the root hash
		Map<String, Object> root = new HashMap<String, Object>();
		root.put("baseUrl", "baseUrl");
		root.put("verificationToken", "verificationToken");
		root.put("pid", "pid");
		root.put("accessions", "accessions");

		String mailBody = contentService.processTemplate("body of article", root);

		assertNotNull(mailBody);

		LOG.info("Test processTemplateTest is passed!");
	}
}
