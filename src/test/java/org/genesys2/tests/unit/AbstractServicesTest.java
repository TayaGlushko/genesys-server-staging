/**
 * Copyright 2015 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
package org.genesys2.tests.unit;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.velocity.app.VelocityEngine;
import org.genesys2.server.aspect.AsAdminAspect;
import org.genesys2.server.persistence.domain.ActivityPostRepository;
import org.genesys2.server.persistence.domain.ArticleRepository;
import org.genesys2.server.persistence.domain.ClassPKRepository;
import org.genesys2.server.persistence.domain.CropRepository;
import org.genesys2.server.persistence.domain.CropRuleRepository;
import org.genesys2.server.persistence.domain.CropTaxonomyRepository;
import org.genesys2.server.persistence.domain.DescriptorRepository;
import org.genesys2.server.persistence.domain.GenesysLowlevelRepository;
import org.genesys2.server.persistence.domain.GenesysLowlevelRepositoryCustomImpl;
import org.genesys2.server.persistence.domain.Taxonomy2Repository;
import org.genesys2.server.persistence.domain.TeamRepository;
import org.genesys2.server.persistence.domain.TraitValueRepository;
import org.genesys2.server.persistence.domain.TraitValueRepositoryImpl;
import org.genesys2.server.persistence.domain.UserPersistence;
import org.genesys2.server.persistence.domain.VerificationTokenRepository;
import org.genesys2.server.service.AclService;
import org.genesys2.server.service.ContentService;
import org.genesys2.server.service.CropService;
import org.genesys2.server.service.DescriptorService;
import org.genesys2.server.service.EMailService;
import org.genesys2.server.service.EMailVerificationService;
import org.genesys2.server.service.ElasticSearchManagementService;
import org.genesys2.server.service.ElasticService;
import org.genesys2.server.service.GenesysFilterService;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.GeoService;
import org.genesys2.server.service.HtmlSanitizer;
import org.genesys2.server.service.InstituteService;
import org.genesys2.server.service.MappingService;
import org.genesys2.server.service.OrganizationService;
import org.genesys2.server.service.PasswordPolicy;
import org.genesys2.server.service.TaxonomyService;
import org.genesys2.server.service.TeamService;
import org.genesys2.server.service.TokenVerificationService;
import org.genesys2.server.service.TraitService;
import org.genesys2.server.service.UserService;
import org.genesys2.server.service.impl.AclServiceImpl;
import org.genesys2.server.service.impl.AuthUserDetailsService;
import org.genesys2.server.service.impl.ContentSanitizer;
import org.genesys2.server.service.impl.ContentServiceImpl;
import org.genesys2.server.service.impl.CropServiceImpl;
import org.genesys2.server.service.impl.DescriptorServiceImpl;
import org.genesys2.server.service.impl.EMailServiceImpl;
import org.genesys2.server.service.impl.EMailVerificationServiceImpl;
import org.genesys2.server.service.impl.ElasticSearchManagementServiceImpl;
import org.genesys2.server.service.impl.ElasticsearchSearchServiceImpl;
import org.genesys2.server.service.impl.FilterHandler;
import org.genesys2.server.service.impl.GenesysFilterServiceImpl;
import org.genesys2.server.service.impl.GenesysServiceImpl;
import org.genesys2.server.service.impl.GeoServiceImpl;
import org.genesys2.server.service.impl.InstituteServiceImpl;
import org.genesys2.server.service.impl.MappingServiceImpl;
import org.genesys2.server.service.impl.OWASPSanitizer;
import org.genesys2.server.service.impl.OrganizationServiceImpl;
import org.genesys2.server.service.impl.SimplePasswordPolicy;
import org.genesys2.server.service.impl.TaxonomyServiceImpl;
import org.genesys2.server.service.impl.TeamServiceImpl;
import org.genesys2.server.service.impl.TokenVerificationServiceImpl;
import org.genesys2.server.service.impl.TraitServiceImpl;
import org.genesys2.server.service.impl.UserServiceImpl;
import org.genesys2.server.service.worker.ElasticUpdater;
import org.genesys2.server.test.PropertyPlacholderInitializer;
import org.genesys2.tests.resttests.SpringTest;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.HierarchyMode;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextHierarchy(@ContextConfiguration(name = "this", classes = AbstractServicesTest.Config.class, initializers = PropertyPlacholderInitializer.class))
@ActiveProfiles("dev")
@Transactional(transactionManager = "transactionManager")
@Rollback(true)
public abstract class AbstractServicesTest extends SpringTest {

	@Configuration
	@DirtiesContext(hierarchyMode = HierarchyMode.CURRENT_LEVEL, classMode = DirtiesContext.ClassMode.AFTER_CLASS)
	public static class Config {

		@Bean
		public UserDetailsService userDetailsService() {
			return new AuthUserDetailsService();
		}

		@Bean
		public UserService userService() {
			return new UserServiceImpl();
		}

		@Bean
		public ContentService contentService() {
			return new ContentServiceImpl();
		}

		@Bean
		public HtmlSanitizer htmlSanitizer() {
			return new OWASPSanitizer();
		}

		@Bean
		public ContentSanitizer contentSanitizer() {
			return new ContentSanitizer();
		}

		@Bean
		public VelocityEngine velocityEngine() {
			return new VelocityEngine();
		}

		@Bean
		public DescriptorService descriptorService() {
			return new DescriptorServiceImpl();
		}

		@Bean
		public ThreadPoolTaskExecutor threadPoolTaskExecutor() {
			return new ThreadPoolTaskExecutor();
		}

		@Bean
		public EMailService eMailService() {
			return new EMailServiceImpl();
		}

		@Bean
		public JavaMailSender mailSender() {
			return Mockito.mock(JavaMailSenderImpl.class);
		}

		@Bean
		public EMailVerificationService emailVerificationService() {
			return new EMailVerificationServiceImpl();
		}

		@Bean
		public TokenVerificationService tokenVerificationService() {
			return new TokenVerificationServiceImpl();
		}

		@Bean
		public TaxonomyService taxonomyService() {
			return new TaxonomyServiceImpl();
		}

		@Bean
		public CropService cropService() {
			return new CropServiceImpl();
		}

		@Bean
		public TeamService teamService() {
			return new TeamServiceImpl();
		}

		@Bean
		public GenesysLowlevelRepository genesysLowlevelRepositoryCustomImpl() {
			return new GenesysLowlevelRepositoryCustomImpl();
		}

		@Bean
		public AclService aclService() {
			return new AclServiceImpl();
		}

		@Bean
		public AsAdminAspect asAdminAspect() {
			return new AsAdminAspect();
		}

		@Bean
		public InstituteService instituteService() {
			return new InstituteServiceImpl();
		}

		@Bean
		public GenesysFilterService genesysFilterService() {
			return new GenesysFilterServiceImpl();
		}

		@Bean
		public MappingService mappingService() {
			return new MappingServiceImpl();
		}

		@Bean
		public GenesysService genesysService() {
			return new GenesysServiceImpl();
		}

		@Bean
		public TraitValueRepository traitValueRepository() {
			return new TraitValueRepositoryImpl();
		}

		@Bean
		public OrganizationService organizationService() {
			return new OrganizationServiceImpl();
		}

		@Bean
		public TraitService traitService() {
			return new TraitServiceImpl();
		}

		@Bean
		public GeoService geoService() {
			return new GeoServiceImpl();
		}

		@Bean
		public PasswordPolicy passwordPolicy() {
			return new SimplePasswordPolicy();
		}
		
		@Bean
		public ElasticService searchService() {
			return new ElasticsearchSearchServiceImpl();
		}

		@Bean
		public ElasticSearchManagementService elasticSearchManagementService() {
			return new ElasticSearchManagementServiceImpl();
		}
		
		@Bean
		public ElasticUpdater elasticUpdater() {
			return new ElasticUpdater();
		}
		
		@Bean
		public FilterHandler filterHandler() {
			return new FilterHandler();
		}
		
		@Bean
		public ObjectMapper objectMapper() {
			return new ObjectMapper();
		}
	}

	@Autowired
	public UserDetailsService userDetailsService;

	@Autowired
	public UserPersistence userPersistence;

	@Autowired
	public ContentSanitizer contentSanitizer;

	@Autowired
	public ActivityPostRepository postRepository;

	@Autowired
	public ClassPKRepository classPkRepository;

	@Autowired
	public DescriptorService descriptorService;

	@Autowired
	public DescriptorRepository descriptorRepository;

	@Autowired
	public JavaMailSender mailSender;

	@Autowired
	public EMailService eMailService;

	@Autowired
	public VerificationTokenRepository verificationTokenRepository;

	@Autowired
	public EMailVerificationService emailVerificationService;

	@Autowired
	public ArticleRepository articleRepository;

	@Autowired
	public HtmlSanitizer htmlSanitizer;

	@Autowired
	public TaxonomyService taxonomyService;

	@Autowired
	public TokenVerificationService tokenVerificationService;

	@Autowired
	public VerificationTokenRepository tokenRepository;

	@Autowired
	public InstituteService instituteService;

	@Autowired
	public TeamRepository teamRepository;

	@Autowired
	public TeamService teamService;

	@Autowired
	public UserService userService;

	@Autowired
	public CropService cropService;

	@Autowired
	public CropRepository cropRepository;

	@Autowired
	public CropRuleRepository cropRuleRepository;

	@Autowired
	public CropTaxonomyRepository cropTaxonomyRepository;

	@Autowired
	public Taxonomy2Repository taxonomy2Repository;

	@Autowired
	public GenesysFilterService genesysFilterService;

	@Autowired
	public MappingService mappingService;
}
