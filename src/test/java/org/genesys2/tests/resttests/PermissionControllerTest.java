package org.genesys2.tests.resttests;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.exception.UserException;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.model.impl.Team;
import org.genesys2.server.model.impl.User;
import org.genesys2.server.service.PasswordPolicy.PasswordPolicyException;
import org.genesys2.server.servlet.model.PermissionJson;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

// FIXME No permissions
@Ignore
public class PermissionControllerTest extends AbstractRestTest {

	private static final Log LOG = LogFactory.getLog(PermissionControllerTest.class);

	@Autowired
	private WebApplicationContext webApplicationContext;

	MockMvc mockMvc;

	private User user = new User();
	private FaoInstitute faoInstitute;
	private Team team;
	private List<FaoInstitute> institutes = new ArrayList<>();
	private PermissionJson permissionJson;
	private JsonNode JSON_OK;

	@Before
	public void setUp() throws UserException, IOException, PasswordPolicyException {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

		user = new User();
		user.setEmail("salexandrbasov@gmail.com");
		user.setPassword("Alexandr19011990");
		user.setName("SYS_ADMIN");

		userService.addUser(user);

		faoInstitute = new FaoInstitute();
		faoInstitute.setFullName("This is name of institute");
		faoInstitute.setCurrent(true);
		faoInstitute.setPgrActivity(true);
		faoInstitute.setMaintainsCollection(true);
		faoInstitute.setPgrActivity(true);
		faoInstitute.setAccessionCount(1);
		faoInstitute.setUniqueAcceNumbs(true);
		faoInstitute.setCode("Code");

		institutes.add(faoInstitute);
		instituteService.update(institutes);

		ObjectMapper objectMapper = new ObjectMapper();
		JSON_OK = objectMapper.readTree("{\"result\":true}");

		permissionJson = new PermissionJson();
		permissionJson.setOid(faoInstitute.getId());
		permissionJson.setClazz(FaoInstitute.class.getName());
		permissionJson.setUuid(user.getEmail());
		permissionJson.setPrincipal(true);
		permissionJson.setRead(true);
		permissionJson.setWrite(true);
		permissionJson.setCreate(true);
		permissionJson.setDelete(false);
		permissionJson.setManage(true);
	}

	@After
	public void tearDown() {
		userPersistence.deleteAll();
		instituteSettingRepository.deleteAll();
		instituteRepository.deleteAll();
	}

	@Test
	public void addPermissionTest() throws Exception {
		LOG.info("Start test-method addPermissionTest");

		ObjectMapper objectMapper = new ObjectMapper();

		mockMvc.perform(post("/api/v0/permission/add").contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(permissionJson))).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(content().string(objectMapper.writeValueAsString(JSON_OK)));

		LOG.info("Test addPermissionTest is passed");
	}

	@Test
	public void updatePermissionsTest() throws Exception {
		LOG.info("Start test-method updatePermissionsTest");

		ObjectMapper objectMapper = new ObjectMapper();

		mockMvc.perform(post("/api/v0/permission/update").contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(permissionJson))).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(content().string(objectMapper.writeValueAsString(JSON_OK)));

		LOG.info("Test updatePermissionsTest is passed");
	}

	@Test
	public void acUserTest() throws Exception {
		LOG.info("Start test-method acUserTest");

		ObjectMapper objectMapper = new ObjectMapper();
		List<String> userEmails = new ArrayList<>();
		userEmails.add(user.getEmail());

		mockMvc.perform(get("/api/v0/permission/autocompleteuser").contentType(MediaType.APPLICATION_JSON).param("term", user.getEmail())).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(content().string(objectMapper.writeValueAsString(userEmails)));

		LOG.info("Test acUserTest is completed");
	}
}
