/*
 * Copyright 2016 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.genesys2.tests.resttests;

import java.io.File;

import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.velocity.app.VelocityEngine;
import org.genesys2.server.aspect.AsAdminAspect;
import org.genesys2.server.filerepository.service.BytesStorageService;
import org.genesys2.server.filerepository.service.ImageGalleryService;
import org.genesys2.server.filerepository.service.RepositoryService;
import org.genesys2.server.filerepository.service.ThumbnailGenerator;
import org.genesys2.server.filerepository.service.aspect.ImageGalleryAspects;
import org.genesys2.server.filerepository.service.impl.FilesystemStorageServiceImpl;
import org.genesys2.server.filerepository.service.impl.ImageGalleryServiceImpl;
import org.genesys2.server.filerepository.service.impl.RepositoryServiceImpl;
import org.genesys2.server.filerepository.service.impl.ThumbnailGenerator1;
import org.genesys2.server.persistence.acl.AclClassPersistence;
import org.genesys2.server.persistence.acl.AclEntryPersistence;
import org.genesys2.server.persistence.acl.AclObjectIdentityPersistence;
import org.genesys2.server.persistence.acl.AclSidPersistence;
import org.genesys2.server.persistence.domain.AccessionCustomRepository;
import org.genesys2.server.persistence.domain.AccessionCustomRepositoryImpl;
import org.genesys2.server.persistence.domain.AccessionHistoricRepository;
import org.genesys2.server.persistence.domain.AccessionRepository;
import org.genesys2.server.persistence.domain.ArticleRepository;
import org.genesys2.server.persistence.domain.CountryRepository;
import org.genesys2.server.persistence.domain.CropRepository;
import org.genesys2.server.persistence.domain.CropRuleRepository;
import org.genesys2.server.persistence.domain.FaoInstituteRepository;
import org.genesys2.server.persistence.domain.FaoInstituteSettingRepository;
import org.genesys2.server.persistence.domain.GenesysLowlevelRepository;
import org.genesys2.server.persistence.domain.GenesysLowlevelRepositoryCustomImpl;
import org.genesys2.server.persistence.domain.MaterialRequestRepository;
import org.genesys2.server.persistence.domain.MaterialSubRequestRepository;
import org.genesys2.server.persistence.domain.MetadataMethodRepository;
import org.genesys2.server.persistence.domain.MetadataRepository;
import org.genesys2.server.persistence.domain.MethodRepository;
import org.genesys2.server.persistence.domain.OAuthAccessTokenPersistence;
import org.genesys2.server.persistence.domain.OAuthClientDetailsPersistence;
import org.genesys2.server.persistence.domain.OAuthRefreshTokenPersistence;
import org.genesys2.server.persistence.domain.OrganizationRepository;
import org.genesys2.server.persistence.domain.ParameterCategoryRepository;
import org.genesys2.server.persistence.domain.ParameterRepository;
import org.genesys2.server.persistence.domain.Taxonomy2Repository;
import org.genesys2.server.persistence.domain.TeamRepository;
import org.genesys2.server.persistence.domain.TraitValueRepository;
import org.genesys2.server.persistence.domain.TraitValueRepositoryImpl;
import org.genesys2.server.persistence.domain.UserPersistence;
import org.genesys2.server.persistence.domain.kpi.KPIParameterRepository;
import org.genesys2.server.persistence.domain.kpi.ObservationRepository;
import org.genesys2.server.persistence.domain.mock.TraitServiceMock;
import org.genesys2.server.service.AclService;
import org.genesys2.server.service.BatchRESTService;
import org.genesys2.server.service.ContentService;
import org.genesys2.server.service.CropService;
import org.genesys2.server.service.DatasetService;
import org.genesys2.server.service.EMailService;
import org.genesys2.server.service.EMailVerificationService;
import org.genesys2.server.service.EasySMTA;
import org.genesys2.server.service.ElasticSearchManagementService;
import org.genesys2.server.service.ElasticService;
import org.genesys2.server.service.GenesysFilterService;
import org.genesys2.server.service.GenesysRESTService;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.GeoService;
import org.genesys2.server.service.HtmlSanitizer;
import org.genesys2.server.service.InstituteFilesService;
import org.genesys2.server.service.InstituteService;
import org.genesys2.server.service.JPATokenStore;
import org.genesys2.server.service.KPIService;
import org.genesys2.server.service.MappingService;
import org.genesys2.server.service.OAuth2ClientDetailsService;
import org.genesys2.server.service.OrganizationService;
import org.genesys2.server.service.PasswordPolicy;
import org.genesys2.server.service.RequestService;
import org.genesys2.server.service.TaxonomyService;
import org.genesys2.server.service.TeamService;
import org.genesys2.server.service.TokenVerificationService;
import org.genesys2.server.service.TraitService;
import org.genesys2.server.service.UserService;
import org.genesys2.server.service.impl.AclServiceImpl;
import org.genesys2.server.service.impl.BatchRESTServiceImpl;
import org.genesys2.server.service.impl.ContentServiceImpl;
import org.genesys2.server.service.impl.CropServiceImpl;
import org.genesys2.server.service.impl.EMailServiceImpl;
import org.genesys2.server.service.impl.EMailVerificationServiceImpl;
import org.genesys2.server.service.impl.EasySMTAMockConnector;
import org.genesys2.server.service.impl.ElasticSearchManagementServiceImpl;
import org.genesys2.server.service.impl.ElasticsearchSearchServiceImpl;
import org.genesys2.server.service.impl.FilterHandler;
import org.genesys2.server.service.impl.GenesysFilterServiceImpl;
import org.genesys2.server.service.impl.GenesysRESTServiceImpl;
import org.genesys2.server.service.impl.GenesysServiceImpl;
import org.genesys2.server.service.impl.GeoServiceImpl;
import org.genesys2.server.service.impl.InstituteFilesServiceImpl;
import org.genesys2.server.service.impl.InstituteServiceImpl;
import org.genesys2.server.service.impl.KPIServiceImpl;
import org.genesys2.server.service.impl.MappingServiceImpl;
import org.genesys2.server.service.impl.OAuth2ClientDetailsServiceImpl;
import org.genesys2.server.service.impl.OAuth2JPATokenStoreImpl;
import org.genesys2.server.service.impl.OWASPSanitizer;
import org.genesys2.server.service.impl.OrganizationServiceImpl;
import org.genesys2.server.service.impl.RequestServiceImpl;
import org.genesys2.server.service.impl.SimplePasswordPolicy;
import org.genesys2.server.service.impl.TaxonomyManager;
import org.genesys2.server.service.impl.TaxonomyServiceImpl;
import org.genesys2.server.service.impl.TeamServiceImpl;
import org.genesys2.server.service.impl.TokenVerificationServiceImpl;
import org.genesys2.server.service.impl.UserServiceImpl;
import org.genesys2.server.service.worker.ElasticUpdater;
import org.genesys2.server.servlet.controller.rest.AccessionController;
import org.genesys2.server.servlet.controller.rest.CacheController;
import org.genesys2.server.servlet.controller.rest.CropsController;
import org.genesys2.server.servlet.controller.rest.DatasetController;
import org.genesys2.server.servlet.controller.rest.InstituteGalleriesController;
import org.genesys2.server.servlet.controller.rest.KPIController;
import org.genesys2.server.servlet.controller.rest.LookupController;
import org.genesys2.server.servlet.controller.rest.OAuthManagementController;
import org.genesys2.server.servlet.controller.rest.OrganizationController;
import org.genesys2.server.servlet.controller.rest.PermissionController;
import org.genesys2.server.servlet.controller.rest.RequestsController;
import org.genesys2.server.servlet.controller.rest.TokenController;
import org.genesys2.server.servlet.controller.rest.TraitsController;
import org.genesys2.server.servlet.controller.rest.UserController;
import org.genesys2.server.servlet.controller.rest.UsersController;
import org.genesys2.server.test.PropertyPlacholderInitializer;
import org.genesys2.spring.config.ElasticsearchConfig;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.CacheManager;
import org.springframework.cache.support.NoOpCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.http.converter.json.Jackson2ObjectMapperFactoryBean;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.HierarchyMode;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IQueue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextHierarchy(@ContextConfiguration(name = "this", classes = { ElasticsearchConfig.class, AbstractRestTest.Config.class }, initializers = PropertyPlacholderInitializer.class))
@EnableAspectJAutoProxy
@ActiveProfiles("dev")
@WebAppConfiguration
public abstract class AbstractRestTest extends SpringTest {

	@Configuration
	@EnableWebMvc
	@EnableAspectJAutoProxy
	@DirtiesContext(hierarchyMode = HierarchyMode.CURRENT_LEVEL, classMode = DirtiesContext.ClassMode.AFTER_CLASS)
	public static class Config {
		
		@Bean
		public ImageGalleryAspects imageGalleryAspects() {
			// This thing makes galleries auto-fill with images
			System.err.println("imagegalleryaspects");
			return new ImageGalleryAspects();
		}

		@Bean
		public InstituteGalleriesController instGalleriesController() {
			return new InstituteGalleriesController();
		}

		@Bean
		public InstituteFilesService instituteFilesService() {
			return new InstituteFilesServiceImpl();
		}

		@Bean
		public RepositoryService repositoryService() {
			return new RepositoryServiceImpl();
		}

		@Bean
		public ImageGalleryService imageGalleryService() {
			return new ImageGalleryServiceImpl();
		}

		@Bean
		public ThumbnailGenerator thumbnailGenerator() {
			return new ThumbnailGenerator1();
		}

		@Bean
		public BytesStorageService bytesStorageService() {
			FilesystemStorageServiceImpl fsss = new FilesystemStorageServiceImpl();
			fsss.setRepositoryBaseDirectory(new File("data/repository"));
			return fsss;
		}

		@Bean
		public RequestsController requestsController() {
			return new RequestsController();
		}

		@Bean
		public RequestService requestService() {
			return new RequestServiceImpl();
		}

		@Bean
		public GeoService geoService() {
			return new GeoServiceImpl();
		}

		@Bean
		public UserController userController() {
			return new UserController();
		}

		@Bean
		public CacheManager cacheManager() {
			return new NoOpCacheManager();
		}

		@Bean
		public UserService userService() {
			return new UserServiceImpl();
		}

		@Bean
		public PasswordPolicy passwordPolicy() {
			return new SimplePasswordPolicy();
		}

		@Bean
		public TeamService teamService() {
			return new TeamServiceImpl();
		}

		@Bean
		public InstituteService instituteService() {
			return new InstituteServiceImpl();
		}

		@Bean
		public ContentService contentService() {
			return new ContentServiceImpl();
		}

		@Bean
		public HtmlSanitizer htmlSanitizer() {
			return new OWASPSanitizer();
		}

		@Bean
		public VelocityEngine velocityEngine() {
			return new VelocityEngine();
		}

		@Bean
		@Qualifier("genesysLowlevelRepositoryCustomImpl")
		public GenesysLowlevelRepository genesysLowlevelRepositoryCustomImpl() {
			return new GenesysLowlevelRepositoryCustomImpl();
		}

		@Bean
		public AclService aclService() {
			return new AclServiceImpl();
		}

		@Bean
		public AsAdminAspect asAdminAspect() {
			return new AsAdminAspect();
		}

		@Bean
		public UsersController restUsersController() {
			return new UsersController();
		}

		@Bean
		public OAuth2ClientDetailsService auth2ClientDetailsService() {
			return new OAuth2ClientDetailsServiceImpl();
		}

		@Bean
		public EMailVerificationService emailVerificationService() {
			return new EMailVerificationServiceImpl();
		}

		@Bean
		public TokenVerificationService tokenVerificationService() {
			return new TokenVerificationServiceImpl();
		}

		@Bean
		public EMailService eMailService() {
			return new EMailServiceImpl();
		}

		@Bean
		public JavaMailSender mailSender() {
			return Mockito.mock(JavaMailSenderImpl.class);
		}

		@Bean
		public ThreadPoolTaskExecutor threadPoolTaskExecutor() {
			return new ThreadPoolTaskExecutor();
		}

		@Bean
		public TraitsController traitsController() {
			return new TraitsController();
		}

		@Bean
		public TraitService traitServiceMock() {
			return new TraitServiceMock();
		}

		@Bean
		public GenesysService genesysService() {
			return new GenesysServiceImpl();
		}

		@Bean
		public CropService cropService() {
			return new CropServiceImpl();
		}

		@Bean
		public TaxonomyService taxonomyService() {
			return new TaxonomyServiceImpl();
		}

		@Bean
		public TraitValueRepository traitValueRepository() {
			return new TraitValueRepositoryImpl();
		}

		@Bean
		public OrganizationService organizationService() {
			return new OrganizationServiceImpl();
		}

		@Bean
		public LookupController lookupController() {
			return new LookupController();
		}

		@Bean
		public PermissionController permissionController() {
			return new PermissionController();
		}

		@Bean
		public OrganizationController organizationController() {
			return new OrganizationController();
		}

		@Bean
		public BatchRESTService batchRESTService() {
			return new BatchRESTServiceImpl();
		}

		@Bean
		public AccessionCustomRepository accessionCustomRepository() {
			return new AccessionCustomRepositoryImpl();
		}

		@Bean
		public TaxonomyManager taxonomyManager() {
			return new TaxonomyManager();
		}

		@Bean
		public EasySMTA easySMTAConnector() {
			return new EasySMTAMockConnector();
		}

		@Bean
		public HttpClientBuilder httpClientBuilder() {
			return HttpClientBuilder.create();
		}

		@Bean
		public TokenController tokenController() {
			return new TokenController();
		}

		@Bean
		public ConsumerTokenServices consumerTokenServices() {
			DefaultTokenServices tokenServices = new DefaultTokenServices();
			tokenServices.setTokenStore(tokenStore());
			return tokenServices;
		}

		@Bean
		public JPATokenStore tokenStore() {
			return new OAuth2JPATokenStoreImpl();
		}

		@Bean
		public OAuthManagementController oAuthManagementController() {
			return new OAuthManagementController();
		}

		@Bean
		public KPIController kpiController() {
			return new KPIController();
		}

		@Bean
		public KPIService kpiService() {
			return new KPIServiceImpl();
		}

		@Bean
		public DatasetController datasetController() {
			return new DatasetController();
		}

		@Bean
		public DatasetService datasetService() {
			return new GenesysServiceImpl();
		}

		@Bean
		public CropsController cropsController() {
			return new CropsController();
		}

		@Bean
		public CacheController cacheController() {
			return new CacheController();
		}

		@Bean
		public MappingService mappingService() {
			return new MappingServiceImpl();
		}

		@Bean
		public GenesysFilterService genesysFilterService() {
			return new GenesysFilterServiceImpl();
		}

		@Bean
		public AccessionController accessionController() {
			return new AccessionController();
		}

		@Bean
		public GenesysRESTService genesysRESTService() {
			return new GenesysRESTServiceImpl();
		}

		@Bean
		public ElasticService elasticService() {
			return new ElasticsearchSearchServiceImpl();
		}

		@Bean
		public FilterHandler filterHandler() {
			return new FilterHandler();
		}

		@Bean
		public Jackson2ObjectMapperFactoryBean objectMapper() {
			final Jackson2ObjectMapperFactoryBean mapperFactoryBean = new Jackson2ObjectMapperFactoryBean();
			mapperFactoryBean.setFeaturesToDisable(SerializationFeature.FAIL_ON_EMPTY_BEANS, DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
			return mapperFactoryBean;
		}

		@Bean
		public ElasticSearchManagementService elasticSearchManagementService() {
			return new ElasticSearchManagementServiceImpl();
		}

		@Bean
		public ElasticUpdater elasticUpdater() {
			return new ElasticUpdater();
		}

		@Bean
		public ElasticUpdater.ElasticNode elasticNode() {
			return null;
		}

		@Bean
		public IQueue<Object> elasticRemoveQueue(HazelcastInstance hazelcast) {
			return hazelcast.getQueue("es-remove");
		}

		@Bean
		public IQueue<Object> elasticUpdateQueue(HazelcastInstance hazelcast) {
			return hazelcast.getQueue("es-update");
		}
	}

	@Autowired
	protected AccessionHistoricRepository accessionHistoricRepository;

	@Autowired
	protected CropRuleRepository cropRuleRepository;

	@Autowired
	protected TaxonomyService taxonomyService;

	@Autowired
	protected Taxonomy2Repository taxonomy2Repository;

	@Autowired
	protected MetadataMethodRepository metadataMethodRepository;

	@Autowired
	protected MetadataRepository metadataRepository;

	@Autowired
	protected DatasetService datasetService;

	@Autowired
	protected ObservationRepository observationRepository;

	@Autowired
	protected KPIParameterRepository kpiParameterRepository;

	@Autowired
	protected KPIService kpiService;
	// @Autowired protected
	// OAuthManagementController oAuthManagementController;

	@Autowired
	protected OAuthAccessTokenPersistence accessTokenPersistence;

	@Autowired
	protected OAuthRefreshTokenPersistence refreshTokenPersistence;

	@Autowired
	protected OAuthClientDetailsPersistence clientDetailsPersistence;

	@Autowired
	protected OAuth2ClientDetailsService clientDetailsService;

	@Autowired
	protected TokenController tokenController;

	@Autowired
	protected ConsumerTokenServices tokenServices;

	@Autowired
	protected TokenStore tokenStore;

	@Autowired
	protected ConsumerTokenServices consumerTokenServices;

	@Autowired
	protected EasySMTA easySMTAConnector;

	@Autowired
	protected JavaMailSender mailSender;

	@Autowired
	protected AccessionRepository accessionRepository;

	@Autowired
	protected AccessionCustomRepository accessionCustomRepository;

	@Autowired
	protected PermissionController permissionController;

	@Autowired
	protected GeoService geoService;

	@Autowired
	protected CountryRepository countryRepository;

	@Autowired
	protected FaoInstituteRepository instituteRepository;

	@Autowired
	protected FaoInstituteSettingRepository instituteSettingRepository;

	@Autowired
	protected MethodRepository methodRepository;

	@Autowired
	protected LookupController lookupController;

	@Autowired
	protected TraitsController traitsController;

	@Autowired
	protected @Qualifier("traitServiceMock") TraitService traitService;

	@Autowired
	protected ArticleRepository articleRepository;

	@Autowired
	protected ContentService contentService;

	@Autowired
	protected JavaMailSender javaMailSender;

	@Autowired
	protected UsersController restUsersController;

	@Autowired
	protected UserService userService;

	@Autowired
	protected UserController userController;

	@Autowired
	protected UserPersistence userPersistence;

	@Autowired
	protected InstituteService instituteService;

	@Autowired
	protected TeamRepository teamRepository;

	@Autowired
	protected AclService aclService;

	@Autowired
	protected AclSidPersistence aclSidPersistence;

	@Autowired
	protected AclEntryPersistence aclEntryPersistence;

	@Autowired
	protected AclObjectIdentityPersistence aclObjectIdentityPersistence;

	@Autowired
	protected AclClassPersistence aclClassPersistence;

	@Autowired
	protected TeamService teamService;

	@Autowired
	protected GenesysService genesysService;

	@Autowired
	protected CropService cropService;

	@Autowired
	protected CropRepository cropRepository;

	@Autowired
	protected ParameterRepository parameterRepository;

	@Autowired
	protected ParameterCategoryRepository parameterCategoryRepository;

	@Autowired
	protected OrganizationService organizationService;

	@Autowired
	protected OrganizationRepository organizationRepository;

	@Autowired
	protected RequestService requestService;

	@Autowired
	protected MaterialRequestRepository materialRequestRepository;

	@Autowired
	protected MaterialSubRequestRepository materialSubRequestRepository;
}
