package org.genesys2.tests.resttests;


import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.exception.UserException;
import org.genesys2.server.model.UserRole;
import org.genesys2.server.model.impl.Article;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.model.impl.Team;
import org.genesys2.server.model.impl.User;
import org.genesys2.server.security.AuthUserDetails;
import org.genesys2.server.service.PasswordPolicy.PasswordPolicyException;
import org.genesys2.server.servlet.controller.rest.model.UserChangedDataJson;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@Ignore
public class UsersControllerTest extends AbstractRestTest {

    private static final Log LOG = LogFactory.getLog(UserControllerTest.class);

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    private User user = new User();
    private FaoInstitute faoInstitute;
    private Team team;
    private List<FaoInstitute> institutes = new ArrayList<>();
    private Article oneArticle = new Article();

    @Before
    public void setUp() throws UserException, PasswordPolicyException {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .build();

        oneArticle.setTitle("title for oneArticle");
        oneArticle.setBody("<h1>Tho BOdy of oneArticle</h1");
        oneArticle.setSlug("smtp.email-verification");
        oneArticle.setLang("en");
        oneArticle.setClassPk(contentService.ensureClassPK(Article.class));
        List<Article> articleList = new ArrayList<>();
        articleList.add(oneArticle);
        contentService.save(articleList);

        faoInstitute = new FaoInstitute();
        faoInstitute.setFullName("This is name of institute");
        faoInstitute.setCurrent(true);
        faoInstitute.setPgrActivity(true);
        faoInstitute.setMaintainsCollection(true);
        faoInstitute.setPgrActivity(true);
        faoInstitute.setAccessionCount(1);
        faoInstitute.setUniqueAcceNumbs(true);
        faoInstitute.setCode("Code");

        user = new User();
        user.setEmail("salexandrbasov@gmail.com");
        user.setPassword("Alexandr19011990");
        user.setName("SYS_ADMIN");

        team = new Team();
        team.setName("name of team");
        team.setVersion(1);

        userService.addUser(user);

        institutes.add(faoInstitute);
        instituteService.update(institutes);

        Map<Integer, Boolean> permission = new HashMap<>();
        permission.put(4, true);
        permission.put(1, false);
        permission.put(2, false);
        permission.put(8, false);
        permission.put(16, true);

        aclService.addPermissions(faoInstitute.getId(), FaoInstitute.class.getName(), "SYS_ADMIN", true, permission);

        List<GrantedAuthority> authorities = new ArrayList<>();
        GrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(UserRole.USER.getName());
        authorities.add(simpleGrantedAuthority);

        AuthUserDetails authUserDetails = new AuthUserDetails(user.getName(), user.getPassword(), authorities);

        // set actual DB user
        authUserDetails.setUser(user);

        UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(authUserDetails, authUserDetails, authorities);

        SecurityContextHolder.getContext().setAuthentication(authToken);

        teamService.addTeam(team.getName());
    }

    @After
    public void tearDown() {
        teamRepository.deleteAll();
        userPersistence.deleteAll();
        instituteSettingRepository.deleteAll();
        instituteRepository.deleteAll();

        aclObjectIdentityPersistence.deleteAll();
        aclClassPersistence.deleteAll();
        aclSidPersistence.deleteAll();
        aclEntryPersistence.deleteAll();

        articleRepository.deleteAll();
        Mockito.reset(javaMailSender);
    }

    @Test
    public void getUsersTest() throws Exception {
        LOG.info("Start test-method getUsersTest");
        ObjectMapper objectMapper = new ObjectMapper();
        mockMvc.perform(get("/api/v0/users").contentType(MediaType.APPLICATION_JSON)
                .param("startRow", "0")
                .param("pageSize", "1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectMapper.writeValueAsString(userService.listWrapped(0, 1))));
        LOG.info("Test getUsersTest passed");
    }

    @Test
    public void getAvailableRolesTest() throws Exception {
        LOG.info("Start test-method getAvailableRolesTest");

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(get("/api/v0/users/available_roles").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectMapper.writeValueAsString(userService.listAvailableRoles())));

        LOG.info("Test getAvailableRolesTest passed");
    }

    @Test
    public void getUserTest() throws Exception {
        LOG.info("Start test-method getUserTest");

        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("user", user);
        resultMap.put("userTeams", teamService.listUserTeams(user));

        mockMvc.perform(get("/api/v0/users/user/{id}", user.getId()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectMapper.writeValueAsString(resultMap)));

        LOG.info("Test getUserTest passed");
    }

    @Test
    public void getUserByUuidTest() throws Exception {
        LOG.info("Start test-method getUserByUuidTest");

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(get("/api/v0/users/user/uuid/{id:.+}", user.getUuid()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectMapper.writeValueAsString(user)));

        LOG.info("Test getUserByUuidTest passed");
    }

    @Test
    public void saveUserTest() throws Exception {
        LOG.info("Start test-method saveUserTest");

        teamRepository.deleteAll();
        userPersistence.deleteAll();

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(put("/api/v0/users/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(user)))
                .andExpect(status().isOk());

        LOG.info("Test saveUserTest passed");
    }

    @Test
    public void updateUserTest() throws Exception {
        LOG.info("Start test-method updateUserTest");

        teamRepository.deleteAll();
        userPersistence.deleteAll();

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(post("/api/v0/users/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(user)))
                .andExpect(status().isOk());

        LOG.info("Test updateUserTest passed");
    }

    @Test
    public void updateDataTest() throws Exception {
        LOG.info("Start test-method updateDataTest");

        UserChangedDataJson userChangedDataJson = new UserChangedDataJson();
        userChangedDataJson.setPwd1("newPwd");
        userChangedDataJson.setPwd2("newPwd");
        userChangedDataJson.setName("newName");
        userChangedDataJson.setEmail("new@mail.ru");
        userChangedDataJson.setUuid(user.getUuid());

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode JSON_OK = objectMapper.readTree("{\"result\":true}");

        mockMvc.perform(post("/api/v0/users/user/data")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userChangedDataJson)))
                .andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(JSON_OK)));
        LOG.info("Test updateDataTest passed");
    }

    @Test
    public void updateRolesTest() throws Exception {
        LOG.info("Start test-method updateRolesTest");

        UserChangedDataJson userChangedDataJson = new UserChangedDataJson();
        userChangedDataJson.setRoles(user.getRoles().toArray(new String[user.getRoles().size()]));
        userChangedDataJson.setUuid(user.getUuid());

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode JSON_OK = objectMapper.readTree("{\"result\":true}");

        mockMvc.perform(post("/api/v0/users/user/roles")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userChangedDataJson)))
                .andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(JSON_OK)));
        LOG.info("Test updateRolesTest passed");
    }

    @Test
    public void sendEmailTest() throws Exception {
        LOG.info("Start test-method sendEmailTest");

        doNothing().when(javaMailSender).send(isA(MimeMessagePreparator.class));

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode JSON_OK = objectMapper.readTree("{\"result\":true}");

        mockMvc.perform(get("/api/v0/users/user/{uuid:.+}/send", user.getUuid())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(JSON_OK)));

        LOG.info("Test sendEmailTest passed");
    }

    @Test
    public void addRoleVettedUser() throws Exception {
        LOG.info("Start test-method addRoleVettedUser");

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode JSON_OK = objectMapper.readTree("{\"result\":true}");

        mockMvc.perform(get("/api/v0/users/user/{uuid:.+}/vetted-user", user.getUuid())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(JSON_OK)));

        LOG.info("Test addRoleVettedUser passed");
    }
}
