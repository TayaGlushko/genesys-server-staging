package org.genesys2.tests.resttests;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.exception.UserException;
import org.genesys2.server.model.UserRole;
import org.genesys2.server.model.impl.User;
import org.genesys2.server.security.AuthUserDetails;
import org.genesys2.server.service.PasswordPolicy.PasswordPolicyException;
import org.genesys2.server.servlet.controller.rest.TokenController;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.DefaultAuthorizationRequest;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

public class TokenControllerTest extends AbstractRestTest {

    private static final Log LOG = LogFactory.getLog(TokenControllerTest.class);

    @Autowired
    WebApplicationContext webApplicationContext;

    MockMvc mockMvc;

    private User user;
    private OAuth2AccessToken accessToken, accessTokenTwo;

    @Before
    public void startUp() throws UserException, PasswordPolicyException {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        user = new User();
        user.setEmail("salexandrbasov@gmail.com");
        user.setPassword("Alexandr190!11990");
        user.setName("SYS_ADMIN");

        userService.addUser(user);


        HashMap<String, String> authorizationParameters = new HashMap<String, String>();
        authorizationParameters.put("scope", "read");
        authorizationParameters.put("username", user.getName());
        authorizationParameters.put("client_id", user.getName());
        authorizationParameters.put("grant", user.getPassword());

        DefaultAuthorizationRequest authorizationRequest = new DefaultAuthorizationRequest(authorizationParameters);
        authorizationRequest.setApproved(true);

        List<GrantedAuthority> authorities = new ArrayList<>();
        GrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(UserRole.ADMINISTRATOR.getName());
        authorities.add(simpleGrantedAuthority);

        authorizationRequest.setAuthorities(authorities);

        HashSet<String> resourceIds = new HashSet<String>();
        resourceIds.add(user.getName());
        authorizationRequest.setResourceIds(resourceIds);

        AuthUserDetails authUserDetails = new AuthUserDetails(user.getUuid(), user.getPassword(), authorities);

        // set actual DB user
        authUserDetails.setUser(user);

        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(authUserDetails, authUserDetails, authorities);

        OAuth2Authentication authenticationRequest = new OAuth2Authentication(authorizationRequest, authenticationToken);

        authenticationRequest.setAuthenticated(true);

        accessToken = ((DefaultTokenServices) tokenServices).createAccessToken(authenticationRequest);

        SecurityContextHolder.getContext().setAuthentication(authenticationRequest);
    }

    @After
    public void teerDown() {
        userPersistence.deleteAll();
        accessTokenPersistence.deleteAll();
        refreshTokenPersistence.deleteAll();
    }

    // FIXME @Transactional
    @Ignore
    @Test
    public void listTokensForUserTest() throws Exception {
        LOG.info("Start test-method listTokensForUserTest");

        mockMvc.perform(get("/api/v0/users/{username}/list/tokens", user.getUuid())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].access_token", not(isEmptyString())))
                .andExpect(jsonPath("$[0].token_type", is(accessToken.getTokenType())))
                .andExpect(jsonPath("$[0].expires_in", is(43199)))
                .andExpect(jsonPath("$[0].scope", is(accessToken.getScope().toArray()[0])))
                .andExpect(jsonPath("$[0].bearer", not(isEmptyOrNullString())))
                .andExpect(jsonPath("$[0].client_id", is(user.getName())));

        LOG.info("Test listTokensForUserTest passed");
    }

    // FIXME Transactional
    @Ignore
    @Test
    public void revokeUserTokenTest() throws Exception {
        LOG.info("Start test-method revokeUserTokenTest");

        OAuth2AccessToken oAuth2AccessTokenForTest = (OAuth2AccessToken) tokenController.listTokensForUser(user.getUuid()).toArray()[0];
        ObjectMapper objectMapper = new ObjectMapper();

        TokenController.SimpleMessage simpleMessage = new TokenController.SimpleMessage("ok", "user token revoked");

        mockMvc.perform(delete("/api/v0/users/{username}/tokens/revoke/{token}", user.getUuid(), oAuth2AccessTokenForTest)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectMapper.writeValueAsString(simpleMessage)));

        LOG.info("Test revokeUserTokenTest passed");
    }

    @Test
    public void listTokensForClientTest() throws Exception {
        LOG.info("Start test-method listTokensForClientTest");

        SecurityContextHolder.getContext().setAuthentication(null);
        accessTokenPersistence.deleteAll();
        refreshTokenPersistence.deleteAll();

        HashMap<String, String> authorizationParameters = new HashMap<String, String>();
        authorizationParameters.put("scope", "read");
        authorizationParameters.put("username", user.getName());
        authorizationParameters.put("client_id", user.getName());
        authorizationParameters.put("grant", user.getPassword());

        DefaultAuthorizationRequest authorizationRequest = new DefaultAuthorizationRequest(authorizationParameters);
        authorizationRequest.setApproved(true);

        List<GrantedAuthority> authorities = new ArrayList<>();
        GrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(UserRole.ADMINISTRATOR.getName());
        authorities.add(simpleGrantedAuthority);

        authorizationRequest.setAuthorities(authorities);

        HashSet<String> resourceIds = new HashSet<String>();
        resourceIds.add(user.getName());
        authorizationRequest.setResourceIds(resourceIds);

        OAuth2Authentication authenticationRequestTwo = new OAuth2Authentication(authorizationRequest, null);
        OAuth2AccessToken oAuth2AccessTokenForTest = ((DefaultTokenServices) tokenServices).createAccessToken(authenticationRequestTwo);

        mockMvc.perform(get("/api/v0/clients/{client}/list/tokens", user.getName())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].access_token", not(isEmptyString())))
                .andExpect(jsonPath("$[0].token_type", is(accessToken.getTokenType())))
                .andExpect(jsonPath("$[0].expires_in", is(43199)))
                .andExpect(jsonPath("$[0].scope", is(accessToken.getScope().toArray()[0])))
                .andExpect(jsonPath("$[0].bearer", not(isEmptyOrNullString())))
                .andExpect(jsonPath("$[0].client_id", is(user.getName())));

        LOG.info("Test listTokensForClientTest passed");
    }

    @Test
    public void revokeClientTokenTest() throws Exception {
        LOG.info("Start test-method revokeClientTokenTest");

        SecurityContextHolder.getContext().setAuthentication(null);
        accessTokenPersistence.deleteAll();
        refreshTokenPersistence.deleteAll();

        HashMap<String, String> authorizationParameters = new HashMap<String, String>();
        authorizationParameters.put("scope", "read");
        authorizationParameters.put("username", user.getName());
        authorizationParameters.put("client_id", user.getName());
        authorizationParameters.put("grant", user.getPassword());

        DefaultAuthorizationRequest authorizationRequest = new DefaultAuthorizationRequest(authorizationParameters);
        authorizationRequest.setApproved(true);

        List<GrantedAuthority> authorities = new ArrayList<>();
        GrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(UserRole.ADMINISTRATOR.getName());
        authorities.add(simpleGrantedAuthority);

        authorizationRequest.setAuthorities(authorities);

        HashSet<String> resourceIds = new HashSet<String>();
        resourceIds.add(user.getName());
        authorizationRequest.setResourceIds(resourceIds);

        OAuth2Authentication authenticationRequestTwo = new OAuth2Authentication(authorizationRequest, null);
        OAuth2AccessToken oAuth2AccessTokenForTest = ((DefaultTokenServices) tokenServices).createAccessToken(authenticationRequestTwo);

        ObjectMapper objectMapper = new ObjectMapper();

        TokenController.SimpleMessage simpleMessage = new TokenController.SimpleMessage("ok", "client token revoked");

        mockMvc.perform(delete("/api/v0/clients/{client}/tokens/revoke/{token}", user.getName(), oAuth2AccessTokenForTest)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectMapper.writeValueAsString(simpleMessage)));

        LOG.info("Test revokeClientTokenTest passed");
    }
}
