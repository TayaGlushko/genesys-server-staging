package org.genesys2.tests.resttests;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.exception.UserException;
import org.genesys2.server.model.UserRole;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.model.impl.Team;
import org.genesys2.server.model.impl.User;
import org.genesys2.server.security.AuthUserDetails;
import org.genesys2.server.service.PasswordPolicy.PasswordPolicyException;
import org.genesys2.server.servlet.controller.rest.UserController;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

// FIXME @Transactional?
@Ignore
public class UserControllerTest extends AbstractRestTest {

    private static final Log LOG = LogFactory.getLog(UserControllerTest.class);

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    private User user = new User();
    private FaoInstitute faoInstitute;
    private Team team;
    private List<FaoInstitute> institutes = new ArrayList<>();

    @Before
    public void setUp() throws UserException, PasswordPolicyException {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .build();


        faoInstitute = new FaoInstitute();
        faoInstitute.setFullName("This is name of institute");
        faoInstitute.setCurrent(true);
        faoInstitute.setPgrActivity(true);
        faoInstitute.setMaintainsCollection(true);
        faoInstitute.setPgrActivity(true);
        faoInstitute.setAccessionCount(1);
        faoInstitute.setUniqueAcceNumbs(true);
        faoInstitute.setCode("Code");

        user = new User();
        user.setEmail("salexandrbasov@gmail.com");
        user.setPassword("Alexandr19011990");
        user.setName("SYS_ADMIN");

        team = new Team();
        team.setName("name of team");
        team.setVersion(1);

        userService.addUser(user);

        institutes.add(faoInstitute);
        instituteService.update(institutes);

        Map<Integer, Boolean> permission = new HashMap<>();
        permission.put(4, true);
        permission.put(1, false);
        permission.put(2, false);
        permission.put(8, false);
        permission.put(16, true);

        aclService.addPermissions(faoInstitute.getId(), FaoInstitute.class.getName(), "SYS_ADMIN", true, permission);

        List<GrantedAuthority> authorities = new ArrayList<>();
        GrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(UserRole.ADMINISTRATOR.getName());
        authorities.add(simpleGrantedAuthority);

        AuthUserDetails authUserDetails = new AuthUserDetails(user.getName(), user.getPassword(), authorities);

        // set actual DB user
        authUserDetails.setUser(user);

        UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(authUserDetails, authUserDetails, authorities);

        SecurityContextHolder.getContext().setAuthentication(authToken);

        teamService.addTeam(team.getName());
    }

    @After
    public void tearDown() {
        teamRepository.deleteAll();
        userPersistence.deleteAll();
        instituteSettingRepository.deleteAll();
        instituteRepository.deleteAll();

        aclObjectIdentityPersistence.deleteAll();
        aclClassPersistence.deleteAll();
        aclSidPersistence.deleteAll();
        aclEntryPersistence.deleteAll();
    }

    @Test
    public void getProfileTest() throws Exception {
        LOG.info("Start test-method getProfileTest");

        ObjectMapper objectMapper = new ObjectMapper();

        user.setPassword(null);
        user.setId(null);

        mockMvc.perform(get("/api/v0/me").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectMapper.writeValueAsString(user)));

        LOG.info("Test getProfileTest passed!");
    }

    @Test
    public void listInstitutesTest() throws Exception {
        LOG.info("Start test-method listInstitutesTest");

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(get("/api/v0/me/institutes").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectMapper.writeValueAsString(institutes)));

        LOG.info("Test listInstitutesTest passed!");
    }

    @Test
    public void listTeamsTest() throws Exception {
        LOG.info("Start test-method listTeamsTest");

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(get("/api/v0/me/teams").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectMapper.writeValueAsString(teamRepository.findAll())));

        LOG.info("Test listTeamsTest passed");
    }

    @Test
    public void createTeamTest() throws Exception {
        LOG.info("Start test-method createTeamTest");

        teamRepository.deleteAll();
        ObjectMapper objectMapper = new ObjectMapper();

        UserController.TeamJson teamJson = new UserController.TeamJson();
        teamJson.name = "name";

        mockMvc.perform(post("/api/v0/me/teams")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(teamJson)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name").value("name"))
                .andExpect(content().string(objectMapper.writeValueAsString(teamRepository.findAll().get(0))));

        LOG.info("Test createTeamTest passed");
    }

    @Test
    public void changeLockTest() throws Exception {
        LOG.info("Start test-method changeLockTest");

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(post("/api/v0/user/{uuid}/locked", user.getUuid())
                .contentType(MediaType.APPLICATION_JSON)
                .content(Boolean.TRUE.toString()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectMapper.writeValueAsString(Boolean.TRUE)));


        LOG.info("Test changeLockTest passed");
    }

    @Test
    public void changeEnabled() throws Exception {
        LOG.info("Start test-method changeEnabled");

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(post("/api/v0/user/{uuid}/enabled", user.getUuid())
                .contentType(MediaType.APPLICATION_JSON)
                .content(Boolean.TRUE.toString()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectMapper.writeValueAsString(Boolean.TRUE)));

        LOG.info("Test changeEnabled passed");
    }

    @Test
    public void leaveTeamTest() throws Exception {
        LOG.info("Start test-method leaveTeamTest");

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode JSON_OK = objectMapper.readTree("{\"result\":true}");

        mockMvc.perform(post("/api/v0/me/teams/{teamId}/leave", teamRepository.findAll().get(0).getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(JSON_OK.toString()));

        LOG.info("Test leaveTeamTest passed");
    }
}
