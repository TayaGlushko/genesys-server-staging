/*
 * Copyright 2016 Global Crop Diversity Trust, www.croptrust.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.genesys2.tests.resttests.docs;

import static org.hamcrest.Matchers.*;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.*;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.filerepository.model.RepositoryImage;
import org.genesys2.server.filerepository.model.RepositoryImageData;
import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.genesys.AccessionId;
import org.genesys2.server.model.genesys.Taxonomy2;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.test.filerepository.TestImage;
import org.genesys2.tests.resttests.AbstractRestTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentation;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

@Transactional(transactionManager = "transactionManager")
public class ApiImagesDocsTest extends AbstractRestTest {

	private static final String ACCENUMB = "ACC001";

	private static final String TEST_INSTCODE = "XXX001";
	final TestImage image1 = new TestImage("maize.jpg", "image/jpg");

	private static final Log LOG = LogFactory.getLog(ApiImagesDocsTest.class);

	@Rule
	public final RestDocumentation restDocumentation = new RestDocumentation("target/generated-snippets");

	@Autowired
	WebApplicationContext webApplicationContext;

	MockMvc mockMvc;
	private static final ObjectMapper objectMapper;

	private FaoInstitute institute;

	static {
		objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
		objectMapper.configure(SerializationFeature.WRITE_EMPTY_JSON_ARRAYS, false);
		objectMapper.setSerializationInclusion(Include.NON_EMPTY);
	}

	@Before
	public void setUp() throws InterruptedException {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
				.apply(documentationConfiguration(this.restDocumentation).uris().withScheme("https").withHost("sandbox.genesys-pgr.org").withPort(443)).build();

		Collection<FaoInstitute> institutes = new ArrayList<>();
		institute = new FaoInstitute();
		institute.setCode(TEST_INSTCODE);
		institutes.add(institute);
		instituteService.update(institutes);

		Taxonomy2 taxonomy = taxonomyService.internalEnsure("Genus", "species", "", "", "");

		Accession acce = new Accession();
		acce.setAccessionId(new AccessionId());
		acce.setAccessionName(ACCENUMB);
		acce.setInstitute(institute);
		acce.setTaxonomy(taxonomy);
		genesysService.saveAccession(acce);
	}

	@After
	public void tearDown() {
		accessionRepository.deleteAll();
		taxonomy2Repository.deleteAll();
		instituteRepository.deleteAll();
	}

	@Test
	public void testListGalleries() throws UnsupportedEncodingException, Exception {
		mockMvc.perform(get("/api/v0/img/{instCode}/_galleries", TEST_INSTCODE).param("page", "1").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON)).andDo(r -> {
					LOG.info("Institute galleries: " + r.getResponse().getContentAsString());
				}).andExpect(jsonPath("$.content", hasSize(0))).andExpect(jsonPath("$.totalElements", is(0))).andExpect(jsonPath("$.last", is(true)))
				.andDo(document("img-instgallery-list", pathParameters(parameterWithName("instCode").description("Institute WIEWS code (e.g. NGA039)")),
						requestParameters(parameterWithName("page").description("The page to return (1 is first page)")),
						responseFields(fieldWithPath("content").description("Array containing registered image galleries of the institute"),
								fieldWithPath("numberOfElements").description("Number of image galleries in the content array"),
								fieldWithPath("totalElements").description("Total number of galleries in Genesys belonging to this institute"), fieldWithPath("size").description("Page size"),
								fieldWithPath("number").description("Current page number"), fieldWithPath("totalPages").description("Page count"),
								fieldWithPath("sort").description("Arrray sorting details"), fieldWithPath("first").ignored(), fieldWithPath("last").ignored())));
	}

	@Test
	public void testUploadImage1() throws UnsupportedEncodingException, Exception {
		final StringBuilder sb = new StringBuilder();
		mockMvc.perform(put("/api/v0/img/{instCode}/acn/{acceNumb}/", TEST_INSTCODE, ACCENUMB).param("originalFilename", image1.getOriginalFilename()).contentType(MediaType.IMAGE_JPEG_VALUE)
				.content(image1.getImageBytes())).andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON)).andDo(r -> {
					LOG.info("Image PUT response: " + r.getResponse().getContentAsString());
					RepositoryImage ri = objectMapper.readValue(r.getResponse().getContentAsString(), RepositoryImage.class);
					LOG.info(ri);
					sb.append(ri.getUuid());
				}).andExpect(jsonPath("$.id", greaterThan(0))).andExpect(jsonPath("$.path", is("/accessions/" + TEST_INSTCODE + "/acn/" + ACCENUMB + "/")))
				.andExpect(jsonPath("$.originalFilename", is(image1.getOriginalFilename()))).andExpect(jsonPath("$.contentType", is("image/jpeg")))
				.andDo(document("img-instgallery-put",
						pathParameters(parameterWithName("instCode").description("Institute WIEWS code (e.g. NGA039)"),
								parameterWithName("acceNumb").description("Existing ACCENUMB in the institute")),
						requestParameters(parameterWithName("originalFilename").description("Filename of the image (e.g. IMG00121.jpg)")),
						responseFields(fieldWithPath("uuid").description("UUID of the image in the Genesys repository"),
								fieldWithPath("identifier").description("Unique identifier assigned to the image by Genesys"),
								fieldWithPath("originalFilename").description("The original filename that you have provided"),
								fieldWithPath("extension").description("Image extension derived from original filename (e.g. '.jpg')"),
								fieldWithPath("contentType").description("The content type of the image (autodetected when possible)"), fieldWithPath("format").description("Image format"),
								fieldWithPath("path").description("Location (directory) of the image in the Genesys repository"),
								fieldWithPath("filename").description("The new generated filename of the image"),
								fieldWithPath("url").description("The relative URL to the image in Genesys repository [path+filename]"),
								fieldWithPath("sha1Sum").description("SHA1 hash of the image bytes"), fieldWithPath("md5Sum").description("MD5 hash of the image bytes"),
								fieldWithPath("width").description("Image width in pixels (autodetected)"), fieldWithPath("height").description("Image height in pixels (autodetected)"),
								fieldWithPath("orientation").description("LANDSCAPE or PORTRAIT orientation of the image (autodetected)"),
								// Ignored stuff
								fieldWithPath("id").ignored(), fieldWithPath("title").ignored(), fieldWithPath("subject").ignored(), fieldWithPath("description").ignored(),
								fieldWithPath("creator").ignored(), fieldWithPath("created").ignored(), fieldWithPath("rightsHolder").ignored(), fieldWithPath("accessRights").ignored(),
								fieldWithPath("license").ignored(), fieldWithPath("extent").ignored(), fieldWithPath("bibliographicCitation").ignored(), fieldWithPath("createdDate").ignored(),
								fieldWithPath("lastModifiedDate").ignored(), fieldWithPath("originalUrl").ignored(), fieldWithPath("dateRetrieved").ignored(), fieldWithPath("dateSubmitted").ignored(),
								fieldWithPath("modified").ignored())));

		UUID uuid = UUID.fromString(sb.toString());
		LOG.info("UUID=" + uuid);

		RepositoryImageData ri = new RepositoryImageData();
		ri.setAccessRights("Access rights");
		ri.setBibliographicCitation("Genesys Museum in Bonn. Digital image. Accession Imagery. Genesys. Web. 9 August 2016.");
		ri.setCreator("John Doe");
		ri.setCreated("August 2016");
		ri.setDescription("Image of leaf of " + ACCENUMB + " taken ...");
		ri.setLicense("https://creativecommons.org/publicdomain/zero/1.0/");
		ri.setRightsHolder("International Institute of Accessions (IIA)");
		ri.setSubject(ACCENUMB);
		ri.setTitle("Leaf of " + ACCENUMB);
		ri.setExtent("4000x2000 pixels, 10MB");
		mockMvc.perform(put("/api/v0/img/{instCode}/acn/{acceNumb}/{uuid}/_metadata", TEST_INSTCODE, ACCENUMB, uuid).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.content(objectMapper.writeValueAsString(ri))).andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON)).andDo(r -> {
					LOG.info("Image metadata put: " + r.getResponse().getContentAsString());
				})
				.andDo(document("img-instgallery-metadata-put",
						pathParameters(parameterWithName("instCode").description("Institute WIEWS code (e.g. NGA039)"), parameterWithName("acceNumb").description("Existing ACCENUMB in the institute"),
								parameterWithName("uuid").description("UUID of the image in the gallery of this accession")),
						requestFields(
								fieldWithPath("title").description("Title is a property that refers to the name or names by which a resource is formally known"),
								fieldWithPath("subject").description("The Subject described or pictured in the resource, accession number"),
								fieldWithPath("description").description(
										"Description of the content of a resource. The description is a potentially rich source of indexable terms and assist the users in their selection of an appropriate resource."),
								fieldWithPath("creator").description("An entity primarily responsible for making the resource"),
								fieldWithPath("created").description("A point or period of time when the resource was created by the Creator"),
								fieldWithPath("rightsHolder").description("Person or an organization owning or managing rights over this resource"),
								fieldWithPath("accessRights").description(
										"Access rights provides information about restrictions to view, search or use a resource based on attributes of the resource itself or the category of user."),
								fieldWithPath("license").description("Legal document giving official permission to do something with the resource. E.g. http://www.gnu.org/licenses/gpl.html"),
								fieldWithPath("extent").description("Size (e.g. bytes, pages, inches, etc.) or duration (e.g. hours, minutes, days, etc.) of a resource"),
								fieldWithPath("bibliographicCitation").description("Formal bibliographic citation for the resource")),
						responseFields(fieldWithPath("uuid").ignored(), fieldWithPath("originalFilename").description("The original filename that you have provided"),
								fieldWithPath("extension").ignored(), fieldWithPath("contentType").ignored(), fieldWithPath("path").ignored(), fieldWithPath("filename").ignored(),
								fieldWithPath("url").ignored(), fieldWithPath("sha1Sum").ignored(), fieldWithPath("md5Sum").ignored(),
								// Ignored stuff
								fieldWithPath("id").ignored(),
								// Updated stuff
								fieldWithPath("width").description("Image width in pixels (autodetected)"), fieldWithPath("height").description("Image height in pixels (autodetected)"),
								fieldWithPath("orientation").description("LANDSCAPE or PORTRAIT orientation of the image (autodetected)"), fieldWithPath("format").description("Image format"),
								fieldWithPath("identifier").description("Permanent unique identifier of the resource. Commonly a Uniform Resource Locator (URL)."),
								fieldWithPath("title").description("Title is a property that refers to the name or names by which a resource is formally known"),
								fieldWithPath("subject").description("The Subject described or pictured in the resource, accession number"),
								fieldWithPath("description").description(
										"Description of the content of a resource. The description is a potentially rich source of indexable terms and assist the users in their selection of an appropriate resource."),
								fieldWithPath("creator").description("An entity primarily responsible for making the resource"),
								fieldWithPath("created").description("A point or period of time when the resource was created by the Creator"),
								fieldWithPath("rightsHolder").description("Person or an organization owning or managing rights over this resource"),
								fieldWithPath("accessRights").description(
										"Access rights provides information about restrictions to view, search or use a resource based on attributes of the resource itself or the category of user."),
								fieldWithPath("license").description("Legal document giving official permission to do something with the resource. E.g. http://www.gnu.org/licenses/gpl.html"),
								fieldWithPath("extent").description("Size (e.g. bytes, pages, inches, etc.) or duration (e.g. hours, minutes, days, etc.) of a resource"),
								fieldWithPath("bibliographicCitation").description("Formal bibliographic citation for the resource"),
								// ignored
								fieldWithPath("createdDate").ignored(), fieldWithPath("lastModifiedDate").ignored(), fieldWithPath("originalUrl").ignored(), fieldWithPath("dateRetrieved").ignored(),
								fieldWithPath("dateSubmitted").ignored(), fieldWithPath("modified").ignored())));

		mockMvc.perform(get("/api/v0/img/{instCode}/acn/{acceNumb}/{uuid}/_metadata", TEST_INSTCODE, ACCENUMB, uuid)).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON)).andDo(r -> {
					LOG.info("Image metadata: " + r.getResponse().getContentAsString());
				})
				.andDo(document("img-instgallery-metadata-get",
						pathParameters(parameterWithName("instCode").description("Institute WIEWS code (e.g. NGA039)"), parameterWithName("acceNumb").description("Existing ACCENUMB in the institute"),
								parameterWithName("uuid").description("UUID of the image in the gallery of this accession")),
						responseFields(fieldWithPath("uuid").description("UUID of the image in the Genesys repository"),
								fieldWithPath("identifier").description("Unique identifier assigned to the image by Genesys"),
								fieldWithPath("originalFilename").description("The original filename that you have provided"),
								fieldWithPath("extension").description("Image extension derived from original filename (e.g. '.jpg')"),
								fieldWithPath("contentType").description("The content type of the image (autodetected when possible)"), fieldWithPath("format").description("Image format"),
								fieldWithPath("path").description("Location (directory) of the image in the Genesys repository"),
								fieldWithPath("filename").description("The new generated filename of the image"),
								fieldWithPath("url").description("The relative URL to the image in Genesys repository [path+filename]"),
								fieldWithPath("sha1Sum").description("SHA1 hash of the image bytes"), fieldWithPath("md5Sum").description("MD5 hash of the image bytes"),
								fieldWithPath("width").description("Image width in pixels (autodetected)"), fieldWithPath("height").description("Image height in pixels (autodetected)"),
								fieldWithPath("orientation").description("LANDSCAPE or PORTRAIT orientation of the image (autodetected)"),
								// Ignored stuff
								fieldWithPath("id").ignored(), fieldWithPath("title").ignored(), fieldWithPath("subject").ignored(), fieldWithPath("description").ignored(),
								fieldWithPath("creator").ignored(), fieldWithPath("created").ignored(), fieldWithPath("rightsHolder").ignored(), fieldWithPath("accessRights").ignored(),
								fieldWithPath("license").ignored(), fieldWithPath("extent").ignored(), fieldWithPath("bibliographicCitation").ignored(), fieldWithPath("createdDate").ignored(),
								fieldWithPath("lastModifiedDate").ignored(), fieldWithPath("originalUrl").ignored(), fieldWithPath("dateRetrieved").ignored(), fieldWithPath("dateSubmitted").ignored(),
								fieldWithPath("modified").ignored())));

		mockMvc.perform(get("/api/v0/img/{instCode}/acn/{acceNumb}", TEST_INSTCODE, ACCENUMB)).andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON)).andDo(r -> {
			LOG.info("Listing image UUIDs: " + r.getResponse().getContentAsString());
		}).andDo(document("img-instgallery-accelist",
				pathParameters(parameterWithName("instCode").description("Institute WIEWS code (e.g. NGA039)"), parameterWithName("acceNumb").description("Existing ACCENUMB in the institute"))));

		mockMvc.perform(delete("/api/v0/img/{instCode}/acn/{acceNumb}/{uuid}", TEST_INSTCODE, ACCENUMB, uuid)).andExpect(status().isOk())
				.andDo(document("img-instgallery-delete", pathParameters(parameterWithName("instCode").description("Institute WIEWS code (e.g. NGA039)"),
						parameterWithName("acceNumb").description("Existing ACCENUMB in the institute"), parameterWithName("uuid").description("UUID of the image in the gallery of this accession"))));
	}
}
