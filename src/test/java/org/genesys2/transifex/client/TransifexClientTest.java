/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.transifex.client;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.apache.commons.lang.RandomStringUtils;
import org.genesys2.server.test.PropertyPlacholderInitializer;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TransifexClientTest.Config.class, initializers = PropertyPlacholderInitializer.class)
public class TransifexClientTest {

    @PropertySource({ "classpath:application.properties", "classpath:/spring/spring.properties" })
	public static class Config{

		@Bean
        public TransifexService transifexService() {
			return new TransifexServiceImpl();
		}
	}

	@Value("${transifex.content.template}")
	private String contentTemplate;

	@Autowired
	private TransifexService transifexService;

	@Test
	public void testPostResource() throws IOException, TransifexException {
		// Post new resource
		String resourceSlug = "test-".concat(RandomStringUtils.randomAlphanumeric(30));
		String resourceTitle = "Test ".concat(RandomStringUtils.randomAlphanumeric(60));

		// This is template our xhtml
		String content = String.format(contentTemplate, resourceTitle, "<p>This is a test</p>");

		transifexService.createXhtmlResource(resourceSlug, resourceTitle, content);

		// Resource posted, must exist
		assertTrue("Resource does not exist", transifexService.resourceExists(resourceSlug));

		// Delete resource
		transifexService.deleteResource(resourceSlug);

		// Resource was deleted, should not exist
		assertFalse("Resource still exists", transifexService.resourceExists(resourceSlug));
	}

	@Test
	public void testUpdateResource() throws IOException, TransifexException {
		// Post new resource
		String resourceSlug = "test-".concat(RandomStringUtils.randomAlphanumeric(30));
		String resourceTitle = "Test ".concat(RandomStringUtils.randomAlphanumeric(60));

		// This is template our xhtml
		String content = String.format(contentTemplate, resourceTitle, "<p>This is a test</p><p>Some other string</p>");

		transifexService.updateXhtmlResource(resourceSlug, resourceTitle, content);

		// Resource posted, must exist
		assertTrue("Resource does not exist", transifexService.resourceExists(resourceSlug));

		try {
			// Sleep for a while to allow testing on www.transifex.com
			Thread.sleep(1000 * 30);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}

		// Update content and post again
		content = String.format(contentTemplate, resourceTitle, "<p>Updated content</p><p>Some other string</p>");

		// What now?
		transifexService.updateXhtmlResource(resourceSlug, resourceTitle, content);

		// Delete resource
		transifexService.deleteResource(resourceSlug);

		// Resource was deleted, should not exist
		assertFalse("Resource still exists", transifexService.resourceExists(resourceSlug));
	}
}
