package org.genesys2.server.persistence.domain.mock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.genesys.AccessionId;
import org.genesys2.server.model.genesys.Metadata;
import org.genesys2.server.model.genesys.Method;
import org.genesys2.server.model.genesys.Parameter;
import org.genesys2.server.model.genesys.ParameterCategory;
import org.genesys2.server.model.impl.Crop;
import org.genesys2.server.persistence.domain.AccessionTraitRepository;
import org.genesys2.server.persistence.domain.GenesysLowlevelRepository;
import org.genesys2.server.persistence.domain.MetadataMethodRepository;
import org.genesys2.server.persistence.domain.MethodRepository;
import org.genesys2.server.persistence.domain.ParameterCategoryRepository;
import org.genesys2.server.persistence.domain.ParameterRepository;
import org.genesys2.server.persistence.domain.TraitValueRepository;
import org.genesys2.server.security.AuthUserDetails;
import org.genesys2.server.service.AclService;
import org.genesys2.server.service.HtmlSanitizer;
import org.genesys2.server.service.TraitService;
import org.genesys2.server.service.impl.GenesysServiceImpl;
import org.genesys2.spring.SecurityContextUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.acls.domain.BasePermission;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class TraitServiceMock implements TraitService{
    public static final Log LOG = LogFactory.getLog(GenesysServiceImpl.class);

    @Autowired
    private GenesysLowlevelRepository genesysLowlevelRepository;
    @Autowired
    private AclService aclService;
    @Autowired
    private HtmlSanitizer htmlSanitizer;

    @Autowired
    private TraitValueRepository traitValueRepository;
    @Autowired
    private ParameterCategoryRepository parameterCategoryRepository;
    @Autowired
    private ParameterRepository parameterRepository;
    @Autowired
    private MethodRepository methodRepository;
    @Autowired
    private AccessionTraitRepository accessionTraitRepository;
//    @Autowired
//    private MetadataRepository metadataRepository;
    @Autowired
    private MetadataMethodRepository metadataMethodRepository;

    @Override
    public List<ParameterCategory> listCategories() {
        return parameterCategoryRepository.findAll(new Sort(new Sort.Order(Sort.Direction.ASC, "id")));
    }

    @Override
    public List<Parameter> listTraits() {
        return parameterRepository.findAll(new Sort(new Sort.Order(Sort.Direction.ASC, "id")));
    }

    @Override
    public Page<Parameter> listTraits(Pageable pageable) {
        return parameterRepository.findAll(pageable);
    }

    @Override
    public Page<Parameter> listTraits(Crop crop, Pageable pageable) {
        return parameterRepository.findByCrop(crop, pageable);
    }

    @Override
    public Parameter getTrait(long traitId) {
        return parameterRepository.findOne(traitId);
    }

    @Override
    public List<Method> getTraitMethods(Parameter trait) {
        return methodRepository.findByParameter(trait);
    }

    @Override
    public Method getMethod(long methodId) {
        return methodRepository.findOne(methodId);
    }

    @Override
    public List<Method> listMethods() {
        return methodRepository.findAll(new Sort(new Sort.Order(Sort.Direction.ASC, "id")));
    }

    @Override
    public Page<Method> listMethods(Pageable pageable) {
        return methodRepository.findAll(pageable);
    }

    @Override
    public List<Metadata> listMetadataByMethod(Method method) {
        return metadataMethodRepository.listMetadataByMethodId(method.getId());
    }

    @Override
    public List<Method> listMethods(AccessionId accession) {
        final List<Long> x = accessionTraitRepository.listMethodIds(accession);
        return x.size() == 0 ? null : methodRepository.findByIds(x);
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    @Transactional(readOnly = false)
    public ParameterCategory addCategory(String name, String i18n) {
        final ParameterCategory category = new ParameterCategory();
        category.setName(name);
        category.setNameL(i18n);
        parameterCategoryRepository.save(category);
        return category;
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    @Transactional(readOnly = true)
    public ParameterCategory getCategory(String name) {
        final ParameterCategory category = parameterCategoryRepository.findByName(name);
        return category;
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    @Transactional(readOnly = false)
    public Parameter addParameter(String rdfUri, Crop crop, String category, String title, String i18n) {

        final Parameter parameter = new Parameter();

        parameter.setRdfUri(StringUtils.defaultIfBlank(rdfUri, null));
        parameter.setCrop(crop);

        // In an ideal world, we should search for category by URI, not name...
        final ParameterCategory parameterCategory = parameterCategoryRepository.findByName(category);

        parameter.setCategory(parameterCategory);
        parameter.setTitle(title);
        parameter.setI18n(StringUtils.defaultIfBlank(i18n, null));

        parameterRepository.save(parameter);

        return parameter;
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    @Transactional(readOnly = true)
    public Parameter getParameter(String rdfUri) {

        if (rdfUri == null || rdfUri.isEmpty()) {
            return null;
        }

        final Parameter parameter = parameterRepository.findByRdfUri(rdfUri); // assumes
        // that
        // crop
        // x
        // title
        // is
        // unique(?)
        return parameter;
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    @Transactional(readOnly = true)
    public Parameter getParameter(Crop crop, String title) {
        final Parameter parameter = parameterRepository.findByCropAndTitle(crop, title); // assumes
        // that
        // crop
        // x
        // title
        // is
        // unique(?)
        return parameter;
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    @Transactional(readOnly = false)
    public Method addMethod(String rdfUri, String description, String i18n, String unit, String fieldName, int fieldType, Integer fieldSize, String options,
                            String range, Parameter parameter) {

        final Method method = new Method();

        method.setRdfUri(StringUtils.defaultIfBlank(rdfUri, null));
        method.setMethod(htmlSanitizer.sanitize(description));
        method.setI18n(StringUtils.defaultIfBlank(i18n, null));
        method.setUnit(StringUtils.defaultIfBlank(StringUtils.trimToNull(unit), null));
        method.setFieldName(StringUtils.trimToNull(fieldName));
        method.setFieldType(fieldType);

        if (fieldType == 0) {
            // Will throw NPE if not provided!
            method.setFieldSize(fieldSize.toString());
        }

        method.setOptions(StringUtils.defaultIfBlank(StringUtils.trimToNull(options), null));
        method.setRange(StringUtils.defaultIfBlank(StringUtils.trimToNull(range), null));
        method.setParameter(parameter);

        methodRepository.save(method);

        // Ensure table for method!
        genesysLowlevelRepository.ensureMethodTable(method);

        return method;
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    @Transactional(readOnly = true)
    public Method getMethod(String rdfUri) {

        if (rdfUri == null || rdfUri.isEmpty()) {
            return null;
        }

        final Method method = methodRepository.findByRdfUri(rdfUri);

        return method;
    }

    @Override
    public Method getMethod(String description, Parameter parameter) {

        if (description == null || description.isEmpty()) {
            return null;
        }

        final Method method = methodRepository.findByMethodAndParameter(description, parameter);

        return method;
    }

    /**
     * Returns datasets to which current user has 'WRITE'
     */
    @Override
    @PreAuthorize("isAuthenticated()")
    public List<Method> listMyMethods() {
        final AuthUserDetails user = SecurityContextUtil.getAuthUser();
        final List<Long> oids = aclService.listIdentitiesForSid(Method.class, user, BasePermission.WRITE);
        LOG.info("Got " + oids.size() + " elements for " + user.getUsername());
        if (oids.size() == 0) {
            return null;
        }

        return methodRepository.findByIds(oids);
    }

    @Override
    public Map<String, Long> getMethodStatistics(Method method) {
        if (method.isCoded()) {
            return traitValueRepository.getStatistics(method);
        } else {
            return null;
        }
    }

    @Override
    public Map<ParameterCategory, List<Parameter>> mapTraits(Crop crop, List<ParameterCategory> parameterCategories) {
        if (parameterCategories == null || parameterCategories.size() == 0) {
            return null;
        }

        final HashMap<ParameterCategory, List<Parameter>> catTraits = new HashMap<ParameterCategory, List<Parameter>>();

        for (final ParameterCategory pc : parameterCategories) {
            ArrayList<Parameter> traits;
            catTraits.put(pc, traits = new ArrayList<Parameter>());
            traits.addAll(parameterRepository.findByCropAndCategory(crop, pc, new Sort(new Sort.Order("title"))));
        }

        return catTraits;
    }

    @Override
    public Map<Long, List<Method>> mapMethods(Crop crop) {
        final HashMap<Long, List<Method>> paramMethods = new HashMap<Long, List<Method>>();

        for (final Method method : methodRepository.findByCrop(crop)) {
            List<Method> list = paramMethods.get(method.getParameter().getId());
            if (list == null) {
                paramMethods.put(method.getParameter().getId(), list = new ArrayList<Method>());
            }
            list.add(method);
        }

        return paramMethods;
    }
}
