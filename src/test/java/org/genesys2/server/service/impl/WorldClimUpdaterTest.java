package org.genesys2.server.service.impl;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.impl.client.HttpClientBuilder;
import org.genesys2.server.model.dataset.DS;
import org.genesys2.server.model.dataset.DSDescriptor;
import org.genesys2.server.persistence.domain.GenesysLowlevelRepository;
import org.genesys2.server.persistence.domain.GenesysLowlevelRepositoryCustomImpl;
import org.genesys2.server.service.DSService;
import org.genesys2.server.service.DescriptorService;
import org.genesys2.server.service.worker.WorldClimUpdater;
import org.genesys2.server.test.JpaRealDataConfig;
import org.genesys2.server.test.PropertyPlacholderInitializer;
import org.genesys2.spring.config.HazelcastConfig;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.spring.cache.HazelcastCacheManager;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WorldClimUpdaterTest.Config.class, initializers = PropertyPlacholderInitializer.class)
@ActiveProfiles("dev")
@Ignore
public class WorldClimUpdaterTest {

	@Import({ HazelcastConfig.class, JpaRealDataConfig.class })
	public static class Config {

		@Bean
		public HazelcastCacheManager cacheManager(HazelcastInstance hazelcastInstance) {
			HazelcastCacheManager cm = new HazelcastCacheManager(hazelcastInstance);
			return cm;
		}

		@Bean
		public WorldClimUpdater worldClimUpdater() {
			return new WorldClimUpdater();
		}

		@Bean
		public HttpClientBuilder httpClientBuilder() {
			return HttpClientBuilder.create();
		}

		@Bean
		public DSService dsService() {
			return new DSServiceImpl();
		}

		@Bean
		public DescriptorService descriptorService() {
			return new DescriptorServiceImpl();
		}

		@Bean
		public GenesysLowlevelRepository genesysLowlevel() {
			return new GenesysLowlevelRepositoryCustomImpl();
		}

	}

	@Autowired
	private WorldClimUpdater worldClimUpdater;

	@Autowired
	private DSService dsService;

	@Test
	public void testUpdateAlt() throws IOException {
		worldClimUpdater.update("alt");

		for (int i = 1; i <= 12; i++) {
			worldClimUpdater.update("tmin" + i);
			worldClimUpdater.update("tmax" + i);
			worldClimUpdater.update("tmean" + i);
			worldClimUpdater.update("prec" + i);
		}

		for (int i = 1; i <= 19; i++) {
			worldClimUpdater.update("bio" + i);
		}
	}

	@Test
	public void deleteDataset() {
		DS ds = dsService.loadDatasetByUuid(WorldClimUpdater.WORLDCLIM_DATASET);
		if (ds != null) {
			for (DSDescriptor dsd : ds.getDescriptors()) {
				dsService.deleteDescriptor(dsd);
			}
			dsService.deleteRows(ds);
			dsService.deleteDataset(ds);
		}
	}

	@Test
	public void testDownload() throws IOException {
		DS ds = dsService.loadDatasetByUuid(WorldClimUpdater.WORLDCLIM_DATASET);
		File file = new File("worldclim " + System.currentTimeMillis() + ".xls");
		BufferedOutputStream fos = new BufferedOutputStream(new FileOutputStream(file));
		dsService.download(ds, fos);
		fos.flush();
		fos.close();
		// file.delete();
	}

	@Test
	public void testDownloadDesc() throws IOException {
		DS ds = dsService.loadDatasetByUuid(WorldClimUpdater.WORLDCLIM_DATASET);
		List<DSDescriptor> dsds = new ArrayList<DSDescriptor>();
		for (DSDescriptor dsd : ds.getDescriptors()) {
			if (dsd.getDescriptor().getCode().startsWith("tmin")) {
				dsds.add(dsd);
			}
		}
		System.out.println("Downloading tmin data: " + dsds.size());

		File file = new File("worldclim " + System.currentTimeMillis() + ".xls");
		BufferedOutputStream fos = new BufferedOutputStream(new FileOutputStream(file));
		dsService.download(ds, dsds, fos);
		fos.flush();
		fos.close();
		// file.delete();
	}
}
