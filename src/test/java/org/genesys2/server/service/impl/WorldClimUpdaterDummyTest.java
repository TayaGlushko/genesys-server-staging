package org.genesys2.server.service.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.*;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.http.impl.client.HttpClientBuilder;
import org.genesys.worldclim.grid.generic.GenericGridFile;
import org.genesys.worldclim.grid.generic.GenericGridZipFile;
import org.genesys.worldclim.grid.generic.Header;
import org.genesys2.server.persistence.domain.GenesysLowlevelRepository;
import org.genesys2.server.persistence.domain.GenesysLowlevelRepositoryCustomImpl;
import org.genesys2.server.service.worker.WorldClimUpdater;
import org.genesys2.server.test.JpaDataConfig;
import org.genesys2.server.test.PropertyPlacholderInitializer;
import org.genesys2.spring.config.HazelcastConfig;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.spring.cache.HazelcastCacheManager;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WorldClimUpdaterDummyTest.Config.class, initializers = PropertyPlacholderInitializer.class)
@ActiveProfiles("dev")
@Ignore
public class WorldClimUpdaterDummyTest {

	@Import({ HazelcastConfig.class, JpaDataConfig.class })
	public static class Config {

		@Bean
		public HazelcastCacheManager cacheManager(HazelcastInstance hazelcastInstance) {
			HazelcastCacheManager cm = new HazelcastCacheManager(hazelcastInstance);
			return cm;
		}

		@Bean
		public WorldClimUpdater worldClimUpdater() {
			return new WorldClimUpdater();
		}

		@Bean
		public HttpClientBuilder httpClientBuilder() {
			return HttpClientBuilder.create();
		}

		@Bean
		public GenesysLowlevelRepository genesysLowlevel() {
			return new GenesysLowlevelRepositoryCustomImpl();
		}
	}

	@Autowired
	private WorldClimUpdater worldClimUpdater;

	@Value("${download.files.dir}")
	private String downloadDir;

	@Test
	public void testDownloadAlt() throws IOException {
		File tempArchive = worldClimUpdater.download("alt_2-5m_bil.zip");
		File destination = new File(downloadDir, "worldclim-alt-temp");
		assertThat("Destination folder must not exist", destination.exists(), is(Boolean.FALSE));
		destination.mkdirs();
		assertThat("Destination folder must exist", destination.exists(), is(Boolean.TRUE));
		assertThat("Destination folder must be a directory", destination.isDirectory(), is(Boolean.TRUE));

		try {
			List<File> files = GenericGridZipFile.unzip(tempArchive, destination);
			for (File file : files) {
				System.out.println("Unzipped " + file.getAbsolutePath());
				file.delete();
			}
		} finally {
			tempArchive.delete();
			destination.delete();
		}
	}

	@Test
	public void testDownloadAltKeep() throws IOException {
		File destination = new File(downloadDir, "worldclim-data");
		destination.mkdirs();
		assertThat("Destination folder must exist", destination.exists(), is(Boolean.TRUE));
		assertThat("Destination folder must be a directory", destination.isDirectory(), is(Boolean.TRUE));

		String[] altFiles = destination.list(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.startsWith("alt") && (name.endsWith(".bil") || name.endsWith(".hdr"));
			}
		});

		if (altFiles == null || altFiles.length == 0) {
			System.out.println("Must download");
			List<File> files = worldClimUpdater.downloadAndExtract(destination, "alt_2-5m_bil.zip");
			for (File file : files) {
				System.out.println("Unzipped " + file.getAbsolutePath());
			}
		} else {
			System.out.println("Files are there");
		}

		GenericGridFile ggf = new GenericGridFile(destination, "alt");
		Header header = ggf.readHeader();

		int varSize = header.getBits() / 8;
		ByteBuffer byteBuffer = header.createByteBuffer();
		FileChannel dataChannel = null;
		try {
			dataChannel = ggf.getDataFileStream().getChannel();

			for (int i = header.getColumns() - 1; i >= 0; i--) {
				dataChannel.read(byteBuffer, i * varSize);
				assertThat("Value will be read!", byteBuffer.getShort(0), notNullValue());
			}
		} finally {
			IOUtils.closeQuietly(dataChannel);
		}
	}

	@Test
	public void testUpdateAltitude() throws IOException {
		File destination = new File(downloadDir, "worldclim-data");
		destination.mkdirs();
		assertThat("Destination folder must exist", destination.exists(), is(Boolean.TRUE));
		assertThat("Destination folder must be a directory", destination.isDirectory(), is(Boolean.TRUE));

		String[] altFiles = destination.list(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.startsWith("alt") && (name.endsWith(".bil") || name.endsWith(".hdr"));
			}
		});

		if (altFiles == null || altFiles.length == 0) {
			System.out.println("Must download");
			List<File> files = worldClimUpdater.downloadAndExtract(destination, "alt_2-5m_bil.zip");
			for (File file : files) {
				System.out.println("Unzipped " + file.getAbsolutePath());
			}
		} else {
			System.out.println("Files are there");
		}

		GenericGridFile ggf = new GenericGridFile(destination, "alt");
		Header header = ggf.readHeader();

		short nullValue = (short) header.getNoDataValue();
		MappedByteBuffer x = ggf.mapDataBuffer();
		for (int i = 0; i < header.getColumns(); i++) {
			System.out.println(x.getShort(i * 2));
			assertThat("Value will be read!", x.getShort(i * 2), notNullValue());
			assertThat("Value will be -9999", x.getShort(i * 2), is(nullValue));
		}
	}
}
