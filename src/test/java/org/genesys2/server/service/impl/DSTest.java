package org.genesys2.server.service.impl;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.genesys2.server.model.dataset.DS;
import org.genesys2.server.model.dataset.DSDescriptor;
import org.genesys2.server.model.dataset.DSQualifier;
import org.genesys2.server.model.impl.Descriptor;
import org.genesys2.server.service.DSService;
import org.genesys2.server.service.DescriptorService;
import org.genesys2.server.test.JpaDataConfig;
import org.genesys2.server.test.PropertyPlacholderInitializer;
import org.genesys2.spring.config.HazelcastConfig;
import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.spring.cache.HazelcastCacheManager;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = DSTest.Config.class, initializers = PropertyPlacholderInitializer.class)
@ActiveProfiles("dev")
@Ignore
public class DSTest {

	@Import({ HazelcastConfig.class, JpaDataConfig.class })
	public static class Config {

		@Bean
		public HazelcastCacheManager cacheManager(HazelcastInstance hazelcastInstance) {
			HazelcastCacheManager cm = new HazelcastCacheManager(hazelcastInstance);
			return cm;
		}

		@Bean
		public DSService dsService() {
			return new DSServiceImpl();
		}

		@Bean
		public DescriptorService descriptorService() {
			return new DescriptorServiceImpl();
		}
	}

	@Autowired
	private DSService dsService;
	@Autowired
	private DescriptorService descriptorService;

	@Test
	public void test1() throws IOException {
		DS ds = generateDataset();
		dsService.saveDataset(ds);
		assertThat("Dataset must have a UUID", ds.getUuid(), notNullValue());

		Descriptor d1 = generateDescriptor("D1");
		descriptorService.saveDescriptor(d1);
		Descriptor d2 = generateDescriptor("D2");
		descriptorService.saveDescriptor(d2);

		DSQualifier dsq1 = dsService.addQualifier(ds, d1);
		DSQualifier dsq2 = dsService.addQualifier(ds, d2);

		Descriptor d3 = generateDescriptor("D3");
		descriptorService.saveDescriptor(d3);
		Descriptor d4 = generateDescriptor("D4");
		descriptorService.saveDescriptor(d4);

		dsService.addDescriptor(ds, d3);
		dsService.addDescriptor(ds, d4);

		// assertThat("DSQualifier must have an id", dsq1.getId(),
		// notNullValue());
		//
		System.out.println(dsq1.getId() + " " + dsq1.getDescriptor());
		assertThat("DSQualifier must be loaded by id", dsq1, notNullValue());
		assertThat("DSQualifier must have a Dataset", dsq1.getDataset(), notNullValue());
		assertThat("DSQualifier must have a Descriptor", dsq1.getDescriptor(), notNullValue());

		DS loaded = dsService.loadDatasetByUuid(ds.getUuid());
		assertThat("Dataset must be loaded by UUID", loaded, notNullValue());
		assertThat("Dataset.qualifiers must not be null", loaded.getQualifiers(), notNullValue());
		assertThat("Dataset must have 1 DSQualifier", loaded.getQualifiers(),  IsCollectionWithSize.hasSize(2));
		assertThat("Dataset must have 1 DSQualifier value", loaded.getQualifiers().get(0), notNullValue());
		assertThat("Dataset must have 1 DSQualifier with id", loaded.getQualifiers().get(0).getId(), notNullValue());

		List<Object[]> rows = new ArrayList<Object[]>();
		for (int i = 0; i < 2; i++) {
			Object[] row = new Object[4];
			row[0] = new Long(i);
			row[1] = new Long(1000 + i);

			row[2] = new Double(2000 + i);
			row[3] = new Double(3000 + i);

			rows.add(row);
			rows.add(row);
		}
		dsService.updateRows(loaded, rows);
		dsService.updateRows(loaded, rows);

		//
		// DSRow dsRow = new DSRow();
		// dsRow.setDataset(ds);
		// dsRowRepo.save(dsRow);
		//
		// DSRowQualifierLong dsrq1 = new DSRowQualifierLong();
		// dsrq1.setDatasetQualifier(dsq1);
		// dsrq1.setRow(dsRow);
		// dsrq1.setValue(100l + i);
		// dsRowQualiRepo.save(dsrq1);
		//
		// DSRowQualifierLong dsrq2 = new DSRowQualifierLong();
		// dsrq2.setDatasetQualifier(dsq2);
		// dsrq2.setRow(dsRow);
		// dsrq2.setValue(1000l + i);
		// dsRowQualiRepo.save(dsrq2);
		// }
		//
		// for (DSRow dsrX : dsRowRepo.findAll()) {
		// assertThat("Must have an id", dsrX.getId(), notNullValue());
		// assertThat("Must have a dataset", dsrX.getDataset(), notNullValue());
		// // assertThat("Must have row qualifiers", dsrX.getRowQualifiers(),
		// // notNullValue());
		// }
		//
		// for (DSRowQualifier<?> dsrqX : dsRowQualiRepo.findAll()) {
		// assertThat("Must have an id", dsrqX.getId(), notNullValue());
		// assertThat("Must have a row", dsrqX.getRow(), notNullValue());
		// assertThat("Must have a value!", dsrqX.getValue(), notNullValue());
		// }

	}

	private Descriptor generateDescriptor(String code) {
		Descriptor d = new Descriptor();
		d.setCode(code);
		return d;
	}

	private DS generateDataset() {
		DS ds = new DS();
		ds.setDescriptors(new ArrayList<DSDescriptor>());
		ds.setQualifiers(new ArrayList<DSQualifier>());
		return ds;
	}
}
