/*
 * Copyright 2016 Global Crop Diversity Trust, www.croptrust.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.genesys2.server.test.filerepository;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;

/**
 * The Class TestImage.
 */
public class TestImage {

	/** The image bytes. */
	private final byte[] imageBytes;

	/** The original filename. */
	private final String originalFilename;

	/** The content type. */
	private final String contentType;

	/**
	 * Instantiates a new test image.
	 *
	 * @param imageFile the image file
	 * @param contentType the content type
	 */
	public TestImage(final String imageFile, final String contentType) {
		try {
			try (InputStream is = TestImage.class.getClassLoader().getResourceAsStream("images/" + imageFile)) {
				imageBytes = IOUtils.toByteArray(is);
			}
		} catch (final IOException e) {
			throw new RuntimeException("Could not create TestImage from resource at images/" + imageFile, e);
		}
		this.originalFilename = imageFile;
		this.contentType = contentType;
	}

	/**
	 * Gets the image bytes.
	 *
	 * @return the image bytes
	 */
	public byte[] getImageBytes() {
		return imageBytes;
	}

	/**
	 * Gets the original filename.
	 *
	 * @return the original filename
	 */
	public String getOriginalFilename() {
		return this.originalFilename;
	}

	/**
	 * Gets the content type.
	 *
	 * @return the content type
	 */
	public String getContentType() {
		return this.contentType;
	}
}
