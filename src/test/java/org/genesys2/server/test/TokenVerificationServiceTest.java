/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.genesys2.server.mock.service.TokenConsumerService;
import org.genesys2.server.mock.service.TokenConsumerServiceImpl;
import org.genesys2.server.model.impl.VerificationToken;
import org.genesys2.server.service.TokenVerificationService;
import org.genesys2.server.service.TokenVerificationService.NoSuchVerificationTokenException;
import org.genesys2.server.service.impl.TokenVerificationServiceImpl;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Tests for {@link TokenVerificationServiceImpl}
 *
 * @author matijaobreza
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TokenVerificationServiceTest.Config.class, initializers = PropertyPlacholderInitializer.class)
@Ignore
public class TokenVerificationServiceTest {

	@ComponentScan(basePackages = { "org.genesys2.server.persistence.domain" })
	public static class Config extends JpaDataConfig {

		@Bean
		public TokenVerificationService tokenVerificationService() {
			return new TokenVerificationServiceImpl();
		}

		@Bean
		public TokenConsumerService tokenConsumerService() {
			return new TokenConsumerServiceImpl();
		}

	}

	@Autowired
	private TokenVerificationService tokenVerificationService;

	@Autowired
	private TokenConsumerService tokenConsumerService;

	@Test(expected = NoSuchVerificationTokenException.class)
	public void consumeException() throws NoSuchVerificationTokenException {
		tokenVerificationService.consumeToken("purpose1", "no-such-uuid", "wrongkey");
	}

	@Test(expected = NoSuchVerificationTokenException.class)
	public void testGenerateAndFail() throws NoSuchVerificationTokenException {
		final VerificationToken t = tokenVerificationService.generateToken("purpose1", null);
		assertTrue("ID should be assigned", t.getId() != null);
		assertTrue("UUID should be assigned", t.getUuid() != null);
		assertTrue("Data should be null", t.getData() == null);

		tokenVerificationService.consumeToken("purpose1", t.getUuid(), "wrongkey");
	}

	@Test
	public void testGenerateAndConsume() {
		final VerificationToken t = tokenVerificationService.generateToken("purpose1", null);
		assertTrue("ID should be assigned", t.getId() != null);
		assertTrue("UUID should be assigned", t.getUuid() != null);
		assertTrue("Data should be null", t.getData() == null);

		try {
			tokenVerificationService.consumeToken("purpose1", t.getUuid(), "wrongkey");
			fail("Exception not thrown");
		} catch (final NoSuchVerificationTokenException e) {

		}

		try {
			final VerificationToken ct = tokenVerificationService.consumeToken("purpose1", t.getUuid(), t.getKey());
			assertTrue("Token not consumed", ct != null);
		} catch (final NoSuchVerificationTokenException e) {
			fail("Token not found");
		}

		try {
			tokenVerificationService.consumeToken("purpose1", t.getUuid(), t.getKey());
			fail("Token still found");
		} catch (final NoSuchVerificationTokenException e) {

		}
	}

	@Test(expected = NoSuchVerificationTokenException.class)
	public void cancelException() throws NoSuchVerificationTokenException {
		tokenVerificationService.cancel("no-such-uuid");
		fail("Token should not be found");
	}

	@Test
	public void testGenerateAndCancel() {
		final VerificationToken t = tokenVerificationService.generateToken("purpose1", null);
		assertTrue("ID should be assigned", t.getId() != null);
		assertTrue("UUID should be assigned", t.getUuid() != null);
		assertTrue("Data should be null", t.getData() == null);

		try {
			tokenVerificationService.cancel(t.getUuid());
		} catch (final NoSuchVerificationTokenException e) {
			fail("Token not canceled");
		}

		try {
			tokenVerificationService.cancel(t.getUuid());
			fail("Exception expected!");
		} catch (final NoSuchVerificationTokenException e) {

		}
	}

	@Test
	public void consumeTokenNoExceptions() {
		final VerificationToken t = tokenVerificationService.generateToken("purpose1", null);
		assertTrue("Token not created", t != null);

		try {
			tokenConsumerService.noExceptions(t);
		} catch (final NoSuchVerificationTokenException e) {
			fail("Token not found");
		}

		try {
			tokenVerificationService.consumeToken(t.getPurpose(), t.getUuid(), t.getKey());
			fail("Token should not be available");
		} catch (final NoSuchVerificationTokenException e) {
		}
	}

	@Test
	public void testTokenAfterRuntime() {
		final VerificationToken t = tokenVerificationService.generateToken("purpose1", null);
		assertTrue("Token not created", t != null);
		try {
			tokenConsumerService.throwRuntimeException(t);
			fail("RuntimeException expected");
		} catch (final NoSuchVerificationTokenException e) {
			fail("Token not found");
		} catch (final RuntimeException e) {
			// ok
		}

		try {
			tokenVerificationService.consumeToken(t.getPurpose(), t.getUuid(), t.getKey());
		} catch (final NoSuchVerificationTokenException e) {
			fail("Token should still be available");
		}
	}

	@Test
	public void testTokenException() {
		final VerificationToken t = tokenVerificationService.generateToken("purpose1", null);
		assertTrue("Token not created", t != null);
		try {
			tokenConsumerService.throwException(t);
			fail("Exception expected");
		} catch (final Exception e) {
			// ok
		}

		try {
			tokenVerificationService.consumeToken(t.getPurpose(), t.getUuid(), t.getKey());
		} catch (final NoSuchVerificationTokenException e) {
			fail("Token should still be available");
		}
	}

	@Test
	public void noConsume() {
		try {
			tokenConsumerService.noToken();
			fail("Token should not be found");
		} catch (final NoSuchVerificationTokenException e) {

		}
	}
}
