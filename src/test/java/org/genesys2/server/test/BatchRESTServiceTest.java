/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;
import org.genesys2.server.aspect.AsAdminAspect;
import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.genesys.Taxonomy2;
import org.genesys2.server.model.impl.Country;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.model.json.Api1Constants;
import org.genesys2.server.persistence.domain.CountryRepository;
import org.genesys2.server.service.AclService;
import org.genesys2.server.service.BatchRESTService;
import org.genesys2.server.service.ContentService;
import org.genesys2.server.service.CropService;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.GeoService;
import org.genesys2.server.service.HtmlSanitizer;
import org.genesys2.server.service.InstituteService;
import org.genesys2.server.service.OrganizationService;
import org.genesys2.server.service.TaxonomyService;
import org.genesys2.server.service.UserService;
import org.genesys2.server.service.impl.AclServiceImpl;
import org.genesys2.server.service.impl.BatchRESTServiceImpl;
import org.genesys2.server.service.impl.ContentServiceImpl;
import org.genesys2.server.service.impl.CropServiceImpl;
import org.genesys2.server.service.impl.GenesysServiceImpl;
import org.genesys2.server.service.impl.GeoServiceImpl;
import org.genesys2.server.service.impl.InstituteServiceImpl;
import org.genesys2.server.service.impl.NonUniqueAccessionException;
import org.genesys2.server.service.impl.OWASPSanitizer;
import org.genesys2.server.service.impl.OrganizationServiceImpl;
import org.genesys2.server.service.impl.RESTApiException;
import org.genesys2.server.service.impl.TaxonomyManager;
import org.genesys2.server.service.impl.TaxonomyServiceImpl;
import org.genesys2.server.service.impl.UserServiceImpl;
import org.genesys2.server.servlet.controller.rest.model.AccessionHeaderJson;
import org.genesys2.spring.config.HazelcastConfig;
import org.genesys2.spring.config.SpringCacheConfig;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.ui.velocity.VelocityEngineFactoryBean;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("dev")
@ContextHierarchy({
	@ContextConfiguration(name = "root", classes = { JpaDataConfig.class, HazelcastConfig.class, SpringCacheConfig.class }, initializers = PropertyPlacholderInitializer.class),
	@ContextConfiguration(classes = BatchRESTServiceTest.Config.class, initializers = PropertyPlacholderInitializer.class)
})
@Ignore
public class BatchRESTServiceTest {
	private final ObjectMapper mapper = new ObjectMapper();

	public static class Config {

		@Bean
		public AsAdminAspect asAdminAspect() {
			return new AsAdminAspect();
		}

		@Bean
		public UserService userService() {
			return new UserServiceImpl();
		}

		@Bean
		public AclService aclService() {
			return new AclServiceImpl();
		}

		@Bean
		public TaxonomyService taxonomyService() {
			return new TaxonomyServiceImpl();
		}

		@Bean
		public BatchRESTService batchRESTService() {
			return new BatchRESTServiceImpl();
		}

		@Bean
		public CropService cropService() {
			return new CropServiceImpl();
		}

		@Bean
		public GenesysService genesysService() {
			return new GenesysServiceImpl();
		}

		@Bean
		public HtmlSanitizer htmlSanitizer() {
			return new OWASPSanitizer();
		}

		@Bean
		public GeoService geoService() {
			return new GeoServiceImpl();
		}

		@Bean
		public ContentService contentService() {
			return new ContentServiceImpl();
		}

		@Bean
		public VelocityEngine velocityEngine() throws VelocityException, IOException {
			final VelocityEngineFactoryBean vf = new VelocityEngineFactoryBean();
			return vf.createVelocityEngine();
		}

		@Bean
		public OrganizationService organizationService() {
			return new OrganizationServiceImpl();
		}

		@Bean
		public InstituteService instituteService() {
			return new InstituteServiceImpl();
		}

		@Bean
		public HttpClientBuilder httpClientBuilder() {
			return HttpClientBuilder.create();
		}

		@Bean
		public TaskExecutor taskExecutor() {
			return new ThreadPoolTaskExecutor();
		}

		@Bean
		public TaxonomyManager taxonomyManager() {
			return new TaxonomyManager();
		}
	}

	@Autowired
	private BatchRESTService batchRESTService;

	@Autowired
	private GenesysService genesysService;

	@Autowired
	private TaxonomyService taxonomyService;

	@Autowired
	private InstituteService instituteService;

	@Autowired
	private CountryRepository countryRepository;

	@Before
	public void setup() {
		System.err.println("Setting up");
		final Collection<FaoInstitute> institutes = new ArrayList<FaoInstitute>();
		for (final String instCode : new String[] { "INS002", "INS001" }) {
			final FaoInstitute institute = new FaoInstitute();
			institute.setFullName(instCode + " institute");
			institute.setCode(instCode);
			institute.setUniqueAcceNumbs(true);
			institutes.add(institute);
		}
		instituteService.update(institutes);

		final Country country = new Country();
		country.setCode2("CT");
		country.setCode3("CTY");
		country.setName("Country");
		country.setCurrent(true);
		countryRepository.save(country);
	}

	@After
	public void teardown() {
		System.err.println("Tearing down");
		for (final String instCode : new String[] { "INS002", "INS001" }) {
			final FaoInstitute institute = instituteService.getInstitute(instCode);
			instituteService.delete(institute.getCode());
		}

		countryRepository.delete(countryRepository.findAll());
	}

	@Test
	public void testTaxonomy() throws NonUniqueAccessionException {
		final String instCode = "INS002";
		final FaoInstitute institute = instituteService.getInstitute(instCode);
		assertTrue("institute is null", institute != null);
		System.err.println(institute);

		final Map<AccessionHeaderJson, ObjectNode> batch = new HashMap<AccessionHeaderJson, ObjectNode>();

		final AccessionHeaderJson dataJson = new AccessionHeaderJson();
		dataJson.acceNumb = "AC 1";
		dataJson.instCode = instCode;

		String genus="Triticum", sp="aestivum", spauthor="L." ,subtaxa="subsp. aestivum", subtauthor="(Desf.) Husn.";
		
		final ObjectNode json = mapper.createObjectNode();
		json.put(Api1Constants.Accession.GENUS, genus);
		json.put(Api1Constants.Accession.SPECIES, sp);
		json.put(Api1Constants.Accession.SPAUTHOR, spauthor);
		json.put(Api1Constants.Accession.SUBTAXA, subtaxa);
		json.put(Api1Constants.Accession.SUBTAUTHOR, subtauthor);
		batch.put(dataJson, json);

		try {
			batchRESTService.ensureTaxonomies(institute, batch);
		} catch (final RESTApiException e) {
			fail(e.getMessage());
		}
		Taxonomy2 loaded = taxonomyService.find(genus, sp, spauthor, subtaxa, subtauthor);
		assertThat("Taxonomy should be found!", loaded, notNullValue());
		assertThat("Genus doesn't match", loaded.getGenus(), is(genus));
		assertThat("Species doesn't match", loaded.getSpecies(), is(sp));
		assertThat("SpAuthor doesn't match", loaded.getSpAuthor(), is(spauthor));
		assertThat("SubTaxa doesn't match", loaded.getSubtaxa(), is(subtaxa));
		assertThat("SubtAuthor doesn't match", loaded.getSubtAuthor(), is(subtauthor));
	}

	@Test
	public void testNewAccession() throws NonUniqueAccessionException {
		final String instCode = "INS002";
		final FaoInstitute institute = instituteService.getInstitute(instCode);
		assertTrue("institute is null", institute != null);
		System.err.println(institute);

		final Map<AccessionHeaderJson, ObjectNode> batch = new HashMap<AccessionHeaderJson, ObjectNode>();

		final AccessionHeaderJson dataJson = new AccessionHeaderJson();
		dataJson.acceNumb = "AC 1";
		dataJson.instCode = instCode;

		final ObjectNode json = mapper.createObjectNode();
		json.put(Api1Constants.Accession.GENUS, "Hordeum");
		batch.put(dataJson, json);

		try {
			batchRESTService.upsertAccessionData(institute, batch);
		} catch (final RESTApiException e) {
			fail(e.getMessage());
		}

		Accession accession = genesysService.getAccession(instCode, "AC 1");
		assertTrue(accession.getId() != null);
		assertTrue(accession.getInstituteCode().equals(instCode));
		assertTrue(accession.getInstitute().getId().equals(institute.getId()));
		assertTrue(accession.getTaxonomy() != null);
		final Taxonomy2 tax = accession.getTaxonomy();
		System.err.println(tax);
		System.err.println(accession.getTaxGenus());

		// Modify taxonomy
		json.put(Api1Constants.Accession.GENUS, "Hordeum");
		json.put(Api1Constants.Accession.SPECIES, "vulgare");
		json.put(Api1Constants.Accession.SPAUTHOR, "L.");
		json.put(Api1Constants.Accession.SUBTAXA, "some subtaxa");
		json.put(Api1Constants.Accession.SUBTAUTHOR, "Subtauthor");
		try {
			batchRESTService.upsertAccessionData(institute, batch);
		} catch (final RESTApiException e) {
			fail(e.getMessage());
		}
		// reload
		accession = genesysService.getAccession(instCode, "AC 1");
		final Taxonomy2 tax2 = accession.getTaxonomy();
		System.err.println(tax2);
		assertFalse(tax2.getId().equals(tax.getId()));
		System.err.println(accession.getTaxGenus());

		// test nothing
		try {
			System.err.println("NO UPDATE!");
			batchRESTService.upsertAccessionData(institute, batch);
		} catch (final RESTApiException e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void testClearOrgCty() throws NonUniqueAccessionException {
		final String instCode = "INS002";
		final FaoInstitute institute = instituteService.getInstitute(instCode);
		assertTrue("institute is null", institute != null);
		System.err.println(institute);

		final Map<AccessionHeaderJson, ObjectNode> batch = new HashMap<AccessionHeaderJson, ObjectNode>();

		final AccessionHeaderJson dataJson = new AccessionHeaderJson();
		dataJson.acceNumb = "AC 100";
		dataJson.instCode = instCode;

		final ObjectNode json = mapper.createObjectNode();
		json.put(Api1Constants.Accession.GENUS, "Hordeum");
		json.put(Api1Constants.Accession.ORIGCTY, "CTY");
		batch.put(dataJson, json);
		System.err.println(json);

		try {
			batchRESTService.upsertAccessionData(institute, batch);
		} catch (final RESTApiException e) {
			fail(e.getMessage());
		}

		Accession accession = genesysService.getAccession(instCode, "AC 100");
		assertTrue(accession.getId() != null);
		assertTrue(accession.getInstituteCode().equals(instCode));
		assertTrue(accession.getInstitute().getId().equals(institute.getId()));
		assertTrue(accession.getOrigin() != null);
		assertTrue("CTY".equals(accession.getOrigin()));
		assertTrue(accession.getCountryOfOrigin() != null);
		assertTrue("Country".equals(accession.getCountryOfOrigin().getName()));

		// Modify taxonomy
		json.putNull(Api1Constants.Accession.ORIGCTY);
		System.err.println(json);
		try {
			batchRESTService.upsertAccessionData(institute, batch);
		} catch (final RESTApiException e) {
			fail(e.getMessage());
		}
		// reload
		accession = genesysService.getAccession(instCode, "AC 100");
		assertTrue(accession.getOrigin() == null);
		assertTrue(accession.getCountryOfOrigin() == null);
	}

	@Test
	public void testUpdateStorage() throws NonUniqueAccessionException {
		final String instCode = "INS002";
		final FaoInstitute institute = instituteService.getInstitute(instCode);
		assertTrue("institute is null", institute != null);
		System.err.println(institute);

		final Map<AccessionHeaderJson, ObjectNode> batch = new HashMap<AccessionHeaderJson, ObjectNode>();

		final AccessionHeaderJson dataJson = new AccessionHeaderJson();
		dataJson.acceNumb = "AC 100";
		dataJson.instCode = instCode;

		final ObjectNode json = mapper.createObjectNode();
		json.put(Api1Constants.Accession.GENUS, "Hordeum");
		json.put(Api1Constants.Accession.ORIGCTY, "CTY");
		json.put(Api1Constants.Accession.STORAGE, "10;30");
		batch.put(dataJson, json);
		System.err.println(json);

		try {
			batchRESTService.upsertAccessionData(institute, batch);
		} catch (final RESTApiException e) {
			fail(e.getMessage());
		}

		Accession accession = genesysService.getAccession(instCode, "AC 100");
		assertTrue(accession.getId() != null);
		assertTrue(accession.getInstituteCode().equals(instCode));
		assertTrue(accession.getInstitute().getId().equals(institute.getId()));
		assertTrue(accession.getOrigin() != null);
		assertTrue("CTY".equals(accession.getOrigin()));
		assertTrue(accession.getCountryOfOrigin() != null);
		assertTrue("Country".equals(accession.getCountryOfOrigin().getName()));

		assertNotNull("StoRage must be", accession.getStoRage());
		assertNotNull("Storage must be", accession.getStorage());
		assertThat("Storage must be 10;30", accession.getStorage(), is("10;30"));

		// Modify STORAGE
		json.put(Api1Constants.Accession.STORAGE, "40");
		System.err.println(json);
		try {
			batchRESTService.upsertAccessionData(institute, batch);
		} catch (final RESTApiException e) {
			fail(e.getMessage());
		}
		// reload
		accession = genesysService.getAccession(instCode, "AC 100");
		assertNotNull("storage should not be null", accession.getStorage());
		assertThat("stoRage should 40", accession.getStorage(), is("40"));
		assertThat("stoRage should be 0-size", accession.getStoRage().size(), is(1));

		// Clear STORAGE
		json.putNull(Api1Constants.Accession.STORAGE);
		System.err.println(json);
		try {
			batchRESTService.upsertAccessionData(institute, batch);
		} catch (final RESTApiException e) {
			fail(e.getMessage());
		}
		// reload
		accession = genesysService.getAccession(instCode, "AC 100");
		assertNull("storage should be null", accession.getStorage());
		assertNotNull("stoRage should be null", accession.getStoRage());
		assertThat("stoRage should be 0-size", accession.getStoRage().size(), is(0));
	}

}
