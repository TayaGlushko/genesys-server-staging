/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;

import org.genesys2.server.service.EasySMTA;
import org.genesys2.server.service.EasySMTA.EasySMTAUserData;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class EasySMTATest {

	@Test
	public void testSelf() throws JsonParseException, JsonMappingException, IOException {
		final ObjectMapper mapper = new ObjectMapper();
		final EasySMTAUserData pid1 = new EasySMTAUserData();
		pid1.setPid("pid");
		final String content = mapper.writeValueAsString(pid1);
		final EasySMTAUserData pidData = mapper.readValue(content, EasySMTA.EasySMTAUserData.class);
		assertTrue(pidData != null);
	}

	@Test
	public void testJSONRead() {
		final String content = "{\"pid\":\"000000\",\"type\":\"in\",\"legalStatus\":\"cg\",\"name\":\"Matija\",\"surname\":\"Obreza\",\"email\":\"matija.obreza@croptrust.org\",\"address\":\"Platz der Vereinten Nationen 7\\r\\n53113 Bonn\",\"country\":\"DEU\",\"countryName\":\"Germany\",\"telephone\":\"\",\"fax\":\"\",\"orgName\":\"\",\"aoName\":\"\",\"aoSurname\":\"\",\"aoEmail\":\"\",\"orgAddress\":\"\",\"orgCountry\":\"\",\"orgCountryName\":\"\",\"aoTelephone\":\"\",\"aoFax\":\"\",\"shipAddrFlag\":\"s\",\"shipAddress\":\"\",\"shipCountry\":\"AFG\",\"shipTelephone\":\"\"}";
		final ObjectMapper mapper = new ObjectMapper();
		try {
			final EasySMTAUserData pidData = mapper.readValue(content, EasySMTA.EasySMTAUserData.class);
			assertTrue(pidData != null);
		} catch (final IOException e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void testJSONReadMissing() {
		final String content = "{\"pid\":\"000000\",\"firstName\":\"Matija\",\"lastName\":\"Obreza\",\"email\":\"matija.obreza@croptrust.org\",\"address\":\"Platz der Vereinten Nationen 7\\r\\n53113 Bonn\",\"country\":\"DEU\",\"countryName\":\"Germany\",\"telephone\":\"\",\"fax\":\"\",\"orgName\":\"\",\"aoName\":\"\",\"aoSurname\":\"\",\"aoEmail\":\"\",\"orgAddress\":\"\",\"orgCountry\":\"\",\"orgCountryName\":\"\",\"aoTelephone\":\"\",\"aoFax\":\"\",\"shipAddrFlag\":\"s\",\"shipAddress\":\"\",\"shipCountry\":\"AFG\",\"shipTelephone\":\"\"}";
		final ObjectMapper mapper = new ObjectMapper();
		try {
			final EasySMTAUserData pidData = mapper.readValue(content, EasySMTA.EasySMTAUserData.class);
			assertTrue(pidData != null);
		} catch (final IOException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	@Test(expected = JsonMappingException.class)
	public void testJSONSurnameLastname() throws JsonParseException, JsonMappingException, IOException {
		final String content = "{\"pid\":\"000000\",\"firstName\":\"Matija\",\"lastName\":\"Obreza\",\"surname\":\"Obreza\",\"email\":\"matija.obreza@croptrust.org\",\"address\":\"Platz der Vereinten Nationen 7\\r\\n53113 Bonn\",\"country\":\"DEU\",\"countryName\":\"Germany\",\"telephone\":\"\",\"fax\":\"\",\"orgName\":\"\",\"aoName\":\"\",\"aoSurname\":\"\",\"aoEmail\":\"\",\"orgAddress\":\"\",\"orgCountry\":\"\",\"orgCountryName\":\"\",\"aoTelephone\":\"\",\"aoFax\":\"\",\"shipAddrFlag\":\"s\",\"shipAddress\":\"\",\"shipCountry\":\"AFG\",\"shipTelephone\":\"\"}";
		final ObjectMapper mapper = new ObjectMapper();
		final EasySMTAUserData pidData = mapper.readValue(content, EasySMTA.EasySMTAUserData.class);
		assertTrue(pidData != null);
	}

	@Test
	public void testJSONNull() throws JsonParseException, JsonMappingException, IOException {
		final String content = "null";
		final ObjectMapper mapper = new ObjectMapper();
		final EasySMTAUserData pidData = mapper.readValue(content, EasySMTA.EasySMTAUserData.class);
		assertTrue(pidData == null);
	}
}
