[[chProperties]]

== Genesys configuration

=== Configuration files

Genesys Server {projectVersion} loads the following configuration file in the order
specified:

. <<application-properties,application.properties>>
. <<spring-properties,spring/spring.properties>>
. <<genesys-properties,genesys.properties>>

The first two files are bundled with the project and provide sensible defaults to 
ensure a functional configuration. They are located in the `WEB-INF/classes` folder.

Any modifications to the defaults settings must be added to the `genesys.properties` file.

==== application.properties

The `application.properties` file makes available the project version and build information. 
Do not modify!

==== spring.properties

`spring.properties` contains the default configuration options for any Genesys Server instance.
You can access its contents directly at
https://bitbucket.org/genesys2/genesys2-server/src/{buildNumber}/src/main/resources/spring/spring.properties[spring.properties on Bitbucket.org]  

==== genesys.properties

Configuration for your instance of the server belong to `genesys.properties` file. This file is not
required and does not come with any Genesys distribution (not even source code).

This file needs to be created in a location that is included on the webapplication's CLASSPATH.

[[genesys-configuration-options]]
=== Configuration options

==== HTTP, CDN and cookies

[cols="1,1,3", options="header"] 
.HTTP-related configuration
|===
|Name
|Default value
|Description

|base.host|localhost|The hostname part of the Genesys URL
|base.hostname|${base.host}:8080|maven and Jetty run on port 8080 by default
|base.url|http://${base.hostname}|Genesys happily operates with relative URLs. The `base.url` is used
in email message templates and any other case where the full URL is needed. 
|base.cookie-domain||Set the domain of the HTTP cookies. This allows us to share
cookies between all .genesys-pgr.org domains.
|base.cookie-secure|false|maven and Jetty don't use HTTPS by default. Switch this to `true` if you 
plan to run on HTTPS protocol.  
|base.cookie-http-only|false|AJAX calls from the browser sometimes require session information for 
user authentication. Keep this value `false`.
|cdn.server|${base.url}|Default configuration does not use a CDN and defaults to the `base.url`.
|cdn.base|${cdn.server}|
|cdn.flags.url|${cdn.base}/html/0/images/flags|
|robots.allow|false|Setting this to `true` will produce a `/robots.txt` response that allows robots to index
the site. 
|=== 

==== Database configuration

Genesys requires a database to function. Feel free to choose from a number of databases supported by 
https://docs.jboss.org/hibernate/orm/4.3/javadocs/org/hibernate/dialect/package-summary.html[Hibernate].
The production server https://www.genesys-pgr.org runs on mysql-server-5.6 with InnoDB. We have seen 
the server run on PostgreSQL database, but that's about it. Contact helpdesk@genesys-pgr.org if you wish
to explore running the server on some other RDBMS.

[cols="1,1,3", options="header"] 
.Database configuration options
|===
|Name
|Default value
|Description
	
|db.url|...|Default mysql connect string used by most developers
|db.driverClassName|...|Defaults to Mysql Connector/J JDBC driver class name `com.mysql.jdbc.Driver`
|db.username|root|Database username
|db.password||Password
|db.showSql|false|Do not log SQL statements
|db.hbm2ddl|true|Apply changes to RDMBS based on current the application data model
|hibernate.dialect|org.hibernate.dialect. MySQL5InnoDBDialect|The
 https://docs.jboss.org/hibernate/orm/4.3/javadocs/org/hibernate/dialect/package-summary.html[SQL dialect] that Hibernate can use when talking to the database 
|c3p0.acquireIncrement|1|Acquire increment for SQL connections. We use 2 in production.
|c3p0.minPoolSize|1|Minimum number of SQL connections in the c3p0 pool We use 5 in production.
|c3p0.maxPoolSize|5|Maximum number of SQL connections in the c3p0 pool. We use 20 in production.
|c3p0.maxIdleTime|10|We use 30 in production.
|=== 


===== Creating a mysql database

Connect to your mysql instance and create a new blank database *genesys2*:

[source,sql,linenums]
----
# Create database
CREATE DATABASE genesys2 DEFAULT CHARSET UTF8;
----

The `DEFAULT CHARSET UTF8` ensures that tables created in this database will use UTF8 character 
encoding by default.

Create user *genesys* with password *pwd* that will access the database from *localhost*:

[source,sql,linenums]
----
GRANT ALL ON genesys2.* TO 'genesys'@'localhost' IDENTIFIED BY 'pwd';
----


===== Database connection settings


Apply the relevant database configuration settings in `genesys.properties` file:

[source,properties,linenums]
----
# genesys2 with mysql database
db.url=jdbc:mysql://localhost/genesys2?useUnicode=true&characterEncoding=UTF-8&useFastDateParsing=false
db.driverClassName=com.mysql.jdbc.Driver
db.username=genesys
db.password=pwd
hibernate.dialect=org.hibernate.dialect.MySQL5InnoDBDialect
----


==== External Services

Genesys uses a number of external service APIs to provide users with advanced functionality.

[cols="1,1,3", options="header"] 
.External APIs
|===
|Name
|Default value
|Description
|google.consumerKey||Google+ sign in
|google.consumerSecret||Google+ sign in
|google.api.key||Google API key, register your application with the https://console.developers.google.com/home/dashboard[Developers Console] 
|google.url.shortener|https://www.googleapis.com/urlshortener/v1/url|Google URL shortener API
|captcha.privateKey|...|reCAPTCHA API private key. https://www.google.com/recaptcha/admin[Google reCAPTCHA]
|captcha.siteKey|...|reCAPTCHA API public or site key. https://www.google.com/recaptcha/admin[Google reCAPTCHA]
|captcha.publicKey|...|Obsolete, use `captcha.siteKey` instead.
|itpgrfa.easysmta.url|https://mls.planttreaty.org/itt/index.php?r=extsys/userinfo|Easy-SMTA URL
|itpgrfa.easysmta.username|bar|Easy-SMTA account username
|itpgrfa.easysmta.password|foo|Easy-SMTA account password
|google.analytics.account||The Google Analytics account for all pages.
|transifex.project|genesys-dev|https://www.transifex.com[Transifex] project name
|transifex.username||Transifex username
|transifex.password||Transifex password
|transifex.base.api.url|https://www.transifex.com/api/2/|Transifex API URL
|transifex.hook.key|this-is-a-test-hook-key|If you want to receive automatic notifications from Transifex, this is the hook.
|transifex.min.translated|80|Threshold of acceptable percentage translated for automatic acceptance.
|===

==== Other stuff

[cols="1,1,3", options="header"] 
.Other stuff
|===
|Name|Default value|Description
|download.files.dir|./data/|Directory where files get stored.
|auto.createContent|false|Should default content be created on application startup? Set this to true
|list.logger.pagination.size|50|??
|news.menu.abbreviate.length|22|The maximum length for abbreviated menu item titles.
|===

==== Email and SMTP

[cols="1,1,3", options="header"] 
.SMTP configuration
|===
|Name|Default value|Description
|mail.user.from|test@localhost|The email address used as the *From:* address
|mail.requests.to|test@localhost|Email address to which requests for material are always *CC'd* 
|mail.host|localhost|SMTP hostname
|mail.port|25|SMTP port 
|mail.smtp.auth|true|Use SMTP authentication
|mail.user.name||Username for SMTP authentication
|mail.user.password||Password for SMTP authentication 
|mail.smtp.ssl.enable|true|Enable SSL
|mail.smtp.starttls.enable|true|Enable STARTTLS
|mail.transport.protocol|smtp|Or `smtps`
|mail.async|true|Emails should be sent in asynchronous mode.
|===

==== Clustering

[cols="1,1,3", options="header"] 
.Clustering configuration
|===
|hazelcast.instanceName|hz-genesys|Hazelcast instance name
|hazelcast.name|genesys-hz-3.5|Hazelcast group name
|hazelcast.password|hazelcasts|Password to join the Hazelcast group
|hazelcast.port|5701|TCP Port
|hazelcast.aws.access-key||AWS Autodetection access key
|hazelcast.aws.secret-key||AWS Autodetection secret key
|hazelcast.aws.region|eu-west-1|Limit search for nodes to the specified AWS region
|hazelcast.aws.security-group|sg-hazelcast|Security group used to identify instances considered for Hazecast group membership 
|elasticsearch.url|http://localhost:9200/|Connection to <<elasticsearch>>
|===

==== Scheduler and Executor pools

[cols="1,1,3", options="header"] 
.Pools and Schedulers configuration
|===
|Name|Default value|Description
|executor.core.pool.size|4|Executor thread pool size
|executor.max.pool.size|16|Maximum thread pool size
|executor.queue.capacity|100|Capacity of the executor queue if all threads are busy
|scheduler.max.pool.size|16|Scheduler thread pool size
|scheduler.tokens.cleanup.hours|1|Frequency of tokens cleanup job execution
|===


=== Other stuff

	# Theme
	theme.defaultThemeName=dev
	

	# TileServer CDN
	#tileserver.cdn='https://s1'
	#tileserver.cdn='https://s1.cdn.genesys-pgr.org','https://s2.cdn.genesys-pgr.org','https://s3.cdn.genesys-pgr.org','https://s4.cdn.genesys-pgr.org'
	tileserver.cdn='${base.url}'
	
	# TileServer Cache
	cache.defaultCacheSize=5000
	cache.tileserver.max-size=1000
	cache.tileserver.time-to-live-seconds=300
	cache.tileserver.max-idle-seconds=0
	cache.tileserver.eviction-policy=LRU
	
		
