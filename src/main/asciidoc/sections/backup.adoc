[[chBackup]]

== Application

Genesys is usually run in a http://download.eclipse.org/jetty/[jetty] instance as described 
in <<installing-genesys.adoc>>. This results in a very simple structure on the filesystem
that is easy to backup and will include the <<configuration>>.


== Configuration

Best practice requires keeping all custom changes to the <<genesys-properties.adoc>> in the
separate `genesys.properties` file.

In addition to Genesys configuration, all Jetty settings are located in the same base dir and are
easy to backup.

== Data

=== Database backup

=== File backup

=== Elasticsearch backup


== Validating backups

