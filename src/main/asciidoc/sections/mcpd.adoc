[[mcpd]]

== Multi-Crop Passport Descriptors

The http://www.bioversityinternational.org/e-library/publications/detail/faobioversity-multi-crop-passport-descriptors-v21-mcpd-v21/[Multi-crop Passport Descriptors (MCPD V.2.1)]
is an update to MCPD V.2 which was released in 2012. The MCPD V.2 was a revision of 
the first FAO/IPGRI publication released in 2001, 
expanded to accommodate emerging needs, such as the broader use of GPS tools, or the 
implementation of the http://www.planttreaty.org[International Treaty on Plant Genetic Resources for 
Food and Agriculture] Multilateral System for access and benefit sharing. 

This MCPD V.2.1 list is an expansion of the first version of the MCPD, the descriptors and allowed values of the first 
version form a subset of those in this revision. The 2001 list, developed jointly by 
http://www.bioversityinternational.org[Bioversity International] (formerly IPGRI) and FAO,
has been widely used and is considered the international standard to facilitate germplasm passport 
information exchange. These descriptors aim to be compatible with Bioversity’s crop descriptor 
lists, with the descriptors used for the FAO World Information and Early Warning System (<<wiews,WIEWS>>) 
on plant genetic resources (PGR), and with the https://www.genesys-pgr.org[Genesys PGR global portal]. 

For each multi-crop passport descriptor, a brief explanation of content, coding scheme and, in 
parentheses, suggested fieldname are provided to assist in the computerized exchange of this type of data.

The authors of the <<mcpd,MCPD>> recognize that networks or groups of users may further expand the 
MCPD list to meet their specific needs. As long as these additions allow for 
an easy conversion to the format proposed in MCPD V.2, basic passport data can be exchanged worldwide in a 
consistent manner.

=== MCPD Descriptors

[cols="1,3", options="header"] 
.MCPD descriptors
|===
|Field name
|Description

|<<puid,PUID>>|Any persistent, unique identifier assigned to the accession so it can be unambiguously 
	referenced at the global level and the information associated with it harvested through automated
	means. Report one PUID for each accession.
 
|<<mcpd-wiews,INSTCODE>>|FAO WIEWS code of the institute where the accession is maintained.
|ACCENUMB|Unique identifier of the accession within a genebank.
|COLLNUMB|Original identifier assigned by the collector(s) of the sample, normally composed of the 
	name or initials of the collector(s) followed by a number (e.g. `FM9909`). This identifier is 
	essential for identifying duplicates held in different collections.

|<<mcpd-wiews,COLLCODE>>|FAO WIEWS code of the institute collecting the sample.
|COLLNAME|Name of the institute collecting the sample. This descriptor should only be used if 
	COLLCODE cannot be filled because the FAO WIEWS code for this institute is not available.
	
|COLLINSTADDRESS|Address of the institute collecting the sample. This descriptor should only 
	be used if COLLCODE cannot be filled because the FAO WIEWS code for this institute is not available.
	
|COLLMISSID|Identifier of the collecting mission used by the Collecting Institute (e.g. `CIATFOR-052`, `CN426`).
|GENUS|Genus name for taxon. Initial upper case letter required.
|SPECIES|Specific epithet portion of the scientific name in lower case letters.

	The abbreviation `sp.` or `spp.` is allowed when exact species name is unknown. 

|SPAUTHOR|Provide the authority for the species name.
|SUBTAXA|Subtaxon can be used to store any additional taxonomic identifier. The following abbreviations
	are allowed: `subsp.` (for subspecies); `convar.` (for convariety); `var.` (for variety); `f.` (for form); `Group` (for 'cultivar group').

|SUBTAUTHOR|Provide the subtaxon authority at the most detailed taxonomic level.
|<<mcpd-cropname,CROPNAME>>|Common name of the crop. Example: `malting barley`, `macadamia`, `maize`. 

|ACCENAME|Either a registered or other designation given to the material received, other than the donor's
	accession number (DONORNUMB) or collecting number (COLLNUMB). First letter upper case.

|ACQDATE|Date on which the accession entered the collection where YYYY is the year, MM is the month 
	and DD is the day. Missing data (MM or DD) should be indicated with hyphens or '00' [double zero].

|ORIGCTY|3-letter ISO 3166-1 code of the country in which the sample was originally collected 
	(e.g. landrace, crop wild relative, farmers' variety), bred or selected (breeding lines, GMOs, 
	segregating populations, hybrids, modern cultivars, etc.).

|COLLSITE|Location information below the country level that describes where the accession was collected, 
	preferable in English. This might include the distance in kilometers and direction from the nearest town, 
	village or map grid reference point, (e.g. `7km south of Curitiba in the state of Parana`).
	
|DECLATITUDE|Latitude expressed in decimal degrees. Positive values are North of the Equator; negative values are South of the Equator (e.g. `-44.6975`).
|DECLONGITUDE|Longitude expressed in decimal degrees. Positive values are East of the Greenwich Meridian; negative values are West of the Greenwich Meridian (e.g. `+120.9123`).

|COORDUNCERT|Uncertainty associated with the coordinates in meters. Leave the value empty if the uncertainty is unknown.

|COORDDATUM|The geodetic datum or spatial reference system upon which the coordinates given in decimal latitude
	 and longitude are based (e.g. `WGS84`, `ETRS89`, `NAD83`). The GPS uses the WGS84 datum.

|GEOREFMETH|The georeferencing method used (`GPS`, determined from `map`, `gazetteer`, or `estimated using software`). 
	Leave the value empty if georeferencing method is not known.
	
|ELEVATION|Elevation of collecting site expressed in meters above sea level. Negative values are not allowed.
|COLLDATE|Collecting date of the sample, where YYYY is the year, MM is the month and DD is the day. Missing data (MM or DD) should be indicated with hyphens or '00' [double szero].
|<<mcpd-wiews,BREDCODE>>|FAO WIEWS code of the institute that has bred the material. If the holding institute 
	has bred the material, the breeding institute code (BREDCODE) should be the same as the holding institute code (INSTCODE). 
	Follows INSTCODE standard.
	
|BREDNAME|Name of the institute (or person) that bred the material. This descriptor should only be used if BREDCODE cannot
	 be filled because the FAO WIEWS code for this institute is not available.
	 
|<<mcpd-sampstat,SAMPSTAT>>|Biological status of the accession.
|ANCEST|Information about either pedigree or other description of ancestral information (e.g. parent variety
	in case of mutant or selection). For example a pedigree `Hanna/7*Atlas//Turk/8*Atlas` or a description `mutation found in Hanna`,
	`selection from Irene` or `cross involving amongst others Hanna and Irene`.

|COLLSRC|Collecting/acquisition source
|<<mcpd-wiews,DONORCODE>>|FAO WIEWS code of the donor institute. Follows INSTCODE standard.
|DONORNAME|Name of the donor institute (or person). This descriptor should be used only if DONORCODE cannot be filled because FAO WIEWS code for this institute is not available.
|DONORNUMB|Identifier assigned to an accession by the donor. Follows ACCENUMB standard.
|OTHERNUMB|Any other identifiers known to exist in other collections for this accession. Use the following
		format: `INSTCODE:ACCENUMB;INSTCODE:identifier;…`  INSTCODE and identifier are separated by a colon `:` without space. 
		Pairs of INSTCODE and identifier are separated by a semicolon `;` without space. 
		When the institute is not known, the identifier should be preceeded by a colon.

|<<mcpd-wiews,DUPLSITE>>|FAO WIEWS code of the institute(s) where a safety duplicate of the accession is maintained.

	The WIEWS institute code for Svalbard Global Seed Vault is NOR051. 

|DUPLINSTNAME|Name of the institute where a safety duplicate of the accession is maintained.
|<<mcpd-storage,STORAGE>>|Type of germplasm storage. If germplasm is maintained under different types of storage, 
	multiple choices are alllowed, separated by a semicolon (e.g. `20;30`).
|MLSSTAT|The status of an accession with regards to the Multilateral System (MLS) of the International Treaty on 
	Plant Genetic Resources for Food and Agriculture. Leave the value empty if the status is not known.
	
|REMARKS|The remarks field is used to add notes or to elaborate on descriptors with value `99` or `999` (= Other). Prefix remarks with the field name they refer to and a colon (:) without space (e.g. `COLLSRC:riverside`). Distinct remarks referring to different fields are separated by semicolon without space.
|===

[[puid]]
==== Persistent unique identifier

Any persistent, unique identifier assigned to the accession so it can be unambiguously 
referenced at the global level and the information associated with it harvested through 
automated means. Report one PUID for each accession.

There are various "types" of PUIDs: DOI, UUID, LSID, etc.

The Secretariat of the ITPGRFA is facilitating the assignment of 
DOI to PGRFA at the accession level (http://www.planttreaty.org/doi).

UUID (https://en.wikipedia.org/wiki/Universally_unique_identifier[Universally unique identifier])
is an identifier standard used in software. A UUID is simply a 128-bit value (16 bytes).

For human-readable display, many systems use a canonical format using hexadecimal text with
inserted hyphen characters. For example:

	de305d54-75b4-431b-adb2-eb6b9e546014

The intent of UUIDs is to enable distributed systems to uniquely identify information *without significant central coordination*. 
In this context the word unique should be taken to mean "practically unique" rather than "guaranteed unique".

Different https://en.wikipedia.org/wiki/Universally_unique_identifier#Variants_and_Versions[variants and versions of UUID] exist.
Version 4 (Random UUID) is most commonly used in software.

Genebanks not applying a true PUID to their accessions should use, and request recipients to 
use, the concatenation of INSTCODE, ACCENUMB, and GENUS as a globally unique identifier 
similar in most respects to the PUID whenever they exchange information on accessions with 
third parties (e.g. `NOR017:NGB17773:ALLIUM`).

[[mcpd-cropname]]
==== Crop name

Genesys will read the CROPNAME as provided and attempt to link the name with an existing crop
record in Genesys. Genesys currently supports the following crop names: 

	apple, banana, barley, beans, breadfruit, cassava, chickpea, coconut, cowpea, eggplant, fababean, 
	fingermillet, grasspea, lentil, lettuce, maize, pearlmillet, pigeonpea, potato, rice, sorghum, 
	sunflower, sweetpotato, taro, tomato, wheat, yam

The up-to-date list of crops and their coded names is available at https://www.genesys-pgr.org/c/

As more data is uploaded to Genesys we will add aliases to crops, making sure that future uploads 
properly link the accession with the specified crop. 

You are encouraged to use the crop names listed above, but more importantly, let 
helpdesk@genesys-pgr.org know if your crop is not yet listed.


[[mcpd-wiews]]
==== Institute codes in MCPD

Values for `INSTCODE`, `COLLCODE`, `BREDCODE`, `DONORCODE` and `DUPLSITE` must be provided as
<<wiews-instcode,FAO WIEWS codes>> of institutes.


[[mcpd-sampstat]]
==== Biological status of accession

The coding scheme proposed can be used at 2 different levels of detail: either by using the 
general codes such as `100`, `200`, `300`, `400`, or by using the more specific codes 
such as `110`, `120`, etc. 

.Allowed values for `SAMPSTAT` field
* `100` Wild 
** `110` Natural 
** `120` Semi-natural/wild 
** `130` Semi-natural/sown 
* `200` Weedy 
* `300` Traditional cultivar/landrace 
* `400` Breeding/research material 
** `410` Breeder's line 
** `411` Synthetic population 
** `412` Hybrid 
** `413` Founder stock/base population 
** `414` Inbred line (parent of hybrid cultivar) 
** `415` Segregating population
** `416` Clonal selection 
** `420` Genetic stock
** `421` Mutant (e.g. induced/insertion mutants, tilling populations)
** `422` Cytogenetic stocks (e.g. chromosome addition/substitution, aneuploids, amphiploids)
** `423` Other genetic stocks (e.g. mapping populations)
* `500` Advanced or improved cultivar (conventional breeding methods)
* `600` GMO (by genetic engineering)
* `999` Other (Elaborate in REMARKS field)


[[mcpd-storage]]
==== Accession storage

If germplasm is maintained under different types of storage, multiple values are allowed. When
an accession is maintained in active- and base collections, `STORAGE` corresponds to `11` and `13` 
and can be encoded as `11;13`.

.Allowed values for `STORAGE` field
* `10` Seed collection 
** `11` Short term 
** `12` Medium term 
** `13` Long term 
* `20` Field collection 
* `30` In vitro collection 
* `40` Cryopreserved collection 
* `50` DNA collection
* `99` Other (elaborate in REMARKS field)


[[mcpd-genesys]]
=== Genesys extensions to MCPD

[cols="1,3", options="header"] 
.MCPD extensions
|===
|Field name
|Description

|<<mcpd-acceurl,ACCEURL>>|Accession URL
|<<mcpd-available,AVAILABLE>>|Indicates current availabilty of accession for distribution
|<<mcpd-historic,HISTORIC>>|Indicates whether the record represents an accession no longer actively maintained by the genebank
|UUID|Universally unique identifier of the accession record
|===

[[mcpd-acceurl]]
==== Accession URL

http://www.ecpgr.cgiar.org/[ECPGR] originally extended the MCPD list with *Accession URL* field `ACCEURL`.
The field should contain the direct link to the provider's on-line portal where additional data about 
the accession may be available.  

.Passport data of IITA's TDr-3616 yam accession
----
ACCEURL: http://my.iita.org/accession2/accession/TDr-3616
----

[[mcpd-available]]
==== Accession availability

Genesys allows end-users to request for material from holding institutes. Accession records marked as *not available* 
in Genesys will be excluded from user's request.

NOTE: In addition to the availability flag, genebanks must opt-in to allow end-users to request for material through Genesys.

[[mcpd-historic]]
==== Historic records

Accessions are on occasion removed from the collection. This is especially true for pre-bred material and 
genetic stocks that are maintained by the genebank for a limited period of time. The records about such
material must not be deleted from the databases as they can potentially be tracked to other collections
where the material is still actively maintained.

The holding genebank may want to mark these records by setting the value of `HISTORIC` field to `true`.

Values `null` (not specified) and `false` indicate that the record represents an actively managed accession.

https://goo.gl/NY0Mmt[List of "historic" accession records on Genesys] 

NOTE: Historic accessions cannot be requested through Genesys.

