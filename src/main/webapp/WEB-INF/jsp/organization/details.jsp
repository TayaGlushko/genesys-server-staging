<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
<title><spring:message code="organization.page.profile.title" arguments="${organization.slug}"
    argumentSeparator="|" /></title>
	<local:content-headers description="${jspHelper.htmlToText(blurp.summary, 150)}" title="${organization.title}" keywords="${organization.slug}, ${organization.title}" />
</head>
<body>
  <h1>
    <c:out value="${organization.title}" />
    <small><c:out value="${organization.slug}" /></small>
  </h1>

  <div class="main-col-header text-right">
    <a class="btn btn-default" href="<c:url value="/org/${organization.slug}/data" />"><spring:message
        code="view.accessions"
      /></a> <a class="btn btn-default" href="<c:url value="/org/${organization.slug}/overview" />"><span
      class="glyphicon glyphicon-eye-open"
    ></span><span style="margin-left: 0.5em;"><spring:message code="data-overview.short" /></span></a> <a class="btn btn-default"
      href="<c:url value="/org/${organization.slug}/map" />"
    ><span class="glyphicon glyphicon-globe"></span><span style="margin-left: 0.5em;"><spring:message
          code="maps.view-map"
        /></span></a>
  </div>

  <security:authorize access="hasRole('ADMINISTRATOR')">
    <a href="<c:url value="/org/${organization.slug}/edit" />" class="close"> <spring:message code="edit" />
    </a>
  </security:authorize>

  <!-- Organization introductory text -->
  <cms:blurb blurb="${blurp}" />

  <c:set value="" var="countryName" />
  <div class="form-horizontal">
    <div class="form-group">
      <label class="col-lg-2 control-label"> <spring:message code="select-country" /></label>
      <div class="col-lg-3">
        <select id="countryNavigation" class="form-control">
          <c:forEach items="${members}" var="faoInstitute" varStatus="status">
            <c:if test="${countryName ne faoInstitute.country.getName(pageContext.response.locale)}">
              <c:set var="countryName" value="${faoInstitute.country.getName(pageContext.response.locale)}" />
              <option value="${faoInstitute.country.code3}">
                <c:out value="${faoInstitute.country.getName(pageContext.response.locale)}" />
              </option>
            </c:if>
          </c:forEach>
        </select>
      </div>
    </div>
    <c:remove var="countryName" />
  </div>

  <c:set value="" var="countryName" />
  <ul class="funny-list">
    <c:forEach items="${members}" var="faoInstitute" varStatus="status">
      <c:if test="${countryName ne faoInstitute.country.getName(pageContext.response.locale)}">
        <c:set var="countryName" value="${faoInstitute.country.getName(pageContext.response.locale)}" />
        <li class="hoofdleter" id="nav-${faoInstitute.country.code3}"><c:out value="${countryName}" /> <small><a
            href="#"
          ><spring:message code="jump-to-top" /></a></small></li>
      </c:if>
      <li class="clearfix ${status.count % 2 == 0 ? 'even' : 'odd'}"><a class="show pull-left"
        href="<c:url value="/wiews/${faoInstitute.code}" />"
      ><b><c:out value="${faoInstitute.code}" /></b> <c:out value="${faoInstitute.fullName}" /></a>
        <div class="pull-right">
          <spring:message code="faoInstitute.accessionCount" arguments="${faoInstitute.accessionCount}" />
        </div></li>
    </c:forEach>
  </ul>


  <c:if test="${statisticsPDCI.count gt 0}">
    <h3>
      <spring:message code="accession.pdci" />
    </h3>
    <p><spring:message code="accession.pdci.stats-text" arguments="${statisticsPDCI.elStats}" /></p>

    <div class="chart chart-histogram">
      <div id="chartPDCI" style="height: 200px"></div>
    </div>

    <p>
      <a href="<c:url value="/content/passport-data-completeness-index" />"><spring:message
          code="accession.pdci.about-link"
        /></a>
    </p>
   <%--  <div class="row">
      <c:forEach items="${statisticsPDCI.histogram}" var="item" varStatus="index">
        <div class="col-xs-1"><c:out value="${index.count}=${item}" /></div>
      </c:forEach>
    </div> --%>
  </c:if>

  <content tag="javascript"> <script type="text/javascript">
	jQuery(document).ready(function() {	
		$("#countryNavigation").on("change", function() {
			window.location.hash="nav-" + this.value;
		});
<c:if test="${statisticsPDCI.count gt 0}">
		GenesysChart.histogram("#chartPDCI", <c:out value="${statisticsPDCI.histogramJson}" escapeXml="false" /> );
</c:if>
	});
	</script> </content>
</body>
</html>