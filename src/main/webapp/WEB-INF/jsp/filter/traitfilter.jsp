<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
<title><spring:message code="filters.page.title" /></title>
</head>
<body>
	<h1><spring:message code="filters.view" /></h1>

<div id="allfilters">

<c:forEach items="${categories}" var="category">
<c:if test="${descriptors[category].size() gt 0}">
	<h2><c:out value="${crop.getName(pageContext.response.locale)}:" /> <c:out value="${category.getName(pageContext.response.locale)}" /></h2>
	<c:forEach items="${descriptors[category]}" var="descriptor">
	<c:if test="${methods[descriptor.id].size() gt 0}">
		<div class="row filter-block">
			<div class="col-lg-3">
			<div dir="ltr"><c:out value="${descriptor.title}" /></div>
			</div>
			<div class="col-lg-9">
				<c:forEach items="${methods[descriptor.id]}" var="method">
					<div><label><input type="checkbox" style="margin-right: 1em;" name="methods" value="gm:${method.id}" /><span dir="ltr"><c:out value="${method.method}" /></span></label></div>
				</c:forEach>
			</div>
		</div>
	</c:if>
	</c:forEach>
</c:if>
</c:forEach>

<div class="clearfix">
	<a id="filtersHref" href=""><button class="btn btn-green pull-left">View!</button></a>
</div>

</div>

</body>
</html>