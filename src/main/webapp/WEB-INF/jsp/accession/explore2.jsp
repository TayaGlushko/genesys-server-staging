<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp" %>
<spring:message code='filter.internal.message.between' var="between" arguments=" "/>
<spring:message code='filter.internal.message.and' var="varEnd" arguments=" "/>
<spring:message code='filter.internal.message.more' var="moreThan" arguments=" "/>
<spring:message code='filter.internal.message.less' var="lessThan" arguments=" "/>
<spring:message code='filter.internal.message.like' var="like" arguments=" "/>
<spring:message code='boolean.true' var="varTrue"/>
<spring:message code='boolean.false' var="varFalse"/>
<spring:message code='boolean.null' var="varNull"/>

<html>
<head>
    <title><spring:message code="accession.page.data.title"/></title>
</head>

<body>
<cms:informative-h1 title="accession.page.data.title" fancy="${pagedData.number eq 0}"
                    info="accession.page.data.intro"/>
<!-- Begin page content -->
<div id="content" class="entry-page explore-page">
    <div class="container-fluid">
        <div id="dialog" class="row"></div>
        <div class="" typeof="">
            <!--Filters-->
				<filters:group>
					<filters:panel id="crops" title="filter.crops">
						<filters:filter availableFilters="${availableFilters}" filterMap="${filters}" filterKey="crops" appliedFilters="${appliedFilters}" type="select" cropList="${crops}" currentCrop="${crop}" />
					</filters:panel>

                    <filters:panel id="lists" title="filter.lists">
                        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}" filterKey="lists" appliedFilters="${appliedFilters}" type="like" />
                    </filters:panel>

                    <filters:panel id="sampstat" title="filter.sampStat">
                        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}" filterKey="sampStat" appliedFilters="${appliedFilters}" type="option" />
                    </filters:panel>

					<filters:panel id="genus" title="filter.taxonomy.genus">
                        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}" filterKey="taxonomy.genus" appliedFilters="${appliedFilters}" type="autocomplete" />
					</filters:panel>

					<filters:panel id="species" title="filter.taxonomy.species">
                        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}" filterKey="taxonomy.species" appliedFilters="${appliedFilters}" type="autocomplete" />
					</filters:panel>

                            <filters:panel id="subtaxa" title="filter.taxonomy.subtaxa">
                        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}" filterKey="taxonomy.subtaxa" appliedFilters="${appliedFilters}" type="autocomplete" />
					</filters:panel>

					<filters:panel id="sciname" title="filter.taxonomy.sciName">
                        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}" filterKey="taxonomy.sciName" appliedFilters="${appliedFilters}" type="autocomplete" />
					</filters:panel>

					<filters:panel id="origcty" title="filter.orgCty.iso3">
                        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}" filterKey="orgCty.iso3" appliedFilters="${appliedFilters}" type="autocomplete" />
					</filters:panel>

					<filters:panel id="instcountry" title="filter.institute.country.iso3">
						<filters:filter availableFilters="${availableFilters}" filterMap="${filters}" filterKey="institute.country.iso3" appliedFilters="${appliedFilters}" type="autocomplete" />
					</filters:panel>

                    <filters:panel id="geolatitude" title="filter.geo.latitude">
                        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}" filterKey="geo.latitude" appliedFilters="${appliedFilters}" type="range" />
                    </filters:panel>

                    <filters:panel id="institutenetworks" title="filter.institute.networks">
                        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}" filterKey="institute.networks" appliedFilters="${appliedFilters}" type="like" />
                    </filters:panel>

                    <filters:panel id="institutecode" title="filter.institute.code">
                        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}" filterKey="institute.code" appliedFilters="${appliedFilters}" type="autocomplete" />
                    </filters:panel>

                    <filters:panel id="acceNumb" title="filter.acceNumb">
                        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}" filterKey="acceNumb" appliedFilters="${appliedFilters}" type="like" />
                    </filters:panel>

                    <filters:panel id="seqNo" title="filter.seqNo">
                        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}" filterKey="seqNo" appliedFilters="${appliedFilters}" type="range" />
                    </filters:panel>

                    <filters:panel id="alias" title="filter.alias">
                        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}" filterKey="alias" appliedFilters="${appliedFilters}" type="like" />
                    </filters:panel>

                    <filters:panel id="sgsv" title="filter.sgsv">
                        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}" filterKey="sgsv" appliedFilters="${appliedFilters}" type="boolean" />
                    </filters:panel>

					<filters:panel id="mlsStatus" title="filter.mlsStatus">
						<filters:filter availableFilters="${availableFilters}" filterMap="${filters}" filterKey="mlsStatus" appliedFilters="${appliedFilters}" type="boolean" />
					</filters:panel>

                    <filters:panel id="art15" title="filter.art15">
                        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}" filterKey="art15" appliedFilters="${appliedFilters}" type="boolean" />
                    </filters:panel>

					<filters:panel id="available" title="filter.available">
						<filters:filter availableFilters="${availableFilters}" filterMap="${filters}" filterKey="available" appliedFilters="${appliedFilters}" type="boolean" />
					</filters:panel>

					<filters:panel id="historic" title="filter.historic">
						<filters:filter availableFilters="${availableFilters}" filterMap="${filters}" filterKey="historic" appliedFilters="${appliedFilters}" type="boolean" />
					</filters:panel>
					
					<filters:panel id="collMissId" title="filter.coll.collMissId">
						<filters:filter availableFilters="${availableFilters}" filterMap="${filters}" filterKey="coll.collMissId" appliedFilters="${appliedFilters}" type="like" />
					</filters:panel>

                    <filters:panel id="storage" title="filter.storage">
                        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}" filterKey="storage" appliedFilters="${appliedFilters}" type="option" />
                    </filters:panel>
				</filters:group>
				<!--List-->
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 main-col-header">
                <div class="nav-header clearfix">
                    <!-- Links -->
                    <div class="pull-right list-view-controls">
                        <div class="btn-group" id="shareLink">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false" id="menuShareLink">
                                <span class="glyphicon glyphicon-share"></span><span><spring:message code="share.link"/></span>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li class="padding10">
                                    <p><spring:message code="share.link.text"/></p>
                                    <input id="shortLink" type="text"
                                           placeholder="<spring:message code="share.link.placeholder" />" value=""/>
                                </li>
                            </ul>
                        </div>


                        <div class="btn-group" id="downloadLink" <c:if test="${pagedData.totalElements ge 200000}">style="display: none"</c:if>>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                <span class="glyphicon glyphicon-download"></span><span><spring:message
                                    code="download"/></span> <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <security:authorize access="isAuthenticated()">
                                    <li>
                                        <form style="display: inline-block" method="post"
                                              action="<c:url value="/download/explore/download/mcpd" />">
                                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                            <input type="hidden" name="filter" value="<c:out value="${jsonFilter}" />"/>
                                            <button class="btn btn-inline" type="submit"><span><spring:message
                                                    code="filter.download-mcpd"/></span></button>
                                        </form>
                                    </li>
                                </security:authorize>
                                <li>
                                    <form style="display: inline-block" method="post"
                                          action="<c:url value="/download/explore/dwca" />">
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                        <input type="hidden" name="filter" value="<c:out value="${jsonFilter}" />"/>
                                        <button class="btn btn-inline" type="submit"><span><spring:message
                                                code="filter.download-dwca"/></span></button>
                                    </form>
                                </li>
                            </ul>
                        </div>

                        <a class="btn btn-default" id="overviewLink"
                           href="<c:url value="/explore/overview"><c:param name="filter">${jsonFilter}</c:param></c:url>"><span
                                class="glyphicon glyphicon-eye-open"></span><span><spring:message
                                code="data-overview.short"/></span></a>
                        <a class="btn btn-default" id="mapLink"
                           href="<c:url value="/explore/map"><c:param name="filter">${jsonFilter}</c:param></c:url>"><span
                                class="glyphicon glyphicon-globe"></span><span><spring:message code="maps.view-map"/></span></a>
                    </div>
                    <!--Pagination-->
                    <filters:pagination pagedData="${pagedData}" jsonFilter="${jsonFilter}"/>
                </div>
            </div>
            <div id="explore-table" class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                <table class="accessions">
                    <thead>
                    <tr>
                        <td class="idx-col"></td>
                        <td/>
                        <td><p><spring:message code="accession.accessionName" /></p></td>
                        <c:forEach items="${selectedColumns}" var="column">
                            <c:choose>
                                <c:when test="${column eq 'scientificName'}">
                                    <td><p><spring:message code="accession.${column}"/></p></td>
                                </c:when>
                                <c:when test="${column eq 'taxonomy.genus'}">
                                    <td class="notimportant hidden-xs"><p><spring:message code="accession.${column}"/></p></td>
                                </c:when>
                                <c:otherwise>
                                    <td class="notimportant hidden-sm hidden-xs"><p><spring:message code="accession.${column}"/></p></td>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${pagedData.content}" var="accession" varStatus="status">
                        <tr class="acn ${status.count % 2 == 0 ? 'even' : 'odd'} ${accession.historic ? 'historic-record' : ''}">
                            <td class="idx-col"><p>${status.count + pagedData.size * pagedData.number}</p></td>
                            <td class="sel" x-aid="${accession.id}" title="<spring:message code="selection.checkbox" />"></td>
                            <td><p><a href="<c:url value="/acn/id/${accession.id}" />"><b><c:out value="${accession.acceNumb}"/></b></a></p></td>
                            <c:forEach items="${selectedColumns}" var="col">
                                <c:choose>
                                    <c:when test="${col eq 'taxonomy.sciName'}">
                                        <td class="hidden-sm hidden-xs"><p><span dir="ltr" class="sci-name"><c:out value="${accession.taxonomy.sciName}"/></span></p></td>
                                    </c:when>
                                    <c:when test="${col eq 'orgCty'}">
                                        <td class="notimportant hidden-sm hidden-xs"><p><c:out value="${jspHelper.getCountry(accession.orgCty.iso3).getName(pageContext.response.locale)}"/></p></td>
                                    </c:when>
                                    <c:when test="${col eq 'sampStat'}">
                                        <td class="notimportant hidden-sm hidden-xs"><p><spring:message code="accession.sampleStatus.${accession.sampStat}"/></p></td>
                                    </c:when>
                                    <c:when test="${col eq 'institute.code'}">
                                        <td class="notimportant hidden-sm hidden-xs"><p><a href="<c:url value="/wiews/${accession.institute.code}" />"><c:out
                                                value="${accession.institute.code}"/></a></p></td>
                                    </c:when>
                                    <c:when test="${col eq 'taxonomy.genus'}">
                                        <td class="notimportant hidden-xs"><p><span dir="ltr" class="sci-name"><c:out value="${accession.taxonomy.genus}"/></span></p></td>
                                    </c:when>
                                    <c:when test="${col eq 'taxonomy.species'}">
                                        <td class="notimportant hidden-sm hidden-xs"><p><span dir="ltr" class="sci-name"><c:out value="${accession.taxonomy.species}"/></span></p></td>
                                    </c:when>
                                    <c:when test="${col eq 'taxonomy.subtaxa'}">
                                        <td class="notimportant hidden-sm hidden-xs"><p><span dir="ltr" class="sci-name"><c:out value="${accession.taxonomy.subtaxa}"/></span></p></td>
                                    </c:when>
                                    <c:when test="${col eq 'crops'}">
                                        <c:if test="${fn:length(accession.crops) == 0}">
                                            <td class="notimportant hidden-sm hidden-xs"></td>
                                        </c:if>
                                        <c:forEach items="${jspHelper.getCrops(accession.crops)}" var="crop">
                                            <td class="notimportant hidden-sm hidden-xs"><p><a href="<c:url value="/c/${crop.shortName}" />"><c:out
                                                    value="${crop.getName(pageContext.response.locale)}"
                                                    /></a></p></td>
                                        </c:forEach>
                                    </c:when>
                                    <c:otherwise>
                                        <c:set var="elVal" value="${accession}" />
                                        <c:forTokens items="${col}" delims="." var="item">
                                            <c:set var="elVal" value="${elVal[item]}" />
                                        </c:forTokens>
                                        <td class="notimportant hidden-sm hidden-xs"><p><c:out value="${elVal}"/></p></td>
                                        <c:remove var="elVal"/>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"></div>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 main-col-header">
                <div class="nav-header clearfix">
                    <!--Pagination-->
                    <filters:pagination pagedData="${pagedData}" jsonFilter="${jsonFilter}"/>
                    <nav class="text-center pull-right">
                        <ul class="pagination">
                            <li>
                                <span><spring:message code="paged.resultsPerPage" /></span>
                            </li>
                            <li>
                                <span>
                                    <select class="form-control results-per-page">
                                        <option value="10" ${pagedData.size == 10 ? "selected" : ""}>10</option>
                                        <option value="20" ${pagedData.size == 20 ? "selected" : ""}>20</option>
                                        <option value="40" ${pagedData.size == 40 ? "selected" : ""}>40</option>
                                        <option value="50" ${pagedData.size == 50 ? "selected" : ""}>50</option>
                                        <option value="75" ${pagedData.size == 75 ? "selected" : ""}>75</option>
                                        <option value="100" ${pagedData.size == 100 ? "selected" : ""}>100</option>
                                    </select>
                                </span>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <!--End of List-->
        </div>
    </div>
</div>

<content tag="javascript">
    <script type="text/javascript">
        var jsonData = ${jsonFilter};

        var page = ${pagedData.number};

        var results = ${pagedData.size};

        var cropNames = [];

        $(document).ready(function () {

            var i18nFilterMessage = {
                between: 'Between  ',
                varEnd: '  and  ',
                moreThan: 'More than  ',
                lessThan: 'Less than  ',
                like: 'Like  ',
                varTrue: 'Yes',
                varFalse: 'No',
                varNull: 'Unknown'
            };

            cropNames = function(){
                var data = null;
                $.ajax({
                    url: "/explore/getCrops",
                    method: 'get',
                    async: false
                }).done(function (response) {
                    data = response;
                });
                return data;
            }();

            $("body").on("keypress", ".string-type", function (e) {
                if (e.keyCode == 13) {
                    var btn = $(this).parent().find("button");
                    console.log(btn);
                    var selectedValue = $(this).parent().parent().find(".like-switcher option:selected").val();

                    if (selectedValue == "like") {
                        GenesysFilter.filterLike(btn, jsonData, i18nFilterMessage);
                    } else {
                        GenesysFilter.filterAutocomplete(btn, jsonData);
                    }
                }
            });

            $("body").on("click", ".filter-list", function () {
                if (jsonData[$(this).attr("i-key")] === undefined) {
                    enableFilter(this);
                }
                var text = $(this).parent().text();
                text = text.substring(0, text.indexOf('('));
                GenesysFilter.filterList($(this), jsonData, text);
            });

            $("body").on("click", ".filter-auto", function () {
                if (jsonData[$(this).attr("i-key")] === undefined) {
                    enableFilter(this);
                }

                var selectedValue = $(this).parent().parent().parent().find(".like-switcher option:selected").val();
                if (selectedValue == "like") {
                    GenesysFilter.filterLike($(this), jsonData, i18nFilterMessage);
                } else {
                    GenesysFilter.filterAutocomplete($(this), jsonData);
                }
            });

            $("body").on("click", ".filter-range", function () {
                if (jsonData[$(this).attr("i-key")] === undefined) {
                    enableFilter(this);
                }
                GenesysFilter.filterRange($(this), jsonData, i18nFilterMessage);
            });

            $("body").on("click", ".filter-bool", function () {
                if (jsonData[$(this).attr("i-key")] === undefined) {
                    enableFilter(this);
                }
                GenesysFilter.filterBoolean($(this), jsonData, i18nFilterMessage);
            });

            $('body').on('keyup keypress click blur change', '.filter-crop', function () {
                GenesysFilter.filterCrop($(this), jsonData);
            });

            $("body").on("click", ".applyBtn", function () {
                applyFilters();
                applySuggestions();
            });

            function applyFilters (path) {
                cleanJsonData();
                var filter = JSON.stringify(jsonData);
                var requestUrl = '//' + location.host + location.pathname + "/json" +
                        (path !== undefined ? path : "?page=1&" + "filter=" + encodeURIComponent(filter));
                requestUrl += "&results=" + results;
                var displayUrl = requestUrl.replace('/json', '');

                window.history.pushState(requestUrl, '', displayUrl);
                $.ajax({
                    url: requestUrl,
                    method: 'get',
                    success: function (response) {
                        renderData(response);
                    }
                });
            }

            function applySuggestions () {
                $.ajax({
                    url: "/explore/listFilterSuggestions",
                    method: 'get',
                    data: {
                        filter: JSON.stringify(jsonData)
                    },
                    success: function (response) {
                        renderListFilterSuggestions(response);
                    }
                });

                $.ajax({
                    url: "/explore/booleanSuggestions",
                    method: 'get',
                    data: {
                        filter: JSON.stringify(jsonData)
                    },
                    success: function (response) {
                        renderBooleanSuggestions(response);
                    }
                });

                $.ajax({
                    url: "/explore/cropSuggestions",
                    method: 'get',
                    data: {
                        filter: JSON.stringify(jsonData)
                    },
                    success: function (response) {
                        renderCropSuggestions(response);
                    }
                });
            }

            function renderListFilterSuggestions (filters) {
                $.each(filters, function (option) {
                    $("div." + option).find("div[class!='panel-body'][class!='filtval complex']").remove();
                    if (filters[option].options[0] == null) {
                        $("div." + option).find("button.applyBtn").before("<div>No suggestions</div>");
                    } else {
                        $.each(filters[option].options, function (index) {
                            var div = '<div>' +
                                    '<label>' +
                                    '<input class="filter-list"' +
                                    'id="' + option + this.value + '_input"' +
                                    ($("div.filtval[x-key='" + option + this.value + "']")[0] != null ? 'checked ' : '') +
                                    'norm-key="' + option + '"' +
                                    'i-key="' + option + '" type="checkbox"' +
                                    'value="' + this.value + '"/>' +
                                    messages[this.name] +
                                    ' (' + filters[option].counts[index] + ')' +
                                    '</label>' +
                                    '</div>';
                            var filtval = $("div.filtval[x-key^='" + option + "']");
                            var attach = filtval[0] != null ? filtval[0] : $("div." + option).find("button.applyBtn");
                            $(attach).before(div);
                        });
                    }
                });
            }

            function renderBooleanSuggestions(filters) {
                $.each(filters, function (filter) {
                    $.each($("input[type='checkbox'][i-key='" + filter + "']"), function (index) {
                        var val = filters[filter][index === 0 ? "T" : "F"];
                        if (val === undefined) {
                            val = 0;
                        }
                        var text = $(this).parent().html();
                        var idx = text.indexOf("(");
                        if (idx !== -1) {
                            text = text.substring(0, idx);
                        }
                        $(this).parent().html(text + " (" + val + ")");
                        if ($("div[x-key='" + filter + $(this).val() + "']")[0] != null) {
                            $($("input[type='checkbox'][i-key='" + filter + "']")[index]).prop("checked", true);
                        }
                    });
                });
            }

//            function renderCropSuggestions(filters) {
//                var cropSelect = $("select[i-key='crops']");
//                var currentCrop = $("div.filtval[i-key='crops']");
//                cropSelect.html("");
//                var option = $("<option disabled>Select crop</option>");
//                if (currentCrop[0] == null) {
//                    option.prop('selected', true);
//                }
//                cropSelect.append(option);
//                $.each(filters, function (name, value) {
//                    var key = Object.keys(this)[0];
//                    option = $('<option value="' + name + '">' +
//                            key + ' (' + value[key] + ')</option>');
//                    if (currentCrop[0] != null && name === currentCrop.html()) {
//                        option.prop('selected', true);
//                    }
//                    cropSelect.append(option);
//                });
//            }

            function renderCropSuggestions(filters) {
                $(".crops input").remove();
                $(".crops label").remove();
                $(".crops br").remove();
                $.each(filters, function (name, value) {
                    var input = $("<input/>", {
                        type: 'radio',
                        name: 'crops',
                        id: 'crops_' + name,
                        'i-key': 'crops',
                        'class': 'filter-crop',
                        value: name
                    });
                    if ($("div.filtval[x-key='crops" + name + "']")[0] != null) {
                        input.prop('checked', true);
                    }
                    var key = Object.keys(this)[0];
                    var label = $("<label/>", {
                        'for': 'crops_' + name,
                        text: key + " (" + value[key] + ")"
                    });
                    var filtval = $("div.filtval[i-key^='crops']");
                    var attach = filtval[0] != null ? filtval[0] : (".crops button.applyBtn");
                    $(attach).before(input);
                    $(attach).before(label);
                    $(attach).before($("<br/>"));
                });
            }

            $("body").on("click", ".filtval", function (event) {
                event.preventDefault();

                var key = $(this).attr("i-key");
                var normKey = GenesysFilter.normKey(key);
                var value = $(this).attr("x-key").replace(normKey, "");

                if (value == "null") value = null;
                GenesysFilterUtil.removeValue(value, key, jsonData);
                applyFilters();
                applySuggestions();

                $(this).remove();
                $('input[i-key=" + normKey + "][value=" + value + "]').prop('checked', false);
            });

            $("#menuShareLink").on("click", function () {
                if ($('#shortLink').val() === '') {
                    $.ajax({
                        type: 'POST',
                        url: '/explore/shorten-url',
                        data: {
                            'page': 1, 'filter': JSON.stringify(jsonData)
                        },
                        success: function (response) {
                            var inp = $("#shortLink");
                            inp.val(response.shortUrl);
                            inp.select();
                        }
                    });
                } else {
                    console.log('No repeat.');
                }
            });

            $("body").on("change", "#more-filters", function() {
                var key = $(this).val();
                var normKey = GenesysFilterUtil.normKey(key);
                var input = $(this).parent().find("input[type='text']");
                input.attr('id', normKey + "_input");
                if (key == "institute.country.iso3") {
                    input.attr('x-source', "/explore/ac/orgCty.iso3");
                } else {
                    input.attr('x-source', "/explore/ac/" + key);
                }

                var btn = $(this).parent().find("button.filter-auto");
                btn.attr("norm-key", normKey);
                btn.attr("i-key", key);
                jsonData[key] = [];
            });

            $.each([$(".firstPage"), $(".previousPage"), $(".nextPage"), $(".lastPage")], function () {
                $(this).find("a").on("click", function (e) {
                    e.preventDefault();
                    applyFilters($(e.target).attr("href"));
                });
            });

            $("nav.text-center").find("form").on("submit", function (e) {
                e.preventDefault();
                applyFilters("?" + $(this).serialize());
            });

            $(".results-per-page").on("change", function () {
                results = $(this).val();
                applyFilters();
            });

            $.each($('#collapseFilters').find("div.panel-collapse"), function () {
                <c:forEach items="${appliedFilters}" var="appliedFilter">
                    var collapseDiv = $(this).find($("div[id*='${appliedFilter.key}']"));
                    if (collapseDiv[0] !== undefined) {
                        $(this).collapse("show");
                    }
                </c:forEach>
            });
            $('#collapseFilters').on('hidden.bs.collapse', function () {
                $("#explore-table").addClass('fullwidth')
            }).children().on('hidden.bs.collapse', function() {
                return false;
            });
            $('#collapseFilters').on('show.bs.collapse', function () {
                $("#explore-table").removeClass('fullwidth');
            }).children().on('show.bs.collapse', function(child) {
                $.each($(child.target).children().find("*[i-key]"), function () {
                    enableFilter(this);
                });
                return true;
            });

            $(window).on("popstate", function(e) {
                if (e.originalEvent.state !== null) {
                    jsonData = JSON.parse(decodeURIComponent((new RegExp('[?|&]filter=' + '([^&;]+?)(&|#|;|$)').exec(location.search)
                            || [null, ''])[1].replace(/\+/g, '%20'))) || [];
                    page = decodeURIComponent((new RegExp('[?|&]page=' + '([^&;]+?)(&|#|;|$)').exec(location.search)
                            || [null, ''])[1].replace(/\+/g, '%20')) || 1;
                    results = decodeURIComponent((new RegExp('[?|&]results=' + '([^&;]+?)(&|#|;|$)').exec(location.search)
                            || [null, ''])[1].replace(/\+/g, '%20')) || 50;
                    var filter = JSON.stringify(jsonData);
                    var requestUrl = '//' + location.host + location.pathname + "/json?page=" + page + "&filter=" + filter + "&results=" + results;
                    $.ajax({
                        url: requestUrl,
                        method: 'get',
                        success: function (response) {
                            renderData(response);
                        }
                    });
                    applySuggestions();
                } else {
                    location.reload();
                }
            });

            GenesysFilterUtil.registerAutocomplete(".filters", jsonData);

            $.ajax({
                url: "/explore/booleanSuggestions",
                method: 'get',
                data: {
                    filter: JSON.stringify(jsonData)
                },
                success: function (response) {
                    renderBooleanSuggestions(response);
                }
            });
        });

        function enableFilter (btn) {
            var key = $(btn).attr("i-key");
            var normKey = GenesysFilter.normKey(key);
            jsonData[key] = [];
        }

        function cleanJsonData() {
            $.each(jsonData, function (key) {
                if ($(this).length == 0) {
                    delete jsonData[key];
                }
            });
        }

        function renderData(pagedData) {
            page = pagedData.number;
            renderPagination(pagedData);
            renderLinks(pagedData);
            renderTableBody(pagedData);
        }

        function renderPagination (pagedData) {
            var re = /\B(?=(\d{3})+(?!\d))/g;
            var totalElements = pagedData.totalElements.toString().replace(re, ",");
            var totalPages = pagedData.totalPages.toString().replace(re, ",");
            $('nav').find(".results").html(messages["accessions.number"].replace("{0}", totalElements));
            $('.pagination').find(".totalPages").html(messages["paged.ofPages"].replace("{0}", totalPages));
            setPageUrl($(".firstPage"), 1);
            setPageUrl($(".previousPage"), page == 0 ? 1 : page);
            setPageUrl($(".nextPage"), page + 2);
            setPageUrl($(".lastPage"), pagedData.totalPages);
            $(".pagination-input").find("input").val(page + 1);
            $("input[name='filter']").val(JSON.stringify(jsonData));
        }

        function setPageUrl (element, page) {
            $(element).find("a").attr("href", "?page=" + page + "&filter=" + encodeURI(JSON.stringify(jsonData)));
        }

        function renderLinks (pagedData) {
            var downloadLink = $("#downloadLink");
            if (pagedData.totalElements <= 200000) {
                downloadLink.show();
                $.each($("#downloadLink").find("input[name='filter']"), function () {
                    $(this).val(JSON.stringify(jsonData));
                });
            } else {
                downloadLink.hide();
            }
            var re = /filter=(.*)/;
            $("#overviewLink").attr("href", $("#overviewLink").attr("href").replace(re, "filter=" + encodeURIComponent(JSON.stringify(jsonData))));
            $("#mapLink").attr("href", $("#mapLink").attr("href").replace(re, "filter=" + encodeURIComponent(JSON.stringify(jsonData))));
        }

        function renderTableBody(pagedData) {
            $("table").find("tbody").remove();
            var tbody = $("<tbody/>");
            for (var i = 0; i < pagedData.numberOfElements; i++) {
                var row = $("<tr/>", {
                    'class': "acn " + (i % 2 == 0 ? 'odd' : 'even') + (pagedData.content[i].historic ? ' historic-record' : '')
                });
                $("<p/>", {
                    text: (i + 1) + pagedData.size * pagedData.number
                }).appendTo($("<td/>", {
                    'class': "idx-col"
                }).appendTo(row));
                $("<td/>", {
                    'class': "sel",
                    'x-aid': pagedData.content[i].id,
                    'title': "<spring:message code="selection.checkbox" />"
                }).appendTo(row);
                $("<b/>", {
                    text: pagedData.content[i].acceNumb
                }).appendTo(
                $("<a/>", {
                    href: '<c:url value="/acn/id/" />' + pagedData.content[i].id
                }).appendTo($("<p/>").appendTo($("<td/>").appendTo(row))));

                <c:forEach items="${selectedColumns}" var="col">
                    <c:choose>
                        <c:when test="${col eq 'taxonomy.sciName'}">
                            $("<span/>", {
                                dir: 'ltr',
                                'class': 'sci-name',
                                text: pagedData.content[i].taxonomy.sciName
                            }).appendTo($("<p/>").appendTo($("<td class='hidden-sm hidden-xs'></td>").appendTo(row)));
                        </c:when>
                        <c:when test="${col eq 'orgCty'}">
                            $("<p/>", {
                                text: messages['geo.iso3166-3.' + pagedData.content[i].orgCty.iso3]
                            }).appendTo($("<td/>", {
                                'class': "notimportant hidden-sm hidden-xs"
                            }).appendTo(row));
                        </c:when>
                        <c:when test="${col eq 'sampStat'}">
                            $("<p/>", {
                                text: messages["accession.sampleStatus." + pagedData.content[i].sampStat]
                            }).appendTo($("<td/>", {
                                'class': "notimportant hidden-sm hidden-xs"
                            }).appendTo(row));
                        </c:when>
                        <c:when test="${col eq 'institute.code'}">
                            $("<a/>", {
                                href: '<c:url value="/wiews/" />' + pagedData.content[i].institute.code,
                                text: pagedData.content[i].institute.code
                            }).appendTo($("<p/>").appendTo($("<td/>", {
                                'class': "notimportant hidden-sm hidden-xs"
                            }).appendTo(row)));
                        </c:when>
                        <c:when test="${col eq 'taxonomy.genus'}">
                            $("<span/>", {
                                dir: "ltr",
                                'class': "sci-name",
                                text: pagedData.content[i].taxonomy.genus
                            }).appendTo($("<p/>").appendTo($("<td/>", {
                                'class': "notimportant hidden-xs"
                            }).appendTo(row)));
                        </c:when>
                        <c:when test="${col eq 'taxonomy.species'}">
                            $("<span/>", {
                                dir: "ltr",
                                'class': "sci-name",
                                text: pagedData.content[i].taxonomy.species
                            }).appendTo($("<p/>").appendTo($("<td/>", {
                                'class': "notimportant hidden-sm hidden-xs"
                            }).appendTo(row)));
                        </c:when>
                        <c:when test="${col eq 'taxonomy.subtaxa'}">
                            $("<span/>", {
                                dir: "ltr",
                                'class': "sci-name",
                                text: pagedData.content[i].taxonomy.subtaxa
                            }).appendTo($("<p/>").appendTo($("<td/>", {
                                'class': "notimportant hidden-sm hidden-xs"
                            }).appendTo(row)));
                        </c:when>
                        <c:when test="${col eq 'crops'}">
                            var cropNamesTd = $("<td/>", {
                                'class': "notimportant hidden-sm hidden-xs"
                            });
                            if (pagedData.content[i].crops != null) {
                                $.each(pagedData.content[i].crops, function (index, shortName) {
                                    $("<a/>", {
                                        href: '<c:url value="/c/" />' + shortName,
                                        text: cropNames[shortName]
                                    }).appendTo(cropNamesTd);
                                });
                            }
                            cropNamesTd.appendTo(row);
                        </c:when>
                    </c:choose>
                </c:forEach>

                row.appendTo(tbody);
            }

            tbody.appendTo($("table"));
        }
    </script>

    <script type="text/javascript" src="<c:url value="/explore/i18n.js"/>"></script>
</content>

</body>
</html>

          