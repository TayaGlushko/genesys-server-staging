
<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
    <title><spring:message code="maps.accession-map"/></title>
</head>
<body>
<cms:informative-h1 title="maps.accession-map" fancy="true" info="maps.accession-map.intro"/>

<div class="main-col-header clearfix">
    <div class="nav-header">
        <div class="pull-right">
            <a class="btn btn-default" href="<c:url value="/explore" />" id="selectArea"><spring:message
                    code="view.accessions"/></a>

            <form style="display: inline-block" method="post" action="<c:url value="/explore/kml" />">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                <input type="hidden" name="filter" value="<c:out value="${jsonFilter}" />"/>
                <button class="btn btn-default" type="submit"><spring:message code="download.kml"/></button>
            </form>
        </div>
        <div class="results">
            <a class="btn btn-default" href="javascript: window.history.go(-1);"><spring:message
                    code="navigate.back"/></a>
        </div>
    </div>
</div>

<div class="applied-filters">
    <ul class="nav nav-pills ">
        <li style="margin-left: 5px" class="active dropdown form-horizontal pull-right">

            <a class="dropdown-toggle" data-toggle="dropdown" href="#"><spring:message code="maps.baselayer.list"/></a>

            <ul class="dropdown-menu">
                <li>
<!--                     <label class="label-map-provider"><input class="map-provider" value="mapQuest" name="provider" style='margin-right: 10px;margin-left: 5px'
                                  type='radio'>MapQuest</label> -->
                    <label class="label-map-provider"><input name="provider" value="openstreetmap" class="map-provider" style='margin-right: 10px;margin-left: 5px'
                                  type='radio'>Openstreetmap</label>
                    <label class="label-map-provider"><input name="provider" value="esriGray" class="map-provider" style='margin-right: 10px;margin-left: 5px'
                                  type='radio'>ESRI Gray</label>
                    <label class="label-map-provider"><input name="provider" value="esriTopo" class="map-provider" style='margin-right: 10px;margin-left: 5px'
                                  type='radio'>ESRI Topo</label>
                </li>
            </ul>

        </li>

        <li class="active dropdown form-horizontal pull-right">

            <a id="get-filters" href="#"><spring:message code="savedmaps.list"/></a>

            <ul id="enabled-filters" class="dropdown-menu"></ul>

        </li>

        <li style="margin-right: 5px" class="active form-horizontal pull-right" data-toggle="modal"
            data-target="#modal-dialog">
            <a href="#"><spring:message code="savedmaps.save"/></a>
        </li>
    </ul>
</div>

<!-- Modal -->
<div class="modal fade" id="modal-dialog" tabindex="-1" role="dialog" aria-labelledby="modal-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <h4 class="modal-title" id="modal-label"><spring:message code="savedmaps.save"/></h4>
            </div>
            <div class="modal-body">
                <input type="text" class="form-control" placeholder="<spring:message code="filter.enter.title"/>"
                       id="filter-title">

                <div id="color">
                    <select name="colorpicker">
                        <option value="#7bd148">#7bd148</option>
                        <option value="#5484ed">#5484ed</option>
                        <option value="#a4bdfc">#a4bdfc</option>
                        <option value="#46d6db">#46d6db</option>
                        <option value="#7ae7bf">#7ae7bf</option>
                        <option value="#51b749">#51b749</option>
                        <option value="#fbd75b">#fbd75b</option>
                        <option value="#ffb878">#ffb878</option>
                        <option value="#ff887c">#ff887c</option>
                        <option value="#dc2127">#dc2127</option>
                        <option value="#dbadff">#dbadff</option>
                        <option value="#e1e1e1">#e1e1e1</option>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><spring:message
                        code="cancel"/></button>
                <button id="save-filter" type="button" class="btn btn-primary" data-dismiss="modal"><spring:message
                        code="save"/></button>
            </div>
        </div>
    </div>
</div>
<%--End modal--%>

<div class="row">
    <div class="col-sm-12">
        <div id="map" class="gis-map gis-map-square"></div>
    </div>
</div>


<content tag="javascript">
    <script type="text/javascript">
        jQuery(document).ready(function () {

          	function niceDec(d, b) {
          	  if (b==null) b=5;
          	  var x=Math.pow(10,b);
          	  return d===null ? null : Math.round(d*x)/x;
          	}
          
            var map = L.map('map').setView([20, 0], 2);
            var topPane = map._createPane('leaflet-top-pane', map.getPanes().mapPane);
            var mapProviders = {
                openstreetmap: L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
                }),
                /*  OpenMapSurfer_Grayscale: L.tileLayer('http://openmapsurfer.uni-hd.de/tiles/roadsg/x={x}&y={y}&z={z}', {
                 maxZoom: 19,
                 attribution: 'Imagery from <a target="_blank" href="http://giscience.uni-hd.de/">GIScience Research Group, University of Heidelberg</a>, Map data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
                 }),*/
                /* mapQuest: L.tileLayer('https://otile{s}-s.mqcdn.com/tiles/1.0.0/sat/{z}/{x}/{y}.png', {
                    attribution: "MapQuest",
                    styleId: 22677,
                    subdomains: ['1', '2', '3', '4'],
                    opacity: 0.6
                }),*/
                esriGray: L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}', {
                	attribution: 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ',
                	maxZoom: 16
                }),
                esriTopo: L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}', {
                	attribution: 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ, TomTom, Intermap, iPC, USGS, FAO, NPS, NRCAN, GeoBase, Kadaster NL, Ordnance Survey, Esri Japan, METI, Esri China (Hong Kong), and the GIS User Community'
                })
                /*Stamen_TonerLines: L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/toner-lines/{z}/{x}/{y}.png', {
                 attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
                 subdomains: 'abcd',
                 opacity: 0.3,
                 minZoom: 0,
                 maxZoom: 20,
                 ext: 'png'
                 }),

                 Acetate_labels: L.tileLayer('http://a{s}.acetate.geoiq.com/tiles/acetate-labels/{z}/{x}/{y}.png', {
                 attribution: '&copy;2012 Esri & Stamen, Data from OSM and Natural Earth',
                 subdomains: '0123',
                 opacity: 0.6,
                 minZoom: 2,
                 maxZoom: 18
                 })*/
            };

            var cookieUtils = {
                getCookie: function (name) {
                    var matches = document.cookie.match(new RegExp(
                            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
                    ));
                    return matches ? decodeURIComponent(matches[1]) : undefined;
                },
                setCookie: function (name, value, options) {
                    options = options || {};

                    var expires = options.expires;

                    if (typeof expires == "number" && expires) {
                        var d = new Date();
                        d.setTime(d.getTime() + expires * 1000);
                        expires = options.expires = d;
                    }
                    if (expires && expires.toUTCString) {
                        options.expires = expires.toUTCString();
                    }

                    value = encodeURIComponent(value);

                    var updatedCookie = name + "=" + value;

                    for (var propName in options) {
                        updatedCookie += "; " + propName;
                        var propValue = options[propName];
                        if (propValue !== true) {
                            updatedCookie += "=" + propValue;
                        }
                    }

                    document.cookie = updatedCookie;
                },
                deleteCookie: function (name) {
                    this.setCookie(name, "", {
                        expires: -1
                    })
                }
            };
            
            $(document).on('cookieUpdate', function () {
                for (var key in mapProviders) {
                    map.removeLayer(mapProviders[key]);
                }
                if (cookieUtils.getCookie('mapProviders')) {
                    mapProviders[cookieUtils.getCookie('mapProviders')].addTo(map);
                    topPane.appendChild(mapProviders[cookieUtils.getCookie('mapProviders')].getContainer());
                    mapProviders[cookieUtils.getCookie('mapProviders')].setZIndex(0);
                } else {
                    mapProviders.esriGray.addTo(map);
                    topPane.appendChild(mapProviders[cookieUtils.getCookie('mapProviders')].getContainer());
                    mapProviders.esriGray.setZIndex(0);
                }
            });

            $('.map-provider').click(function (e) {
                var providerName = $(this).val();
                if ($(this).is(":checked")) {
                    cookieUtils.setCookie('mapProviders', providerName, 0);
                    $(document).trigger('cookieUpdate');
                } else {
                    providerName = '';
                    cookieUtils.setCookie('mapProviders', providerName, 0);
                    $(document).trigger('cookieUpdate');
                }
            });
            
            $('#color select').simplecolorpicker();
            
            var selectedBaseMap = cookieUtils.getCookie('mapProviders') || 'esriGray';
            if (mapProviders[selectedBaseMap] === undefined) {
				selectedBaseMap = 'esriGray';
            }
            [].forEach.call($('.label-map-provider input:radio'), function(radio){
                 $(radio).prop('checked', $(radio).val() === selectedBaseMap);
            });

            mapProviders[selectedBaseMap].addTo(map);
            topPane.appendChild(mapProviders[selectedBaseMap].getContainer());
            mapProviders[selectedBaseMap].setZIndex(0);
            
            $("#save-filter").on("click", function (event) {
                event.preventDefault();

                var title = $("#filter-title").val();
                var filter =${jsonFilter};

                $.ajax({
                    url: "/savedmaps/save",
                    type: "post",
                    dataType: "json",
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify({
                        title: title,
                        filter: JSON.stringify(filter),
                        color: $('#color select').val()
                    }),
                    success: function (data) {
                    },
                    error: function (error) {
                        console.log(error)
                    }
                });
            });

            $("#get-filters").on("click", function (event) {
                event.preventDefault();
                $.ajax({
                    url: "/savedmaps/get",
                    type: "get",
                    dataType: "json",
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        $.each(data, function (idx, filter) {

                            var li = "<li><label  class='saved-filter' x-fil='" + filter.filter + "' x-color='" + filter.color + "'><input style='margin-right: 10px;margin-left: 5px' type='checkbox'>" + filter.title + "</label></li>";

                            if ($("#enabled-filters").is(":visible")) {
                                $("#enabled-filters").append(li);
                            } else {
                                $("#enabled-filters").empty();
                            }
                        });
                    },
                    error: function (error) {
                        console.log(error)
                    }
                });
                $("#enabled-filters").toggle();
            });

            L.tileLayer("{s}/explore/tile/{z}/{x}/{y}?filter=" + '${jsonFilter}', {
                attribution: "Accession localities from <a href='${props.baseUrl}'>Genesys PGR</a>",
                styleId: 22677,
                subdomains: [${props.tileserverCdn}]
            }).addTo(map);

            $("#selectArea").hide();
            var filterJson =${jsonFilter};
            var filterJsonObj = {};
            if (typeof filterJson !== 'undefined') {
                filterJsonObj = JSON.parse(JSON.stringify(filterJson));

            }


            var layers = {};
            $("body").on("click", ".saved-filter", function (e) {
                var title = $(this).text();
                var filter = $(this).attr("x-fil");
                var tilesColor = $(this).attr("x-color").substring(1);
                var savedFilterObj = JSON.parse(filter);

                if ($(this).find("input:checkbox").is(":checked")) {
                    if (layers[title] == null) {
                        layers[title] = L.tileLayer("{s}/explore/tile/{z}/{x}/{y}?filter=" + filter + "&color=" + tilesColor, {
                            attribution: "<a href='${props.baseUrl}'>Genesys</a>",
                            styleId: 22677,
                            subdomains: [${props.tileserverCdn}]
                        }).addTo(map);
                    }
                } else {
                    if (layers[title] != null) {
                        map.removeLayer(layers[title]);
                        layers[title] = null;
                    }
                }

            });

            var locationFilter = new L.LocationFilter({
                adjustButton: false,
                bounds: map.getBounds().pad(-0.1)
            }).addTo(map);
            locationFilter.on("change", function (e) {
                // Do something when the bounds change.
                // Bounds are available in `e.bounds`.
                var bounds = locationFilter.getBounds();
                filterJson['geo.latitude'] = [{range: [niceDec(nicebounds.getSouth()), niceDec(bounds.getNorth())]}];
                filterJson['geo.longitude'] = [{range: [niceDec(bounds.getWest()), niceDec(bounds.getEast())]}];
            });

            map.on("viewreset", function () {
                if (locationFilter.isEnabled())
                    return;
                var mapBounds = map.getBounds().pad(-0.1);
                locationFilter.setBounds(mapBounds);
            });
            locationFilter.on("enabled", function () {
                // Do something when enabled.
                var bounds = locationFilter.getBounds();
                filterJson['geo.latitude'] = [{range: [niceDec(bounds.getSouth()), niceDec(bounds.getNorth())]}];
                filterJson['geo.longitude'] = [{range: [niceDec(bounds.getWest()), niceDec(bounds.getEast())]}];
                $("#selectArea").show();
            });

            locationFilter.on("disabled", function () {
                // Do something when disabled.
                $("#selectArea").hide();
            });

            $("#selectArea").on("click", function (e) {
                this.href = this.href + '?filter=' + JSON.stringify(filterJson);
            });

            var loadDetailsTimeout = null;
            var clickMarker = null;
            map.on("click", function (e) {
                if (clickMarker != null) {
                    map.removeLayer(clickMarker);
                    clickMarker = null;
                }
                if (map.getZoom() > 4) {
                    if (loadDetailsTimeout != null) {
                        clearTimeout(loadDetailsTimeout);
                    }

                    var point = this.latLngToLayerPoint(e.latlng);
                    point.x -= 5;
                    point.y += 5;
                    var sw = this.layerPointToLatLng(point);
                    point.x += 10;
                    point.y -= 10;
                    var ne = this.layerPointToLatLng(point);
                    loadDetailsTimeout = setTimeout(function () {

                        var filterBounds = filterJsonObj;
                        console.log(filterBounds);
                        filterBounds['geo.latitude'] = [{range: [sw.lat, ne.lat]}];
                        filterBounds['geo.longitude'] = [{range: [sw.lng, ne.lng]}];
                        //console.log(JSON.stringify(filterBounds));
                        $.ajax("<c:url value="/explore/geoJson"><c:param name="limit" value="11" /></c:url>&filter=" + JSON.stringify(filterBounds), {
                            type: "GET",
                            dataType: 'json',
                            success: function (respObject) {
                                if (respObject.features == null || respObject.features.length == 0)
                                    return;

                                var c = "";
                                respObject.features.forEach(function (a, b) {
                                    if (b < 10)
                                        c += "<a href='<c:url value="/acn/id/" />" + a.id + "'>" + a.properties.acceNumb + " " + a.properties.instCode + "</a><br />";
                                    else
                                        c += "...";
                                });
                                clickMarker = L.rectangle([sw, ne], {stroke: false, fill: true}).addTo(map);
                                clickMarker.bindPopup(c).openPopup();
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                console.log(textStatus);
                                console.log(errorThrown);
                            }
                        });

                    }, 200);
                }
            });
            map.on("dblclick", function (e) {
                if (loadDetailsTimeout != null) {
                    //console.log("cleared" + loadDetailsTimeout);
                    clearTimeout(loadDetailsTimeout);
                    loadDetailsTimeout = null;
                }
            });
        });
    </script>
</content>
</body>
</html>