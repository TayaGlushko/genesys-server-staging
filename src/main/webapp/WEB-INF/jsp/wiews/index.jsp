<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
    <title><spring:message code="faoInstitutes.page.list.title"/></title>
</head>
<body>
<h1>
    <spring:message code="faoInstitutes.page.list.title"/>
</h1>

<div class="main-col-header clearfix">
    <div class="nav-header">
        <div>
            <c:if test="${activeOnly eq true}">
                <a href="<c:url value="/wiews/active/map" />"><spring:message code="maps.view-map"/></a>
                <a href="<c:url value="/wiews/" />"><spring:message code="faoInstitutes.viewAll"/></a>
            </c:if>
            <c:if test="${activeOnly ne true}">
                <a href="<c:url value="/wiews/active" />"><spring:message code="faoInstitutes.viewActiveOnly"/></a>
            </c:if>
        </div>
        <local:paginate page="${pagedData}"/>
    </div>
</div>

<div class="row">
	<ul class="funny-list">
	    <c:forEach items="${pagedData.content}" var="faoInstitute" varStatus="status">
	        <li class="clearfix ${faoInstitute.current ? '' : 'not-current'}"><a
	                class="show pull-left" href="<c:url value="/wiews/${faoInstitute.code}" />"><b><c:out
	                value="${faoInstitute.code}"/></b> <c:out value="${faoInstitute.fullName}"/></a>
	
	            <div class="pull-right"><spring:message code="faoInstitute.accessionCount"
	                                                    arguments="${faoInstitute.accessionCount}"/></div>
	        </li>
	    </c:forEach>
	</ul>
</div>

</body>
</html>