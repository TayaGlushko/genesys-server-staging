<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
<title><spring:message code="metadata.page.title" /></title>
</head>
<body>
	<h1>
		<c:out value="${trait.getTitle(pageContext.response.locale)}" />
		<small><c:out value="${trait.crop.getName(pageContext.response.locale)}" /></small>
	</h1>
	<div>
		<spring:message code="filter.crop" />: <b><a href="<c:url value="/c/${trait.crop.shortName}/descriptors" />"><c:out value="${trait.crop.getName(pageContext.response.locale)}" /></a></b>
	</div>
	<div>
		<spring:message code="descriptor.category" />: <c:out value="${trait.category.getName(pageContext.response.locale)}" />
	</div>
	<h4>
		<spring:message code="ce.methods" />
	</h4>
	<table>
		<thead>
			<tr>
				<td><spring:message code="ce.trait" /></td>
				<td><spring:message code="ce.sameAs" /></td>
				<td><spring:message code="ce.method" /></td>
				<td><spring:message code="unit-of-measure" /></td>
				<td><spring:message code="method.fieldName" /></td>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${traitMethods}" var="method">
				<tr>
					<td><c:out value="${method.parameter.getTitle(pageContext.response.locale)}" /></td>
					<td>
					<c:if test="${method.parameter.rdfUri ne null}">
					   <a href="<c:url value="${method.parameter.rdfUri}" />">
					      <c:out value="${method.parameter.getRdfUriId()}" />
					   </a>
					</c:if>
					</td>
					<td><a href="<c:url value="/descriptors/${trait.id}/${method.id}" />"><c:out value="${method.getMethod(pageContext.response.locale)}" /></a></td>
					<td><c:out value="${method.unit}" /></td>
					<td><c:out value="${method.fieldName}" /></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</body>
</html>