<%@ include file="/WEB-INF/jsp/init.jsp" %>

<c:forEach items="${cropTaxonomies.content}" var="cropTaxonomy" varStatus="status">
<li class="${((cropTaxonomies.size * cropTaxonomies.number) + status.count) % 2 == 0 ? 'even' : 'odd'}"><a href="<c:url value="/acn/t/${cropTaxonomy.taxonomy.genus}/${cropTaxonomy.taxonomy.species}" />"><c:out value="${cropTaxonomy.taxonomy.taxonName}" /></a></li>
</c:forEach>
