<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
<title><spring:message code="request.page.title" /></title>
</head>
<body>
	<h1>
		<spring:message code="request.page.title" />
	</h1>

	<gui:alert type="info" display="${pagedData == null}">
		<spring:message code="selection.empty-list-warning" />
	</gui:alert>

	<c:if test="${pagedData != null}">
	
		<div class="main-col-header clearfix">
		<div class="nav-header pull-left">
			<div class="results"><spring:message code="accessions.number" arguments="${pagedData.totalElements}" /></div>
			<div class="pagination">
				<spring:message code="paged.pageOfPages" arguments="${pagedData.number+1},${pagedData.totalPages}" />
				<a href="<spring:url value=""><spring:param name="page" value="${pagedData.number eq 0 ? 1 : pagedData.number}" /><spring:param name="filter" value="${jsonFilter}" /></spring:url>"><spring:message code="pagination.previous-page" /></a>
				<a href="<spring:url value=""><spring:param name="page" value="${pagedData.number+2}" /><spring:param name="filter" value="${jsonFilter}" /></spring:url>"><spring:message code="pagination.next-page" /></a>
			</div>
		</div>
		</div>
	
	
		<gui:alert type="info">
			<spring:message code="request.total-vs-available" arguments="${totalCount},${availableCount}" />
		</gui:alert>

		<cms:blurb blurb="${blurp}" />

		<table class="accessions">
			<thead>
				<tr>
					<td class="idx-col"></td>
					<td><spring:message code="accession.availability" /></td>
					<td><spring:message code="accession.historic" /></td>
					<td><spring:message code="accession.accessionName" /></td>
					<td class="notimportant"><spring:message code="accession.taxonomy" /></td>
					<%-- <td class="notimportant"><spring:message code="accession.origin" /></td> --%>
					<td><spring:message code="accession.holdingInstitute" /></td>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${pagedData.content}" var="accession" varStatus="status">
					<tr id="a${accession.id}" class="acn targeted ${status.count % 2 == 0 ? 'even' : 'odd'} ${accession.availability!=false and accession.institute.allowMaterialRequests ? '' : 'not-available'} ${accession.historic ? 'historic-record' : ''}">
						<td class="idx-col"><c:out value="${status.count + pagedData.size * pagedData.number}" /></td>
						<td><spring:message code="accession.availability.${accession.availability}" /></td>
						<td><spring:message code="accession.historic.${accession.historic}" /></td>
						<td><a href="<c:url value="/acn/id/${accession.id}" />"><b><c:out value="${accession.accessionName}" /></b></a></td>
						<td class="notimportant"><c:out value="${accession.taxonomy.taxonName}" /></td>
						<%-- <td class="notimportant"><c:out value="${accession.countryOfOrigin.getName(pageContext.response.locale)}" /></td> --%>
						<td class="notimportant"><a href="<c:url value="/wiews/${accession.institute.code}" />"><c:out value="${accession.institute.code}" /></a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>

		<c:if test="${availableCount gt 0}">
			<form method="post" action="<c:url value="/request/start" />" class="form-horizontal">
				<div class="form-actions">
					<input class="btn btn-primary" type="submit" value="<spring:message code="request.start-request" />" />
				</div>
                <!-- CSRF protection -->
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
			</form>
		</c:if>

	</c:if>
</body>
</html>