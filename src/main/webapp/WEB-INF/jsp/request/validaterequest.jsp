<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
    <title><spring:message code="request.validate-request"/></title>
</head>
<body>
	<h1>
		<spring:message code="request.validate-request"/>
	</h1>

	<cms:blurb blurb="${blurp}" />

	<gui:alert type="warning" display="${error ne null}">
		<spring:message code="validate.no-such-key"/>
	</gui:alert>

<form role="form" class="form-vertical validate" action="<c:url value="/request/${tokenUuid}/validate" />" method="post">

   <div class="form-group">
		<label class="col-lg-3 control-label"><spring:message code="request.validation-key" /></label>
		<div class="col-lg-9">
        	<input type="text" id="key" name="key" value="<c:out value="${key}" />" class="span1 form-control" maxlength="10"/>
    	</div>
    </div>
    <div class="form-group">
	    <div class="col-lg-3">
	        <input type="submit" value="<spring:message code="request.button-validate" />" class="btn btn-primary"/>
	    </div>
	</div>
    <!-- CSRF protection -->
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
</form>


</body>
</html>