<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
<title><c:out value="${project.code} - ${project.name}" /></title>
<meta name="description" content="<c:out value="${jspHelper.htmlToText(blurp.summary)}" />" />
</head>
<body>
	<h1>
		<c:out value="${project.name}" />
		<small>
			<c:out value="${project.code}" />
		</small>
	</h1>

	<security:authorize access="hasRole('ADMINISTRATOR') or hasPermission(#project, 'ADMINISTRATION')">
		<a href="<c:url value="/acl/${project.getClass().name}/${project.id}/permissions"><c:param name="back"><c:url value="/project/${project.code}" /></c:param></c:url>" class="close">
			<spring:message code="edit-acl" />
		</a>
	</security:authorize>
	<security:authorize access="hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER') or hasPermission(#project, 'ADMINISTRATION')">
		<a href="<c:url value="/project/${project.code}/edit" />" class="close">
			<spring:message code="edit" />
		</a>
	</security:authorize>

	<!-- Project text -->
	<cms:blurb blurb="${blurp}" />

	<div class="content-section-2015">
		<h3>
			<span>
				<spring:message code="heading.general-info" />
			</span>
		</h3>
		<div class="row">
			<div class="col-md-offset-2 col-md-10">
				<ul class="funny-list statistics">
					<li class="clearfix odd">
						<span class="stats-number">
							<fmt:formatNumber value="${countByProject}" />
						</span>
						<spring:message code="faoInstitutes.stat.accessionCount" />
					</li>
				</ul>

				<c:if test="${countByProject gt 0}">
					<a class="btn btn-primary" title="<spring:message code="faoInstitute.data-title" arguments="${project.name}" />" href="<c:url value="/project/${project.code}/data" />">
						<span class="glyphicon glyphicon-list"></span>
						<spring:message code="view.accessions" />
					</a>
					<a class="btn btn-default" title="<spring:message code="faoInstitute.overview-title" arguments="${project.name}" />" href="<c:url value="/project/${project.code}/overview" />">
						<span class="glyphicon glyphicon-eye-open"></span>
						<spring:message code="data-overview.short" />
					</a>
					<div class="btn-group">
						<form class="form-horizontal" method="post" action="<c:url value="/download/project/${project.code}/download" />">
							<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<span class="glyphicon glyphicon-download"></span>
								<span>
									<spring:message code="download" />
								</span>
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<security:authorize access="isAuthenticated()">
									<li>
										<button name="mcpd" class="btn btn-inline" type="submit">
											<spring:message code="filter.download-mcpd" />
										</button>
										<button name="pdci" class="btn btn-inline" type="submit">
											<spring:message code="filter.download-pdci" />
										</button>
									</li>
								</security:authorize>
								<li>
									<button name="dwca" class="btn btn-inline" type="submit">
										<spring:message code="metadata.download-dwca" />
									</button>
								</li>
							</ul>
						</form>
					</div>
				</c:if>
			</div>
		</div>

		<c:if test="${statisticsCrop ne null and statisticsCrop.totalCount gt 0}">
			<h4>
				<span>
					<spring:message code="faoInstitute.stat-by-crop" />
				</span>
			</h4>
			<div class="row">
				<div class="col-md-offset-2 col-md-10">
					<local:term-result termResult="${statisticsCrop}" type="crop" />
				</div>
			</div>
		</c:if>

		<c:if test="${statisticsGenus ne null and statisticsGenus.totalCount gt 0}">
			<h4>
				<span>
					<spring:message code="faoInstitute.stat-by-genus" />
				</span>
			</h4>
			<div class="row">
				<div class="col-md-offset-2 col-md-10">
					<local:term-result termResult="${statisticsGenus}" type="genus" />
				</div>
			</div>
		</c:if>

		<c:if test="${statisticsTaxonomy ne null and statisticsTaxonomy.totalCount gt 0}">
			<h4>
				<span>
					<spring:message code="faoInstitute.stat-by-species" />
				</span>
			</h4>
			<div class="row">
				<div class="col-md-offset-2 col-md-10">
					<local:term-result termResult="${statisticsTaxonomy}" type="species" />
				</div>
			</div>
		</c:if>

		<c:if test="${statisticsOrigCty ne null and statisticsOrigCty.totalCount gt 0}">
			<h4>
				<span>
					<spring:message code="accession.orgCty" />
				</span>
			</h4>
			<div class="row">
				<div class="col-md-offset-2 col-md-10">
					<local:term-result termResult="${statisticsOrigCty}" type="country" />
				</div>
			</div>
		</c:if>
	</div>

	<c:if test="${fn:length(accessionLists) gt 0}">
		<div class="content-section-2015">
			<h3>
				<span>
					<spring:message code="project.accessionLists" />
				</span>
			</h3>
			<div class="row">
				<div class="col-md-offset-2 col-md-10">
					<ul>
						<c:forEach items="${accessionLists}" var="accessionList">
							<li>
								<a href="<c:url value="/explore"><c:param name="filter" value='{"lists":["${accessionList.uuid}"]}' /></c:url>">
									<c:out value="${accessionList.title}" />
								</a>
							</li>
						</c:forEach>
					</ul>
				</div>
			</div>
		</div>
	</c:if>

	<c:if test="${countByProject gt 0}">
		<div class="content-section-2015">
			<h3>
				<span>
					<spring:message code="heading.see-also" />
				</span>
			</h3>
			<div class="row">
				<div class="col-md-offset-2 col-md-10">
					<ul class="see-also">
						<li>
							<a href="<c:url value="/project/${project.code}/data/map" />">
								<spring:message code="see-also.map" />
							</a>
						</li>
						<li>
							<a href="<c:url value="/project/${project.code}/overview" />">
								<spring:message code="see-also.overview" />
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</c:if>

</body>
</html>
