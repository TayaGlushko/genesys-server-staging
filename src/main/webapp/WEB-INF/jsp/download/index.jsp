<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
<title><spring:message code="download.page.title" /></title>
</head>
<body>
	<h1><spring:message code="download.page.title" /></h1>
	<cms:blurb blurb="${info}" />

<%--
<pre>
	path=<c:out value="${path}" />
	query=<c:out value="${query}" />
</pre>
--%>
	<c:if test="${path ne null}">
		<form id="downlodform" method="post" action="${path}">
			<input type="hidden" name="${_csrf.parameterName}" value="<c:out value="${_csrf.token}" />"/>
			<c:forEach items="${query.keySet()}" var="name">
				<input type="hidden" name="${name}" value='<c:out value="${query[name]}" />' />
			</c:forEach>
			<input class="btn btn-primary" type="submit" value="<spring:message code="download.download-now" />" />
		</form>
	</c:if>
</body>
</html>