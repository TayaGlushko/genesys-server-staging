<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>

<title><c:out value="${title}" /></title>
</head>
<body>
	<div class="main-col">
		<h1>
			<spring:message code="article.edit-article" />
		</h1>

		<%@ include file="transifex-responses.jspf" %>

		<c:set var="url" value="/content/save-article/${article.lang}"/>
		<c:set var="cancel" value="${article.id ne null ? (article.targetId ne null? '/content/'.concat(article.slug).concat('/').concat(article.classPk.shortName).concat('/').concat(article.targetId).concat('/').concat(article.lang) : '/content/'.concat(article.slug)) : '/content' }"/>

		<c:if test="${article.lang eq 'en'}">
			<div class="form-group">
				<form method="post" action="<c:url value="/content/transifex"/>">
					<input type="hidden" name="slug" value="${article.slug}" />
					<input type="hidden" name="targetId" value="${article.targetId}" />
					<input type="hidden" name="classPkShortName" value="${article.classPk.shortName}" />
					
					<input type="submit" name="post" class="btn btn-default" value="<spring:message code="article.post-to-transifex" />" />
					<input type="submit" name="remove" class="btn btn-default" value="<spring:message code="article.remove-from-transifex" />" />
					<input type="submit" name="fetch-all" class="btn btn-default" value="<spring:message code="article.fetch-from-transifex" />" />
					<!-- CSRF protection -->
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
				</form>
			</div>
			<div><c:out value="${resource}" /></div>
		</c:if>

		<form dir="${article.lang=='fa' || article.lang=='ar' ? 'rtl' : 'ltr'}" role="form" id="editForm" class="" action="<c:url value="${url}" />" method="post">
			<c:choose>
				<c:when test="${article.id ne null}">
					<input type="hidden" name="id" value="${article.id}" />
				</c:when>
				<c:when test="${article.targetId ne null}">
					<input type="hidden" name="targetId" value="${article.targetId}" />
					<input type="hidden" name="classPkShortName" value="${article.classPk.shortName}" />
				</c:when>
			</c:choose>			

			<div class="form-group">
				<label for="article-slug" class="control-label">
					<spring:message code="article.slug" />
				</label>

				<div class="controls">
					<input type="text" id="article-slug" name="slug" value="<c:out value="${article.slug}" />" class="span9 form-control required" />
				</div>
			</div>
			
			<div class="form-group">
				<label for="article-title" class="control-label">
					<spring:message code="article.title" />
				</label>

				<div class="controls">
					<input type="text" id="article-title" name="title" value="<c:out value="${article.title}" />" class="span9 form-control required" />
				</div>
			</div>

			<div dir="${article.lang=='fa' || article.lang=='ar' ? 'rtl' : 'ltr'}" class="form-group">
				<label for="article-body" class="control-label">
					<spring:message code="article.body" />
				</label>

				<div class="controls">
					<textarea id="article-body" name="body" class="span9 required form-control html-editor"><c:out value="${article.body}" escapeXml="false" /></textarea>
				</div>
			</div>
			<div class="form-group">
				<label for="article-summary" class="control-label">
					<spring:message code="article.summary" />
				</label>

				<div class="controls">
					<textarea id="article-summary" name="summary" class="span9 required form-control html-editor"><c:out value="${article.summary}" escapeXml="false" /></textarea>
				</div>
			</div>

			<input type="submit" value="<spring:message code="save"/>" class="btn btn-primary" />
			<a href="<c:url value="${cancel}" />" class="btn btn-default">Cancel</a>
			<!-- CSRF protection -->
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		</form>
	</div>

	<content tag="javascript">
    <script type="text/javascript">
        jQuery(document).ready(function () {
          <local:tinyMCE selector=".html-editor" directionality="document.getElementById('editForm').dir" />
        });
    </script>
	</content>
</body>
</html>