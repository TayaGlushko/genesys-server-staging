<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
  <c:out value="${head}" escapeXml="false" />
  <title><c:out value="${title}" escapeXml="false" /></title>
</head>
<body>
<content tag="header">
  <div class="hidden-xs top-image">
    <img src="<c:url value="/html/0/images/aim/banner-${jspHelper.randomInt(1, 4)}.jpg" />" title="${title}"/>
  </div>
  <div class="top-title">
    <div class="container">
      <h1>
        <c:out value="${title}" escapeXml="false"/>
      </h1>
    </div>
  </div>
</content>

<div class="article">
  <div class="right-side main-col col-md-9">
    <div class="free-text">
      <c:out value="${bodyHeader}" escapeXml="false"/>
      <c:out value="${bodyContent}" escapeXml="false"/>
      <c:out value="${bodyFooter}" escapeXml="false"/>
    </div>
  </div>

  <div class="col-md-3 col-xs-12">
    <c:out value="${menu}" escapeXml="false"/>
  </div>
</div>

<content tag="javascript">
  <script type="text/javascript">
    $(document).ready(function () {
      $('a[href="' + window.location.pathname + '"]').parent().addClass('active-link');
    });
  </script>
</content>

</body>
</html>