<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
<title><spring:message code="http-error.404" /></title>
<style type="text/css">
  * {margin:0; padding:0; outline:none; font-family:Arial, Helvetica, sans-serif; font-size:16px;}
  body {background:#e7e5df;}      
  .container {margin:0 auto; width:1170px;}
  .header {background:#4d4c46; height: 70px;}
  .header .logo {display:block; padding:0 0 5px 0;}
  .menu {background:#2b2924; border-bottom:10px solid #fff;}
  .menu a {background:#2b2924; color:#fff; display:block; font-weight:bold; padding:10px 50px; text-decoration:none; text-transform:uppercase;}
  .content {background:url('<c:url value="/html/0/images/bg_error.jpg" />') no-repeat; min-height:500px; margin-top:20px; position:relative; width:990px;}
  .content .error-title {
    color:#88ba42;
    font-size:50px;
    font-weight:bold;
    line-height:130%;
    padding-top:50px;
    position:absolute;
    top:57px;
    left:50px;
    text-align:center;
    height:235px;
    width:235px;
  }
  .content .error-title span {display:block; font-size:90px;}
  .content .error-msg {float:right; margin:60px 40px 0 0; width:560px;}
  .content .error-msg h1 {color:#fff; font-size:48px; padding:0 0 30px 0;}
  .content .error-msg p {color:#fff; font-size:20px; padding:0 0 15px 0;}
  .content .error-msg p small {font-size:16px;}
  .content .error-msg ul {background:rgba(255, 255, 255, 0.2); padding:10px 0 10px 30px; width:300px;}
  .content .error-msg ul li {color:#fff; font-style:italic; padding:3px 0;}
</style>
</head>
<body>
    <div class="header">
      <div class="container">
        <a href="<c:url value="/" />" class="logo"><img src="<c:url value="/html/0/images/logo_genesys.png" />" alt="Genesys - Gateway to Genetic Resources" /></a>
      </div>
    </div>
    
    <div class="menu">
      <div class="container">
        <a href="<c:url value="/" />">Home</a>
      </div>
    </div>
    
    <div class="container">
      <div class="content">
        <div class="error-title"><span>404</span>error</div>
        <div class="error-msg">
          <h1>404 <spring:message code="http-error.404" /></h1>
          <p><spring:message code="http-error.404.text" htmlEscape="false" /></p>
        </div>
      </div>
    </div>
</body>
</html>