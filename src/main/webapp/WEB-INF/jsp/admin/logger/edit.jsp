<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags" %>

<html>
<head>
<title><spring:message code="logger.edit.page" /></title>
</head>

<body>

	<form method="post" class="form-horizontal"
		action="<c:url value="/admin/logger/changeLoger"/>">
		<!-- CSRF protection -->
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" /> <input type="hidden" name="loggerName"
			value="${logger.name}" />

		<div class="row">
			<label class="col-sm-4 control-label"><spring:message
					code="logger.name" /></label>
			<div class="col-sm-8">
				<p class="form-control-static"><c:out value="${logger.name}" /></p>
			</div>
		</div>
		<div class="row">
			<label class="col-sm-4 control-label"><spring:message
					code="logger.log-level" /></label>
			<div class="col-sm-8">
				<p class="form-control-static"><c:out value="${logger.level}" /></p>
			</div>
		</div>
		<div class="row">
			<label class="col-sm-4 control-label"><spring:message
					code="logger.appenders" /></label>
			<div class="col-sm-8">
				<c:forEach items="${appenders}" var="appender">
					<p class="form-control-static">
						<c:out value="${appender.name}" />
					</p>
				</c:forEach>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-4 control-label"><spring:message
					code="logger.log-level" /></label>

			<div class="col-sm-8">
				<select class="form-control" name="loggerLevel">
					<c:forTokens items="all,debug,info,warn,error,fatal,off,trace"
						delims="," var="level">
						<option value="${level}"
							${level == logger.level.toString().toLowerCase() ? "selected" : ""}>
							<c:out value="${level}" /></option>
					</c:forTokens>
				</select>
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-offset-4 col-sm-8">
				<button type="submit" class="btn btn-primary">
					<spring:message code="save" />
				</button>
				<a class="btn btn-default"
					href="<c:url value="/admin/logger/" />">
					<spring:message code="cancel" />
				</a>
			</div>
		</div>

	</form>
	</div>
</body>
</html>
