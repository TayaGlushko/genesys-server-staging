<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <title><spring:message code="loggers.list.page"/></title>
</head>
<body>

<div class="main-col-header clearfix">
    <div class="nav-header pull-left">
		<div class="results"><spring:message code="paged.totalElements" arguments="${loggers.totalElements}" /></div>
        <div class="pagination">
			<spring:message code="paged.pageOfPages" arguments="${loggers.number},${loggers.totalPages}" />
            <a class="${pageNumber-1 eq 0 ? 'disabled' :''}" href="?page=${loggers.number-1 eq 0 ? 1 : loggers.number-1}"><spring:message code="pagination.previous-page" /></a> <a href="?page=${loggers.number + 1}"><spring:message code="pagination.next-page" /></a>
        </div>
    </div>
</div>

<div class="row">
    <form method="post" action=<c:url value="/admin/logger/addLoger"/> >
        <div class="form-group col-sm-10">
            <input class="form-control" type="text" name="nameNewLogger" placeholder="com.example.package.Class"/>
        </div>

        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <div class="form-group col-sm-2">
            <input type="submit" class="btn" value="<spring:message code="logger.add-logger" />" />
        </div>
    </form>
</div>

    <ul class="funny-list">
    <c:forEach items="${loggers.content}" var="logger" varStatus="status">
    	<li class="${status.count % 2 == 0 ? 'even' : 'odd'}">
            <a href="<c:url value="/admin/logger/${logger.name}." />"><c:out value="${logger.name}" /></a>
			<div class="pull-right"><c:out value="${logger.level}" /></div>
		</li>
    </c:forEach>
    </ul>
    
</body>
</html>
