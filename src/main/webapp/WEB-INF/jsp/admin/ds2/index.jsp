<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
<title><spring:message code="admin.page.title" /></title>
</head>
<body>
    <%@ include file="/WEB-INF/jsp/admin/menu.jsp" %>

	<h3>worldclim.org</h3>
	<form method="post" action="<c:url value="/admin/ds2/worldclim/update" />">
		<input type="submit" class="btn btn-default" value="Update worldclim.org" />
        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form>
	
	<form method="post" action="<c:url value="/admin/ds2/worldclim/delete" />">
		<input type="submit" class="btn btn-default" value="Delete worldclim.org" />
        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form>
	
</body>
</html>