<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
<title><spring:message code="repository.gallery" /></title>
</head>
<body>
	<div class="row">
		<div class="col-md-12">
			<a href="<c:url value="/admin/r/g" />" class="btn btn-default"><spring:message
					code="cancel" /></a>
			<h4>
				Updating metadata for image gallery
				<c:out value="${imageGallery.path}" />
			</h4>

			<form action="<c:url value="/admin/r/g/update" />" method="post">
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" /> 
				<input type="hidden" name="path" value="<c:out value="${imageGallery.path}" />">

				<div class="form-group">
					<label for="title"><spring:message
							code="repository.gallery.title" /></label> <input type="text" id="title"
						name="title" value="<c:out value="${imageGallery.title}" />"
						class="form-control">
				</div>

				<div class="form-group">
					<label for="title"><spring:message
							code="repository.gallery.path" /></label> <span class=""><c:out
							value="${imageGallery.path}" /></span>
				</div>

				<div class="form-group">
					<label for="subject"><spring:message
							code="repository.gallery.description" /></label>
					<textarea id="description" name="description" class="form-control"><c:out
							escapeXml="false" value="${imageGallery.description}" /></textarea>
				</div>

				<button type="submit" class="btn btn-default">
					<spring:message code="save" />
				</button>
			</form>
		</div>
	</div>
</body>
</html>
