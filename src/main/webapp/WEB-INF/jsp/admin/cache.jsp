<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
<title><spring:message code="admin.cache.page.title" /></title>
</head>
<body>
  <h1>
    <spring:message code="admin.cache.page.title" />
  </h1>

  <%@ include file="/WEB-INF/jsp/admin/menu.jsp" %>


  <form method="post" action="<c:url value="/admin/cache/clearTilesCache" />">
    <input type="submit" class="btn btn-default" value="Clear Tiles cache" />
    <!-- CSRF protection -->
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
  </form>

  <form method="post" action="<c:url value="/admin/cache/clearCaches" />">
    <input type="submit" class="btn btn-default" value="Clear all caches" />
    <!-- CSRF protection -->
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
  </form>

  <c:forEach items="${cacheMaps}" var="cacheMap">
    <h3>
      <c:out value="${cacheMap.serviceName}" />
      <c:out value="${cacheMap.name}" />
    </h3>
    <form method="post" action="<c:url value="/admin/cache/clearCache" />">
      <input type="hidden" name="name" value="${cacheMap.name}" />
      <input type="submit" class="btn btn-default" value="Clear" />
      <!-- CSRF protection -->
      <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    </form>
    <c:set value="${cacheMap.mapStats}" var="mapStat" />

    <div class="row">
      <div class="col-xs-6 col-sm-4">
        <spring:message code="cache.stat.map.ownedEntryCount" />
      </div>
      <div class="col-xs-6 col-sm-8">
        <c:out value="${mapStat.ownedEntryCount}" />
      </div>
    </div>

    <div class="row">
      <div class="col-xs-6 col-sm-4">
        <spring:message code="cache.stat.map.lockedEntryCount" />
      </div>
      <div class="col-xs-6 col-sm-8">
        <c:out value="${mapStat.lockedEntryCount}" />
      </div>
    </div>

    <div class="row">
      <div class="col-xs-6 col-sm-4">
        <spring:message code="cache.stat.map.puts" />
      </div>
      <div class="col-xs-6 col-sm-8">
        <c:out value="${mapStat.putOperationCount}" />
      </div>
    </div>

    <div class="row">
      <div class="col-xs-6 col-sm-4">
        <spring:message code="cache.stat.map.hits" />
      </div>
      <div class="col-xs-6 col-sm-8">
        <c:out value="${mapStat.hits}" />
      </div>
    </div>
  </c:forEach>

  <c:forEach items="${cacheOthers}" var="cacheOther">
    <h3>
      <c:out value="${cacheOther}" />
      <c:out value="${cacheOther}" />
    </h3>
  </c:forEach>
</body>
</html>