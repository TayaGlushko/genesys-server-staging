<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
<title><spring:message code="admin.page.title" /></title>
</head>
<body>
  <%@ include file="/WEB-INF/jsp/admin/menu.jsp" %>

	<form method="post" action="<c:url value="/admin/admin-action" />">
		<input type="submit" class="btn btn-default" name="accenumbnumb" value="ACCENUMB-NUMB" />
		<!-- CSRF protection -->
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	</form>

	<form method="post" action="<c:url value="/admin/assign-uuid" />">
    <input type="submit" class="btn btn-default" value="Assign missing UUIDs" />
    <!-- CSRF protection -->
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
  </form>

  <form method="post" action="<c:url value="/admin/pdci" />">
    <input type="submit" class="btn btn-default" value="Calculate missing PDCI" />
    <!-- CSRF protection -->
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
  </form>

  <h3>Country data</h3>
  <form method="post" action="<c:url value="/admin/refreshCountries" />">
    <input type="submit" class="btn btn-default" value="Refresh country data" />
    <!-- CSRF protection -->
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
  </form>
  <form method="post" action="<c:url value="/admin/admin-action" />">
		<input type="submit" class="btn btn-default" name="georegion" value="Refresh geo Regions" />
    <!-- CSRF protection -->
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
  </form>
  <form method="post" action="<c:url value="/admin/updateAlternateNames" />">
    <input type="submit" class="btn btn-default" value="Update alternate GEO names" />
    <!-- CSRF protection -->
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
  </form>
  <form method="post" action="<c:url value="/admin/updateITPGRFA" />">
    <input type="submit" class="btn btn-default" class="btn btn-default" value="Update country ITPGRFA status" />
    <!-- CSRF protection -->
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
  </form>



  <h3>WIEWS</h3>
  <form method="post" action="<c:url value="/admin/refreshWiews" />">
    <input type="submit" class="btn btn-default" value="Refresh WIEWS data" />
    <!-- CSRF protection -->
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
  </form>

  <h3>Svalbard Global Seed Vault</h3>
  <form method="post" action="<c:url value="/admin/updateSGSV" />">
    <input type="submit" class="btn btn-default" value="Update SGSV" />
    <!-- CSRF protection -->
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
  </form>

  <h3>Accession</h3>
  <form method="post" action="<c:url value="/admin/updateAccessionCountryRefs" />">
    <input type="submit" class="btn btn-default" class="btn btn-default" value="Update accession country info" />
    <!-- CSRF protection -->
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
  </form>
  <form method="post" action="<c:url value="/admin/updateInstituteCountryRefs" />">
    <input type="submit" class="btn btn-default" class="btn btn-default" value="Update WIEWS country info" />
    <!-- CSRF protection -->
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
  </form>
  <form method="post" action="<c:url value="/admin/updateAccessionInstituteRefs" />">
    <input type="submit" class="btn btn-default" value="Update accession institute info" />
    <!-- CSRF protection -->
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
  </form>
  <form method="post" action="<c:url value="/admin/convertNames" />">
    <input type="submit" class="btn btn-default" value="Convert old names to aliases" />
    <!-- CSRF protection -->
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
  </form>
  <h3>C&amp;E</h3>
  <form method="post" action="<c:url value="/admin/refreshMetadataMethods" />">
    <input type="submit" class="btn btn-default" class="btn btn-default" value="Recalculate metadata methods" />
    <!-- CSRF protection -->
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
  </form>

  <h3>Content</h3>
  <form method="post" action="<c:url value="/admin/sanitize" />">
    <input type="submit" class="btn btn-default" value="Sanitize HTML content" />
    <!-- CSRF protection -->
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
  </form>

  <form method="post" action="<c:url value="/admin/reindexEntity" />">
    <select name="entity">
      <option value="org.genesys2.server.model.impl.Country">Countries</option>
      <option value="org.genesys2.server.model.impl.FaoInstitute">WIEWS Institutes</option>
      <option value="org.genesys2.server.model.impl.ActivityPost">Posts</option>
      <option value="org.genesys2.server.model.impl.Article">Articles</option>
      <option value="org.genesys2.server.model.impl.Organization">Organizations</option>
      <option value="org.genesys2.server.model.genesys.Accession">Accessions</option>
      <option value="org.genesys2.server.model.genesys.AccessionAlias">Accession alias</option>
      <option value="org.genesys2.server.model.genesys.Taxonomy2">Taxonomy2</option>
    </select> <input type="submit" class="btn btn-default" value="Reindex search indexes" />
    <!-- CSRF protection -->
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
  </form>

  <h3>Taxonomy</h3>
  <form method="post" action="<c:url value="/admin/cleanup-taxonomies" />">
    <input type="submit" class="btn btn-default" value="Cleanup taxonomies" />
    <!-- CSRF protection -->
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
  </form>

</body>
</html>