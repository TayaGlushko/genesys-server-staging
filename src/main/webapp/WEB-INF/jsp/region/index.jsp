<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
    <title><spring:message code="region.page.list.title"/></title>
</head>
<body>
<cms:informative-h1 title="region.page.list.title" fancy="true" info="region.page.list.intro"/>

<c:set var="url" value="/${pageContext.response.locale}/json/v0/geo/regions${not empty isoCode? '/'.concat(isoCode) :''}"/>

<div id="regionTree"></div>

<content tag="javascript">
    <%--<script src="<c:url value="/html/js/jstree.min.js" />"></script>--%>
    <script type="text/javascript">

        $(document).ready(function () {

            $("#regionTree").jstree({
                'core': {
                    'data': {
                        "url": '${url}' ,
                        "dataType": "json"
                    }
                }
            }).bind('select_node.jstree', function (e, data) {

                if (!!data.node.original.href) {

                    document.location.href = '/' + '${pageContext.response.locale}' + data.node.original.href;
                }

                return data.instance.toggle_node(data.node);

            }).bind("loaded.jstree", function (event, data) {
                $(this).jstree("open_all");
            })

        });
    </script>
</content>

</body>
</html>
