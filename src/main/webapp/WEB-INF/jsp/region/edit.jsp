<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
  <title><c:out value="${region.getName(pageContext.response.locale)}" /></title>
</head>
<body>
<h1>
	<c:out value="${region.getName(pageContext.response.locale)}" />
</h1>

<form role="form" class="form-horizontal" action="<c:url value="/geo/regions/${region.isoCode}/update" />" method="post">
  <div class="form-group">
    <label for="blurp-body" class="col-lg-12 control-label"><spring:message code="blurp.blurp-body" /></label>
    <div class="controls col-lg-12">
				<textarea id="blurp-body" name="blurp" class="span9 required html-editor">
					<c:out value="${blurp.body}" />
				</textarea>
    </div>
  </div>

  <div class="form-group">
    <label for="crop-summary" class="col-lg-12 control-label"><spring:message code="crop.summary" /></label>
    <div class="controls col-lg-12">
				<textarea id="crop-summary" name="summary" class="span9 required html-editor">
					<c:out value="${blurp.summary}" />
				</textarea>
    </div>
  </div>

  <input type="submit" value="<spring:message code="save"/>" class="btn btn-primary" /> <a href="<c:url value="/geo/regions/${region.isoCode}" />" class="btn btn-default"> <spring:message code="cancel" />
</a>
  <!-- CSRF protection -->
  <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
</form>


<content tag="javascript">
  <script type="text/javascript">
    <local:tinyMCE selector=".html-editor" />
  </script>
</content>

</body>
</html>