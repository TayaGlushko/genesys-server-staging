<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
<title><spring:message code="page.home.title" /></title>
</head>
<body class="welcome-page">
	<div class="row no-space intro">
		<div class="hidden-sm hidden-xs col-md-6 col-lg-6">
			<div class="intro-image">
			</div>
		</div>
		<div class="col-sm-12 col-md-6 col-lg-6 no-space">
			<div class="intro-text">
				<c:if test="${welcomeBlurp ne null}">
					<div class="">
						<cms:blurb blurb="${welcomeBlurp }"/>
						<a class="btn btn-default" href="<c:url value="/content/about/about" />"><spring:message code="welcome.read-more" /></a>
					</div>
				</c:if>
			</div>
		</div>	
	</div>

	<div class="row no-space map">
		<div id="stats" class="col-md-4 col-sm-12">
			<h2 class="short text-right">
				<spring:message code="maps.accession-map" />
			</h2>

			<div class="stats-map">
				<div class="all-stats">
					<%-- 	        <div class="one-stat"><a href="<c:url value="/geo/" />"><spring:message code="stats.number-of-countries" arguments="${numberOfCountries}" /></a></div> --%>
					<div class="one-stat">
						<a href="<c:url value="/explore" />">
							<spring:message code="stats.number-of-accessions" arguments="${numberOfActiveAccessions}" />
						</a>
					</div>
					<%-- <div class="one-stat">
						<a href="<c:url value="/explore"><c:param name="filter" value='{"historic":[true]}' /></c:url>">
							<spring:message code="stats.number-of-historic-accessions" arguments="${numberOfHistoricAccessions}" />
						</a>
					</div> --%>
					<div class="one-stat">
						<a href="<c:url value="/wiews/active" />">
							<spring:message code="stats.number-of-institutes" arguments="${numberOfInstitutes}" />
						</a>
					</div>
				</div>
				<p><spring:message code="maps.accession-map.intro" /></p>
			</div>
		</div>

		<div class="hidden-xs col-md-8 col-sm-12">
		<%-- 	<h2>
				<spring:message code="maps.accession-map" />
			</h2> --%>
			<div class="">
				<div id="globalmap" x-href="<c:url value="/explore/map" />" class="gis-map"></div>
			</div>
		</div>
	</div>

	<div class="row see-also-row">
		<div class="col-md-4 col-sm-12 no-space">
			<div class="see-also-block">
				<div class="content">
					<h2 class="short text-right">
						<spring:message code="welcome.search-genesys" />
					</h2>
					<div class="body">
							<div class="row">
								<div class="col-lg-4 hidden-md">
									<div class="see-also-img" style="background-image: url('/html/1/images/three-1.jpg');">
									</div>
								</div>
								<div class="col-md-12 col-lg-8  no-space">
						
						<p><spring:message code="search.search-query-missing" /></p>
						<form action="<c:url value="/acn/search2" />" class="form-inline">
							<div class="form-group">
								<input type="text" name="q" class="form-control" placeholder="<spring:message code="search.input.placeholder" />" />
								<%-- <input type="submit" class="btn btn-primary" value="<spring:message code="search.button.label" />" /> --%>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
				</div>
			</div>

		<!-- Block Organizations -->
		<div class="col-md-4 col-sm-12 no-space">
			<div class="see-also-block" >
				<div class="content">
					<h2 class="short text-right">
						<spring:message code="welcome.networks" />
					</h2>
						<div class="body" dir="ltr">
							<div class="row">
								<div class="col-lg-4 hidden-md">
									<div class="see-also-img" style="background: url('/html/1/images/three-2.jpg') 62% no-repeat;">
									</div>
								</div>
								<div class="col-md-12 col-lg-8  no-space network-list">
					
						<span>&bull;</span>
							<a href="<c:url value="/org/CGIAR" />">CGIAR International Genebanks</a>
						<span>&bull;</span>
						<a href="<c:url value="/org/EURISCO" />">ECPGR EURISCO network</a>
						<span>&bull;</span>
						<a href="<c:url value="/org/USDA" />">USDA ARS NPGS</a>
						<span>&bull;</span>
						<a href="<c:url value="/org/COGENT" />">COGENT</a>
					</div>
				</div>
											
						</div>
			</div>
		</div>
		
			</div>
			
			<!-- Block Help -->
			<div class="col-md-4 col-sm-12 no-space">
				<div class="see-also-block" x-href="/1/content/help/how-to-use-genesys" >
		
				<div class="content">
					<h2 class="short text-right">
						<spring:message code="menu.help" />
					</h2>
						<div class="body">
							<div class="row">
								<div class="col-lg-4 hidden-md">
									<div class="see-also-img" style="background: url('/html/1/images/three-3.jpg') 82% no-repeat;">
									</div>
								</div>
									<div class="col-md-12 col-lg-8  no-space">
									<a><spring:message code="help.page.intro" /></a>
				</div>
			</div>

		</div>
	</div>
				</div>
			</div>
		</div>

		<div class="center-divider">
			<div class="row white-background">

		<!-- left column end / middle columns start -->
			<div class="col-lg-8 col-md-8 no-space" id="middle-col">
				<h2 class="short text-right">
					<spring:message code="activity.recent-activity" />
			</h2>

			<div class="tab-content">
				<div class="tab-pane active" id="news-feed">
					<security:authorize access="hasRole('ADMINISTRATOR')">
						<a href="<c:url value="/content/activitypost/new" />" class="pull-right close" style="">
							<spring:message code="activitypost.add-new-post" />
						</a>
					</security:authorize>

					<div class="all-posts">
						<c:forEach items="${lastNews}" var="activityPost" varStatus="status">
							<cms:activitypost activityPost="${activityPost}" />
						</c:forEach>
					</div>

					<a id="show-moar-news" href="<c:url value="/content/news" />"><spring:message code="news.content.page.all.title" /></a>
				</div>
			</div>
		</div>

		<!-- middle column end / right columns start -->
		<div class="col-lg-4 col-md-4 no-space" id="right-col">
			<c:if test="${sideBlurp ne null}">
				<div class="content-block">
					<h2 class="short text-right"><c:out value="${sideBlurp.title}" /></h2>
					<cms:blurb blurb="${sideBlurp}"/>
				</div>
			</c:if>
			
			
			<div class="content-block" id="crop-list">
				<h2 class="short text-right">
					<spring:message code="crop.croplist" />
				</h2>
				<sec:authorize access="hasRole('ADMINISTRATOR')">
					<form method="post" action="<c:url value="/c/rebuild" />">
						<input type="submit" class="btn form-control" value="Rebuild" />
						<!-- CSRF protection -->
						<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
					</form>
				</sec:authorize>
				<%-- <div class="dropdown padding10">
					<input class="form-control autocomplete-genus" placeholder="<spring:message code="autocomplete.genus" />" x-source="<c:url value="/explore/ac/taxonomy.genus" />" />
				</div>
			</div> --%>

			<c:if test="${cropList ne null and cropList.size() gt 0}">
				<c:set var="cropCount" value="${cropList.size()}" />
					<ul class="nav">
						<div class="row">
							<div class="col-md-12">
								<li class="all-crops">
									<a class="show" href="<c:url value="/explore/" />">
										<spring:message code="crop.all-crops" />
									</a>
								</li>
						<c:forEach items="${cropList}" var="crop" varStatus="status">
							<li>
								<a class="show" href="<c:url value="/explore/c/${crop.shortName}" />">
									<c:out value="${crop.getName(pageContext.response.locale)}" />
								</a>
							</li>
							<%-- ${cropCount / 3} ${cropCount / 3 == 0} idx=${status.count} --%>
							<c:if test="${status.count % (cropCount / 3) == 0}">
									</div>
									<div class="col-md-12">
							</c:if>
						</c:forEach>
							</div>
						</div>
					</ul>
				</div>
				</div>
			</c:if>
		</div>
	</div>
	
	
	<content tag="javascript">
<!-- Index Javascripts -->
<script type="text/javascript">
  $( document ).ready(function() {
    $(".autocomplete-genus").each(function() {
		var t=$(this); 
		t.autocomplete({ delay: 200, minLength: 3, source: t.attr('x-source'),  
			messages: { noResults: '', results: function() {} },
			select: function(event, ui) { document.location.pathname='/acn/t/'+ui.item.value; } });
	});
	
/* 	var map = L.map('globalmap', {minZoom:2, maxZoom:2, dragging:false, zoomControl:false, keyboard:false}).setView([20,0], 2);
	L.tileLayer('https://otile{s}-s.mqcdn.com/tiles/1.0.0/sat/{z}/{x}/{y}.png', {
	    attribution: "MapQuest",
	    styleId: 22677,
	    noWrap: true,
	    subdomains: ['1','2','3','4'],
	    opacity: 0.6
	}).addTo(map);
	L.tileLayer("{s}/explore/tile/{z}/{x}/{y}?filter={}", {
	    attribution: "<a href='${props.baseUrl}'>Genesys</a>",
	    styleId: 22677,
	    noWrap: true,
	    subdomains: [${props.tileserverCdn}]
	}).addTo(map); */
  });
</script>
</content>

</body>
</html>