<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
    <title><c:out value="${title}" /></title>
    <local:content-headers description="${jspHelper.htmlToText(article.summary, 150)}" title="${title}"/>
</head>
<body class="about-page">
<content tag="header">
	<div class="top-image">
	</div>
	<c:if test="${article.id ne null}">
  <security:authorize access="hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')">
  	<div style="position: relative;">
  	<div class="" style="position: absolute; right: 0;">
		        <form class="inline-form pull-right" method="post" action="<c:url value="/content/transifex"/>">
		            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		            <input id="articleSlug" type="hidden" name="slug" value="${article.slug eq 'blurp' ? ''.concat(article.classPk.shortName).concat('-').concat(article.targetId) : article.slug}"/>
		
		            <c:choose>
		                <c:when test="${article.targetId ne null}">
		                    <a class="btn btn-default" href="<c:url value="/content/edit/${article.slug}/${article.classPk.shortName}/${article.targetId}/${article.lang}" />"><spring:message
		                            code="edit"/></a>
		                </c:when>
		                <c:otherwise>
		                    <a class="btn btn-default" href="<c:url value="/content/edit/${article.slug}/${article.lang}" />"><spring:message
		                            code="edit"/></a>
		                </c:otherwise>
		            </c:choose>
		
		            <input type="submit" name="fetch-all" class="btn btn-default"
		                   value="<spring:message code="article.fetch-from-transifex" />"/>
		        </form>
		</div>
		</div>
  </security:authorize>
	</c:if>
</content>

<div class="row">
	<h1 class="green-bg">
		<c:out value="${title}" escapeXml="false"/>
	</h1>

    <local:not-translated display="${article.lang != pageContext.response.locale.language}" />

<%@ include file="/WEB-INF/jsp/content/transifex-responses.jspf" %>

<div class="article clearfix">
    <div class="right-side main-col">

        <div class="free-text" dir="${article.lang=='fa' || article.lang=='ar' ? 'rtl' : 'ltr'}">
            <c:out value="${article.body}" escapeXml="false"/>
        </div>

        <div class="article-timestamp">
            <local:prettyTime date="${article.postDate.time}" locale="${pageContext.response.locale}"/>
        </div>

        <div class="share-article">
            <p>
                <spring:message code="article.share"/>
            </p>
            <ul class="list-inline">
                <%--
                <li class="envelope">
                    <a href="">
                        <i class="fa fa-envelope-o"></i>
                    </a>
                </li>
                 --%>
                <li class="twitter">
                    <local:tweet text="${article.title}" hashTags="GenesysPGR"/>
                </li>
                <li class="linkedin">
                    <local:linkedin-share text="${article.title}" summary="${article.body}"/>
                </li>
            </ul>
        </div>

        <%-- <div class="see-also">
            <h3>
                <span>
                    <spring:message code="heading.see-also" />
                </span>
            </h3>
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <div class="see-also-block" style="background-image: url('<c:url value="/html/0/images/pic_news_1.png" />')">
                        <h2>History of Genesys</h2>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="see-also-block" style="background-image: url('<c:url value="/html/0/images/pic_news_1.png" />')">
                        <h2>How to use Genesys</h2>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="see-also-block" style="background-image: url('<c:url value="/html/0/images/pic_news_1.png" />')">
                        <h2>About Genesys Data 123 </h2>
                    </div>
                </div>
            </div>
        </div> --%>
    </div>


    <div class="col-md-3 sidebar-nav col-xs-12">
        <cms:menu key="${menu.key}" items="${menu.items}"/>
    </div>
</div>

<content tag="javascript">
    <script type="text/javascript">
        $(document).ready(function () {
            $('a[href="' + window.location.pathname + '"]').parent().addClass('active-link');
        });
    </script>
</content>

</body>
</html>