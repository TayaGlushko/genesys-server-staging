<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
  <c:out value="${head}" escapeXml="false" />
  <title><c:out value="${title}" /></title>
</head>
<body class="about-page">
<content tag="header">
  <div class="top-image">
    <%-- <img src="<c:url value="/html/0/images/aim/banner-${jspHelper.randomInt(1, 4)}.jpg" />" title="${title}"/> --%>
  </div>
</content>

<div class="row">
	<h1 class="green-bg">
	  <c:out value="${title}" escapeXml="false"/>
	</h1>

<div class="article clearfix">
  <div class="right-side main-col">
    <div class="free-text">
      <c:out value="${bodyHeader}" escapeXml="false"/>
      <c:out value="${bodyContent}" escapeXml="false"/>
      <c:out value="${bodyFooter}" escapeXml="false"/>
    </div>
  </div>

  <div class="col-md-3 sidebar-nav col-xs-12">
    <c:out value="${menu}" escapeXml="false"/>
  </div>
</div>

<content tag="javascript">
  <script type="text/javascript">
    $(document).ready(function () {
      $('a[href="' + window.location.pathname + '"]').parent().addClass('active-link');
    });
  </script>
</content>

</body>
</html>