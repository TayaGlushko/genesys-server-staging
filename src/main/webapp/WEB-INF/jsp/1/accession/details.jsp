<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
<title><spring:message code="accession.page.profile.title" arguments="${accession.accessionName}"
    argumentSeparator="|"
  /></title>
	<local:content-headers description="${jspHelper.htmlToText(blurp.summary, 150)}" title="${accession.accessionName} | ${accession.instituteCode}" keywords="${accession.accessionName}" />
</head>
<body class="accession-page" typeof="germplasm:GermplasmAccession">
	<h1 class="green-bg">
		<span property="dwc:catalogNumber">
			<c:out value="${accession.accessionName}" />
		</span>
		<small property="dwc:institutionCode" datatype="germplasmType:wiewsInstituteID"><c:out value="${accession.instituteCode}" /></small>
	</h1>

	<div class="main-col-header acn">
		<div class="" x-aid="${accession.id}">
			<div class="checkbox">
				<label>
					<input type="checkbox" value="">
					<a class="add" href=""><spring:message code="selection.add" arguments="${accession.accessionName}" /></a> <a class="remove" href=""><spring:message code="selection.remove"
							arguments="${accession.accessionName}" /></a>
				</label>
			</div>
		</div>
	</div>

	<gui:alert type="warning" display="${accession.historic eq true or accession.getClass().simpleName eq 'AccessionHistoric'}">
		<spring:message code="accession.this-is-a-historic-entry" />
	</gui:alert>

	<gui:alert type="info" display="${accession.inTrust eq true}">
		<spring:message code="accession.inTrust.true" />
	</gui:alert>

	<gui:alert type="info" display="${accession.inSvalbard eq true}">
		<spring:message code="accession.inSvalbard.true" />
	</gui:alert>

	<gui:alert type="info" display="${accession.mlsStatus eq true}">
		<spring:message code="accession.mlsStatus.true" />
	</gui:alert>

	<%--
		<gui:alert type="warning" display="${accession.availability eq false}">
			<spring:message code="accession.not-available-for-distribution" />
		</gui:alert>
		<gui:alert type="info" display="${accession.availability eq true}">
			<spring:message code="accession.available-for-distribution" />
		</gui:alert>
	--%>

	<div class="crop-details">
		<div class="basic-info">
			<h4 class="section-heading">
				<spring:message code="accession.page.profile.title" arguments="${accession.accessionName}" argumentSeparator="|" /></h4>
			<div class="section-inner-content">

				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<p>
							<spring:message code="accession.holdingInstitute" />
						</p>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<p>
							<a property="dwc:institutionCode" href="<c:url value="/wiews/${accession.instituteCode}" />"> <c:out value="${accession.institute.fullName}" />
							</a>
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<p>
							<spring:message code="accession.holdingCountry" />
						</p>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<p>
							<a href="<c:url value="/geo/${accession.institute.country.code3}" />"><c:out value="${accession.institute.country.getName(pageContext.response.locale)}" /></a>
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<p>
							<spring:message code="accession.accessionName" />
						</p>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<p>
							<c:out value="${accession.accessionName}" />
						</p>
					</div>
				</div>

				<c:if test="${crops ne null}">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<p>
								<spring:message code="accession.crop" />
							</p>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<p>
								<c:forEach items="${crops}" var="crop">
									<a href="<c:url value="/c/${crop.shortName}" />"><c:out value="${crop.getName(pageContext.response.locale)}" /></a>
								</c:forEach>
							</p>
						</div>
					</div>
				</c:if>

				<c:if test="${accession.countryOfOrigin ne null}">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<p>
								<spring:message code="accession.origin" />
							</p>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<p>
								<img class="flag" src="<c:url value="${cdnFlagsUrl}" />/${accession.origin.toUpperCase()}.svg" /> <a href="<c:url value="/geo/${accession.origin}" />"> <c:out
										value="${accession.countryOfOrigin.getName(pageContext.response.locale)}" />
								</a>
							</p>
						</div>
					</div>
				</c:if>

				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<p>
							<spring:message code="taxonomy.genus" />
						</p>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" property="dwc:genus">
						<p>
							<a href="<c:url value="/acn/t/${accession.taxonomy.genus}" />"><span dir="ltr" class="sci-name">
									<c:out value="${accession.taxonomy.genus}" />
								</span></a>
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<p>
							<spring:message code="taxonomy.species" />
						</p>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<p>
							<a href="<c:url value="/acn/t/${accession.taxonomy.genus}/${accession.taxonomy.species}" />"><span class="sci-name" dir="ltr">
									<c:out value="${accession.taxonomy.genus} ${accession.taxonomy.species}" />
								</span></a> &mdash; <a href="<c:url value="/wiews/${accession.institute.code}/t/${accession.taxonomy.genus}/${accession.taxonomy.species}" />"><spring:message code="accession.taxonomy-at-institute"
									arguments="${accession.taxonomy.genus} ${accession.taxonomy.species}|||${accession.institute.code}" argumentSeparator="|||" /></a>
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<p>
							<spring:message code="taxonomy.taxonName" />
						</p>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<p>
							<span dir="ltr" class="sci-name">
								<c:out value="${accession.taxonomy.taxonName}" />
							</span>
						</p>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<p>
							<spring:message code="accession.sampleStatus" />
						</p>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" property="germplasm:biologicalStatus">
						<p>
							<spring:message code="accession.sampleStatus.${accession.sampleStatus}" />
						</p>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<p>
							<spring:message code="accession.storage" />
						</p>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<c:forEach items="${accession.stoRage}" var="storage">
							<p>
								<spring:message code="accession.storage.${storage}" />
							</p>
						</c:forEach>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<p>
							<spring:message code="accession.acqDate" />
						</p>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<p>
							<c:out value="${accession.acquisitionDate}" />
						</p>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<p>
							<spring:message code="accession.availability" />
						</p>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<p>
							<spring:message code="accession.availability.${accession.availability}" />
						</p>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<p>
							<spring:message code="accession.otherNames" />
						</p>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<c:forEach items="${accessionAliases}" var="accessionAlias">
							<p>
								<c:out value="${accessionAlias.name}" />
								<c:if test="${accessionAlias.instCode != ''}">
									<a href="<c:url value="/wiews/${accessionAlias.instCode}" />"><c:out value="${accessionAlias.instCode}" /></a>
								</c:if>
								<c:if test="${accessionAlias.usedBy != ''}">
								<c:out value="${accessionAlias.usedBy}" />
						</c:if>
								<c:if test="${accessionAlias.lang != ''}">
									<c:out value="${accessionAlias.lang}" />
								</c:if>
								<spring:message code="accession.aliasType.${accessionAlias.aliasType}" />
							</p>
						</c:forEach>
					</div>
				</div>

				<c:if test="${accession.duplSite ne null}">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<p>
							<spring:message code="accession.duplSite" />
						</p>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<c:forEach items="${accession.duplSite.split('[;,]')}" var="duplSite">
							<p>
								<spring:message code="${duplSite}" />
							</p>
						</c:forEach>
					</div>
				</div>
				</c:if>


				<c:if test="${accessionExchange ne null}">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<p>
								<spring:message code="accession.donor.institute" />
							</p>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<p>
								<c:if test="${accessionExchange.donorInstitute eq null}">
									<c:out value="${accessionExchange.donorName}" />
								</c:if>
								<c:out value="${accessionExchange.donorInstitute}" />
							</p>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<p>
								<spring:message code="accession.donor.accessionNumber" />
							</p>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<p>
								<c:out value="${accessionExchange.accNumbDonor}" />
							</p>
						</div>
					</div>
				</c:if>

				<c:if test="${accession.uuid ne null}">
					<div class="row text-muted">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<p>
								<spring:message code="accession.uuid" />
							</p>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<p>
								<span property="dc:identifier">
									urn:uuid:
									<c:out value="${accession.uuid}" />
								</span>
							</p>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<p>
								<spring:message code="accession.purl" />
							</p>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<p property="dc:identifier">
								<a href="<c:url value="https://purl.org/germplasm/id/${accession.uuid}" />"><c:out value="https://purl.org/germplasm/id/${accession.uuid}" /></a>
							</p>
						</div>
					</div>
				</c:if>


				<c:if test="${accession.acceUrl ne null}">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<p>
								<spring:message code="accession.acceUrl" />
							</p>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<p>
								<a target="_blank" href="<c:out value="${accession.acceUrl}"  />"><c:out value="${accession.acceUrl}" /></a>
							</p>
						</div>
					</div>
				</c:if>


			</div>
			<!-- End: inner -->
		</div>
		<!-- End: basic info section -->


		<c:if test="${imageGallery ne null}">
			<h4 class="row section-heading">
				<spring:message code="accession.imageGallery" />
			</h4>
			<div class="row imagegallery thumbs" id="accession-images-thumbs">
				<c:forEach items="${imageGallery.images}" var="image">
					<div x-uuid="<c:out value="${image.uuid}" />" x-ext="<c:out value="${image.extension}" />" style="cursor: pointer;" class="col-xs-6 col-sm-3 col-md-2 col-lg-2">
						<img src="<c:url value="/repository/d${image.path}_thumb/${thumbnailFormat}_${image.uuid}.png" />" alt="<c:out value="${image.title}" />" />
					</div>
				</c:forEach>
			</div>

			<div class="imagegallery image-frame row" id="accession-image-view">
				<img class="theimage" />
				<div class="metadata">
					<div class="title">
						<spring:message code="repository.file.title" />
						<span class="val"></span>
					</div>
					<div class="subject">
						<spring:message code="repository.file.subject" />
						<span class="val"></span>
					</div>
					<div class="description">
						<spring:message code="repository.file.description" />
						<span class="val"></span>
					</div>
					<div class="creator">
						<spring:message code="repository.file.creator" />
						<span class="val"></span>
					</div>
					<div class="created">
						<spring:message code="repository.file.created" />
						<span class="val"></span>
					</div>
					<div class="rightsHolder">
						<spring:message code="repository.file.rightsHolder" />
						<span class="val"></span>
					</div>
					<div class="accessRights">
						<spring:message code="repository.file.accessRights" />
						<span class="val"></span>
					</div>
					<div class="license">
						<spring:message code="repository.file.license" />
						<span class="val"></span>
					</div>
					<div class="extent">
						<spring:message code="repository.file.extent" />
						<span class="val"></span>
					</div>
					<div class="bibliographicCitation">
						<spring:message code="repository.file.bibliographicCitation" />
						<span class="val"></span>
					</div>

					<!-- Extras -->
					<div class="downloadLink">
						<a href=""><spring:message code="repository.gallery.downloadImage" /></a>
					</div>
				</div>
			</div>
		</c:if>

		<c:if test="${accessionCollect ne null}">
			<div class="collect-info">
				<h4 class="row section-heading">
					<spring:message code="accession.collecting" />
				</h4>
				<div class="section-inner-content clearfix">
					<div class="row">
						<c:if test="${accessionCollect.collCode ne null}">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<p>
									<spring:message code="accession.collecting.institute" />
								</p>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<p>
									<c:out value="${accessionCollect.collCode}" />
								</p>
							</div>
						</c:if>
					</div>
					<c:if test="${accessionCollect.collName ne null or accessionCollect.collInstAddress ne null}">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<p>
									<spring:message code="accession.collecting.institute" />
								</p>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<p class="ltr">
									<c:out value="${accessionCollect.collName} ${accessionCollect.collInstAddress}" />
								</p>
							</div>
						</div>
					</c:if>
					<c:if test="${accessionCollect.collMissId ne null}">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<p>
									<spring:message code="accession.collecting.mission" />
								</p>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<p>
									<c:out value="${accessionCollect.collMissId}" />
								</p>
							</div>
							<%--				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"><a href="<c:url value="/collectingmission"><c:param name="collMissId" value="${accessionCollect.collMissId}" /></c:url>"><c:out value="${accessionCollect.collMissId}" /></a></div> --%>
						</div>
					</c:if>
					<c:if test="${accessionCollect.collDate ne null}">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<p>
									<spring:message code="accession.collecting.date" />
								</p>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<p>
									<c:out value="${accessionCollect.collDate}" />
								</p>
							</div>
						</div>
					</c:if>
					<c:if test="${accessionCollect.collNumb ne null}">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<p>
									<spring:message code="accession.collecting.number" />
								</p>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<p>
									<c:out value="${accessionCollect.collNumb}" />
								</p>
							</div>
						</div>
					</c:if>
					<c:if test="${accessionCollect.collSite ne null}">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<p>
									<spring:message code="accession.collecting.site" />
								</p>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<p>
									<c:out value="${accessionCollect.collSite}" />
								</p>
							</div>
						</div>
					</c:if>
					<c:if test="${accessionCollect.collSrc ne null}">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<p>
									<spring:message code="accession.collecting.source" />
								</p>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<p>
									<spring:message code="accession.collectingSource.${accessionCollect.collSrc}" />
								</p>
							</div>
						</div>
					</c:if>
				</div>
			</div>
		</c:if>

		<c:if test="${accessionBreeding ne null}">
			<div class="collect-info">
				<h4 class="row section-heading">
					<spring:message code="accession.breeding" />
				</h4>
				<div class="section-inner-content clearfix">
					<c:if test="${accessionBreeding.breederCode ne null}">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<p>
									<spring:message code="accession.breederCode" />
								</p>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<p>
									<c:out value="${accessionBreeding.breederCode}" />
								</p>
							</div>
						</div>
					</c:if>
					<c:if test="${accessionBreeding.pedigree ne null}">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<p>
									<spring:message code="accession.pedigree" />
								</p>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<p>
									<c:out value="${accessionBreeding.pedigree}" />
								</p>
							</div>
						</div>
					</c:if>
				</div>
			</div>
		</c:if>


		<c:if test="${accessionGeo ne null}">
			<div class="collect-info">
				<h4 class="row section-heading">
					<spring:message code="accession.geo" />
				</h4>
				<div class="section-inner-content clearfix">


					<c:if test="${accessionGeo.latitude ne null and accessionGeo.longitude ne null}">

						<div class="row" itemscope itemtype="http://schema.org/GeoCoordinates">
							<%--  <h3>
          <spring:message code="accession.collecting.site" />
        </h3> --%>
							<div id="map" class="map-container"></div>
							<%-- <table class="map-data">
          <tr>
            <td><spring:message code="filter.geo.latitude" />:</td>
            <td><c:out value="${accessionGeo.latitude}" /></td>
          </tr>
          <tr>
            <td><spring:message code="filter.geo.longitude" />:</td>
            <td><c:out value="${accessionGeo.longitude}" /></td>
          </tr>
          <tr>
            <td><spring:message code="accession.elevation" />:</td>
            <td><c:out value="${accessionGeo.elevation}" /></td>
          </tr>
        </table> --%>
						</div>
					</c:if>


					<c:if test="${accessionCollect.collSite ne ''}">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<p>
									<spring:message code="accession.collecting.site" />
								</p>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<p>
									<c:out value="${accessionCollect.collSite}" />
								</p>
							</div>
						</div>
					</c:if>
					<c:if test="${accessionGeo.latitude ne null}">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<p>
									<spring:message code="accession.geolocation" />
								</p>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<p>
									<c:out value="${accessionGeo.latitude},${accessionGeo.longitude}" />
								</p>
							</div>
						</div>
					</c:if>
					<c:if test="${accessionGeo.elevation ne null}">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<p>
									<spring:message code="accession.elevation" />
								</p>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<p>
									<c:out value="${accessionGeo.elevation}" />
									<span class="uom">m</span>
								</p>
							</div>
						</div>
					</c:if>
					<c:if test="${accessionGeo.datum ne null}">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<p>
									<spring:message code="accession.geo.datum" />
								</p>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<p>
									<c:out value="${accessionGeo.datum}" />
								</p>
							</div>
						</div>
					</c:if>
					<c:if test="${accessionGeo.method ne null}">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<p>
									<spring:message code="accession.geo.method" />
								</p>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<p>
									<c:out value="${accessionGeo.method}" />
								</p>
							</div>
						</div>
					</c:if>
					<c:if test="${accessionGeo.uncertainty ne null}">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<p>
									<spring:message code="accession.geo.uncertainty" />
								</p>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<p>
									<c:out value="${accessionGeo.uncertainty}" />
									<span class="uom">m</span>
								</p>
							</div>
						</div>
					</c:if>
				</div>
			</div>
		</c:if>

		<!-- WorldClim.org -->
		<c:if test="${worldclimJson ne null}">
			<div class="collect-info">
				<h4 class="row section-heading">
					<spring:message code="worldclim.monthly.title" />
				</h4>
				<div class="section-inner-content clearfix">
					<!-- Charts -->
					<div class="row">
						<div class="col-xs-12 col-sm-6">
							<h5>
								<spring:message code="worldclim.monthly.temperatures.title" />
							</h5>
							<div id="temperatureChart" style="height: 150px;"></div>
						</div>
						<div class="col-xs-12 col-sm-6">
							<h5>
								<spring:message code="worldclim.monthly.precipitation.title" />
							</h5>
							<div id="precipitationChart" style="height: 150px;"></div>
						</div>
					</div>

					<!-- Data -->
					<div class="row">
						<div class="col-sm-offset-4 col-sm-8 col-xs-12">
							<div class="row">
								<c:forEach var="i" begin="0" end="11">
									<div class="col-xs-1 text-right">
										<strong><c:out value="${jspHelper.monthShortNames(pageContext.response.locale)[i]}" /></strong>
									</div>
								</c:forEach>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4 col-xs-12">
							<spring:message code="worldclim.monthly.tempMin" />
						</div>
						<div class="col-sm-8 col-xs-12">
							<div class="row">
								<c:forEach var="i" begin="0" end="11">
									<div class="col-xs-1 text-right">
										<fmt:formatNumber value="${worldclimJson.tempMin[i]}" />
									</div>
								</c:forEach>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4 col-xs-12">
							<spring:message code="worldclim.monthly.tempMean" />
						</div>
						<div class="col-sm-8 col-xs-12">
							<div class="row">
								<c:forEach var="i" begin="0" end="11">
									<div class="col-xs-1 text-right">
										<fmt:formatNumber value="${worldclimJson.tempMean[i]}" />
									</div>
								</c:forEach>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4 col-xs-12">
							<spring:message code="worldclim.monthly.tempMax" />
						</div>
						<div class="col-sm-8 col-xs-12">
							<div class="row">
								<c:forEach var="i" begin="0" end="11">
									<div class="col-xs-1 text-right">
										<fmt:formatNumber value="${worldclimJson.tempMax[i]}" />
									</div>
								</c:forEach>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4 col-xs-12">
							<spring:message code="worldclim.monthly.precipitation" />
						</div>
						<div class="col-sm-8 col-xs-12">
							<div class="row">
								<c:forEach var="i" begin="0" end="11">
									<div class="col-xs-1 text-right">
										<fmt:formatNumber value="${worldclimJson.precipitation[i]}" />
									</div>
								</c:forEach>
							</div>
						</div>
					</div>

					<%-- Needs work 
		<h4><spring:message code="worldclim.other-climate" /></h4>
		<c:forEach items="${worldclimJson.other.keySet()}" var="worldclimDescriptor">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"><c:out value="${worldclimDescriptor.title}" /></div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"><fmt:formatNumber value="${worldclimJson.other[worldclimDescriptor]}" /><span class="uom"><c:out value="${worldclimDescriptor.uom}" /></span></div>
		</div>
		</c:forEach>
		--%>
				</div>
			</div>
		</c:if>

		<c:if test="${svalbardData ne null}">
			<div class="collect-info">
				<h4>
					<spring:message code="accession.svalbard-data" />
				</h4>
				<div class="section-inner-content clearfix">
					<c:if test="${svalbardData.unitId ne null}">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<spring:message code="accession.svalbard-data.url" />
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<a target="_blank" title="<spring:message code="accession.svalbard-data.url-title" />" href="<c:url value="http://nordgen.org/sgsv/index.php?app=data_unit&unit=sgsv&unit_id=${svalbardData.unitId}" />"><spring:message
										code="accession.svalbard-data.url-text" arguments="${accession.accessionName}" /></a>
							</div>
						</div>
					</c:if>

					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<spring:message code="accession.svalbard-data.taxonomy" />
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"><c:out value="${svalbardData.taxonomy}" /></div>
					</div>

					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<spring:message code="accession.svalbard-data.depositDate" />
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"><c:out value="${svalbardData.depositDate}" /></div>
					</div>
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<spring:message code="accession.svalbard-data.boxNumber" />
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"><c:out value="${svalbardData.boxNumber}" /></div>
					</div>
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<spring:message code="accession.svalbard-data.quantity" />
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"><c:out value="${svalbardData.quantity}" /></div>
					</div>

					<c:if test="${svalbardData.acceUrl ne null}">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<spring:message code="accession.acceUrl" />
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<a href="<c:out value="${svalbardData.acceUrl}" />"><c:out value="${svalbardData.acceUrl}" /></a>
							</div>
						</div>
					</c:if>
				</div>
			</div>
		</c:if>


		<c:if test="${fn:length(accessionRemarks) gt 0}">
			<div class="collect-info">
				<h4 class="section-heading">
					<spring:message code="accession.remarks" />
				</h4>
				<div class="section-inner-content clearfix">
					<c:forEach items="${accessionRemarks}" var="accessionRemark">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<c:out value="${accessionRemark.fieldName}" />
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<c:out value="${accessionRemark.remark}" />
							</div>
						</div>
					</c:forEach>
				</div>
			</div>
		</c:if>
	</div>


	<c:if test="${pdci ne null}">
		<div class="crop-details">
			<div class="data-index">
				<h4 class="section-heading">
					<spring:message code="accession.pdci" />
				</h4>
				<div class="section-inner-content clearfix">
					<div class="jumbotron pdci-score">
						<div>
							<h3>
								<spring:message code="accession.pdci.jumbo" arguments="${pdci.score}" />
							</h3>
							<small> <c:if test="${institutePDCI ne null and institutePDCI.count > 0}">
									<spring:message code="accession.pdci.institute-avg" arguments="${institutePDCI.avg}" />
								</c:if> <a href="<c:url value="/content/passport-data-completeness-index" />"><spring:message code="accession.pdci.about-link" /></a></small>
						</div>
					</div>
					<div class="">
						<local:pdci value="${pdci}" />
					</div>
				</div>
			</div>
		</div>
	</c:if>

	<c:if test="${methods.size() gt 0}">
		<div class="collect-info">
			<h4 class="section-heading">
				<spring:message code="accession.methods" />
			</h4>
			<div class="section-inner-content clearfix">
				<c:forEach items="${methods}" var="method" varStatus="status">
					<div class="row targeted" id="method${method.id}">
						<div class="col-xs-6 col-sm-4">
							<c:out value="${method.parameter.title}" />
						</div>
						<div class="col-xs-6 col-sm-4">
							<c:forEach items="${methodValues[method.id]}" var="val">
								<div>
									<c:out value="${method.decode(val.value)}" />
									<span class="uom">
										<c:out value="${method.unit}" />
									</span>
									<sup><a href="#metadata-${val.experimentId}"><c:out value="${val.experimentId}" /></a></sup>
								</div>
							</c:forEach>
						</div>
						<div class="col-xs-12 col-sm-4">
							<c:out value="${method.method}" />
						</div>
					</div>
				</c:forEach>
			</div>
		</div>
	</c:if>

	<c:if test="${metadatas.size() gt 0}">
		<div class="collect-info">
			<h4 class="section-heading">
				<spring:message code="accession.metadatas" />
			</h4>
			<div class="section-inner-content clearfix">
				<ul class="funny-list">
					<c:forEach items="${metadatas}" var="metadata" varStatus="status">
						<li class="clearfix targeted ${status.count % 2 == 0 ? 'even' : 'odd'}" id="metadata-${metadata.id}">
							<div class="show pull-left">
								<sup><c:out value="${metadata.id}" /></sup> <a href="<c:url value="/data/view/${metadata.id}" />"><c:out value="${metadata.title}" /></a>
							</div>
							<div class="pull-right">
								<c:out value="${metadata.instituteCode}" />
							</div>
							<div class="pull-right">
								<c:out value="${metadata.description}" />
							</div>
						</li>
					</c:forEach>
				</ul>
			</div>
		</div>
	</c:if>


	<div class="section-inner-content clearfix">
		<p>
			<c:if test="${accession.lastModifiedBy ne null}">
				<spring:message code="audit.lastModifiedBy" arguments="${jspHelper.userFullName(accession.lastModifiedBy)}" />
			</c:if>
			<local:prettyTime date="${accession.lastModifiedDate}" locale="${pageContext.response.locale}" />
		</p>
	</div>

	<content tag="javascript"> <c:if test="${accessionGeo ne null and accessionGeo.latitude ne null}">
		<script type="text/javascript">
				jQuery(document).ready(function() {
					var map = L.map('map', { scrollWheelZoom: false }).setView([${accessionGeo.latitude}, ${accessionGeo.longitude}], 4);
					L.tileLayer('https://otile{s}-s.mqcdn.com/tiles/1.0.0/sat/{z}/{x}/{y}.png', {
					    attribution: "MapQuest",
					    styleId: 22677,
					    subdomains: ['1','2','3','4'],
					    opacity: 0.6
					}).addTo(map);
					var marker = L.marker([${accessionGeo.latitude}, ${accessionGeo.longitude}]).addTo(map);
				});
		</script>
	</c:if>
	<c:if test="${worldclimJson ne null}">
		<script type="text/javascript">
			var monthNames = ${jspHelper.toJson(jspHelper.monthShortNames(pageContext.response.locale))};
			var worldclim = ${jspHelper.toJson(worldclimJson)};
			function arrayToData(array) {
				var ret=[];
				for (var i=0; i<12; i++) {
					ret[i]=[i, array[i]];
				}
				return ret;
			}
			var temperatureChart = [
				{ label: "Temperatures", data: arrayToData(worldclim.tempMean), lines: { show: true }, color: "rgb(255,50,50)" },
				{ id: "max", data: arrayToData(worldclim.tempMax), lines: { show: true, lineWidth: 0, fill: false }, color: "rgb(255,50,50)" },
				{ id: "min", data: arrayToData(worldclim.tempMin), lines: { show: true, lineWidth: 0, fill: 0.2 }, color: "rgb(255,50,50)", fillBetween: "max" }
			];
			var precipitationChart = [
				{ label: "Precipitation", data: arrayToData(worldclim.precipitation), bars: { show: true, align: "center" }, color: "rgb(50,50,255)" }
			];
			var temperaturePlot = $.plot($("#temperatureChart"), temperatureChart, {
				xaxis: { tickLength: 0, tickFormatter: function(v, o) {
					return monthNames[v];
				} },
				yaxis: {
						tickFormatter: function (v) {
							return v + " °C";
						}
				},
				legend: { show: false }
			});
			var precipitationPlot = $.plot($("#precipitationChart"), precipitationChart, {
				xaxis: { tickLength: 0, tickFormatter: function(v, o) {
					return monthNames[v];
				} },
				yaxis: {
						tickFormatter: function (v) {
							return v + " mm";
						}
				},
				legend: { show: false }
			});
			$( window ).resize(function() {
				precipitationPlot.resize();
				precipitationPlot.setupGrid();
				precipitationPlot.draw();
				
				temperaturePlot.resize();
				temperaturePlot.setupGrid();
				temperaturePlot.draw();
			});
		</script>
	</c:if>
	<c:if test="${imageGallery ne null}">
		<script type="text/javascript">
  		function fillMetadata(metadataBox, metadata) {
  	  		var box=$(metadataBox);
  	  		box.children().hide();
  	  		for (var i in metadata) {
//				console.log(i);
				var elem = box.find('.' + i)[0];
//				console.log(elem);
				if (elem !== undefined) {
					$(elem).children().first().html('');
				}
				if (metadata[i] === null || metadata[i].length === 0) {
					continue;
				}
				if (elem !== undefined) {
					$(elem).children().first().text(metadata[i]);
					$(elem).show();
				}
  	  	  	}
			var elem = box.find('.downloadLink').show().find('a').first().attr('href', box.parent().parent().find('.theimage').first().attr('src'));
  	  	}
 		function showImage(imagegalleryFrame, imageUuid, imageExt) {
			var baseHref = '<c:url value="/repository/d${imageGallery.path}" />';
			var imageViewer = $(imagegalleryFrame).find('.theimage').first();
			var metadataBox = $(imagegalleryFrame).find('.metadata').first();
			imageViewer.attr('src', baseHref + imageUuid + imageExt);
//			console.log('Image source: ' + imageViewer.src)
//			console.log(imageViewer.attr('src'));
			$(imagegalleryFrame).show();
			$(metadataBox).hide();

			// Load metadata
			$.ajax(baseHref + imageUuid + imageExt + "?metadata", {
				method: 'get',
				dataType : 'json',
				success : function(respObject) {
//					console.log(respObject);
					fillMetadata(metadataBox, respObject);
					$(metadataBox).show();
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(textStatus);
					console.log(errorThrown);
				}
			});
 	 	}
		$(document).ready(function() {
			var galleryView=$('#accession-image-view')[0];
			var galleryThumbnails = $('#accession-images-thumbs div');
			galleryThumbnails.click(function(ev) {
				showImage(galleryView, $(this).attr('x-uuid'),  $(this).attr('x-ext'));
			});
			if (galleryThumbnails.length > 0) {
				// Show first image
				showImage(galleryView, $(galleryThumbnails[0]).attr('x-uuid'), $(galleryThumbnails[0]).attr('x-ext'));
			}
		});
	</script>
	</c:if>
	<script type="text/javascript">
		<%@ include file="/WEB-INF/jsp/wiews/ga.jsp" %>
		_pageDim = { institute: '${accession.instituteCode}', genus: '${accession.taxonomy.genus}' };
	</script>
	</content>
</body>
</html>