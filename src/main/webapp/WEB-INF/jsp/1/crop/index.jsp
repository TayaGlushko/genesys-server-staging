<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
	<title><c:out value="${crop.getName(pageContext.response.locale)}" /></title>
	<meta name="description" content="<c:out value="${jspHelper.htmlToText(blurp.summary)}" />" />
</head>
<body class="crop-page">
<h1 class="green-bg"><c:out value="${crop.getName(pageContext.response.locale)}"/></h1>

<security:authorize access="hasRole('ADMINISTRATOR') or hasPermission(#crop, 'ADMINISTRATION')">
	<a href="<c:url value="/acl/${crop.getClass().name}/${crop.id}/permissions"><c:param name="back"><c:url value="/c/${crop.shortName}" /></c:param></c:url>"
	   class="close">
		<spring:message code="edit-acl"/>
	</a>
</security:authorize>
<security:authorize access="hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER') or hasPermission(#crop, 'ADMINISTRATION')">
	<a href="<c:url value="/c/${crop.shortName}/edit" />" class="close">
		<spring:message code="edit"/>
	</a>
</security:authorize>

<gui:alert type="error" display="${crop eq null}">
	<spring:message code="data.error.404"/>
</gui:alert>

<c:if test="${blurp ne null}">
<div class="collect-info about-section">
	<div class="section-inner-content">
		<div class="row">
			<div class="preview col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<c:out value="${blurp.summary}" escapeXml="false" />
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<cms:blurb blurb="${blurp}"/>
			</div>
		</div>
	</div>
</div>
</c:if>

<div class="collect-info general-info">
	<h4 class="section-heading">
		<spring:message code="heading.general-info" />
	</h4>
	<div class="section-inner-content">
		<div class="statistics">
			<div class="row">

				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<p>
						<spring:message code="faoInstitutes.stat.accessionCount" />
						<span class="stats-number">
							<fmt:formatNumber value="${cropCount}" />
						</span>
					</p>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<a class="btn btn-primary" title="" href="<c:url value="/c/${crop.shortName}/data" />"> <span class="glyphicon glyphicon-list"></span> <spring:message code="view.accessions" /></a> <a
						class="btn btn-default" title="" href="<c:url value="/c/${crop.shortName}/overview" />"> <span class="glyphicon glyphicon-eye-open"></span> <spring:message code="data-overview.short" /></a> <a
						class="btn btn-default" href="<c:url value="/c/${crop.shortName}/descriptors" />"><spring:message code="crop.view-descriptors" /></a>
				</div>
			</div>
		</div>
	</div>
</div>

	<div class="collect-info map-section">
	<h4 class="section-heading"><spring:message code="chart.collections.title" /></h4>
	<div class="section-inner-content clearfix">
		<div class="map-wrapper">
			<div id="cropMap" style="min-height: 500px; min-width: 200px; margin: 0 auto"></div>
		</div>
	</div>
</div>

<div class="collect-info tax-rules">
	<h4 class="section-heading"><spring:message code="crop.taxonomy-rules"/></h4>
	<div class="section-inner-content">
		<div class="row">
		<c:forEach items="${cropRules}" var="rule">
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<p class="${rule.included ? '' : 'excluded'}">
					<b><c:out value="${rule.included ? '+' : '-'}" /></b>
					<c:out value="${rule.genus}" />
					<c:out value="${rule.species eq null ? '*' : rule.species}" />
				</p>
			</div>
		</c:forEach>
		<c:forEach items="${cropTaxonomies.content}" var="cropTaxonomy" varStatus="status">
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<p>
					<a href="<c:url value="/acn/t/${cropTaxonomy.taxonomy.genus}/${cropTaxonomy.taxonomy.species}" />"><c:out value="${cropTaxonomy.taxonomy.taxonName}" /></a>
				</p>
			</div>
		</c:forEach>
		<c:if test="${cropTaxonomies.hasNext()}">
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<p id="loadMoreTaxonomies">
					<spring:message code="label.load-more-data" />
				</p>
			</div>
		</c:if>		
		</div>
	</div>
</div>

	
<content tag="javascript">
	<script type="text/javascript">
		jQuery(document).ready(function () {
			$("body").on("click", "#loadMoreTaxonomies", function (event) {
				event.preventDefault();
				var loader = $(this);
				var page = loader.attr("page") || 2;
				$.ajax({
					url: "<c:url value="/c/${crop.shortName}/ajax/taxonomies" />",
					type: "GET",
					data: {
						"page": page
					},
					success: function (data) {
						loader.parent().before(data);
						loader.attr("page", parseInt(page) + 1);
					},
					error: function (err) {
						loader.remove();
					}
				});
			});
		});
	</script>

    <script type="text/javascript" src="<c:url value="/html/js/genesyshighcharts.min.js" />"></script>
    <script type="text/javascript">
        $(function () {
            'use strict';

            $.getJSON('/${pageContext.response.locale}/explore/0/charts/data/country-collection-size?filter=${jsonFilter}', function (data) {
                var mapData = Highcharts.geojson(Highcharts.maps['custom/world']);

                $('#cropMap').highcharts('Map', {
                    chart: {
                        borderWidth: 0
                    },

                    title: {
                        text: '<spring:message code="chart.collections.title" />'
                    },
<%--
                    subtitle: {
                        text: '<spring:message code="chart.attribution-text" />' 
                    },
--%>
                    legend: {
                        enabled: false
                    },

                    mapNavigation: {
                        enabled: true,
                        buttonOptions: {
                            verticalAlign: 'top'
                        }
                    },

                    series: [{
                        name: 'World',
                        mapData: mapData,
                        color: '#88ba42',
                        enableMouseTracking: false
                    }, {
                        type: 'mapbubble',
                        mapData: mapData,
                        name: '<spring:message code="chart.collections.series" />',
                        joinBy: ['iso-a2', 'code'],
                        data: data,
                        sizeBy: 'width',
                        color: '#88ba42',
                        minSize: '3',
                        maxSize: '30%',
                        tooltip: {
                            pointFormat: '{point.country}: {point.z}'
                        }
                    }]
                });
            });
        });
    </script>
</content>
</body>
</html>