<%@ include file="/WEB-INF/jsp/init.jsp" %>

<c:forEach items="${cropTaxonomies.content}" var="cropTaxonomy" varStatus="status">
	<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
		<p>
			<a href="<c:url value="/acn/t/${cropTaxonomy.taxonomy.genus}/${cropTaxonomy.taxonomy.species}" />"><c:out value="${cropTaxonomy.taxonomy.taxonName}" /></a>
		</p>
	</div>
</c:forEach>
