<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
    <title><spring:message code="faoInstitutes.page.list.title"/></title>
</head>
<body class="institutes-page">
	<cms:informative-h1 title="faoInstitutes.page.list.title" fancy="true" info="" />

	<div class="main-col-header clearfix">
		<div class="nav-header clearfix">
			<div class="pull-right list-view-controls">
				<c:if test="${activeOnly eq true}">
					<a href="<c:url value="/wiews/active/map" />" class="list-map-view">
						<spring:message code="maps.view-map" />
					</a>
					<a href="<c:url value="/wiews/" />" class="list-all-view">
						<spring:message code="faoInstitutes.viewAll" />
					</a>
				</c:if>
				<c:if test="${activeOnly ne true}">
					<a href="<c:url value="/wiews/active" />" class="list-all-view">
						<spring:message code="faoInstitutes.viewActiveOnly" />
					</a>
				</c:if>
			</div>
			<local:paginate2 page="${pagedData}"/>
		</div>
	</div>

	<div class="row institute-list">
			<c:forEach items="${pagedData.content}" var="faoInstitute" varStatus="status">
				<div class="col-sm-12 clearfix ${faoInstitute.current ? '' : 'not-current'}">
					<a class="show pull-left" href="<c:url value="/wiews/${faoInstitute.code}" />">
						<b>
							<c:out value="${faoInstitute.code}" />
						</b>
						<c:out value="${faoInstitute.fullName}" />
					</a>

					<div class="pull-right">
						<spring:message code="faoInstitute.accessionCount" arguments="${faoInstitute.accessionCount}" />
					</div>
				</div>
			</c:forEach>
	</div>
	
	<local:paginate2 page="${pagedData}"/>
	

</body>
</html>