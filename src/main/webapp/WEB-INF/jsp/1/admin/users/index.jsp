<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
<title><spring:message code="user.page.list.title" /></title>
</head>
<body>
	<table class="table table-striped">
		<thead>
			<tr>
				<th class="col-xs-5"><spring:message code="registration.full-name" /></th>
				<th class="col-xs-2"> 1</th>
				<th class="col-xs-5"><spring:message code="registration.email" /></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${pagedData.content}" var="user" varStatus="status">
				<tr>
					<td class="col-xs-5"><c:if test="${not user.systemAccount}">
							<a href="<c:url value="/admin/users/${user.uuid}" />"><c:out
									value="${user.name}" /></a>
						</c:if></td>
					<td class="col-xs-2"><c:if test="${user.systemAccount}">SYSTEM</c:if>
						<c:if test="${not user.enabled}">DISABLED</c:if> <c:if
							test="${user.accountLocked}">LOCKED</c:if></td>
					<td class="col-xs-5"><c:out value="${user.email}" /></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>

	<local:paginate2 page="${pagedData}" />
</body>
</html>