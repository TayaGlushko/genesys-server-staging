<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
<title><spring:message code="userprofile.update.title" /></title>
</head>
<body>
	<!-- Any errors? -->
	<gui:alert type="danger" display="${not empty emailError}">
		<c:out value="${emailError}" />
	</gui:alert>

	<form role="form" class="form-horizontal validate" action="<c:url value="/admin/users/${user.uuid}/update" />" method="post">
		<div class="form-group">
			<label for="name" class="col-lg-2 control-label"><spring:message code="registration.full-name" /></label>
			<div class="col-lg-3">
				<input type="text" id="name" name="name" class="span3 form-control" value="${user.name}" />
			</div>
		</div>
		<div class="form-group">
			<label for="email" class="col-lg-2 control-label"><spring:message code="registration.email" /></label>
			<div class="col-lg-3">
				<input type="text" id="email" name="email" class="span3 form-control" value="${user.email}" />
			</div>
		</div>

		<c:if test="${user.loginType eq 'PASSWORD'}">
			<div class="form-group">
				<label for="currentPassword" class="col-lg-2 control-label"><spring:message code="registration.current-password" /></label>
				<div class="col-lg-3">
					<input type="password" id="currentPassword" name="currentPwd" class="span3 form-control" autocomplete="off" />
				</div>
			</div>
			<div class="form-group">
				<label for="password" class="col-lg-2 control-label"><spring:message code="registration.password" /></label>
				<div class="col-lg-3">
					<input type="password" id="password" name="pwd1" class="span3 form-control" autocomplete="off" />
				</div>
			</div>
			<div class="form-group">
				<label for="confirm_password" class="col-lg-2 control-label"><spring:message code="registration.confirm-password" /></label>
				<div class="col-lg-3">
					<input type="password" id="confirm_password" name="pwd2" class="span3 required form-control" autocomplete="off" equalTo="#pwd1" />
				</div>
			</div>
		</c:if>

		<div class="form-group">
			<div class="col-lg-offset-2 col-lg-10">
				<input type="submit" value="<spring:message code="save"/>" class="btn btn-primary" />
				<a class="btn btn-default" href="<c:url value="/admin/users/${user.uuid}" />" class="btn"> <spring:message code="cancel" />
				</a>
			</div>
		</div>
        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form>
	
	<security:authorize access="hasRole('ADMINISTRATOR')">
	<h1>
		<spring:message code="user.roles" />
	</h1>
	<form role="form" class="form-horizontal validate" action="<c:url value="/profile/${user.uuid}/update-roles" />" method="post">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		<c:forEach items="${availableRoles}" var="role">
		<div class="form-group">
			<div class="col-lg-12">
				<label><input type="checkbox" name="role" value="<c:out value="${role}" />" ${user.hasRole(role) ? 'checked="true"' : ''} /><c:out value="${role}" /></label>
			</div>
		</div>
		</c:forEach>
		<div class="form-group">
			<div class="col-lg-offset-2 col-lg-10">
				<input type="submit" value="<spring:message code="save"/>" class="btn btn-primary" />
			</div>
		</div>
	</form>
	</security:authorize>
</body>
</html>