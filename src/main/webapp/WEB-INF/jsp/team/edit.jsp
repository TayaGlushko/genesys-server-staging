<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
<title><spring:message code="team.profile.update.title" /></title>
</head>
<body>
	<h1>
		<spring:message code="team.profile.update.title" />
	</h1>

	<form role="form" class="form-horizontal validate" action="<c:url value="/team/${team.uuid}/update" />" method="post">
		<div class="form-group">
			<label for="name" class="col-lg-2 control-label"><spring:message code="team.team-name" /></label>
			<div class="col-lg-3">
				<input type="text" id="name" name="teamName" class="span3 form-control" value="${team.name}" />
			</div>
		</div>

		<div class="form-group">
			<div class="col-lg-offset-2 col-lg-10">
				<input type="submit" value="<spring:message code="save"/>" class="btn btn-primary" />
                <a class="btn btn-default" href="<c:url value="/team/${team.uuid}" />" class="btn"> <spring:message code="cancel" />
				</a>
			</div>
		</div>
        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form>
</body>
</html>