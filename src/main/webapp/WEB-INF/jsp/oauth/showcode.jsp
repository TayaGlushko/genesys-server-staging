<%@ include file="/WEB-INF/jsp/init.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><c:out value="${code}" /></title>
</head>
<body>
	<h1><spring:message code="oauth2.authorization-code" /></h1>
	<div class="jumbotron">
		<spring:message code="oauth2.authorization-code-instructions" />
		<b><c:out value="${code}" /></b>
	</div>
</body>
</html>
