<!DOCTYPE html>

<%@ include file="init.jsp" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
	<title><spring:message code="page.home.title" /></title>
	<link href="<c:url value="/content/news/feed.atom" />" type="application/atom+xml" rel="alternate" title="Genesys News ATOM Feed" />
</head>
<body>
	<div class="row no-space intro">
		<div class="hidden-xs col-sm-6">
			<div class="intro-image">
				<img src="<c:url value="/html/0/images/aim/svalbard.jpg" />" />
			</div>
		</div>
		<div class="col-xs-12 col-sm-6">
			<div class="intro-text">
				<c:if test="${welcomeBlurp ne null}">
					<div class="">
						<cms:blurb blurb="${welcomeBlurp}" />
						<a class="btn btn-default" href="<c:url value="/content/about/about" />"><spring:message code="welcome.read-more" /></a>
					</div>
				</c:if>
			</div>
		</div>	
	</div>

	<div class="row no-space">
		<div id="stats" class="col-sm-4">
			<h2 class="short">
				<spring:message code="maps.accession-map" />
			</h2>

			<div class="stats-map padding10">
				<p><spring:message code="maps.accession-map.intro" /></p>
				<div class="all-stats">
					<%-- 	        <div class="one-stat"><a href="<c:url value="/geo/" />"><spring:message code="stats.number-of-countries" arguments="${numberOfCountries}" /></a></div> --%>
					<div class="one-stat">
						<a href="<c:url value="/explore" />">
							<spring:message code="stats.number-of-accessions" arguments="${numberOfActiveAccessions}" />
						</a>
					</div>
					<%-- <div class="one-stat">
						<a href="<c:url value="/explore"><c:param name="filter" value='{"historic":[true]}' /></c:url>">
							<spring:message code="stats.number-of-historic-accessions" arguments="${numberOfHistoricAccessions}" />
						</a>
					</div> --%>
					<div class="one-stat">
						<a href="<c:url value="/wiews/active" />">
							<spring:message code="stats.number-of-institutes" arguments="${numberOfInstitutes}" />
						</a>
					</div>
				</div>
			</div>
		</div>

		<div class="col-sm-8">
		<%-- 	<h2>
				<spring:message code="maps.accession-map" />
			</h2> --%>
			<div class="">
				<div id="globalmap" x-href="<c:url value="/explore/map" />" class="gis-map"></div>
			</div>
		</div>
	</div>

	<div class="hidden-xs row see-also-row">
		<div class="col-sm-4">
			<div class="see-also-block" style="background-image: url('<c:url value="/html/0/images/aim/three-1.jpg" />')">
				<div class="content">
					<h2 class="short">
						<spring:message code="search.input.placeholder" />
					</h2>
					<div class="padding10 white-background body">
						<p><spring:message code="search.search-query-missing" /></p>
						<form action="<c:url value="/acn/search2" />" class="form-inline">
							<div class="form-group">
								<input type="text" name="q" class="form-control" />
								<input type="submit" class="btn btn-primary" value="<spring:message code="search.button.label" />" />
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="see-also-block" style="background-image: url('<c:url value="/html/0/images/aim/three-2.jpg" />')">
				<div class="content">
					<h2 class="short">
						<spring:message code="organization.page.list.title" />
					</h2>
					<div class="padding10 white-background body" dir="ltr">
						<a href="<c:url value="/org/CGIAR" />">CGIAR International Genebanks</a>
						&bull;
						<a href="<c:url value="/org/EURISCO" />">ECPGR EURISCO network</a>
						&bull;
						<a href="<c:url value="/org/USDA" />">USDA ARS NPGS</a>
						&bull;
						<a href="<c:url value="/org/COGENT" />">COGENT</a>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="see-also-block" x-href="<c:url value="/content/help/how-to-use-genesys" />" style="background-image: url('<c:url value="/html/0/images/aim/three-3.jpg" />')">
				<div class="content">
					<h2 class="short">
						<spring:message code="menu.help" />
					</h2>
					<div class="padding10 white-background body"><a><spring:message code="help.page.intro" /></a></div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">

		<!-- left column end / middle columns start -->
		<div class="col-md-8" id="middle-col">
			<h2>
				<spring:message code="activity.recent-activity" />
			</h2>

			<div class="tab-content">
				<div class="tab-pane active" id="news-feed">
					<security:authorize access="hasRole('ADMINISTRATOR')">
						<a href="<c:url value="/content/activitypost/new" />" class="pull-right close" style="">
							<spring:message code="activitypost.add-new-post" />
						</a>
					</security:authorize>

					<div class="all-posts">
						<c:forEach items="${lastNews}" var="activityPost" varStatus="status">
							<cms:activitypost activityPost="${activityPost}" />
						</c:forEach>
					</div>

					<a id="show-moar-news" href="<c:url value="/content/news" />"><spring:message code="news.content.page.all.title" /></a>
				</div>
			</div>
		</div>

		<!-- middle column end / right columns start -->
		<div class="col-md-4" id="right-col">
			<c:if test="${sideBlurp ne null}">
				<div class="content-block">
					<h2><c:out value="${sideBlurp.title}" /></h2>
					<cms:blurb blurb="${sideBlurp}" />
				</div>
			</c:if>
			
			
			<div class="content-block" id="crop-list-dropdown">
				<h2>
					<spring:message code="crop.croplist" />
				</h2>
				<sec:authorize access="hasRole('ADMINISTRATOR')">
					<form method="post" action="<c:url value="/c/rebuild" />">
						<input type="submit" class="btn form-control" value="Rebuild" />
						<!-- CSRF protection -->
						<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
					</form>
				</sec:authorize>
				<div class="dropdown">
					<input class="form-control autocomplete-genus" placeholder="<spring:message code="autocomplete.genus" />" x-source="<c:url value="/explore/ac/taxonomy.genus" />" />
				</div>
			</div>

			<c:if test="${cropList ne null and cropList.size() gt 0}">
				<div class="content-block" id="crop-list">
					<ul class="nav">
						<li class="all-crops">
							<a class="show" href="<c:url value="/explore/" />">
								<spring:message code="crop.all-crops" />
							</a>
						</li>

						<c:forEach items="${cropList}" var="crop" varStatus="status">
							<li>
								<a class="show" href="<c:url value="/explore/c/${crop.shortName}" />">
									<c:out value="${crop.getName(pageContext.response.locale)}" />
								</a>
							</li>
						</c:forEach>
					</ul>
				</div>
			</c:if>
		</div>
	</div>
	
	
	<content tag="javascript">
<script type="text/javascript">
  $( document ).ready(function() {
    $(".autocomplete-genus").each(function() {
		var t=$(this); 
		t.autocomplete({ delay: 200, minLength: 3, source: t.attr('x-source'),  
			messages: { noResults: '', results: function() {} },
			select: function(event, ui) { document.location.pathname='/acn/t/'+ui.item.value; } });
	});
  });
</script>
</content>

</body>
</html>