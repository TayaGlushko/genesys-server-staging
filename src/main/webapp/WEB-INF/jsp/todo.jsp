<!DOCTYPE html>

<%@ include file="init.jsp" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
<title><spring:message code="page.home.title" /></title>
</head>
<body>
	<h1>:-(</h1>
	<p>Sorry, but this function is not implemented. We're working on it.</p>
</body>
</html>