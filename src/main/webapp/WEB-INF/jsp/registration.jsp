<!DOCTYPE html>

<%@ include file="init.jsp" %>

<html>
<head>
<title><spring:message code="registration.page.title" /></title>
</head>
<body>
	<h1>
		<spring:message code="registration.title" />
	</h1>
	
	<security:authorize access="hasRole('ADMINISTRATOR')">
		<a href="<c:url value="/content/registration/edit" />" class="close"> <spring:message code="edit" />
		</a>
	</security:authorize>

	<cms:blurb blurb="${blurp}" />

	<gui:alert type="danger" display="${not empty error}">
		<spring:message code="${error}" />
	</gui:alert>

	<gui:alert type="error" display="${param['exist'] ne null}">
		<spring:message code="registration.user-exists" />
	</gui:alert>

	<form role="form" method="POST" action="new-user.html" class="form-horizontal validate">
		<div class="form-group">
			<label for="email" class="col-lg-2 control-label"><spring:message code="registration.email" /></label>
			<div class="col-lg-3">
				<input type="text" id="email" name="email" class="span3 required email form-control" />
			</div>
		</div>
		<div class="form-group">
			<label for="password" class="col-lg-2 control-label"><spring:message code="registration.password" /></label>
			<div class="col-lg-3">
				<input type="password" id="password" name="password" class="span3 required form-control" />
			</div>
		</div>
		<div class="form-group">
			<label for="confirm_password" class="col-lg-2 control-label"><spring:message code="registration.confirm-password" /></label>
			<div class="col-lg-3">
				<input type="password" id="confirm_password" name="confirm_password" class="span3 required form-control" equalTo="#password" />
			</div>
		</div>
		<div class="form-group">
			<label for="name" class="col-lg-2 control-label"><spring:message code="registration.full-name" /></label>
			<div class="col-lg-3">
				<input type="text" id="name" name="name" class="span3 required form-control" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-lg-2 control-label"><spring:message code="captcha.text" /></label>
			<div class="col-lg-3">
				<local:captcha siteKey="${captchaSiteKey}" />
			</div>
		</div>
		<div class="form-group">
			<div class="col-lg-offset-2 col-lg-10">
				<input type="submit" value="<spring:message code="registration.create-account"/>" class="btn btn-primary" /> <a class="btn btn-default" href="<c:url value="/" />" id="registration" class="btn"> <spring:message code="cancel" />
				</a>
			</div>
		</div>
        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form>

<content tag="javascript">
<script type="text/javascript" src="/html/js/login.js"></script>
</content>
</body>
</html>