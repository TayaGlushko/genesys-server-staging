<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
<title><spring:message code="country.page.list.title" /></title>
</head>
<body>
	<cms:informative-h1 title="country.page.list.title" fancy="true" info="country.page.list.intro" />

	<div class="main-col-header clearfix">
	<div class="nav-header pull-left">
		<div class="results">
			<spring:message code="paged.totalElements" arguments="${countries.size()}" />
		</div>
	</div>
	</div>

	<div id="letter-top" class="main-col-header">
	<c:set value="" var="hoofdleter" />
	<c:forEach items="${countries}" var="country" varStatus="status">
		<c:if test="${hoofdleter ne country.getName(pageContext.response.locale).substring(0, 1)}">
			<c:set var="hoofdleter" value="${country.getName(pageContext.response.locale).substring(0, 1)}" />
			<a class="letter-pointer" href="#letter-${hoofdleter}"><c:out value="${hoofdleter}" /></a>
		</c:if>
	</c:forEach>
	</div>


	<c:set value="" var="hoofdleter" />
	<div class="row">
		<ul class="funny-list">
			<c:forEach items="${countries}" var="country" varStatus="status">
				<c:if test="${hoofdleter ne country.getName(pageContext.response.locale).substring(0, 1)}">
					<c:set var="hoofdleter" value="${country.getName(pageContext.response.locale).substring(0, 1)}" />
					<li id="letter-${hoofdleter}" class="hoofdleter"><c:out value="${hoofdleter}" />
						<small><a href="#letter-top"><spring:message code="jump-to-top" /></a></small>
					</li>
				</c:if>
				<li><a class="show ${not country.current ? 'disabled' : ''}" href="<c:url value="/geo/${country.code3}" />"><c:out value="${country.getName(pageContext.response.locale)}" /></a></li>
			</c:forEach>
		</ul>
	</div>
	<c:remove var="hoofdleter" />

</body>
</html>