<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
<title><spring:message code="country.page.profile.title" arguments="${country.getName(pageContext.response.locale)}" argumentSeparator="|" /></title>

<local:content-headers description="${jspHelper.htmlToText(blurp.summary, 150)}" title="${country.getName(pageContext.response.locale)}" />
</head>
<body>
	<h1>
		<c:out value="${country.getName(pageContext.response.locale)}" />
		<img class="country-flag bigger" src="<c:url value="${cdnFlagsUrl}/${country.code3.toUpperCase()}.svg" />" />
	</h1>

	<!-- Is country current? -->
	<gui:alert type="info" display="${not country.current}">
		<spring:message code="country.page.not-current" />
		<c:if test="${country.replacedBy != null}">
			<spring:message code="country.replaced-by" arguments="${country.replacedBy.code3}" />
			<a href="${country.replacedBy.code3}"><c:out value="${country.replacedBy.getName(pageContext.response.locale)}" /></a>
		</c:if>	
	</gui:alert>
	
	<div class="jumbotron">
		<spring:message code="country.stat.countByOrigin" arguments="${countByOrigin}" />
		<c:if test="${countByOrigin gt 0}">
			<a href="<c:url value="/geo/${country.code3}/data" />"><spring:message code="view.accessions" /></a>
			<a href="<c:url value="/geo/${country.code3}/overview" />"><spring:message code="data-overview.short" /></a>
		</c:if>
	</div>

	<security:authorize access="hasRole('ADMINISTRATOR')">
		<a href="<c:url value="/geo/${country.code3}/edit" />" class="close"> <spring:message code="edit" />
		</a>
	</security:authorize>

	<cms:blurb blurb="${blurp}" />

	<gui:alert type="info" display="${itpgrfa != null and itpgrfa.contractingParty=='Yes'}">
		<spring:message code="country.is-itpgrfa-contractingParty" arguments="${country.getName(pageContext.response.locale)}" argumentSeparator="|" />
	</gui:alert>

	<div>
		ISO-3166 3-alpha:
		<c:out value="${country.code3}" />
		<br /> ISO-3166 2-alpha:
		<c:out value="${country.code2}" />
	</div>
	<c:if test="${country.wikiLink ne null}">
		<div>
			<spring:message code="country.more-information" />
			<a target="_blank" rel="nofollow" href="<c:out value="${country.wikiLink}" />"><c:out value="${country.wikiLink}" /></a>
		</div>
	</c:if>
	

	<%-- Show map? --%>
	<c:forEach items="${genesysInstitutes}" var="faoInstitute" varStatus="status">
		<c:if test="${faoInstitute.latitude ne null and faoInstitute.longitude ne null}">
			<c:set value="true" var="showMap" />
		</c:if>
	</c:forEach>
	<c:if test="${showMap!=null && showMap}">
		<div class="row" style="">
		<div class="col-sm-12">
			<div id="map" class="gis-map"></div>
		</div>
		</div>
	</c:if>
	
	
	<%-- 	<h3>
		<spring:message code="country.statistics" />
	</h3>
 --%>
	<h3>
		<spring:message code="country.stat.countByLocation" arguments="${countByLocation}" />
	</h3>
	<ul class="funny-list">
		<c:forEach items="${genesysInstitutes}" var="faoInstitute" varStatus="status">
			<li class="clearfix ${status.count % 2 == 0 ? 'even' : 'odd'}"><a class="show pull-left" href="<c:url value="/wiews/${faoInstitute.code}" />"><b><c:out value="${faoInstitute.code}" /></b> <c:out value="${faoInstitute.fullName}" /></a>
				<div class="pull-right">
					<spring:message code="faoInstitute.accessionCount" arguments="${faoInstitute.accessionCount}" />
				</div></li>
		</c:forEach>
	</ul>

	<h4>
		<spring:message code="country.page.faoInstitutes" arguments="${faoInstitutes.size()}" />
	</h4>

	<ul class="funny-list">
		<c:forEach items="${faoInstitutes}" var="faoInstitute" varStatus="status">
			<li class="${status.count % 2 == 0 ? 'even' : 'odd'}"><a class="show" href="<c:url value="/wiews/${faoInstitute.code}" />"><b><c:out value="${faoInstitute.code}" /></b> <c:out value="${faoInstitute.fullName}" /></a></li>
		</c:forEach>
	</ul>


<c:if test="${showMap!=null and showMap}">
<content tag="javascript">
	<script type="text/javascript">
		jQuery(document).ready(function() {
			var map=GenesysMaps.map("${pageContext.response.locale.language}", $("#map"), {
				maxZoom: 6, /* WIEWS does not provide enough detail */
				zoom: 6
			}, function (opts, map) {
				var bounds=new GenesysMaps.BoundingBox();
				var marker;
				<c:forEach items="${genesysInstitutes}" var="faoInstitute" varStatus="status">
				<c:if test="${faoInstitute.latitude ne null and faoInstitute.longitude ne null}">
				marker = L.marker([${faoInstitute.latitude}, ${faoInstitute.longitude}]).addTo(map);
				marker.bindPopup("<a href='<c:url value="/wiews/${faoInstitute.code}" />'><c:out value="${faoInstitute.fullName}" /></a>");
				bounds.add([${faoInstitute.latitude}, ${faoInstitute.longitude}]);
				</c:if>
				</c:forEach>
				map.fitBounds(bounds.getBounds());
			});
		});
	</script>
</content>
</c:if>

</body>
</html>