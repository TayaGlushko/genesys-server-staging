<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
<title><spring:message code="userprofile.password" /></title>
</head>
<body>
	<h1>
		<spring:message code="userprofile.password" />
	</h1>

	<cms:blurb blurb="${blurp}" />

	<gui:alert type="danger" display="${not empty error}">
		<spring:message code="${error}" />
	</gui:alert>

	<form class="form-horizontal" action="<c:url value="/profile/password/reset"/>" method="post">
		<div class="form-group">
			<label class="col-lg-2 control-label">
				<spring:message code="captcha.text" />
			</label>
			<div class="col-lg-3">
				<local:captcha siteKey="${captchaSiteKey}" />
			</div>
		</div>
		<div class="form-group">
			<label for="email" class="col-lg-2 control-label">
				<spring:message code="userprofile.enter.email" />
			</label>
			<div class="col-lg-3">
				<input type="text" id="email" name="email" class="span3 form-control" />
			</div>
			<div class="col-lg-1">
				<input type="submit" value="<spring:message code="userprofile.email.send" />" class="btn btn-primary" />
			</div>
		</div>
		<!-- CSRF protection -->
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	</form>

</body>
</html>