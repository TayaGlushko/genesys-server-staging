<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
<title><spring:message code="user.page.list.title" /></title>
</head>
<body>
	<h1>
		<spring:message code="user.page.list.title" />
	</h1>

	<div class="main-col-header clearfix">
	<div class="nav-header pull-left">
		<local:paginate page="${pagedData}" />
	</div>
	</div>
	
	<table class="funny-list">
		<c:forEach items="${pagedData.content}" var="user" varStatus="status">
			<tr class="clearfix ${status.count % 2 == 0 ? 'even' : 'odd'}">
				<td><c:if test="${not user.systemAccount}"><a href="<c:url value="/profile/${user.uuid}" />"><c:out value="${user.name}" /></a></c:if></td>
				<td><c:out value="${user.uuid}" /></td>
				<td><c:out value="${user.email}" /></td>
				<td>
					<c:if test="${user.systemAccount}">SYSTEM</c:if>
					<c:if test="${not user.enabled}">DISABLED</c:if>
					<c:if test="${user.accountLocked}">LOCKED</c:if>
				</td>
			</tr>
		</c:forEach>
	</table>


</body>
</html>