<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
<title><spring:message code="selection.page.title" /></title>
</head>
<body>
	<cms:informative-h1 title="selection.page.title" fancy="true" info="selection.page.intro" />

	<gui:alert type="warning" display="${resultFromSave ne null}">
		<spring:message code="${resultFromSave}" />
	</gui:alert>

	<c:if test="${pagedData == null}">
	    <gui:alert type="info">
	        <spring:message code="selection.empty-list-warning"/>
	    </gui:alert>
	    
	    <security:authorize access="isAuthenticated()">
	    	<c:if test="${fn:length(userAccessionLists) gt 0}">
			<div class="">
				<spring:message code="userlist.list-my-lists" /> 
		        <ul class="funny-list" role="" id="user-accession-lists">
		            <c:forEach items="${userAccessionLists}" var="userList">
		                <li><a href="/sel/load?list=${userList.uuid}"><c:out value="${userList.title}" /></a>
		                <div class="pull-right"><c:out value="${userList.description}" /></div>
		                </li>
		            </c:forEach>
		        </ul>
			</div>
			</c:if>
		</security:authorize>
	</c:if>

<c:if test="${pagedData != null}">

 		<div class="main-col-header clearfix">
		<div class="nav-header">
			<div class="results"><spring:message code="accessions.number" arguments="${pagedData.totalElements}" /></div>

			<div class="row">
				<div class="col-sm-12 col-md-6">
					<div class="pagination">
						<spring:message code="paged.pageOfPages" arguments="${pagedData.number+1},${pagedData.totalPages}" />
						<a href="<spring:url value=""><spring:param name="page" value="${pagedData.number eq 0 ? 1 : pagedData.number}" /><spring:param name="filter" value="${jsonFilter}" /></spring:url>"><spring:message code="pagination.previous-page" /></a>
						<a href="<spring:url value=""><spring:param name="page" value="${pagedData.number+2}" /><spring:param name="filter" value="${jsonFilter}" /></spring:url>"><spring:message code="pagination.next-page" /></a>
					</div>
				</div>
				<div class="col-sm-12 col-md-6" style="text-align: right; padding-top: 12px">
					<form style="display: inline-block" method="post" action="<c:url value="/sel/dwca" />">
						<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
						<button class="btn btn-default" type="submit"><spring:message code="filter.download-dwca" /></button>
					</form>
					<form style="display: inline-block" method="post" action="<c:url value="/sel/download/mcpd" />">
						<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
						<button class="btn btn-default" type="submit"><spring:message code="filter.download-mcpd" /></button>
					</form>
				</div>
			</div>
		</div>
		</div>

		<table class="accessions">
			<thead>
				<tr>
					<td class="idx-col"></td>
					<td />
					<td><spring:message code="accession.accessionName" /></td>
					<td><spring:message code="accession.taxonomy" /></td>
					<td class="notimportant"><spring:message code="accession.origin" /></td>
					<td class="notimportant"><spring:message code="accession.holdingInstitute" /></td>
					<%-- <td class="notimportant"><spring:message code="accession.holdingCountry" /></td> --%>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${pagedData.content}" var="accession" varStatus="status">
					<tr id="a${accession.id}" class="acn targeted ${accession.historic ? 'historic-record' : ''} ${status.count % 2 == 0 ? 'even' : 'odd'}">
						<td class="idx-col"><c:out value="${status.count + pagedData.size * pagedData.number}" /></td>
						<td class="sel ${selection.containsId(accession.id) ? 'picked' : ''}" x-aid="${accession.id}"></td>
						<td><a href="<c:url value="/acn/id/${accession.id}" />"><b><c:out value="${accession.accessionName}" /></b></a></td>
						<td><c:out value="${accession.taxonomy.taxonName}" /></td>
						<td class="notimportant"><c:out value="${accession.countryOfOrigin.getName(pageContext.response.locale)}" /></td>
						<td class="notimportant"><a href="<c:url value="/wiews/${accession.institute.code}" />"><c:out value="${accession.institute.code}" /></a></td>
						<%-- <td class="notimportant"><a href="<c:url value="/geo/${accession.institute.country.code3}" />"><c:out value="${accession.institute.country.getName(pageContext.response.locale)}" /></a></td> --%>
					</tr>
				</c:forEach>
			</tbody>
		</table>

		<form method="post" action="<c:url value="/sel/order" />" class="form-vertical">
			<div class="form-actions">
				<a href="<c:url value="/sel/" />"><button class="btn btn-default" type="button"><spring:message code="selection.reload-list" /></button></a>
				<button class="btn btn-primary" type="submit"><spring:message code="selection.send-request" /></button>
				<a href="<c:url value="/sel/clear" />" class="btn btn-default"><spring:message code="selection.clear" /></a>
				<a href="<c:url value="/sel/map" />" class="btn btn-default"><spring:message code="selection.map" /></a>
			</div>
      <!-- CSRF protection -->
      <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		</form>
		
	</c:if>

	<div style="margin-top: 60px">
	  <!-- Nav tabs -->
	  <ul class="nav nav-tabs" role="tablist">
	    <li role="presentation" class="active"><a href="#tab-add-many" aria-controls="tab-add-many" role="tab" data-toggle="tab"><spring:message code="selection.add-many" /></a></li>
	    <security:authorize access="isAuthenticated()">
	    <li role="presentation"><a href="#tab-manage-list" aria-controls="tab-manage-list" role="tab" data-toggle="tab"><spring:message code="user.accession.list.title" /></a></li>
	    </security:authorize>
	  </ul>
	
	  <!-- Tab panes -->
	  <div class="tab-content">
	    <div role="tabpanel" class="tab-pane active" id="tab-add-many">
	    
	    		<form method="post" action="<c:url value="/sel/add-many" />" class="">
						<div class="form-group">
							<label for="accessionIds" class="control-label"><spring:message code="selection.add-many.accessionIds" /></label>
							<div class="controls">
								<textarea class="form-control" placeholder="IG 523121, TMB-1" name="accessionIds"></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="accessionIds" class="control-label"><spring:message code="selection.add-many.instCode" /></label>
							<div class="controls">
								<input class="form-control" placeholder="XXX000" name="instCode" />
							</div>
						</div>
						<div class="form-group">
							<input type="submit" class="btn btn-primary" value="<spring:message code="selection.add-many.button" />" />
						</div>
						<!-- CSRF protection -->
						<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
					</form>
	    
	    </div>


	    <security:authorize access="isAuthenticated()">
	    <div role="tabpanel" class="tab-pane" id="tab-manage-list">

				<div class="form-group">
				    <div class="dropdown">
				        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><spring:message code="userlist.list-my-lists" /> <b
				                class="caret"></b></a>
				        <ul class="dropdown-menu" role="menu" id="user-accession-lists">
				            <c:forEach items="${userAccessionLists}" var="userList">
			                <li><a href="/sel/load?list=${userList.uuid}"><c:out value="${userList.title}" /></a></li>
				            </c:forEach>
				        </ul>
				    </div>
				</div>
	    
				<div class="form-group">
        <form method="post" action="<c:url value="/sel/userList"/>" role="form">
            <div class="form-group">
                <label for="accessionListTitle"><spring:message code="userlist.title" /></label>
                <input id="accessionListTitle" type="text" name="title" class="form-control"
                       value="${selection.userAccessionList.title}"/>
            </div>
            <div class="form-group">
                <input type="checkbox" id="accessionListShared" name="shared" value="true" 
                       ${selection.userAccessionList.shared ? "checked" : ""}/>
                <label for="accessionListShared"><spring:message code="userlist.shared" /></label>
            </div>
            <div class="form-group">
                <label for="accessionListDescr"><spring:message code="userlist.description" /></label>
                <textarea name="description" rows="3" id="accessionListDescr"
                          class="form-control"><c:out value="${selection.userAccessionList.description}"/></textarea>
            </div>
            <div class="form-group">
                <c:if test="${selection.userAccessionList.uuid ne null}">
                    <input type="hidden" name="uuid" value="${selection.userAccessionList.uuid}"/>
                    <input type="submit" class="btn btn-primary" value="<spring:message code="userlist.update-list"/>" name="update" />
                    <input type="submit" class="btn btn-default" value="<spring:message code="userlist.disconnect"/>" name="disconnect" />
                    <input type="submit" class="btn btn-default" value="<spring:message code="delete"/>" name="delete" />
                    
                  <security:authorize access="hasRole('ADMINISTRATOR') or hasPermission(#selection.userAccessionList, 'ADMINISTRATION')">
										<a class="btn btn-default" href="<c:url value="/acl/org.genesys2.server.model.impl.AccessionList/${selection.userAccessionList.id}/permissions"><c:param name="back"><c:url value="/sel" /></c:param></c:url>">
											<spring:message code="edit-acl"/>
										</a>
									</security:authorize>
                </c:if>
                <input type="submit" class="btn btn-default" value="<spring:message code="userlist.make-new-list"/>" name="save"/>
            </div>
            <!-- CSRF protection -->
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		      </form>
		    </div>
	    </div>
	    </security:authorize>
	  </div>
	</div>
	
</body>
</html>