<%@ tag description="Display pagination" pageEncoding="UTF-8" %>
<%@ tag body-content="empty" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="pagedData" required="true" type="org.springframework.data.domain.Page" %>
<%@ attribute name="jsonFilter" required="true" type="java.lang.String" %>

<nav class="text-center pull-left">
    <form method="get" action="">
        <ul class="pagination pagination-lg">
            <li>
                 <span class="results">
                     <spring:message code="accessions.number" arguments="${pagedData.totalElements}"/>
                 </span>
            </li>
            <li>
                 <span aria-label="<spring:message code="pagination.previous-page" />" class="firstPage">
                     <span aria-hidden="true">
                         <a href="<spring:url value=""><spring:param name="page" value="1" /><spring:param name="filter" value="${jsonFilter}" /></spring:url>">
                             &laquo;
                         </a>
                     </span>
                 </span>
            </li>
            <li>
                 <span aria-label="<spring:message code="pagination.previous-page" />" class="previousPage">
                     <span aria-hidden="true">
                         <a href="<spring:url value=""><spring:param name="page" value="${pagedData.number eq 0 ? 1 : pagedData.number}" /><spring:param name="filter" value="${jsonFilter}" /></spring:url>">
                             &lsaquo;
                         </a>
                     </span>
                 </span>
            </li>
            <li>
                 <span class="pagination-input">
                     <input class="form-control" style="display: inline; max-width: 5em; text-align: center"
                            type="text" name="page" value="${pagedData.number + 1}"/>
                 </span>
            </li>
            <li>
                 <span aria-label="<spring:message code="pagination.next-page" />" class="nextPage">
                     <span aria-hidden="true">
                         <a href="<spring:url value=""><spring:param name="page" value="${pagedData.number + 2}" /><spring:param name="filter" value="${jsonFilter}" /></spring:url>">
                             &rsaquo;
                         </a>
                     </span>
                 </span>
            </li>
            <li>
                 <span aria-label="<spring:message code="pagination.next-page" />" class="lastPage">
                     <span aria-hidden="true">
                         <a href="<spring:url value=""><spring:param name="page" value="${pagedData.totalPages}" /><spring:param name="filter" value="${jsonFilter}" /></spring:url>">
                             &raquo;
                         </a>
                     </span>
                 </span>
            </li>
            <li>
                 <span class="totalPages">
                     <spring:message code="paged.ofPages" arguments="${pagedData.totalPages}"/>
                 </span>
            </li>
        </ul>
        <input type="hidden" name="filter" value="<c:out value="${jsonFilter}" />"/>
    </form>
</nav>