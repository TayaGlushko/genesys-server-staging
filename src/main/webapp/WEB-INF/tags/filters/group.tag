<%@ tag description="Display filter group" pageEncoding="UTF-8" %>
<%@ tag body-content="scriptless" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 filters">
    <div class="panel-group" id="accordion">
        <div class="panel panel-default" id="panelFilters">
            <div class="panel-heading">
                <h4 class="panel-title"><a data-toggle="collapse" href="#collapseFilters">
                    <spring:message code="filters.toggle-filters" />
                </a></h4>
            </div>
            <div id="collapseFilters" class="panel-collapse collapse in">
                <div class="panel-body">
                    <div class="panel-group">

                    <jsp:doBody/>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>