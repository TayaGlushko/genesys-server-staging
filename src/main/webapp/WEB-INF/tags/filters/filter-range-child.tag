<%@ tag description="Display filter" pageEncoding="UTF-8" %>
<%@ tag body-content="empty" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="filter" required="true" type="org.genesys2.server.model.filters.GenesysFilter" %>
<%@ attribute name="filterMap" required="true" type="java.util.Map" %>
<%@ attribute name="appliedFilters" required="true" type="org.genesys2.server.service.impl.FilterHandler.AppliedFilters" %>
<%@ attribute name="wrap" required="true" type="java.lang.Boolean" %>

<spring:message code="filter.internal.message.between" var="between" arguments=" "/>
<spring:message code="filter.internal.message.and" var="varEnd" arguments=" "/>
<spring:message code="filter.internal.message.more" var="moreThan" arguments=" "/>
<spring:message code="filter.internal.message.less" var="lessThan" arguments=" "/>
<spring:message code="filter.internal.message.like" var="like" arguments=" "/>
<spring:message code="boolean.true" var="varTrue"/>
<spring:message code="boolean.false" var="varFalse"/>
<spring:message code="boolean.null" var="varNull"/>

<c:if test="${wrap}">
    <div class="input-group">
</c:if>
    <c:set var="normalizedKey" value="${filter.key.replace('.', '-').replace(':', '_')}"/>
    <c:set var="appliedFilter" value="${appliedFilters.get(filter.key)}"/>
    <div class="input-group">
        <input id="<c:out value="${normalizedKey}" />_input_1" class="span5 form-control"
               type="text"/>
        <input id="<c:out value="${normalizedKey}" />_input_2" class="span5 form-control"
               type="text"/>
            <span class="input-group-btn">
                <button class="btn notimportant filter-range"
                        norm-key="<c:out value="${normalizedKey}" />"
                        i-key="<c:out value="${filter.key}" />">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </button>
            </span>
    </div>
<c:if test="${wrap}">
    </div>
</c:if>
<c:forEach items="${filterMap[appliedFilter.key]}" var="value">
    <%@include file="filtval.jspf" %>
</c:forEach>
