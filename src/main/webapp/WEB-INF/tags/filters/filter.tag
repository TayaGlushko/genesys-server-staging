<%@ tag description="Display filter" pageEncoding="UTF-8"%>
<%@ tag body-content="empty"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="filters" tagdir="/WEB-INF/tags/filters"%>
<%@ attribute name="availableFilters" required="true" type="java.util.Map"%>
<%@ attribute name="filterMap" required="true" type="java.util.Map"%>
<%@ attribute name="filterKey" required="true" type="java.lang.String"%>
<%@ attribute name="appliedFilters" required="true" type="org.genesys2.server.service.impl.FilterHandler.AppliedFilters"%>
<%@ attribute name="type" required="true" type="java.lang.String"%>
<%@ attribute name="cropList" required="false" type="java.util.Map"%>
<%@ attribute name="currentCrop" required="false" type="org.genesys2.server.model.impl.Crop"%>

<c:set var="filter" value="${availableFilters[filterKey]}" />
<c:set var="normalizedKey" value="${filter.key.replace('.', '-').replace(':', '_')}" />
<c:set var="appliedFilter" value="${appliedFilters.get(filter.key)}" />

<div id="collapse_filt_${filterKey}" class="${filterKey}">

<c:choose>
	<c:when test="${type eq 'select'}">
		<!-- <select name="crops" i-key="crops" class="form-control filter-crop"> -->
	<!-- 		<option disabled selected>Select crop</option> -->
			<c:forEach items="${cropList}" var="c" varStatus="i">
				<c:forEach items="${c.value}" var="val">
					<input type="radio" name="crops" id="crops_${i.count}" i-key="crops" class="filter-crop" value="${c.key}" ${c.key == currentCrop.shortName? 'checked' : ''} />
					<label for="crops_${i.count}">
						<c:out value="${val.key}" /> (${val.value})
					</label>
					<br/>
				</c:forEach>
			</c:forEach>
		<!-- </select> -->
	</c:when>
	<c:when test="${type eq 'like'}">
		<select class="form-control like-switcher">
			<option value="like"><spring:message code="filter.string.like" /></option>
			<option value="equals"><spring:message code="filter.string.equals" /></option>
		</select>
		<div class="input-group">
			<input type="text" class="span2 form-control string-type" id="<c:out value="${normalizedKey}" />_input">
			<span class="input-group-btn">
				<button class="btn btn-secondary notimportant filter-auto" type="button" norm-key="<c:out value="${normalizedKey}" />" i-key="<c:out value="${filter.key}" />">
					<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
				</button>
			</span>
		</div>
	</c:when>
	<c:when test="${type eq 'option'}">
		<c:forEach items="${filter.options}" var="option" varStatus="i">
			<div>
				<label>
					<input class="filter-list" id="<c:out value="${normalizedKey}${option.value}" />_input" ${fn:contains(filterMap[appliedFilter.key], option.value)?'checked':''}
						norm-key="<c:out value="${normalizedKey}" />" i-key="<c:out value="${filter.key}" />" type="checkbox" value="${option.value}" />
					<spring:message code="${option.name}" />
					(${filter.counts[i.index]})
				</label>
			</div>
		</c:forEach>
	</c:when>
	<c:when test="${type eq 'autocomplete'}">
		<div class="input-group">
			<input id="<c:out value="${normalizedKey}" />_input" class="span2 form-control autocomplete-filter string-type" x-source="<c:url value="${filter.autocompleteUrl}" />"
				placeholder="<spring:message code="filter.autocomplete-placeholder" />" type="text" />
			<span class="input-group-btn">
				<button class="btn btn-secondary notimportant filter-auto" type="button" norm-key="<c:out value="${normalizedKey}" />" i-key="<c:out value="${filter.key}" />">
					<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
				</button>
			</span>
		</div>
	</c:when>
	<c:when test="${type eq 'range'}">
		<c:choose>
			<c:when test="${filterKey eq 'geo.latitude'}">
				<filters:filter-range-child filter="${availableFilters['geo.latitude']}" appliedFilters="${appliedFilters}" filterMap="${filterMap}" wrap="true" />

				<spring:message code="filter.geo.longitude" />:
                            <filters:filter-range-child filter="${availableFilters['geo.longitude']}" appliedFilters="${appliedFilters}" filterMap="${filterMap}" wrap="true" />

				<spring:message code="filter.geo.elevation" />:
                            <filters:filter-range-child filter="${availableFilters['geo.elevation']}" appliedFilters="${appliedFilters}" filterMap="${filterMap}" wrap="true" />
			</c:when>
			<c:otherwise>
				<filters:filter-range-child filter="${availableFilters[filterKey]}" appliedFilters="${appliedFilters}" filterMap="${filterMap}" wrap="false" />
			</c:otherwise>
		</c:choose>
	</c:when>
	<c:when test="${type eq 'boolean'}">
		<div>
			<label>
				<input type="checkbox" ${fn:contains(filterMap[appliedFilter.key], 'true')?'checked':''} class="filter-bool" i-key="<c:out value="${filter.key}" />" id="<c:out value="${normalizedKey}" />"
					value="true">
				<spring:message code="boolean.true" />
			</label>
		</div>
		<div>
			<label>
				<input type="checkbox" ${fn:contains(filterMap[appliedFilter.key], 'false')?'checked':''} class="filter-bool" i-key="<c:out value="${filter.key}" />" id="<c:out value="${normalizedKey}" />"
					value="false">
				<spring:message code="boolean.false" />
			</label>
		</div>
		<c:if test="${filter.allowsNull}">
			<div>
				<label>
					<input type="checkbox" ${fn:contains(filterMap[appliedFilter.key], 'null')?'checked':''} class="filter-bool" i-key="<c:out value="${filter.key}" />" id="<c:out value="${normalizedKey}" />"
						value="null">
					<spring:message code="boolean.null" />
				</label>
			</div>
		</c:if>
	</c:when>
	<c:otherwise />
</c:choose>

<c:if test="${type ne 'range'}">
	<c:forEach items="${filterMap[appliedFilter.key]}" var="value">
		<%@include file="filtval.jspf"%>
	</c:forEach>
</c:if>

<c:if test="${filterKey eq 'taxonomy.genus'}">
	<c:set var="filter" value="${availableFilters['taxonomy.species']}" />
	<c:set var="normalizedKey" value="${filter.key.replace('.', '-').replace(':', '_')}" />
	<c:set var="appliedFilter" value="${appliedFilters.get(filter.key)}" />
	<c:forEach items="${filterMap[appliedFilter.key]}" var="value">
		<%@include file="filtval.jspf"%>
	</c:forEach>
	<c:set var="filter" value="${availableFilters['taxonomy.subtaxa']}" />
	<c:set var="normalizedKey" value="${filter.key.replace('.', '-').replace(':', '_')}" />
	<c:set var="appliedFilter" value="${appliedFilters.get(filter.key)}" />
	<c:forEach items="${filterMap[appliedFilter.key]}" var="value">
		<%@include file="filtval.jspf"%>
	</c:forEach>
	<c:set var="filter" value="${availableFilters['taxonomy.sciName']}" />
	<c:set var="normalizedKey" value="${filter.key.replace('.', '-').replace(':', '_')}" />
	<c:set var="appliedFilter" value="${appliedFilters.get(filter.key)}" />
	<c:forEach items="${filterMap[appliedFilter.key]}" var="value">
		<%@include file="filtval.jspf"%>
	</c:forEach>
	<c:set var="filter" value="${availableFilters['orgCty.iso3']}" />
	<c:set var="normalizedKey" value="${filter.key.replace('.', '-').replace(':', '_')}" />
	<c:set var="appliedFilter" value="${appliedFilters.get(filter.key)}" />
	<c:forEach items="${filterMap[appliedFilter.key]}" var="value">
		<%@include file="filtval.jspf"%>
	</c:forEach>
	<c:set var="filter" value="${availableFilters['institute.country.iso3']}" />
	<c:set var="normalizedKey" value="${filter.key.replace('.', '-').replace(':', '_')}" />
	<c:set var="appliedFilter" value="${appliedFilters.get(filter.key)}" />
	<c:forEach items="${filterMap[appliedFilter.key]}" var="value">
		<%@include file="filtval.jspf"%>
	</c:forEach>
</c:if>

<button type="button" class="btn btn-success applyBtn">
	<spring:message code="filter.apply" />
</button>
</div>
