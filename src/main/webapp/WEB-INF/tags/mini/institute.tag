<%@ tag description="Display FAO institute data" pageEncoding="UTF-8" %>
<%@ tag body-content="empty" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ attribute name="institute" required="true"
	type="org.genesys2.server.model.impl.FaoInstitute" %>

<div class="genesys-mini genesys-institute">
	<div class="title ${institute.current ? '' : 'not-current'}">
		<a href="<c:url value="/wiews/${institute.code}" />"><c:out
				value="${institute.code}" /></a>
		<c:out value="${institute.fullName}" />
		<span class="acronym"><c:out value="${institute.acronym}" /></span>
	</div>
	<div class="accessionCount">
		<c:choose>
			<c:when test="${institute.accessionCount gt 0}">
				<spring:message code="faoInstitutes.stat.accessionCount" />
				<a href="<c:url value="/wiews/${institute.code}/data" />"><fmt:formatNumber value="${institute.accessionCount}" /></a>
			</c:when>
			<c:when test="${! institute.current}">
				<!-- Link to active -->
				<a href="<c:url value="/wiews/${faoInstitute.vCode}" />">
					<spring:message code="faoInstitute.view-current-institute" arguments="${institute.vCode}" />
				</a>
			</c:when>
			<c:otherwise>
				<spring:message code="faoInstitute.no-accessions-registered" />
			</c:otherwise>
		</c:choose>
	</div>
</div>
