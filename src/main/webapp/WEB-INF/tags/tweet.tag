<%@ tag description="Tweet this!" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="text" required="false" type="java.lang.String" description="A text parameter appears pre-selected in a Tweet composer. The Tweet author may easily remove the text with a single delete action." %>
<%@ attribute name="hashTags" required="false" type="java.lang.String" description="Add a comma-separated list of hashtags to a Tweet using the hashtags parameter. Omit a preceding “#” from each hashtag; the Tweet composer will automatically add the proper space-separated hashtag by language." %>
<%@ attribute name="dataSize" required="false" type="java.lang.String" description="regular or large" %>
<%@ attribute name="iconOnly" required="false" type="java.lang.Boolean" description="Render only Twitter icon, no text?" %>

<c:url var="refUrl" value="${props.baseUrl}${pageContext.request.getAttribute('javax.servlet.forward.request_uri')}" />

<c:url var="twatUrl" value="https://twitter.com/intent/tweet">
  <c:param name="url" value="${refUrl}" />
  <c:if test="${text != null and text ne ''}">
    <c:param name="text" value="${jspHelper.htmlToText(text, 300)}" />
  </c:if>
  <c:if test="${hashTags != null and hashTags ne ''}">
    <c:param name="hashtags" value="${hashTags}" />
  </c:if>
</c:url>
<a class="twitter-share-button" target="_blank" href="${twatUrl}" data-size="${dataSize}" title="<spring:message code="twitter.tweet-this" />">
	<i class="fa fa-twitter"></i>
	<c:if test="${iconOnly eq null or not iconOnly}">
		<spring:message code="twitter.tweet-this" />
	</c:if>
</a>
