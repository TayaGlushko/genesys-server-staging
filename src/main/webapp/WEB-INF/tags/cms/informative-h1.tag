<%@ tag description="Display menu with menu items" pageEncoding="UTF-8" %>
<%@ tag body-content="empty" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="local" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="cms" tagdir="/WEB-INF/tags/cms" %>
<%@ attribute name="title" required="true" type="java.lang.String" %>
<%@ attribute name="fancy" required="false" rtexprvalue="true" type="java.lang.Boolean" %>
<%@ attribute name="info" required="false" type="java.lang.String" %>

<c:if test="${fancy eq true}">
	<div class="informative-h1 green-bg row">
		<div class="col-md-12 col-sm-12">
			<h1>
				<spring:message code="${title}" />
			</h1>
			<c:if test="${info ne null and info ne ''}">
			<p><spring:message code="${info}" /></p>
			</c:if>
		</div>
		<%-- <div class="col-md-6 hidden-sm">
			xxxx
		</div> --%>
	</div>
</c:if>
<c:if test="${fancy eq null or not fancy or info eq null}">
	<div class="informative-h1 green-bg row">
		<div class="col-md-12 col-sm-12">
			<h1>
				<spring:message code="${title}" />
			</h1>
		</div>
	</div>
</c:if>
