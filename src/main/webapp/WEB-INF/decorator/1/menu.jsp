<%@ include file="/WEB-INF/jsp/init.jsp" %>

<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-inverse navbar-fixed-top">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand nav-logo clearfix" href="/1/welcome"><img src="/html/1/images/GENESYS-ICON.svg" /><img src="/html/1/images/GENESYS-LOGO.svg" /></a>
		</div>
		<div id="navbar" class="collapse navbar-collapse">
			<div class="col-md-6 no-space">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand nav-logo clearfix" href="/1/welcome"><img src="/html/1/images/GENESYS-ICON.svg" /><img src="/html/1/images/GENESYS-LOGO.svg" /></a>
				</div>

				<ul class="nav navbar-nav nav-menu">
					<li class="active">
						<a href="<c:url value="/1/welcome" />" title="<spring:message code="menu.home" />"> <spring:message code="menu.home" />
						</a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <spring:message code="menu.about" /> <span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a href="<c:url value="/1/content/about/about" />"> <spring:message code="menu.about" />
								</a>
							</li>
							<li>
								<a href="<c:url value="/1/content/about/contact" />"> <spring:message code="menu.contact" />
								</a>
							</li>
							<li>
								<a href="<c:url value="/1/content/about/what-is-genesys" />"> <spring:message code="menu.what-is-genesys" />
								</a>
							</li>
							<%-- <li><a href="<c:url value="/1/content/about/about" />"><i class="fa fa-caret-right"></i> <spring:message code="menu.who-uses-genesys" /></a></li> --%>
							<li>
								<a href="<c:url value="/1/content/about/history-of-genesys" />"> <spring:message code="menu.history-of-genesys" />
								</a>
							</li>
							<li>
								<a href="<c:url value="/1/content/about/newsletter" />"> <spring:message code="menu.newsletter" />
								</a>
							</li>
							<li>
								<a href="<c:url value="/1/content/about/frequently-asked-questions" />"> <spring:message code="menu.faq" />
								</a>
							</li>
							<%-- <li role="separator" class="divider"></li>
						<li class="dropdown-header">Nav header</li>
						<li>
							<a href="#">Separated link</a>
						</li>
						<li>
							<a href="#">One more separated link</a>
						</li> --%>
						</ul>
					</li>
					<li>
						<a href="<c:url value="/1/explore" />"> <spring:message code="menu.browse" />
						</a>
					</li>
					<%-- <li><a href="<c:url value="/1/data/" />"><spring:message code="menu.datasets" /></a></li> --%>
					<%-- <li><a href="<c:url value="/1/descriptors/" />"><spring:message code="menu.descriptors" /></a></li> --%>
					<li>
						<a href="<c:url value="/1/geo/" />"> <spring:message code="menu.countries" />
						</a>
					</li>
					<li>
						<a href="<c:url value="/1/wiews/active" />"> <spring:message code="menu.institutes" />
						</a>
					</li>
					<li>
						<a href="<c:url value="/1/sel/" />"> <spring:message code="menu.my-list" /> <span class="badge" x-size="${selection.size() gt 0 ? selection.size() : '0'}" id="selcounter"><c:out value="${selection.size() gt 0 ? selection.size() : '0'}" /></span>
						</a>
					</li>

					<%--<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
						Dropdown
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li>
							<a href="#">Action</a>
						</li>
						<li>
							<a href="#">Another action</a>
						</li>
						<li>
							<a href="#">Something else here</a>
						</li>
						<li role="separator" class="divider"></li>
						<li class="dropdown-header">Nav header</li>
						<li>
							<a href="#">Separated link</a>
						</li>
						<li>
							<a href="#">One more separated link</a>
						</li>
					</ul>
				</li> --%>
				</ul>
			</div>
			<div class="col-md-6 no-space">

				<ul class="nav navbar-nav navbar-right">
					<%@ include file="languages.jspf" %>
				</ul>

				<ul class="nav navbar-nav navbar-right">
					<security:authorize access="isAnonymous()">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"> <spring:message code="page.login" /> <span class="caret"></span>
							</a>
							<ul class="dropdown-menu pull-left">
								<li>
									<form id="loginForm" role="form" method="post" action="<c:url value="/login-attempt" />">
										<div class="form-group">
											<label for="username">
												<spring:message code="login.username" />
											</label>
											<input type="email" class="form-control" id="username" name="username" placeholder="<spring:message code="login.username"/>" />
										</div>
										<div class="form-group">
											<label for="password">
												<spring:message code="login.password" />
											</label>
											<input type="password" class="form-control" id="password" name="password" placeholder="<spring:message code="login.password"/>" />
										</div>
										<div class="checkbox">
											<label>
												<input type="checkbox" name="_spring_security_remember_me" id="_spring_security_remember_me" />
												<spring:message code="login.remember-me" />
											</label>
										</div>
										<button type="submit" class="btn btn-primary">
											<spring:message code="login.login-button" />
										</button>
										<span class="or">-</span>
										<a href="<c:url value="/google/login" />" class="btn btn-default google-signin"> <spring:message code="login.with-google-plus" />
										</a> <a href="<c:url value="/1/registration" />" class="btn btn-default"> <spring:message code="login.register-now" />
										</a>
										<!-- CSRF protection -->
										<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
									</form>
								</li>
							</ul>
						</li>
					</security:authorize>
					<security:authorize access="isAuthenticated()">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <spring:message code="user.pulldown.heading"
									arguments="${user.user.name}" /> <span class="caret"></span>
							</a>
							<ul class="dropdown-menu pull-left">
								<li>
									<a href="<c:url value="/1/profile/${user.username}" />"> <spring:message code="user.pulldown.profile" />
									</a>
								</li>
								<li>
									<a id="logout1" href="#" onclick="document.getElementById('logoutForm').submit();"> <spring:message code="user.pulldown.logout" />
									</a>
								</li>


								<security:authorize access="hasRole('ADMINISTRATOR')">
									<li role="separator" class="divider"></li>
									<li>
										<a href="<c:url value="/1/admin/" />"> <spring:message code="user.pulldown.administration" />
										</a>
									</li>
									<li>
										<a href="<c:url value="/1/profile/list" />"> <spring:message code="user.pulldown.users" />
										</a>
									</li>
									<li>
										<a href="<c:url value="/1/team" />"> <spring:message code="user.pulldown.teams" />
										</a>
									</li>
									<li>
										<a href="<c:url value="/1/admin/oauth-clients/" />"> <spring:message code="user.pulldown.oauth-clients" />
										</a>
									</li>
									<li>
										<a href="<c:url value="/1/content" />"> <spring:message code="user.pulldown.manage-content" />
										</a>
									</li>
									<li>
										<a href="<c:url value="/1/project" />"> <spring:message code="project.page.list.title" />
										</a>
									</li>
								</security:authorize>
							</ul>
						</li>
					</security:authorize>
				</ul>

				<div class="header-search">
					<form action="/1/acn/search2" class="form-inline">
						<div class="form-group">
							<input type="text" name="q" class="form-control" placeholder="Search Genesys..." />
							<input type="submit" class="btn btn-primary" value="" />
						</div>
					</form>
				</div>
			</div>

		</div>
		<!--/.nav-collapse -->
	</div>
</nav>

<form id="logoutForm" action="/logout" method="post">
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form>