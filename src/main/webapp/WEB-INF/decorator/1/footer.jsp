<%@ include file="/WEB-INF/jsp/init.jsp" %>

<!-- Footer -->

<footer class="footer">
	<div class="container-fluid">
		<!-- <p class="text-muted">Place sticky footer content here.</p> -->
		<div class="footer-logo">
			<a href="/1/welcome"><img src="/html/1/images/GENESYS-ICON.svg" alt="Genesys - Gateway to Genetic Resources" /><img src="/html/1/images/GENESYS-LOGO_black.svg"
				alt="Genesys - Gateway to Genetic Resources" /></a>
		</div>

		<div id="nav-foot" class="clearfix">
			<ul class="nav text-center">
				<li>
					<a href="<c:url value="/1/content/about/about" />"><spring:message code="menu.about" /></a>
				</li>
				<li>
					<a href="<c:url value="/1/content/about/contact" />"><spring:message code="menu.contact" /></a>
				</li>
				<li>
					<a href="<c:url value="/1/content/legal/disclaimer" />"><spring:message code="menu.disclaimer" /></a>
				</li>
				<li>
					<a target="_blank" href="https://bitbucket.org/genesys2/genesys2-server/issues/new"><spring:message code="menu.report-an-issue" /></a>
				</li>
				<li class="notimportant">
					<a target="_blank" href="https://bitbucket.org/genesys2/genesys2-server.git"><spring:message code="menu.scm" /></a>
				</li>
				<li class="notimportant">
					<a target="_blank" href="https://www.transifex.com/crop-trust/genesys/"><spring:message code="menu.translate" /></a>
				</li>
				<li>
					<a href="<c:url value="/1/content/legal/terms" />"><spring:message code="menu.terms" /></a>
				</li>
				<li>
					<a href="<c:url value="/1/content/legal/copying" />"><spring:message code="menu.copying" /></a>
				</li>
				<li>
					<a href="<c:url value="/1/content/legal/privacy" />"><spring:message code="menu.privacy" /></a>
				</li>
			</ul>
			<div class="text-center" id="copyright">
				<p>
					<spring:message code="footer.copyright-statement" />
				</p>
			</div>
		</div>

		<div class="text-center footer-link">
			<spring:message code="page.rendertime" arguments="${springExecuteTime}" />
			<br /> <a target="_blank" href="<c:url value="https://bitbucket.org/genesys2/genesys2-server/commits/${buildRevision}" />"><c:out value="${buildName}" /></a>
		</div>
	</div>
</footer>

<%-- Placed at the end of the document so the pages load faster --%>
<c:choose>
	<c:when test="${requestContext.theme.name eq 'one'}">
		<script type="text/javascript" src="<c:url value="/html/1/js/all.min.js" />"></script>
	</c:when>
	<c:when test="${requestContext.theme.name eq 'all'}">
		<script type="text/javascript" src="<c:url value="/html/1/js/libraries.min.js" />"></script>
		<script type="text/javascript" src="<c:url value="/html/1/js/genesys.js" />"></script>
	</c:when>
	<c:otherwise>
		<script type="text/javascript" src="<c:url value="/html/1/js/libraries.js" />"></script>
		<script type="text/javascript" src="<c:url value="/html/1/js/genesys.js" />"></script>
	</c:otherwise>
</c:choose>

<script type="text/javascript">
  L.Icon.Default.imagePath = '<c:url value="/html/1/styles/images" />';
<%--dynCss.config.debug=true;--%>
  //enableSessionWarning(${pageContext.session.maxInactiveInterval});
</script>

<!-- Footer/End -->
