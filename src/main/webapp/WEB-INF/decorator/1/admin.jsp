<%@ include file="/WEB-INF/jsp/init.jsp" %>

<!DOCTYPE html>

<html lang="${pageContext.response.locale.language}" dir="${pageContext.response.locale.language=='fa' || pageContext.response.locale.language=='ar' ? 'rtl' : 'ltr'}"
	xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:schema="http://schema.org/" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dwc="http://rs.tdwg.org/dwc/terms/"
	xmlns:germplasm="http://purl.org/germplasm/germplasmTerm#" xmlns:germplasmType="http://purl.org/germplasm/germplasmType#">
<head>
<title><sitemesh:write property="title" /></title>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="language" content="${pageContext.response.locale.language}" />
<meta name="robots" content="noindex, nofollow" />

<!-- CSRF protection-->
<meta name="_csrf" content="${_csrf.token}" />
<!-- default header name is X-CSRF-TOKEN -->
<meta name="_csrf_header" content="${_csrf.headerName}" />

<!-- Links -->
<link rel="shortcut icon" href="<c:url value="/html/0/images/genesys.png" />" />

<!-- opensearch.org -->
<link rel="search" hreflang="${pageContext.response.locale.language}" type="application/opensearchdescription+xml" href="<c:url value="/acn/opensearch/desc" />"
	title="<spring:message code="search.input.placeholder" />" />

<!-- l10n -->
<link rel="alternate" hreflang="en" href="<c:url value="/en${pageContext.request.getAttribute('org.genesys2.server.servlet.filter.LocaleURLFilter.INTERNALURL')}" />" />
<link rel="alternate" hreflang="ar" href="<c:url value="/ar${pageContext.request.getAttribute('org.genesys2.server.servlet.filter.LocaleURLFilter.INTERNALURL')}" />" />
<link rel="alternate" hreflang="de" href="<c:url value="/de${pageContext.request.getAttribute('org.genesys2.server.servlet.filter.LocaleURLFilter.INTERNALURL')}" />" />
<link rel="alternate" hreflang="es" href="<c:url value="/es${pageContext.request.getAttribute('org.genesys2.server.servlet.filter.LocaleURLFilter.INTERNALURL')}" />" />
<link rel="alternate" hreflang="fa" href="<c:url value="/fa${pageContext.request.getAttribute('org.genesys2.server.servlet.filter.LocaleURLFilter.INTERNALURL')}" />" />
<link rel="alternate" hreflang="fr" href="<c:url value="/fr${pageContext.request.getAttribute('org.genesys2.server.servlet.filter.LocaleURLFilter.INTERNALURL')}" />" />
<link rel="alternate" hreflang="pt" href="<c:url value="/pt${pageContext.request.getAttribute('org.genesys2.server.servlet.filter.LocaleURLFilter.INTERNALURL')}" />" />
<link rel="alternate" hreflang="ru" href="<c:url value="/ru${pageContext.request.getAttribute('org.genesys2.server.servlet.filter.LocaleURLFilter.INTERNALURL')}" />" />
<link rel="alternate" hreflang="zh" href="<c:url value="/zh${pageContext.request.getAttribute('org.genesys2.server.servlet.filter.LocaleURLFilter.INTERNALURL')}" />" />

<!-- Custom styles for this template -->
<%@ include file="css.jsp" %>

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->

<sitemesh:write property="head" />
<meta name="author" content="Global Crop Diversity Trust" />
</head>

<security:authentication var="user" property="principal" />

<body>
	<%@ include file="header.jsp" %>

	<nav class="navbar navbar-default navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-admin" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<c:url value="/1/admin/" />"> <spring:message code="page.home.title" />
				</a>
			</div>
			<div id="navbar-admin" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="hidden-sm">
						<a href="<c:url value="/1/welcome" />" title="<spring:message code="menu.home" />"> <spring:message code="menu.home" />
						</a>
					</li>
					<li>
						<a class="" href="<c:url value="/1/admin/cache/" />"> <spring:message code="menu.admin.caches" />
						</a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <spring:message code="menu.admin.usermanagement" /> <span class="caret"></span>
						</a>
						<ul class="dropdown-menu pull-left">
							<li>
								<a href="<c:url value="/1/admin/users/" />"> <spring:message code="user.pulldown.users" />
								</a>
							</li>
							<li>
								<a href="<c:url value="/1/admin/teams/" />"> <spring:message code="user.pulldown.teams" />
								</a>
							</li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <spring:message code="menu.admin.repository" /> <span class="caret"></span>
						</a>
						<ul class="dropdown-menu pull-left">
							<li>
								<a href="<c:url value="/1/admin/r/files/" />"> <spring:message code="menu.admin.repository.files" />
								</a>
							</li>
							<li>
								<a href="<c:url value="/1/admin/r/g" />"> <spring:message code="menu.admin.repository.galleries" />
								</a>
							</li>
						</ul>
					</li>
					<li>
						<a class="" href="<c:url value="/1/admin/logger/" />"> <spring:message code="menu.admin.loggers" />
						</a>
					</li>
					<li>
						<a class="" href="<c:url value="/1/admin/ds2/" />"> <spring:message code="menu.admin.ds2" />
						</a>
					</li>
					<li>
						<a class="" href="<c:url value="/1/admin/kpi/" />"> <spring:message code="menu.admin.kpi" />
						</a>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<%@ include file="languages.jspf" %>
				</ul>
			</div>
		</div>
	</nav>

	<!-- Begin page content -->
	<div id="content" class="<sitemesh:write property="body.class" />">
		<div class="container-fluid">
			<div id="content-header" class="row">
				<sitemesh:write property="page.header" />
			</div>
			<div id="content-body" class="">
				<div class="row">
					<h1 class="col-xs-12 page-title">
						<sitemesh:write property="title" />
					</h1>
				</div>
				<div id="dialog" class="row"></div>
				<div id="content-body-content" class="" typeof="<sitemesh:write property="body.typeof" />">
					<sitemesh:write property="body" />
				</div>
			</div>
		</div>
	</div>

	<%@ include file="footer.jsp" %>
	<sitemesh:write property="page.javascript" />
	<%@ include file="ga.jsp" %>
</body>
</html>
