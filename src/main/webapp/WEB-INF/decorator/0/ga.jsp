<%@ include file="/WEB-INF/jsp/init.jsp" %>

<c:if test="${googleAnalyticsAccount ne null}">
<script type="text/javascript">
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', '<c:out value="${googleAnalyticsAccount}" />', 'auto');
var p={};
try {
	if (_pageDim && _pageDim.institute != null) {
		p.dimension1=_pageDim.institute;
	}
	if (_pageDim && _pageDim.genus != null) {
		p.dimension2=_pageDim.genus;
	}
	console.log(p);
	ga('send', 'pageview', p);
} catch (e) {
	console.log(e);
	ga('send', 'pageview');
}

// Other trackers?
try {
	if (_pageTrackers && _pageTrackers.length > 0) {
		for (var i=_pageTrackers.length-1; i>=0; i--) {
			var trackerName='gat'+i;
			ga('create', _pageTrackers[i], 'auto', {'name': trackerName});
			ga(trackerName+'.send', 'pageview');
		}
	}
} catch (e) {
	console.log(e);
}
</script>
</c:if>