<%@ include file="/WEB-INF/jsp/init.jsp" %>

<c:choose>
  <c:when test="${requestContext.theme.name eq 'one'}">
    <link href="<c:url value="/html/0/styles/all.min.css" />" type="text/css" rel="stylesheet" />
  </c:when>
  <c:when test="${requestContext.theme.name eq 'all'}">
    <link href="<c:url value="/html/0/styles/bootstrap.min.css" />" type="text/css" rel="stylesheet" />
    <link href="<c:url value="/html/0/styles/other.min.css" />" type="text/css" rel="stylesheet" />
    <link href="<c:url value="/html/0/styles/genesys.css" />" type="text/css" rel="stylesheet" />
    <link href="<c:url value="/html/0/styles/pages.css" />" type="text/css" rel="stylesheet" />
  </c:when>
  <c:otherwise>
    <link href="<c:url value="/html/0/styles/bootstrap.css" />" type="text/css" rel="stylesheet" />
    <link href="<c:url value="/html/0/styles/jquery-ui.css" />" type="text/css" rel="stylesheet" />
    <link href="<c:url value="/html/0/styles/forza.css" />" type="text/css" rel="stylesheet" />
    <link href="<c:url value="/html/0/styles/leaflet.css" />" type="text/css" rel="stylesheet" />
    <link href="<c:url value="/html/0/styles/jquery.simplecolorpicker.css" />" type="text/css" rel="stylesheet" />
    <link href="<c:url value="/html/0/styles/jquery.simplecolorpicker-regularfont.css" />" type="text/css" rel="stylesheet" />
    <link href="<c:url value="/html/0/styles/genesys.css" />" type="text/css" rel="stylesheet" />
    <link href="<c:url value="/html/0/styles/pages.css" />" type="text/css" rel="stylesheet" />
  </c:otherwise>
</c:choose>

