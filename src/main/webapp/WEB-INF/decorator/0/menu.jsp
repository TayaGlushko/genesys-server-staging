<%@ include file="/WEB-INF/jsp/init.jsp" %>

<div class="navbar genesys-navbar" role="navigation" id="nav-main">
  <div class="container">
    <div class="navbar-collapse">
      <ul class="nav navbar-nav">
        <li id="menubar-home" class=""><a href="<c:url value="/welcome" />" title="<spring:message code="menu.home" />"><spring:message code="menu.home" /><%-- <img src="<c:url value="/html/0/images/genesys.png" />" /> --%></a></li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><spring:message code="menu.about" /></a>
            <ul class="dropdown-menu about">
                <li><a href="<c:url value="/content/about/about" />"><i class="fa fa-caret-right"></i> <spring:message code="menu.about" /></a></li>
                <li><a href="<c:url value="/content/about/contact" />"><i class="fa fa-caret-right"></i> <spring:message code="menu.contact" /></a></li>
                <li><a href="<c:url value="/content/about/what-is-genesys" />"><i class="fa fa-caret-right"></i> <spring:message code="menu.what-is-genesys" /></a></li>
                <%-- <li><a href="<c:url value="/content/about/about" />"><i class="fa fa-caret-right"></i> <spring:message code="menu.who-uses-genesys" /></a></li> --%>
                <li><a href="<c:url value="/content/about/history-of-genesys" />"><i class="fa fa-caret-right"></i> <spring:message code="menu.history-of-genesys" /></a></li>
                <li><a href="<c:url value="/content/about/newsletter" />"><i class="fa fa-caret-right"></i> <spring:message code="menu.newsletter" /></a></li>
                <li><a href="<c:url value="/content/about/frequently-asked-questions" />"><i class="fa fa-caret-right"></i> <spring:message code="menu.faq" /></a></li>
            </ul>
        </li>
		<li><a href="<c:url value="/explore" />"><spring:message code="menu.browse" /></a></li>
		<%-- <li><a href="<c:url value="/data/" />"><spring:message code="menu.datasets" /></a></li> --%>
		<%-- <li><a href="<c:url value="/descriptors/" />"><spring:message code="menu.descriptors" /></a></li> --%>
		<li><a href="<c:url value="/geo/" />"><spring:message code="menu.countries" /></a></li>
		<li><a href="<c:url value="/wiews/active" />"><spring:message code="menu.institutes" /></a></li>
		<li><a href="<c:url value="/sel/" />"><spring:message code="menu.my-list" />
				<span class="badge" x-size="${selection.size() gt 0 ? selection.size() : '0'}" id="selcounter"><c:out value="${selection.size() gt 0 ? selection.size() : '0'}" /></span></a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">        
        <li class="right"><local:twitter-follow screenName="GenesysPGR" /></li>
      </ul>
    </div>
  </div>
</div>
