<%@ include file="/WEB-INF/jsp/init.jsp" %>

<!DOCTYPE html>

<html lang="${pageContext.response.locale.language}" dir="${pageContext.response.locale.language=='fa' || pageContext.response.locale.language=='ar' ? 'rtl' : 'ltr'}"
	xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
	xmlns:schema="http://schema.org/"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:dwc="http://rs.tdwg.org/dwc/terms/"
	xmlns:germplasm="http://purl.org/germplasm/germplasmTerm#"
	xmlns:germplasmType="http://purl.org/germplasm/germplasmType#"
>
<head >
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="author" content="Global Crop Diversity Trust" />
    
    <!-- CSRF protection-->
    <meta name="_csrf" content="${_csrf.token}"/>
    <!-- default header name is X-CSRF-TOKEN -->
    <meta name="_csrf_header" content="${_csrf.headerName}"/>

	<!-- Links -->
	<link rel="shortcut icon" href="<c:url value="/html/0/images/0/genesys.png" />" />

	<!-- opensearch.org -->
	<link rel="search" hreflang="${pageContext.response.locale.language}" type="application/opensearchdescription+xml" href="<c:url value="/acn/opensearch/desc" />" title="<spring:message code="search.input.placeholder" />" />
	
	<!-- l10n -->
	<link rel="alternate" hreflang="en" href="<c:url value="/en${pageContext.request.getAttribute('org.genesys2.server.servlet.filter.LocaleURLFilter.INTERNALURL')}" />" />
	<link rel="alternate" hreflang="ar" href="<c:url value="/ar${pageContext.request.getAttribute('org.genesys2.server.servlet.filter.LocaleURLFilter.INTERNALURL')}" />" />
	<link rel="alternate" hreflang="de" href="<c:url value="/de${pageContext.request.getAttribute('org.genesys2.server.servlet.filter.LocaleURLFilter.INTERNALURL')}" />" />
	<link rel="alternate" hreflang="es" href="<c:url value="/es${pageContext.request.getAttribute('org.genesys2.server.servlet.filter.LocaleURLFilter.INTERNALURL')}" />" />
	<link rel="alternate" hreflang="fa" href="<c:url value="/fa${pageContext.request.getAttribute('org.genesys2.server.servlet.filter.LocaleURLFilter.INTERNALURL')}" />" />
	<link rel="alternate" hreflang="fr" href="<c:url value="/fr${pageContext.request.getAttribute('org.genesys2.server.servlet.filter.LocaleURLFilter.INTERNALURL')}" />" />
	<link rel="alternate" hreflang="pt" href="<c:url value="/pt${pageContext.request.getAttribute('org.genesys2.server.servlet.filter.LocaleURLFilter.INTERNALURL')}" />" />
	<link rel="alternate" hreflang="ru" href="<c:url value="/ru${pageContext.request.getAttribute('org.genesys2.server.servlet.filter.LocaleURLFilter.INTERNALURL')}" />" />
	<link rel="alternate" hreflang="zh" href="<c:url value="/zh${pageContext.request.getAttribute('org.genesys2.server.servlet.filter.LocaleURLFilter.INTERNALURL')}" />" />

    <title><sitemesh:write property="title" /></title>

    <!-- Custom styles for this template -->
    <%@ include file="css.jsp" %>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

	<sitemesh:write property="head" />
</head>

<body>
	<div class="site-wrapper">
		<security:authentication var="user" property="principal" />

		<%@ include file="header.jsp" %>
		<%@ include file="menu.jsp" %>

		<div id="content" class="clearfix">
			<div id="content-header">
				<sitemesh:write property="page.header" />
			</div>
			<div class="container">
				<div id="dialog"></div>
				<div class="clearfix" typeof="<sitemesh:write property="body.typeof" />">
					<sitemesh:write property="body" />
				</div>
			</div>
		</div>
	</div>

	<%@ include file="footer.jsp" %>
	<sitemesh:write property="page.javascript" />
	<%@ include file="ga.jsp" %>
</body>
</html>
