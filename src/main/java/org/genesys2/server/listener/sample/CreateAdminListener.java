/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.listener.sample;

import java.util.HashSet;
import java.util.Set;

import org.genesys2.server.exception.UserException;
import org.genesys2.server.listener.RunAsAdminListener;
import org.genesys2.server.model.UserRole;
import org.genesys2.server.model.impl.User;
import org.genesys2.server.service.PasswordPolicy.PasswordPolicyException;
import org.genesys2.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service("createAdminListener")
public class CreateAdminListener extends RunAsAdminListener {

	@Autowired
	private UserService userService;

	@Override
	public void init() throws Exception {
		_logger.info("Checking for at least one account");

		if (userService.listUsers(new PageRequest(0, 1)).getTotalElements() == 0) {
			createDefaultAccounts();
		}

		if (userService.getSystemUser("SYSTEM") == null) {
			createAdmin(true, "SYSTEM", null, "SYSTEM");
		}
	}

	private void createDefaultAccounts() throws UserException, PasswordPolicyException {
		createAdmin(true, "SYSTEM", null, "SYSTEM");
		createAdmin(false, "admin@example.com", "admin", "First Admin");
	}

	private void createAdmin(boolean systemAccount, String email, String passwd, String name) throws UserException, PasswordPolicyException {
		final User user = new User();
		user.setSystemAccount(systemAccount);
		user.setEmail(email);
		user.setPassword(passwd);
		user.setName(name);
		final Set<UserRole> userRoles = new HashSet<UserRole>();
		userRoles.add(UserRole.ADMINISTRATOR);
		user.setRoles(userRoles);
		// user.setUserGroups(userGroupService.getUserGroupList());

		userService.addUser(user);
		_logger.warn("Admin account for " + email + " has been successfully added.");
	}
}
