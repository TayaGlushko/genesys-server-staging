/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.listener.sample;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map.Entry;

import org.apache.commons.io.IOUtils;
import org.genesys2.server.listener.RunAsAdminListener;
import org.genesys2.server.model.impl.Article;
import org.genesys2.server.service.ContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This startup listener enumerates the resources in "default-content" directory
 * of this package. Each resource name represents a content slug (URL) of a
 * global content article and the contents is a JSON map of languages with
 * {"title":"..", "body":".."} values.
 *
 * <pre>
 * {
 * 	"en": {
 * 		"title": "Your user account is created",
 * 		"body": "The user account is now registered. You may use it to log in."
 * 	}, "sl": {
 * 	...
 *  }
 * }
 * </pre>
 *
 * @author mobreza
 */
@Component
public class CreateContentListener extends RunAsAdminListener {

	@Value("${auto.createContent}")
	private final boolean createContent = false;

	@Autowired
	private ContentService contentService;

	@Override
	public void init() throws Exception {
		createArticles();
		createMenus();
	}

	private void createArticles() throws IOException, JsonProcessingException {
		if (!createContent) {
			_logger.info("Skipping menu creation on startup.");
			return;
		}

		_logger.info("Checking if default content exists");
		final ClassLoader classLoader = CreateContentListener.class.getClassLoader();
		final PathMatchingResourcePatternResolver rpr = new PathMatchingResourcePatternResolver(classLoader);
		final String resourcePath = "/default-content/*";
		final Resource[] rs = rpr.getResources(resourcePath);
		for (final Resource r : rs) {
			_logger.info(r.getFilename());

			final String slug = r.getFilename();

			final ObjectMapper mapper = new ObjectMapper();
			final InputStream stream = r.getInputStream();
			final JsonNode json = mapper.readTree(stream);
			IOUtils.closeQuietly(stream);

			final Iterator<Entry<String, JsonNode>> it = json.fields();
			while (it.hasNext()) {
				final Entry<String, JsonNode> entry = it.next();
				final Locale locale = new Locale(entry.getKey());

				// Load from default locale if exists
				final Article article = contentService.getGlobalArticle(slug, locale, false);

				// If nothing is found, parse the resource and create content
				if (article == null) {
					contentService.updateGlobalArticle(slug, locale, entry.getValue().get("title").asText(), entry.getValue().get("body").asText(), null);
					_logger.info("Created article for slug: " + slug + " lang=" + locale.getLanguage());
				}
			}
		}
	}

	/**
	 * Generates CMS Menu and MenuItems
	 */
	private void createMenus() {
		_logger.info("Checking if default menus exist");

		// legal
		contentService.ensureMenuItem("legal", "/content/legal/disclaimer", "menu.disclaimer");
		contentService.ensureMenuItem("legal", "/content/legal/terms", "menu.terms");
		contentService.ensureMenuItem("legal", "/content/legal/copying", "menu.copying");
		contentService.ensureMenuItem("legal", "/content/legal/privacy", "menu.privacy");

		// about
		contentService.ensureMenuItem("about", "/content/about/about", "menu.about");
		contentService.ensureMenuItem("about", "/content/about/contact", "menu.contact");
		contentService.ensureMenuItem("about", "/content/about/what-is-genesys", "menu.what-is-genesys");
		contentService.ensureMenuItem("about", "/content/about/history-of-genesys", "menu.history-of-genesys");
		contentService.ensureMenuItem("about", "/content/about/newsletter", "menu.newsletter");
		contentService.ensureMenuItem("about", "/content/about/frequently-asked-questions", "menu.faq");
		contentService.ensureMenuItem("about", "/content/help/how-to-use-genesys", "menu.how-to-use-genesys");
		

		// legal
		contentService.ensureMenuItem("help", "/content/help/how-to-use-genesys", "menu.how-to-use-genesys");
//		contentService.ensureMenuItem("help", "/content/help/about-genesys-data", "menu.about-genesys-data");
//		contentService.ensureMenuItem("help", "/content/help/who-uses-genesys", "menu.who-uses-genesys");
//		contentService.ensureMenuItem("help", "/content/help/how-to-use-genesys", "menu.how-to-use-genesys");
		contentService.ensureMenuItem("help", "/content/about/about", "menu.about");

	}
}
