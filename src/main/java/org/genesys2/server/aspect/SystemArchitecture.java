/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class SystemArchitecture {

	/**
	 * Securing services pointcuts
	 */

	@Pointcut("businessServices() || sampleServices() || securityServices() || controllers()")
	public void allServices() {
	}

	@Pointcut("execution(* org.genesys2.server.service.*.*(..))")
	public void businessServices() {
	}

	@Pointcut("execution(* org.genesys2.server.listener.sample.*.*(..))")
	public void sampleServices() {
	}

	@Pointcut("execution(* org.genesys2.server.security.*.*(..))")
	public void securityServices() {
	}

	@Pointcut("execution(* org.genesys2.server.servlet.controller.*.*(..))")
	public void controllers() {
	}

	/**
	 * ACL-based pointcuts
	 */

	// USER
	@Pointcut("execution(* org.genesys2.server.service.impl.UserServiceImpl.addUser(..))")
	public void addUserModel() {
	}

	@Pointcut("execution(* org.genesys2.server.service.impl.UserServiceImpl.updateUser(..))")
	public void changeUserModel() {
	}

	@Pointcut("execution(* org.genesys2.server.service.impl.UserServiceImpl.removeUser(..))")
	public void removeUserModel() {
	}

}
