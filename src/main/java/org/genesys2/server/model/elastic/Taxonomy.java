package org.genesys2.server.model.elastic;

import org.apache.commons.lang.StringUtils;
import org.genesys2.server.model.genesys.Taxonomy2;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldIndex;
import org.springframework.data.elasticsearch.annotations.FieldType;

public class Taxonomy {

	private String sciName;
	@Field(index = FieldIndex.not_analyzed, type = FieldType.String)
	private String genus;
	@Field(index = FieldIndex.not_analyzed, type = FieldType.String)
	private String species;
	// Enable non-indexed access to genus+species
	@Field(index = FieldIndex.not_analyzed, type = FieldType.String)
	private String genusSpecies;
	@Field(index = FieldIndex.not_analyzed, type = FieldType.String)
	private String spAuthor;
	@Field(index = FieldIndex.not_analyzed, type = FieldType.String)
	private String subtaxa;
	@Field(index = FieldIndex.not_analyzed, type = FieldType.String)
	private String subtAuthor;

	public static Taxonomy from(Taxonomy2 taxonomy) {
		if (taxonomy == null) {
			return null;
		}
		Taxonomy t = new Taxonomy();
		t.sciName = taxonomy.getTaxonName();
		t.genus = taxonomy.getGenus();
		t.species = taxonomy.getSpecies();
		t.genusSpecies = taxonomy.getGenus() + " " + taxonomy.getSpecies();
		t.spAuthor = StringUtils.defaultIfBlank(taxonomy.getSpAuthor(), null);
		t.subtaxa = StringUtils.defaultIfBlank(taxonomy.getSubtaxa(), null);
		t.subtAuthor = StringUtils.defaultIfBlank(taxonomy.getSubtAuthor(), null);
		return t;
	}

	public String getSciName() {
		return sciName;
	}

	public void setSciName(String sciName) {
		this.sciName = sciName;
	}

	public String getGenus() {
		return genus;
	}

	public void setGenus(String genus) {
		this.genus = genus;
	}

	public String getSpecies() {
		return species;
	}

	public void setSpecies(String species) {
		this.species = species;
	}

	public void setGenusSpecies(String genusSpecies) {
		this.genusSpecies = genusSpecies;
	}

	public String getGenusSpecies() {
		return genusSpecies;
	}

	public String getSpAuthor() {
		return spAuthor;
	}

	public void setSpAuthor(String spAuthor) {
		this.spAuthor = spAuthor;
	}

	public String getSubtaxa() {
		return subtaxa;
	}

	public void setSubtaxa(String subtax) {
		this.subtaxa = subtax;
	}

	public String getSubtAuthor() {
		return subtAuthor;
	}

	public void setSubtAuthor(String subtAuthor) {
		this.subtAuthor = subtAuthor;
	}

}