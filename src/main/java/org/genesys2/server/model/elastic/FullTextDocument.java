/**
 * Copyright 2016 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
package org.genesys2.server.model.elastic;

import java.util.Date;

import org.genesys2.server.model.impl.ClassPK;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldIndex;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * <code>FullTextDocument</code> is used in Elasticsearch mapping
 */
@Document(indexName = "fulltext", refreshInterval = "60s")
public class FullTextDocument {
	@Id
	private Long id;

	@Field(index = FieldIndex.not_analyzed, type = FieldType.String)
	private String urlToContent;

	@Field(index = FieldIndex.not_analyzed, type = FieldType.Object)
	private ClassPK classPK;

	@Field(type = FieldType.String)
	private String body;

	@Field(type = FieldType.String)
	private String summary;

	@Field(index = FieldIndex.not_analyzed, type = FieldType.String)
	private String language;

	@Field(index = FieldIndex.not_analyzed, type = FieldType.Date)
	private Date createdDate;

	@Field(index = FieldIndex.not_analyzed, type = FieldType.Date)
	private Date lastModifiedDate;

	/** The score here will boost result relevance */
	private Float score = 0.5f;

	public Long getId() {
		return this.id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public String getUrlToContent() {
		return this.urlToContent;
	}

	public void setUrlToContent(final String urlToContent) {
		this.urlToContent = urlToContent;
	}

	public ClassPK getClassPK() {
		return this.classPK;
	}

	public void setClassPK(final ClassPK classPK) {
		this.classPK = classPK;
	}

	public String getBody() {
		return this.body;
	}

	public void setBody(final String body) {
		this.body = body;
	}

	public String getSummary() {
		return this.summary;
	}

	public void setSummary(final String summary) {
		this.summary = summary;
	}

	public String getLanguage() {
		return this.language;
	}

	public void setLanguage(final String language) {
		this.language = language;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(final Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getLastModifiedDate() {
		return this.lastModifiedDate;
	}

	public void setLastModifiedDate(final Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public Float getScore() {
		return score;
	}

	public void setScore(Float score) {
		this.score = score;
	}
}
