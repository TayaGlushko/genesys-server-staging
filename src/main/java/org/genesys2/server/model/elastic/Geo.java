package org.genesys2.server.model.elastic;

import org.apache.commons.lang.StringUtils;
import org.genesys2.server.model.genesys.AccessionGeo;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldIndex;
import org.springframework.data.elasticsearch.annotations.FieldType;

public class Geo {

	private String method;
	private Double uncertainty;
	private Double latitude;
	private Double longitude;
	private Double[] coordinates;
	private Double elevation;
	@Field(index = FieldIndex.not_analyzed, type = FieldType.String)
	private String datum;

	public static Geo from(AccessionGeo ag) {
		if (ag == null)
			return null;
		Geo g = new Geo();
		g.datum = StringUtils.defaultIfBlank(ag.getDatum(), null);
		g.elevation = ag.getElevation();
		if (ag.getLongitude() != null && ag.getLatitude() != null)
			g.coordinates = new Double[] { ag.getLongitude(), ag.getLatitude() };
		g.longitude = ag.getLongitude();
		g.latitude = ag.getLatitude();
		g.uncertainty = ag.getUncertainty();
		g.method = StringUtils.defaultIfBlank(ag.getMethod(), null);
		return g;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public Double getUncertainty() {
		return uncertainty;
	}

	public void setUncertainty(Double uncertainty) {
		this.uncertainty = uncertainty;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double[] getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(Double[] coordinates) {
		this.coordinates = coordinates;
	}

	public Double getElevation() {
		return elevation;
	}

	public void setElevation(Double elevation) {
		this.elevation = elevation;
	}

	public String getDatum() {
		return datum;
	}

	public void setDatum(String datum) {
		this.datum = datum;
	}

	public String getLatitudeAndLongitude() {
		if (this.latitude==null && this.longitude==null) {
			return null;
		}
		return this.latitude + ", " + this.longitude;
	}
}