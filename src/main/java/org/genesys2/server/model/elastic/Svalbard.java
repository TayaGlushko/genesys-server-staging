package org.genesys2.server.model.elastic;

import org.genesys2.server.model.genesys.SvalbardData;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldIndex;
import org.springframework.data.elasticsearch.annotations.FieldType;

public class Svalbard {

	private Float qty;
	@Field(index = FieldIndex.not_analyzed, type = FieldType.String)
	private String boxNo;
	@Field(index = FieldIndex.not_analyzed, type = FieldType.String)
	private String depositDate;

	public static Svalbard from(SvalbardData svalbardData) {
		if (svalbardData == null)
			return null;
		Svalbard s = new Svalbard();
		s.boxNo = svalbardData.getBoxNumber();
		s.qty = svalbardData.getQuantity();
		s.depositDate = svalbardData.getDepositDate();
		return s;
	}

	public Float getQty() {
		return qty;
	}

	public void setQty(Float qty) {
		this.qty = qty;
	}

	public String getBoxNo() {
		return boxNo;
	}

	public void setBoxNo(String boxNo) {
		this.boxNo = boxNo;
	}

	public String getDepositDate() {
		return depositDate;
	}

	public void setDepositDate(String depositDate) {
		this.depositDate = depositDate;
	}

}