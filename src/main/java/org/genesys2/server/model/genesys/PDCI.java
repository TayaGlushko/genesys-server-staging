/**
 * Copyright 2015 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.genesys;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.genesys2.server.model.VersionedModel;

@Entity
@Table(name = "pdci")
public class PDCI extends VersionedModel {
	private static final long serialVersionUID = -1312366054528702261L;

	public static final String[] independentItems = { "genus", "species", "spAuthor", "subTaxa", "subtAuthor", "cropName", "acqDate", "sampStat", "donorCode",
			"donorNumb", "otherNumb", "duplSite", "storage", "donorName", "duplInstName", "acceUrl", "mlsStat" };
	public static final String[] dependentItems = { "origCty", "collSite", "latitude", "longitude", "elevation", "collDate", "bredCode", "ancest", "collSrc",
			"acceName", "collNumb", "collCode", "collName" };

	@OneToOne(cascade = {}, fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "accessionId")
	private AccessionId accession;

	private Float score = null;
	private Float scoreHist = null;

	private int acqDate = 0;
	private int sampStat = 0;
	private int donorCode = 0;
	private int genus = 0;
	private int species = 0;
	private int spAuthor = 0;
	private int subTaxa = 0;
	private int subtAuthor = 0;
	private int donorNumb = 0;
	private int otherNumb = 0;
	private int duplSite = 0;
	private int storage = 0;
	private int donorName = 0;
	private int duplInstName = 0;
	private int acceUrl = 0;
	private int mlsStat = 0;
	private int origCty = 0;
	private int latitude = 0;
	private int longitude = 0;
	private int collSite = 0;
	private int elevation = 0;
	private int collDate = 0;
	private int collSrc = 0;
	private int collNumb = 0;
	private int collCode = 0;
	private int collName = 0;
	private int ancest = 0;
	private int acceName = 0;

	private int bredCode = 0;
	private int cropName = 0;

	@PrePersist
	@PreUpdate
	protected void prePersist() {
		this.score = calculateScore();
		this.scoreHist = (float) (Math.ceil(this.score * 2) / 2);
	}

	public AccessionId getAccession() {
		return accession;
	}

	public void setAccession(AccessionId accession) {
		this.accession = accession;
	}

	public int getAcqDate() {
		return acqDate;
	}

	public int getSampStat() {
		return sampStat;
	}

	public int getDonorCode() {
		return donorCode;
	}

	public int getGenus() {
		return genus;
	}

	public int getSpecies() {
		return species;
	}

	public int getSpAuthor() {
		return spAuthor;
	}

	public int getSubTaxa() {
		return subTaxa;
	}

	public int getSubtAuthor() {
		return subtAuthor;
	}

	public int getDonorNumb() {
		return donorNumb;
	}

	public int getOtherNumb() {
		return otherNumb;
	}

	public int getDuplSite() {
		return duplSite;
	}

	public int getStorage() {
		return storage;
	}

	public int getDonorName() {
		return donorName;
	}

	public int getDuplInstName() {
		return duplInstName;
	}

	public int getAcceUrl() {
		return acceUrl;
	}

	public int getMlsStat() {
		return mlsStat;
	}

	public int getOrigCty() {
		return origCty;
	}

	public int getLatitude() {
		return latitude;
	}

	public int getLongitude() {
		return longitude;
	}

	public int getCollSite() {
		return collSite;
	}

	public int getElevation() {
		return elevation;
	}

	public int getCollDate() {
		return collDate;
	}

	public int getCollSrc() {
		return collSrc;
	}

	public int getCollNumb() {
		return collNumb;
	}

	public int getCollCode() {
		return collCode;
	}

	public int getCollName() {
		return collName;
	}

	public int getAncest() {
		return ancest;
	}

	public int getAcceName() {
		return acceName;
	}

	public int getBredCode() {
		return bredCode;
	}

	public void setAcqDate(int score) {
		this.acqDate = score;
	}

	public void setSampStat(int score) {
		this.sampStat = score;
	}

	public void setDonorCode(int score) {
		this.donorCode = score;
	}

	public void setGenus(int score) {
		this.genus = score;
	}

	public void setSpecies(int score) {
		this.species = score;
	}

	public void setSpAuthor(int score) {
		this.spAuthor = score;
	}

	public void setSubTaxa(int score) {
		this.subTaxa = score;
	}

	public void setSubtAuthor(int score) {
		this.subtAuthor = score;
	}

	public void setDonorNumb(int score) {
		this.donorNumb = score;
	}

	public void setOtherNumb(int score) {
		this.otherNumb = score;
	}

	public void setDuplSite(int score) {
		this.duplSite = score;
	}

	public void setStorage(int score) {
		this.storage = score;
	}

	public void setDonorName(int score) {
		this.donorName = score;
	}

	public void setDuplInstName(int score) {
		this.duplInstName = score;
	}

	public void setAcceUrl(int score) {
		this.acceUrl = score;
	}

	public void setMlsStat(int score) {
		this.mlsStat = score;
	}

	public void setOrigCty(int score) {
		this.origCty = score;
	}

	public void setLatitude(int score) {
		this.latitude = score;
	}

	public void setLongitude(int score) {
		this.longitude = score;
	}

	public void setCollSite(int score) {
		this.collSite = score;
	}

	public void setElevation(int score) {
		this.elevation = score;
	}

	public void setCollDate(int score) {
		this.collDate = score;
	}

	public void setCollSrc(int score) {
		this.collSrc = score;
	}

	public void setCollNumb(int score) {
		this.collNumb = score;
	}

	public void setCollCode(int score) {
		this.collCode = score;
	}

	public void setCollName(int score) {
		this.collName = score;
	}

	public void setAncest(int score) {
		this.ancest = score;
	}

	public void setAcceName(int score) {
		this.acceName = score;
	}

	public void setBredCode(int score) {
		this.bredCode = score;
	}

	public Float getScore() {
		if (score == null) {
			score = calculateScore();
		}
		return score;
	}

	public void setScore(Float score) {
		this.score = score;
	}

	public Float getScoreHist() {
		if (scoreHist == null) {
			this.scoreHist = (float) (Math.ceil(getScore() * 2) / 2);
		}
		return scoreHist;
	}

	public void setScoreHist(Float scoreHist) {
		this.scoreHist = scoreHist;
	}

	private Float calculateScore() {
		float total = getTotal();
		return total / 100f;
	}

	@Transient
	public float getTotal() {
		float score = 0;

		score += acqDate;
		score += sampStat;
		score += donorCode;
		score += genus;
		score += species;
		score += spAuthor;
		score += subTaxa;
		score += subtAuthor;
		score += donorNumb;
		score += otherNumb;
		score += duplSite;
		score += storage;
		score += donorName;
		score += duplInstName;
		score += acceUrl;
		score += mlsStat;
		score += origCty;
		score += latitude;
		score += longitude;
		score += collSite;
		score += elevation;
		score += collDate;
		score += collSrc;
		score += collNumb;
		score += collCode;
		score += collName;
		score += ancest;
		score += acceName;

		score += bredCode;
		score += cropName;
		return score;
	}

	public void setCropName(int score) {
		this.cropName = score;
	}

	public int getCropName() {
		return cropName;
	}

	public String[] getIndependentItems() {
		return independentItems;
	}

	public String[] getDependentItems() {
		return dependentItems;
	}

	public static Set<String> getPdciItems() {
		Set<String> descriptors = new HashSet<String>();
		for (java.lang.reflect.Method method : PDCI.class.getMethods()) {
			if (method.getName().startsWith("get") && method.getReturnType() == int.class) {
				String str = method.getName().substring(3);
				descriptors.add(str.substring(0, 1).toLowerCase() + str.substring(1));
			}
		}
		return descriptors;
	}

	/**
	 * Reset all scores to 0.
	 */
	public void reset() {
		score = null;
		scoreHist = null;

		acqDate = 0;
		sampStat = 0;
		donorCode = 0;
		genus = 0;
		species = 0;
		spAuthor = 0;
		subTaxa = 0;
		subtAuthor = 0;
		donorNumb = 0;
		otherNumb = 0;
		duplSite = 0;
		storage = 0;
		donorName = 0;
		duplInstName = 0;
		acceUrl = 0;
		mlsStat = 0;
		origCty = 0;
		latitude = 0;
		longitude = 0;
		collSite = 0;
		elevation = 0;
		collDate = 0;
		collSrc = 0;
		collNumb = 0;
		collCode = 0;
		collName = 0;
		ancest = 0;
		acceName = 0;

		bredCode = 0;
		cropName = 0;
	}
}
