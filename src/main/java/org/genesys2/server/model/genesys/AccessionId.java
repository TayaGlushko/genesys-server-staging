/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.genesys;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import org.genesys2.server.model.IdUUID;
import org.genesys2.server.model.VersionedAuditedModel;
import org.genesys2.server.model.impl.AccessionList;

/**
 * Entity holds the assigned accession identifiers regardless of active or
 * historic records.
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "acce")
public class AccessionId extends VersionedAuditedModel implements IdUUID {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6224417080504343264L;

	@Column(columnDefinition = "binary(16)")
	protected UUID uuid;

	@OneToMany(cascade = {}, fetch = FetchType.LAZY, mappedBy = "accession", orphanRemoval = false)
	private List<AccessionTrait> traits;

	@ManyToMany(cascade = {}, fetch = FetchType.LAZY)
	@JoinTable(name = "accelistitems", joinColumns = @JoinColumn(name = "acceid"), inverseJoinColumns = @JoinColumn(name = "listid"))
	private Set<AccessionList> lists;

	@PrePersist
	private void prepersist() {
		if (uuid == null) {
			uuid = UUID.randomUUID();
		}
	}

	@Override
	public UUID getUuid() {
		return this.uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public Set<AccessionList> getLists() {
		return lists;
	}

	public void setLists(Set<AccessionList> lists) {
		this.lists = lists;
	}
	//
	// public List<AccessionTrait> getTraits() {
	// return traits;
	// }
	//
	// public void setTraits(List<AccessionTrait> traits) {
	// this.traits = traits;
	// }
}
