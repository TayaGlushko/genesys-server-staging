package org.genesys2.server.model.json;

import java.util.HashMap;
import java.util.Map;

import org.genesys2.server.model.impl.Descriptor;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Helper class for temperature and precipitation charts
 */
public class WorldclimJson {
	// Monthlies
	private Long[] precipitation = new Long[12];
	private Double[] tempMin = new Double[12];
	private Double[] tempMean = new Double[12];
	private Double[] tempMax = new Double[12];

	@JsonIgnore
	private Map<Descriptor, Object> other = new HashMap<Descriptor, Object>();

	public Long[] getPrecipitation() {
		return precipitation;
	}

	public Double[] getTempMax() {
		return tempMax;
	}

	public Double[] getTempMean() {
		return tempMean;
	}

	public Double[] getTempMin() {
		return tempMin;
	}

	public void addOther(Descriptor descriptor, Object val) {
		other.put(descriptor, val);
	}

	public Map<Descriptor, Object> getOther() {
		return this.other;
	}
}
