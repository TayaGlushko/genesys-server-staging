/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.kpi;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;

@Entity
public class StringListDimension extends FixedListDimension<String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7027186911390675241L;
	@Column(name = "listvalue", nullable = false)
	@ElementCollection()
	@CollectionTable(name = "kpidimensionstring", joinColumns = @JoinColumn(name = "dimensionId"))
	private Set<String> values = new HashSet<String>();

	public void setValues(Set<String> values) {
		this.values = values;
	}

	@Override
	public Set<String> getValues() {
		return this.values;
	}

}
