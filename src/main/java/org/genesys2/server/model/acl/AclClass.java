/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.acl;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.genesys2.server.model.BusinessModel;

@Entity
@Table(name = "acl_class")
public class AclClass extends BusinessModel {

	
	private static final long serialVersionUID = 7634040355879912092L;
	@Column(name = "class", nullable = false, unique = true, length = 255)
	private String aclClass;

	public String getAclClass() {
		return aclClass;
	}

	public void setAclClass(String aclClass) {
		this.aclClass = aclClass;
	}
}
