package org.genesys2.server.model.dataset;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("2")
public class DSValueDouble extends DSValue<Double> {

	private Double vald;

	@Override
	public Double getValue() {
		return this.vald;
	}

	@Override
	public void setValue(Double value) {
		this.vald = value;
	}

}
