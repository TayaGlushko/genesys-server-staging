/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.impl;

import java.beans.Transient;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.persistence.Cacheable;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import net.sf.oval.constraint.Email;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import org.genesys2.server.model.BusinessModel;
import org.genesys2.server.model.UserRole;
import org.genesys2.server.servlet.controller.rest.serialization.UserSerializer;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Cacheable
@Entity
@Table(name = "\"user\"")
@JsonSerialize(using = UserSerializer.class)
public class User extends BusinessModel {

	
	private static final long serialVersionUID = 4564013753931115445L;

	@Column(length = 36, unique = true)
	private String uuid;

	// validation
	@NotNull(message = "sample.error.not.null")
	@NotEmpty(message = "sample.error.not.empty")
	@Email(message = "sample.error.wrong.email")
	// hibernate
	@Column(name = "email", nullable = false, unique = true)
	private String email;

	// validation
	@NotNull(message = "sample.error.not.null")
	@NotEmpty(message = "sample.error.not.empty")
	// hibernate
	@Column(name = "password", nullable = false)
	private String password;

	// validation
	@NotNull(message = "sample.error.not.null")
	@NotEmpty(message = "sample.error.not.empty")
	// hibernate
	@Column(name = "name", nullable = false)
	private String name;

	@Enumerated(EnumType.STRING)
	@Column(name = "loginType", length=10, nullable=false, columnDefinition="VARCHAR(10) DEFAULT 'PASSWORD'")
	private LoginType loginType = LoginType.PASSWORD;

	// validation
	@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
	@ElementCollection
	@Enumerated(EnumType.STRING)
	@CollectionTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"))
	@Column(name = "user_role")
	private Set<UserRole> roles = new HashSet<UserRole>();

	/**
	 * System accounts cannot log in through web or otherwise.
	 */
	@Column(nullable = false, updatable = false, name = "sys")
	private boolean systemAccount = false;

	@Column
	private boolean enabled = true;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = true)
	private Date lockedUntil;

	@PrePersist
	void ensureUUID() {
		if (this.uuid == null) {
			this.uuid = UUID.nameUUIDFromBytes(email.getBytes()).toString();
		}
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LoginType getLoginType() {
		return loginType;
	}

	public void setLoginType(LoginType loginType) {
		this.loginType = loginType;
	}

	public Set<UserRole> getRoles() {
		return roles;
	}

	public void setRoles(Set<UserRole> roles) {
		this.roles = roles;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof User)) {
			return false;
		}

		final User user = (User) o;

		if (email != null ? !email.equals(user.email) : user.email != null) {
			return false;
		}
		if (name != null ? !name.equals(user.name) : user.name != null) {
			return false;
		}
		if (password != null ? !password.equals(user.password) : user.password != null) {
			return false;
		}
		if (roles != null ? !roles.equals(user.roles) : user.roles != null) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		int result = email != null ? email.hashCode() : 0;
		result = 31 * result + (password != null ? password.hashCode() : 0);
		result = 31 * result + (name != null ? name.hashCode() : 0);
		result = 31 * result + (roles != null ? roles.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "User id=" + id + " email=" + email;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public void setSystemAccount(boolean systemAccount) {
		this.systemAccount = systemAccount;
	}

	public boolean isSystemAccount() {
		return systemAccount;
	}

	public boolean isEnabled() {
		return this.enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public Date getLockedUntil() {
		return this.lockedUntil;
	}

	public void setLockedUntil(Date lockedUntil) {
		this.lockedUntil = lockedUntil;
	}

	@Transient
	public boolean isAccountLocked() {
		return this.lockedUntil != null && this.lockedUntil.after(new Date());
	}

	@Transient
	public boolean isAccountExpired() {
		// We don't support account expiration
		return false;
	}

	@Transient
	public boolean isPasswordExpired() {
		// We don't support password expiration
		return false;
	}

	public boolean hasRole(String roleName) {
		for (final UserRole userRole : getRoles()) {
			if (userRole.getName().equalsIgnoreCase(roleName)) {
				return true;
			}
		}
		return false;
	}

}
