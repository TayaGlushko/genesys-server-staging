/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
//import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.AclAwareModel;
import org.genesys2.server.model.GlobalVersionedAuditedModel;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Cacheable
@Entity
@Table(name = "crop")
public class Crop extends GlobalVersionedAuditedModel implements AclAwareModel {

	private static final long serialVersionUID = -2686341831839109257L;

	public static final Log LOG = LogFactory.getLog(Crop.class);

	/**
	 * Crop short name used as short name in URLs
	 */
	@Column(nullable = false, length = 50, unique = true)
	private String shortName;

	@Column(name = "otherName", nullable = false)
	@ElementCollection(fetch = FetchType.LAZY)
	@CollectionTable(name = "cropname", joinColumns = @JoinColumn(name = "cropId", referencedColumnName = "id"), uniqueConstraints = { @UniqueConstraint(columnNames = "otherName") })
	@OrderBy("otherName")
	private List<String> otherNames;

	@Column(nullable = false, length = 200)
	private String name;

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String description;

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String i18n;

	/**
	 * Rules
	 */
	@JsonIgnore
	@OneToMany(mappedBy = "crop", cascade = { CascadeType.REMOVE }, orphanRemoval = true)
	private List<CropRule> cropRules;

	@Transient
	@JsonIgnore
	private transient JsonNode i18nJ;

	@Transient
	@JsonIgnore
	private transient JsonNode i18nJO;

	@Transient
	@JsonIgnore
	private transient JsonNode i18nJU;

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<CropRule> getCropRules() {
		return cropRules;
	}

	public void setCropRules(List<CropRule> cropRules) {
		this.cropRules = cropRules;
	}

	public String getI18n() {
		return i18n;
	}

	public void setI18n(String i18n) {
		this.i18n = i18n;
	}

	public void updateI18n(String i18n) {
		// additional sanity check (but already checked elsewhere?)
		if (!(i18n == null || StringUtils.isBlank(i18n))) {
			update(i18n);
		}
	}


	/*
	 * This is meant to be a complex language tag merge operation... is there an
	 * easier way?
	 */
	private synchronized void update(String i18n) {
		this.i18n = i18n;
		/**
		 * TODO Need to properly code and test this! ObjectMapper mapper = new
		 * ObjectMapper(); try { this.i18nJO = mapper.readTree(this.i18n);
		 * this.i18nJU = mapper.readTree(i18n); } catch (IOException e) {
		 * System.err.println("I18n = " + i18n); e.printStackTrace(); } if
		 * (this.i18nJU != null) { Iterator<String> jui =
		 * this.i18nJU.fieldNames(); while (jui.hasNext()) { String uField =
		 * jui.next(); if (this.i18nJO.has(uField)) { JsonNode juf =
		 * this.i18nJU.get(uField); JsonNode jof = this.i18nJO.get(uField);
		 * Iterator<String> ulocales = juf.fieldNames(); while
		 * (ulocales.hasNext()) { String locale = ulocales.next(); // TODO -
		 * append/overwrite jof.ufield.locale with // juf.ufield.locale } } }
		 * this.i18n = this.i18nJO.asText(); }
		 */
	}

	private synchronized void getI18nJson() {
		if (this.i18nJ == null && !StringUtils.isBlank(this.i18n)) {
			final ObjectMapper mapper = new ObjectMapper();
			try {
				this.i18nJ = mapper.readTree(this.i18n);
			} catch (final IOException e) {
				System.err.println("I18n = " + this.i18n);
				e.printStackTrace();
			}
		}
	}

	private synchronized String translate(String field, Locale locale) {
		getI18nJson();
		return this.i18nJ != null && this.i18nJ.has(field) && this.i18nJ.get(field).has(locale.getLanguage()) ? this.i18nJ.get(field).get(locale.getLanguage())
				.textValue() : null;
	}

	@JsonIgnore
	@Transient
	private transient final Map<String, Map<String, String>> i18nMap = new HashMap<String, Map<String, String>>();

	private synchronized Map<String, String> buildVernacularMap(String field) {
		getI18nJson();
		if (!this.i18nMap.containsKey(field) && this.i18nJ.has(field)) {
			this.i18nMap.put(field, new HashMap<String, String>());
			final JsonNode fieldMap = this.i18nJ.get(field);
			final Iterator<String> languages = fieldMap.fieldNames();
			while (languages.hasNext()) {
				final String language = languages.next();
				final String vernacular = fieldMap.get(language).textValue();
				this.i18nMap.get(field).put(language, vernacular);
			}

		}
		return i18nMap.get(field);
	}

	public String getName(Locale locale) {
		return StringUtils.defaultIfBlank(translate("name", locale), this.name);
	}

	public String getDescription(Locale locale) {
		return StringUtils.defaultIfBlank(translate("description", locale), this.name);
	}

	/**
	 * Method to return the map of available vernacular names of crops
	 * 
	 * @return Map<String,String> with language code as the key and the
	 *         vernacular string as the value.
	 */
	@JsonIgnore
	public Map<String, String> getLocalNameMap() {
		return buildVernacularMap("name");
	}

	/**
	 * Method to return the map of available vernacular definitions of crops
	 * 
	 * @return Map<String,String> with language code as the key and the
	 *         vernacular string as the value.
	 */
	@JsonIgnore
	public Map<String, String> getLocalDefinitionMap() {
		return buildVernacularMap("description");
	}

	public List<String> getOtherNames() {
		return otherNames;
	}

	public void setOtherNames(List<String> otherNames) {
		this.otherNames = otherNames;
	}
}
