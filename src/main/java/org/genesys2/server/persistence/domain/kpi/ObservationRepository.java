/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.persistence.domain.kpi;

import java.util.List;

import org.genesys2.server.model.kpi.DimensionKey;
import org.genesys2.server.model.kpi.Execution;
import org.genesys2.server.model.kpi.ExecutionRun;
import org.genesys2.server.model.kpi.Observation;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface ObservationRepository extends JpaRepository<Observation, Long>, ObservationCustomRepository {

	@Query("select o from Observation o where o.executionRun=?1 order by o.value desc")
	List<Observation> findByExecutionRun(ExecutionRun executionRun, Pageable page);

	List<Observation> findByExecutionRun(ExecutionRun executionRun);

	@Query("select o from Observation o where o.executionRun.execution=?1 and ?2 member of o.dimensions order by o.executionRun.timestamp desc")
	List<Observation> listObservationsByDimensionKey(Execution execution, DimensionKey next, Pageable page);

	@Modifying
	@Query("delete from Observation o where o.executionRun=?1")
	void deleteByExecutionRun(ExecutionRun executionRun);

}
