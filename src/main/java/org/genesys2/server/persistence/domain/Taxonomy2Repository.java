/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.persistence.domain;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import org.genesys2.server.model.genesys.Taxonomy2;
import org.genesys2.server.model.impl.Crop;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface Taxonomy2Repository extends JpaRepository<Taxonomy2, Long> {

	@Query("select distinct t.genus from Taxonomy2 t where t.genus like ?1")
	List<String> autocompleteGenus(String term, Pageable page);

	@Query("select distinct t.genus from Taxonomy2 t join t.cropTaxonomies ct where lower(t.genus) like lower(?1) and ct.crop = ?2")
	List<String> autocompleteGenusByCrop(String term, Crop crop, Pageable page);

	@Query("select distinct t.species from Taxonomy2 t where lower(t.species) like lower(?1)")
	List<String> autocompleteSpecies(String term, Pageable page);

	@Query("select distinct t.species from Taxonomy2 t join t.cropTaxonomies ct where lower(t.species) like lower(?1) and ct.crop = ?2")
	List<String> autocompleteSpeciesByCrop(String term, Crop crop, Pageable page);

	@Query("select distinct t.species from Taxonomy2 t where lower(t.species) like lower(?1) and t.genus in (?2)")
	List<String> autocompleteSpeciesByGenus(String term, List<String> genus, Pageable page);

	@Query("select distinct t.taxonName from Taxonomy2 t where lower(t.taxonName) like lower(?1)")
	List<String> autocompleteTaxonomy(String term, Pageable page);

	@Query("select distinct t.subtaxa from Taxonomy2 t where lower(t.subtaxa) like lower(?1)")
	List<String> autocompleteSubtaxa(String string, Pageable page);

	@Query("select distinct t.subtaxa from Taxonomy2 t join t.cropTaxonomies ct where lower(t.subtaxa) like lower(?1) and ct.crop = ?2")
	List<String> autocompleteSubtaxaByCrop(String string, Crop crop, Pageable page);

	@Query("select distinct t.subtaxa from Taxonomy2 t where lower(t.subtaxa) like lower(?1) and t.genus in (?2)")
	List<String> autocompleteSubtaxaByGenus(String string, List<String> genus, Pageable page);

	@Query("select distinct t.subtaxa from Taxonomy2 t where lower(t.subtaxa) like lower(?1) and t.species in (?2)")
	List<String> autocompleteSubtaxaBySpecies(String string, List<String> species, Pageable page);

	@Query("select distinct t.subtaxa from Taxonomy2 t where lower(t.subtaxa) like lower(?1) and t.genus in (?2) and t.species in (?3)")
	List<String> autocompleteSubtaxaByGenusAndSpecies(String string, List<String> genus, List<String> species, Pageable page);
	
	@Query("select t from Taxonomy2 t where lower(t.genus) = lower(?1) and lower(t.species) = lower(?2) and lower(t.spAuthor) = lower(?3) and lower(t.subtaxa) = lower(?4) and lower(t.subtAuthor) = lower(?5)")
	Taxonomy2 findByGenusAndSpeciesAndSpAuthorAndSubtaxaAndSubtAuthor(String genus, String species, String spAuthor, String subtaxa, String subtAuthor);

	List<Taxonomy2> findByGenus(String genus);

	List<Taxonomy2> findByGenusAndSpecies(String genus, String species);

	List<Taxonomy2> findByGenusAndSpeciesAndSubtaxa(String genus, String species, String subtaxa);

	@Query(nativeQuery = true, value = "SELECT ct.taxonomyId FROM croptaxonomy ct " +
			"UNION ALL SELECT t2.taxGenus FROM taxonomy2 t2 " +
			"UNION ALL SELECT t3.taxSpecies FROM taxonomy2 t3 " +
			"UNION ALL SELECT a.taxonomyId2 FROM accession a " +
			"UNION ALL SELECT a2.taxGenus FROM accession a2 " +
			"UNION ALL SELECT ah.taxGenus FROM accessionhistoric ah " +
			"UNION ALL SELECT ah2.taxonomyId2 FROM accessionhistoric ah2;")
	Set<BigInteger> findTaxonomyReferencedIds();

	@Query("select t.id from Taxonomy2 t")
	Set<Long> findTaxonomyIds();

	@Query("delete from Taxonomy2 t where t.id in (?1)")
	@Modifying
	void removeUnusedIds(Set<Long> ids);
}
