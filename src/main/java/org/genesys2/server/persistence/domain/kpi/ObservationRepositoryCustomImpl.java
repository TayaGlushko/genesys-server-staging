/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.persistence.domain.kpi;

import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.kpi.DimensionKey;
import org.genesys2.server.model.kpi.Execution;
import org.genesys2.server.model.kpi.ExecutionRun;
import org.genesys2.server.model.kpi.Observation;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public class ObservationRepositoryCustomImpl implements ObservationCustomRepository {
	public static final Log LOG = LogFactory.getLog(ObservationRepositoryCustomImpl.class);

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<Observation> findObservations(ExecutionRun executionRun, Set<DimensionKey> dks, Pageable page) {
		StringBuilder where = new StringBuilder();
		final int OFFSET = 3;
		for (int i = 0; i < dks.size(); i++) {
			where.append("and ?").append(i + OFFSET).append(" member of o.dimensions ");
		}

		Query q = entityManager.createQuery("select o from Observation o where o.executionRun=?1 and o.dimensionCount=?2 " + where.toString()
				+ " order by o.timestamp desc");
		q.setParameter(1, executionRun);
		q.setParameter(2, dks.size());

		int i = OFFSET;
		for (DimensionKey dk : dks) {
			q.setParameter(i, dk);
			i++;
		}

		q.setFirstResult(page.getOffset());
		q.setMaxResults(page.getPageSize());

		return q.getResultList();
	}

	@Override
	public Observation findLastObservation(Execution execution, Set<DimensionKey> dks) {
		StringBuilder where = new StringBuilder();
		final int OFFSET = 3;
		for (int i = 0; i < dks.size(); i++) {
			where.append("and ?").append(i + OFFSET).append(" member of o.dimensions ");
		}

		Query q = entityManager.createQuery("select o from Observation o where o.execution=?1 and o.dimensionCount=?2 " + where.toString()
				+ " order by o.timestamp desc", Observation.class);
		q.setMaxResults(1);
		q.setParameter(1, execution);
		q.setParameter(2, dks.size());

		int i = OFFSET;
		for (DimensionKey dk : dks) {
			q.setParameter(i, dk);
			i++;
		}

		return (Observation) q.getSingleResult();
	}
}
