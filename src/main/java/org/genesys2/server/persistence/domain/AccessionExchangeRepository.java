/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.persistence.domain;

import java.util.Collection;
import java.util.List;

import org.genesys2.server.model.genesys.AccessionExchange;
import org.genesys2.server.model.genesys.AccessionId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface AccessionExchangeRepository extends JpaRepository<AccessionExchange, Long> {

	AccessionExchange findByAccession(AccessionId accession);

	@Modifying
	@Query("delete from AccessionExchange ae where ae.accession.id in ?1")
	void deleteForAccessions(Collection<Long> accessionIds);

	@Query("from AccessionExchange ae where ae.accession.id in ?1")
	List<AccessionExchange> findAllFor(Collection<Long> accessionIds);
}
