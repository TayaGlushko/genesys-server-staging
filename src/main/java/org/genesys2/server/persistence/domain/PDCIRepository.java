/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.persistence.domain;

import java.util.Collection;
import java.util.List;

import org.genesys2.server.model.genesys.PDCI;
import org.genesys2.server.model.impl.FaoInstitute;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PDCIRepository extends JpaRepository<PDCI, Long> {

	@Query("select pdci from PDCI pdci where pdci.accession.id=?1")
	PDCI findByAccessionId(long accessionId);

	@Query("select pdci from PDCI pdci where pdci.accession.id in (?1)")
	List<PDCI> findByAccessionId(Collection<Long> accessionIds);
	
	/**
	 * Gets [ min(pdci.score), max(pdci.score), avg(pdci.score), count(pdci) ]
	 * for PDCI of {@link FaoInstitute}
	 * 
	 * @param faoInstitute
	 * @return [ Float min(pdci.score), Float max(pdci.score), Double
	 *         avg(pdci.score), Long count(pdci) ]
	 */
	@Query("select min(pdci.score), max(pdci.score), avg(pdci.score), count(pdci) from Accession a, PDCI pdci where a.institute = ?1 and a.historic = false and pdci.accession=a")
	Object statistics(FaoInstitute faoInstitute);

	/**
	 * Get PDCI histogram for {@link FaoInstitute} by
	 * {@link PDCI#getScoreHist()}
	 * 
	 * @param faoInstitute
	 * @return List of [ scoreHist, count ]
	 */
	@Query("select pdci.scoreHist, count(pdci) from Accession a, PDCI pdci where a.institute = ?1 and pdci.accession=a group by pdci.scoreHist")
	List<Object[]> histogram(FaoInstitute faoInstitute);

}
