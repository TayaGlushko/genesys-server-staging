package org.genesys2.server.persistence.domain.dataset;

import org.genesys2.server.model.dataset.DSDescriptor;
import org.genesys2.server.model.dataset.DSRow;
import org.genesys2.server.model.dataset.DSValue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface DSValueRepository extends JpaRepository<DSValue<?>, Long> {

	DSValue<?> findByRowAndDatasetDescriptor(DSRow dsr, DSDescriptor dsd);

	@Modifying
	@Query("delete from DSValue dsv where dsv.datasetDescriptor = ?1")
	int deleteFor(DSDescriptor dsd);

}
