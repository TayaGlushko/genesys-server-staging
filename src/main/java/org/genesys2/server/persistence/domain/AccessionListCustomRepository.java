/**
 * Copyright 2015 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.persistence.domain;

import java.util.Collection;

import org.genesys2.server.model.impl.AccessionList;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilters;

public interface AccessionListCustomRepository {

	int addAll(AccessionList loaded, Collection<Long> accessionIds);
	
	int addAll(AccessionList loaded, AppliedFilters filters);

	int removeAll(AccessionList accessionList);
}
