/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.persistence.acl;

import java.util.List;

import org.genesys2.server.model.acl.AclEntry;
import org.genesys2.server.model.acl.AclObjectIdentity;
import org.genesys2.server.model.acl.AclSid;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AclEntryPersistence extends JpaRepository<AclEntry, Long> {

	@Query("select ae from AclEntry ae where ae.aclObjectIdentity = :aclObjectIdentity")
	List<AclEntry> findByObjectIdentity(@Param("aclObjectIdentity") AclObjectIdentity aclObjectIdentity);

	/**
	 * @param objectIdentityId
	 *            - id of domain object
	 * @param aclClass
	 *            - domain class
	 * @param sid
	 *            - user's email
	 * @return - returns lists of user's permissions
	 */
	@Query("select ae from AclEntry ae join ae.aclObjectIdentity aoi join aoi.aclClass ac join ae.aclSid sid where aoi.objectIdIdentity=?1 and ac.aclClass=?2 and sid.sid=?3")
	List<AclEntry> findByObjectIdentityAndObjectClassIdAndSid(long objectIdentityId, String aclClass, String sid);

	/**
	 * @param sid
	 *            - user's email
	 * @param aclClass
	 *            - class of domain object
	 * @return - returns lists of user's permissions on domain class
	 */
	@Query("select ae from AclEntry ae join ae.aclObjectIdentity aoi join aoi.aclClass ac join ae.aclSid sid where sid.sid=?1 and ac.aclClass=?2")
	List<AclEntry> findBySidAndAclClass(String sid, String aclClass);

	/**
	 * @param sid
	 *            - user's email
	 * @param objectIdIdentity
	 *            - id of domain object
	 * @param mask
	 *            - mask for permissions
	 * @param className
	 *            - class name
	 * @return - returns lists of user's permissions on domain class
	 */
	@Query("select count(ae) from AclEntry ae join ae.aclObjectIdentity aoi join aoi.aclClass ac join ae.aclSid sid where sid.sid=?1 and aoi.objectIdIdentity=?2 and ae.mask=?3 and ac.aclClass=?4")
	Long findBySidAndObjectIdentityAndMask(String sid, long objectIdIdentity, long mask, String className);

	/**
	 * @param sid
	 *            - user's email
	 * @param aclClass
	 *            - class of domain object
	 * @param mask
	 *            - mask for permissions
	 * @return - returns lists of user's permissions on domain class
	 */
	@Query("select aoi.objectIdIdentity from AclEntry ae join ae.aclObjectIdentity aoi join aoi.aclClass ac join ae.aclSid sid where sid.sid=?1 and ac.aclClass=?2 and ae.mask=?3")
	List<Long> findObjectIdentitiesBySidAndAclClassAndMask(String sid, String aclClass, long mask);

	/**
	 * Calculates max. ace_order for acl_object_identity to avoid DuplicateIndex
	 * exception (acl_object_identity + ace_order is unique index)
	 *
	 * @param aclObjectEntityId
	 *            - id of acl_object_identity table
	 * @return - max. ace_order value for current objectIdentityId
	 */
	@Query("select max(ae.aceOrder) from AclEntry ae join ae.aclObjectIdentity aoi where aoi.id = ?1")
	Long getMaxAceOrderForObjectEntity(long aclObjectEntityId);

	@Query("select distinct ae.aclSid from AclEntry ae join ae.aclObjectIdentity aoi join aoi.aclClass ac where aoi.objectIdIdentity = :objectIdIdentity and ac.aclClass = :aclClass")
	List<AclSid> getSids(@Param("objectIdIdentity") long objectIdIdentity, @Param("aclClass") String aclClass);

}
