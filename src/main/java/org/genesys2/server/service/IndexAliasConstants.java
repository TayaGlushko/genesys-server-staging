/**
 * Copyright 2016 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service;

public interface IndexAliasConstants {

	String PASSPORT_TYPE = "mcpd";

	String INDEX_FULLTEXT = "fulltext";

	String INDEX_PASSPORT = "passport";

	String INDEX_GENESYS = "genesys";

	String INDEX_GENESYS_ARCHIVE = "genesysarchive";

	String INDEXALIAS_FULLTEXT_READ = "fulltextRead";

	String INDEXALIAS_FULLTEXT_WRITE = "fulltextWrite";

	String INDEXALIAS_PASSPORT_READ = "passport";

	String INDEXALIAS_PASSPORT_WRITE = "passportWrite";
}
