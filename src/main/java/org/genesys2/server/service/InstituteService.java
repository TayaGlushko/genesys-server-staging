/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service;

import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.genesys2.server.model.impl.Country;
import org.genesys2.server.model.impl.FaoInstitute;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

/**
 * @author mobreza
 */
public interface InstituteService {

	Page<FaoInstitute> list(Pageable p);

	FaoInstitute getInstitute(String wiewsCode);
	FaoInstitute findInstitute(String wiewsCode);

	List<FaoInstitute> getInstitutes(Collection<String> wiewsCodes);

	List<FaoInstitute> listByCountry(Country country);

	List<FaoInstitute> listByCountryActive(Country country);

	Page<FaoInstitute> listActive(Pageable pageable);

	void update(Collection<FaoInstitute> institutes);

	void updateAbout(FaoInstitute faoInstitute, String body, String summary, Locale locale);

	void updateCountryRefs();

    void setCodeSGSV(FaoInstitute faoInstitute, String codeSGSV);

    void updateSettings(FaoInstitute faoInstitute, Map<String, String> settings);

	long countActive();

	void setUniqueAcceNumbs(FaoInstitute faoInstitute, boolean uniqueAcceNumbs);

	void setAllowMaterialRequests(FaoInstitute faoInstitute, boolean allowMaterialRequests);

	void delete(String instCode);

	List<FaoInstitute> listMyInstitutes(Sort sort);

	List<FaoInstitute> autocomplete(String ac);

	Page<FaoInstitute> listPGRInstitutes(Pageable pageable);

}
