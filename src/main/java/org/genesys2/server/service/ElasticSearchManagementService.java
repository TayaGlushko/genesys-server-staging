/**
 * Copyright 2016 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service;

import org.genesys2.server.service.impl.FilterHandler.AppliedFilters;
import org.springframework.data.elasticsearch.ElasticsearchException;

public interface ElasticSearchManagementService {

	/**
	 * Move alias to a different index
	 *
	 * @param aliasName
	 * @param indexName
	 */
	void realias(String aliasName, String indexName);

	/**
	 * Delete an ES alias
	 *
	 * @param aliasName
	 */
	void deleteAlias(String aliasName);

	/**
	 * Checks if alias exists
	 *
	 * @param aliasName
	 */
	boolean aliasExists(String aliasName);

	/**
	 * Refreshes specified index
	 *
	 * @param className
	 */
	void refreshIndex(String className);

	/**
	 * Create new indexes and fill them with data
	 */
	void regenerateIndexes(String indexName) throws ElasticsearchException;

	/**
	 * Reindex part of accession database
	 *
	 * @param filters
	 */
	void reindex(AppliedFilters filters);

	/**
	 * Reindex part of accession database
	 *
	 * @param type
	 */
	void reindex(String type);

	/**
	 * Delete an unused index (no aliases)
	 *
	 * @param indexName
	 */
	void deleteIndex(String indexName);
}
