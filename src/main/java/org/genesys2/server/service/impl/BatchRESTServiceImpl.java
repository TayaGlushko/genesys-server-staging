/**
 * Copyright 2015 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import static org.genesys2.util.NumberUtils.areEqual;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.genesys.AccessionAlias;
import org.genesys2.server.model.genesys.AccessionAlias.AliasType;
import org.genesys2.server.model.genesys.AccessionBreeding;
import org.genesys2.server.model.genesys.AccessionCollect;
import org.genesys2.server.model.genesys.AccessionData;
import org.genesys2.server.model.genesys.AccessionExchange;
import org.genesys2.server.model.genesys.AccessionGeo;
import org.genesys2.server.model.genesys.AccessionId;
import org.genesys2.server.model.genesys.AccessionRemark;
import org.genesys2.server.model.genesys.Taxonomy2;
import org.genesys2.server.model.impl.Country;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.model.json.Api1Constants;
import org.genesys2.server.persistence.domain.AccessionCustomRepository;
import org.genesys2.server.service.AccessionOpResponse;
import org.genesys2.server.service.AccessionOpResponse.UpsertResult;
import org.genesys2.server.service.BatchRESTService;
import org.genesys2.server.service.CropService;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.GeoService;
import org.genesys2.server.service.InstituteService;
import org.genesys2.server.service.OrganizationService;
import org.genesys2.server.service.TaxonomyService;
import org.genesys2.server.servlet.controller.rest.model.AccessionAliasJson;
import org.genesys2.server.servlet.controller.rest.model.AccessionHeaderJson;
import org.genesys2.server.servlet.controller.rest.model.AccessionNamesJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Service
public class BatchRESTServiceImpl implements BatchRESTService {

	private final Log LOG = LogFactory.getLog(getClass());

	@Autowired
	GenesysService genesysService;

	@Autowired
	GeoService geoService;

	@Autowired
	TaxonomyService taxonomyService;

	@Autowired
	CropService cropService;

	@Autowired
	OrganizationService organizationService;

	@Autowired
	InstituteService instituteService;

	@Autowired
	AccessionCustomRepository accessionCustomRepository;

	@Autowired
	private TaxonomyManager taxonomyManager;

	@Override
	// Read-only, everything happens in manager
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#institute, 'WRITE') or hasPermission(#institute, 'CREATE')")
	public void ensureTaxonomies(FaoInstitute institute, Map<AccessionHeaderJson, ObjectNode> batch) throws RESTApiException {
		for (final AccessionHeaderJson dataJson : batch.keySet()) {
			final ObjectNode accnJson = batch.get(dataJson);

			final Taxonomy2 current = new Taxonomy2();
			// Load JSON values into "current"
			current.setGenus(stringIfProvided(accnJson.get(Api1Constants.Accession.GENUS), current.getGenus()));
			current.setGenus(stringIfProvided(accnJson.get(Api1Constants.Accession.GENUS_NEW), current.getGenus()));
			current.setSpecies(StringUtils.defaultIfBlank(stringIfProvided(accnJson.get(Api1Constants.Accession.SPECIES), current.getSpecies()), "sp."));
			current.setSpAuthor(StringUtils.defaultIfBlank(stringIfProvided(accnJson.get(Api1Constants.Accession.SPAUTHOR), current.getSpAuthor()), ""));
			current.setSubtaxa(StringUtils.defaultIfBlank(stringIfProvided(accnJson.get(Api1Constants.Accession.SUBTAXA), current.getSubtaxa()), ""));
			current.setSubtAuthor(StringUtils.defaultIfBlank(stringIfProvided(accnJson.get(Api1Constants.Accession.SUBTAUTHOR), current.getSubtAuthor()), ""));

			if (current.getGenus().contains("sp.")) {
				throw new RESTApiException("GENUS cannot contain 'sp.'. Offending genus: " + current.getGenus());
			}

			if (LOG.isDebugEnabled()) {
				LOG.debug("Ensuring " + current);
			}

			Taxonomy2 ensuredTaxonomy = null;
			try {
				ensuredTaxonomy = taxonomyService.find(current.getGenus(), current.getSpecies(), current.getSpAuthor(), current.getSubtaxa(),
						current.getSubtAuthor());
			} catch (Throwable e) {
				LOG.warn("*** lower(t.genus)=lower(" + current.getGenus() + ") and lower(t.species)=lower(" + current.getSpecies()
						+ ") and lower(t.spauthor)=lower(" + current.getSpAuthor() + ") and lower(t.subtaxa)=lower(" + current.getSubtaxa()
						+ ") and lower(subtauthor)=lower(" + current.getSubtAuthor() + ")");
			}
			if (ensuredTaxonomy == null) {
				LOG.warn("Adding new taxonomy: " + current);
				ensuredTaxonomy = taxonomyManager.ensureTaxonomy2(current.getGenus(), current.getSpecies(), current.getSpAuthor(), current.getSubtaxa(),
						current.getSubtAuthor());
				LOG.info("Registered: " + ensuredTaxonomy);

				ensuredTaxonomy = taxonomyService.find(current.getGenus(), current.getSpecies(), current.getSpAuthor(), current.getSubtaxa(),
						current.getSubtAuthor());

				if (ensuredTaxonomy == null) {
					throw new RuntimeException("Something is seriously wrong with taxonomyManager!");
				}
			}
		}
	}

	@Override
	@Transactional
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#institute, 'WRITE') or hasPermission(#institute, 'CREATE')")
	public List<AccessionOpResponse> upsertAccessionData(FaoInstitute institute, Map<AccessionHeaderJson, ObjectNode> batch) throws RESTApiException {
		LOG.info("Batch processing " + batch.size() + " entries for " + institute.getCode());

		// Modify date
		Date modifyDate = new Date();

		final boolean useUniqueAcceNumbs = institute.hasUniqueAcceNumbs();

		final List<AccessionOpResponse> upsertResponses = new ArrayList<AccessionOpResponse>();

		final List<Accession> toSave = new ArrayList<Accession>();
		final List<AccessionCollect> toSaveColl = new ArrayList<AccessionCollect>();
		final List<AccessionCollect> toRemoveColl = new ArrayList<AccessionCollect>();
		final List<AccessionGeo> toSaveGeo = new ArrayList<AccessionGeo>();
		final List<AccessionGeo> toRemoveGeo = new ArrayList<AccessionGeo>();
		final List<AccessionBreeding> toSaveBreed = new ArrayList<AccessionBreeding>();
		final List<AccessionBreeding> toRemoveBreed = new ArrayList<AccessionBreeding>();
		final List<AccessionExchange> toSaveExch = new ArrayList<AccessionExchange>();
		final List<AccessionExchange> toRemoveExch = new ArrayList<AccessionExchange>();
		final List<AccessionRemark> toSaveRemarks = new ArrayList<AccessionRemark>();
		final List<AccessionRemark> toRemoveRemarks = new ArrayList<AccessionRemark>();

		final Map<AccessionId, ArrayNode> acceNames = new HashMap<AccessionId, ArrayNode>();
		final Map<AccessionId, ArrayNode> otherNumbs = new HashMap<AccessionId, ArrayNode>();
		final Map<AccessionId, ArrayNode> donorNumbs = new HashMap<AccessionId, ArrayNode>();
		final Map<AccessionId, ArrayNode> collNumbs = new HashMap<AccessionId, ArrayNode>();

		List<String> acceNumbs = new ArrayList<String>();
		List<String> genera = new ArrayList<String>();
		for (AccessionHeaderJson dataJson : batch.keySet()) {
			acceNumbs.add(dataJson.acceNumb);
			genera.add(dataJson.genus);
		}

		List<Accession> loaded = null;

		try {
			loaded = accessionCustomRepository.find(institute, acceNumbs, genera);
		} catch (NonUniqueAccessionException e) {
			LOG.warn(e.getMessage());
			throw new RESTApiException(e.getMessage());
		}

		for (final AccessionHeaderJson dataJson : batch.keySet()) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Loading accession " + dataJson);
			}

			AccessionOpResponse upsertResponse = new AccessionOpResponse(dataJson.instCode, dataJson.acceNumb, dataJson.genus);
			upsertResponses.add(upsertResponse);
			UpsertResult upsertResult = null;

			if (!institute.getCode().equals(dataJson.instCode)) {
				throw new RESTApiException("Accession does not belong to instCode=" + institute.getCode() + " acn=" + dataJson);
			}

			Accession accession = loaded.stream()
					.filter(a -> useUniqueAcceNumbs ? (a.getAccessionName().equalsIgnoreCase(dataJson.acceNumb))
							: (a.getAccessionName().equalsIgnoreCase(dataJson.acceNumb) && a.getTaxonomy().getGenus().equalsIgnoreCase(dataJson.genus)))
					.findFirst().orElse(null);

			final ObjectNode accnJson = batch.get(dataJson);

			if (accession == null) {
				if (LOG.isDebugEnabled())
					LOG.debug("New accession " + dataJson);

				accession = new Accession();
				accession.setAccessionId(new AccessionId());

				accession.setInstitute(institute);
				accession.setAccessionName(dataJson.acceNumb);

				if (accnJson.get(Api1Constants.Accession.GENUS) == null && accnJson.get(Api1Constants.Accession.GENUS_NEW) == null) {
					throw new RESTApiException("Cannot create new accession without specifying genus");
				}

				upsertResult = new UpsertResult(UpsertResult.Type.INSERT);

			} else {
				if (LOG.isTraceEnabled())
					LOG.trace("*** Updating accession " + dataJson);

				upsertResult = new UpsertResult(UpsertResult.Type.UPDATE);
				upsertResult.setUUID(accession.getUuid());
			}

			if (accession.getAccessionId().getId() == null || useUniqueAcceNumbs && accnJson.get(Api1Constants.Accession.GENUS) != null
					|| accnJson.get(Api1Constants.Accession.GENUS_NEW) != null || accnJson.get(Api1Constants.Accession.SPECIES) != null
					|| accnJson.get(Api1Constants.Accession.SPAUTHOR) != null || accnJson.get(Api1Constants.Accession.SUBTAXA) != null
					|| accnJson.get(Api1Constants.Accession.SUBTAUTHOR) != null) {

				updateTaxonomy(accession, accnJson);
			}

			updateCrop(accession, accnJson.get(Api1Constants.Accession.CROPNAME));
			updateAcceNumb(accession, accnJson.get(Api1Constants.Accession.ACCENUMB_NEW));
			updateOrgCty(accession, accnJson.get(Api1Constants.Accession.ORIGCTY));
			updateUuid(accession, accnJson.get(Api1Constants.Accession.UUID));
			updateRemarks(accession, accnJson.get(Api1Constants.Accession.REMARKS), toSaveRemarks, toRemoveRemarks);
			updateStorage(accession, accnJson);

			// TODO Move other setters to methods

			JsonNode value = accnJson.get(Api1Constants.Accession.ACQDATE);
			if (value != null) {
				final String acqDate = value.isNull() ? null : value.textValue();
				accession.setAcquisitionDate(acqDate);
			}
			value = accnJson.get(Api1Constants.Accession.MLSSTAT);
			if (value != null) {
				if (!value.isNull() && !value.isBoolean()) {
					throw new RESTApiDataTypeException("If provided, 'mlsStat' must be a boolean");
				}
				final Boolean inMls = value.isNull() ? null : value.asBoolean();
				accession.setMlsStatus(inMls);
			}
			value = accnJson.get(Api1Constants.Accession.INTRUST);
			if (value != null) {
				if (!value.isNull() && !value.isBoolean()) {
					throw new RESTApiDataTypeException("If provided, 'inTrust' must be a boolean");
				}
				final Boolean inTrust = value.isNull() ? null : value.asBoolean();
				accession.setInTrust(inTrust);
			}
			value = accnJson.get(Api1Constants.Accession.AVAILABLE);
			if (value != null) {
				if (!value.isNull() && !value.isBoolean()) {
					throw new RESTApiDataTypeException("If provided, 'available' must be a boolean");
				}
				final Boolean availability = value.isNull() ? null : value.asBoolean();
				accession.setAvailability(availability);
			}
			value = accnJson.get(Api1Constants.Accession.HISTORIC);
			if (value != null) {
				if (value.isNull() || !value.isBoolean()) {
					throw new RESTApiDataTypeException("If provided, 'historic' must be a boolean");
				}
				final boolean historic = value.asBoolean();
				accession.setHistoric(historic);
			}

			value = accnJson.get(Api1Constants.Accession.ACCENAME);
			if (value != null) {
				acceNames.put(accession.getAccessionId(), toMcpdArray(accnJson, Api1Constants.Accession.ACCENAME));
			}

			value = accnJson.get(Api1Constants.Accession.OTHERNUMB);
			if (value != null) {
				otherNumbs.put(accession.getAccessionId(), toMcpdArray(accnJson, Api1Constants.Accession.OTHERNUMB));
			}

			value = accnJson.get(Api1Constants.Accession.SAMPSTAT);
			if (value != null) {
				if (!value.isNull() && !value.isNumber()) {
					throw new RESTApiDataTypeException("If provided, 'sampStat' must be a number");
				}
				final Integer sampStat = value.isNull() || !value.isNumber() ? null : value.asInt();
				accession.setSampleStatus(sampStat);
			}

			value = accnJson.get(Api1Constants.Accession.DUPLSITE);
			if (value != null) {
				final String duplSite = arrayToString(toMcpdArray(accnJson, Api1Constants.Accession.DUPLSITE));
				accession.setDuplSite(StringUtils.defaultIfBlank(duplSite, null));
			}

			value = accnJson.get(Api1Constants.Accession.ACCEURL);
			if (value != null) {
				if (!value.isNull() && !value.isTextual()) {
					throw new RESTApiDataTypeException("If provided, 'acceUrl' must be a String");
				}
				final String acceUrl = value.isNull() ? null : value.textValue();
				accession.setAcceUrl(acceUrl);
			}

			if (accnJson.has(Api1Constants.Accession.COLL)) {
				final ObjectNode collecting = (ObjectNode) accnJson.get(Api1Constants.Accession.COLL);
				AccessionCollect accnColl = genesysService.listAccessionCollect(accession.getAccessionId());
				if (accnColl == null) {
					accnColl = new AccessionCollect();
					accnColl.setAccession(accession.getAccessionId());
				}
				value = collecting.get(Api1Constants.Collecting.COLLDATE);
				if (value != null) {
					accnColl.setCollDate(StringUtils.defaultIfBlank(value.textValue(), null));
				}
				value = collecting.get(Api1Constants.Collecting.COLLNUMB);
				if (value != null) {
					accnColl.setCollNumb(StringUtils.defaultIfBlank(value.textValue(), null));
					collNumbs.put(accession.getAccessionId(), toMcpdArray(collecting, Api1Constants.Collecting.COLLNUMB));
				}
				value = collecting.get(Api1Constants.Collecting.COLLSRC);
				if (value != null) {
					if (!value.isNull() && !value.isNumber()) {
						throw new RESTApiDataTypeException("If provided, 'collSrc' must be a number");
					}
					accnColl.setCollSrc(value.isNumber() ? value.intValue() : null);
				}
				value = collecting.get(Api1Constants.Collecting.COLLCODE);
				if (value != null) {
					accnColl.setCollCode(arrayToString(toMcpdArray(collecting, Api1Constants.Collecting.COLLCODE)));
				}
				value = collecting.get(Api1Constants.Collecting.COLLNAME);
				if (value != null) {
					accnColl.setCollName(arrayToString(toMcpdArray(collecting, Api1Constants.Collecting.COLLNAME)));
				}
				value = collecting.get(Api1Constants.Collecting.COLLINSTADDRESS);
				if (value != null) {
					accnColl.setCollInstAddress(arrayToString(toMcpdArray(collecting, Api1Constants.Collecting.COLLINSTADDRESS)));
				}
				value = collecting.get(Api1Constants.Collecting.COLLSITE);
				if (value != null) {
					accnColl.setCollSite(StringUtils.defaultIfBlank(value.textValue(), null));
				}
				value = collecting.get(Api1Constants.Collecting.COLLMISSID);
				if (value != null) {
					accnColl.setCollMissId(StringUtils.defaultIfBlank(value.textValue(), null));
				}

				if (!accnColl.isEmpty()) {
					toSaveColl.add(accnColl);
				} else if (accnColl.getId() != null) {
					toRemoveColl.add(accnColl);
				}
			}

			if (accnJson.has(Api1Constants.Accession.GEO)) {
				final ObjectNode geo = (ObjectNode) accnJson.get(Api1Constants.Accession.GEO);
				AccessionGeo accnGeo = genesysService.listAccessionGeo(accession.getAccessionId());
				if (accnGeo == null) {
					accnGeo = new AccessionGeo();
					accnGeo.setAccession(accession.getAccessionId());
				}
				value = geo.get(Api1Constants.Geo.LATITUDE);
				if (value != null) {
					if (!value.isNull() && !value.isNumber()) {
						throw new RESTApiDataTypeException("If provided, 'latitude' must be a number");
					}
					accnGeo.setLatitude(value.isNumber() ? value.asDouble() : null);
				}
				value = geo.get(Api1Constants.Geo.LONGITUDE);
				if (value != null) {
					if (!value.isNull() && !value.isNumber()) {
						throw new RESTApiDataTypeException("If provided, 'longitude' must be a number");
					}
					accnGeo.setLongitude(value.isNumber() ? value.asDouble() : null);
				}
				value = geo.get(Api1Constants.Geo.ELEVATION);
				if (value != null) {
					if (!value.isNull() && !value.isNumber()) {
						throw new RESTApiDataTypeException("If provided, 'elevation' must be a number");
					}
					accnGeo.setElevation(value.isNumber() ? value.asDouble() : null);
				}
				value = geo.get(Api1Constants.Geo.COORDUNCERT);
				if (value != null) {
					if (!value.isNull() && !value.isNumber()) {
						throw new RESTApiDataTypeException("If provided, 'coordUncert' must be a number");
					}
					accnGeo.setUncertainty(value.isNumber() ? value.asDouble() : null);
				}
				value = geo.get(Api1Constants.Geo.COORDDATUM);
				if (value != null) {
					accnGeo.setDatum(StringUtils.defaultIfBlank(value.textValue(), null));
				}
				value = geo.get(Api1Constants.Geo.GEOREFMETH);
				if (value != null) {
					accnGeo.setMethod(StringUtils.defaultIfBlank(value.textValue(), null));
				}
				if (!accnGeo.isEmpty()) {
					toSaveGeo.add(accnGeo);
				} else if (accnGeo.getId() != null) {
					toRemoveGeo.add(accnGeo);
				}
			}

			if (accnJson.has(Api1Constants.Accession.BREDCODE) || accnJson.has(Api1Constants.Accession.ANCEST)) {
				AccessionBreeding accnBred = genesysService.listAccessionBreeding(accession.getAccessionId());
				if (accnBred == null) {
					accnBred = new AccessionBreeding();
					accnBred.setAccession(accession.getAccessionId());
				}
				value = accnJson.get(Api1Constants.Accession.BREDCODE);
				if (value != null) {
					accnBred.setBreederCode(arrayToString(toMcpdArray(accnJson, Api1Constants.Accession.BREDCODE)));
				}
				value = accnJson.get(Api1Constants.Accession.ANCEST);
				if (value != null) {
					accnBred.setPedigree(StringUtils.defaultIfBlank(value.textValue(), null));
				}
				if (!accnBred.isEmpty()) {
					toSaveBreed.add(accnBred);
				} else if (accnBred.getId() != null) {
					toRemoveBreed.add(accnBred);
				}
			}

			if (accnJson.has(Api1Constants.Accession.DONORCODE) || accnJson.has(Api1Constants.Accession.DONORNUMB)
					|| accnJson.has(Api1Constants.Accession.DONORNAME)) {
				AccessionExchange accnExch = genesysService.listAccessionExchange(accession.getAccessionId());
				if (accnExch == null) {
					accnExch = new AccessionExchange();
					accnExch.setAccession(accession.getAccessionId());
				}
				value = accnJson.get(Api1Constants.Accession.DONORCODE);
				if (value != null) {
					accnExch.setDonorInstitute(StringUtils.defaultIfBlank(value.textValue(), null));
				}
				value = accnJson.get(Api1Constants.Accession.DONORNUMB);
				if (value != null) {
					accnExch.setAccNumbDonor(StringUtils.defaultIfBlank(value.textValue(), null));
				}
				value = accnJson.get(Api1Constants.Accession.DONORNAME);
				if (value != null) {
					accnExch.setDonorName(StringUtils.defaultIfBlank(value.textValue(), null));
				}

				final ArrayNode donorNumb = accnJson.arrayNode();
				String donorNumbStr = "";
				if (accnExch.getDonorInstitute() != null) {
					donorNumbStr = accnExch.getDonorInstitute();
				}

				if (accnExch.getAccNumbDonor() != null) {
					donorNumb.add(donorNumbStr + ":" + accnExch.getAccNumbDonor());
					donorNumbs.put(accession.getAccessionId(), donorNumb);
				} else {
					donorNumbs.put(accession.getAccessionId(), null);
				}

				if (!accnExch.isEmpty()) {
					toSaveExch.add(accnExch);
				} else if (accnExch.getId() != null) {
					toRemoveExch.add(accnExch);
				}
			}

			accession.setLastModifiedDate(modifyDate);
			toSave.add(accession);
			// Set upsert result
			upsertResponse.setResult(upsertResult);
		}

		if (toSave.size() > 0) {
			LOG.info("Storing " + toSave.size() + " accessions.");
			List<Accession> savedData = genesysService.saveAccessions(institute, toSave);

			// Iterate savedData to extract UUIDs
			upsertResponses.stream().forEach(response -> {
				Accession accession = savedData.stream().filter(a -> useUniqueAcceNumbs ? (a.getAccessionName().equalsIgnoreCase(response.getAcceNumb()))
						: (a.getAccessionName().equalsIgnoreCase(response.getAcceNumb()) && a.getTaxonomy().getGenus().equalsIgnoreCase(response.getGenus())))
						.findFirst().orElse(null);

				UpsertResult result = response.getResult();
				if (accession != null) {
					if (result.getUuid() == null) {
						result.setAction(UpsertResult.Type.INSERT);
					} else {
						result.setAction(UpsertResult.Type.UPDATE);
					}
					result.setUUID(accession.getUuid());
				}
			});
		}

		if (toSaveColl.size() > 0) {
			genesysService.saveCollecting(toSaveColl);
		}
		if (toRemoveColl.size() > 0) {
			genesysService.removeCollecting(toRemoveColl);
		}

		if (toSaveGeo.size() > 0) {
			genesysService.saveGeo(toSaveGeo);
		}
		if (toRemoveGeo.size() > 0) {
			genesysService.removeGeo(toRemoveGeo);
		}

		if (toSaveBreed.size() > 0) {
			genesysService.saveBreeding(toSaveBreed);
		}
		if (toRemoveBreed.size() > 0) {
			genesysService.removeBreeding(toRemoveBreed);
		}

		if (toSaveExch.size() > 0) {
			genesysService.saveExchange(toSaveExch);
		}
		if (toRemoveExch.size() > 0) {
			genesysService.removeExchange(toRemoveExch);
		}

		if (toSaveRemarks.size() > 0) {
			genesysService.saveRemarks(toSaveRemarks);
		}
		if (toRemoveRemarks.size() > 0) {
			genesysService.removeRemarks(toRemoveRemarks);
		}

		updateAccessionAliases(acceNames, AliasType.ACCENAME, false);
		updateAccessionAliases(otherNumbs, AliasType.OTHERNUMB, true);
		updateAccessionAliases(donorNumbs, AliasType.DONORNUMB, true);
		updateAccessionAliases(collNumbs, AliasType.COLLNUMB, false);

		LOG.info("Done saving data");
		return upsertResponses;
	}

	private boolean updateCrop(Accession accession, JsonNode value) throws RESTApiDataTypeException {
		if (value != null) {
			if (value.isNull()) {
				if (!StringUtils.equals(null, accession.getCropName())) {
					accession.setCropName(null);
					accession.setCrop(null);
					return true;
				}
			}

			if (!value.isTextual()) {
				throw new RESTApiDataTypeException("cropName must be a String");
			}

			final String cropName = value.textValue().trim();
			if (!StringUtils.equals(cropName, accession.getCropName())) {
				accession.setCropName(cropName);
				accession.setCrop(cropService.getCrop(cropName));
				return true;
			}
		}
		return false;
	}

	private boolean updateStorage(AccessionData accession, ObjectNode accnJson) throws RESTApiDataTypeException {
		boolean updated = false;

		// MUST BE ARRAY
		if (accnJson.has(Api1Constants.Accession.STORAGE)) {
			updateAccessionStorage(accession, toMcpdArray(accnJson, Api1Constants.Accession.STORAGE));
		}

		return updated;
	}

	private static boolean updateAccessionStorage(AccessionData accession, ArrayNode arr) {
		boolean updated = false;

		List<Integer> as = accession.getStoRage();
		List<Integer> toRemove = new ArrayList<Integer>(as);

		// Sometimes { "storage": null } is provided.
		if (arr != null) {
			Iterator<JsonNode> it = arr.elements();

			while (it.hasNext()) {
				JsonNode storageElem = it.next();
				int stor = Integer.parseInt(storageElem.asText());
				if (!as.contains(stor)) {
					updated = true;
					as.add(stor);
				} else {
					// Cast needed to remove the object
					toRemove.remove((Integer) stor);
				}
			}
		}

		if (toRemove.size() > 0) {
			as.removeAll(toRemove);
			updated = true;
		}

		return updated;
	}

	private boolean updateRemarks(AccessionData accession, JsonNode jsonNode, List<AccessionRemark> toSaveRemarks, List<AccessionRemark> toRemoveRemarks)
			throws RESTApiDataTypeException {
		if (jsonNode == null || jsonNode.isNull()) {
			return false;
		}

		if (!jsonNode.isArray()) {
			throw new RESTApiDataTypeException("'remarks' must be an array");
		}
		ArrayNode arr = (ArrayNode) jsonNode;
		Iterator<JsonNode> it = arr.elements();
		List<AccessionRemark> existingRemarks = genesysService.listAccessionRemarks(accession.getAccessionId());

		if (existingRemarks != null)
			toRemoveRemarks.addAll(existingRemarks);

		while (it.hasNext()) {
			JsonNode n = it.next();
			if (n == null || n.isNull()) {
				continue;
			}

			String remarkText = n.textValue();
			if (StringUtils.isBlank(remarkText))
				continue;

			String fieldName = null, remark = null;
			int pos = remarkText.indexOf(':');
			if (pos >= 0 && !remarkText.substring(0, pos).trim().contains(" ")) {
				String[] mcpdRemark = remarkText.split(":", 2);
				if (mcpdRemark.length == 2) {
					fieldName = StringUtils.defaultIfBlank(mcpdRemark[0].trim(), null);
					remark = StringUtils.defaultIfBlank(mcpdRemark[1].trim(), null);
				} else {
					remark = StringUtils.defaultIfBlank(mcpdRemark[0].trim(), null);
				}
			} else {
				remark = StringUtils.defaultIfBlank(remarkText, null);
			}

			if (remark == null && fieldName == null) {
				continue;
			}

			LOG.debug("fieldName=" + fieldName + " remark=" + remark);

			AccessionRemark ar = findRemark(existingRemarks, fieldName, remark);
			if (ar == null) {
				ar = new AccessionRemark();
				ar.setAccession(accession.getAccessionId());
				ar.setFieldName(fieldName);
				ar.setRemark(remark);
				toSaveRemarks.add(ar);
			} else {
				toRemoveRemarks.remove(ar);
			}
		}

		return false;
	}

	private AccessionRemark findRemark(List<AccessionRemark> existingRemarks, String fieldName, String remark) {
		if (existingRemarks == null)
			return null;
		for (AccessionRemark ar : existingRemarks) {
			if (StringUtils.equals(ar.getFieldName(), fieldName) && StringUtils.equals(ar.getRemark(), remark))
				return ar;
		}
		return null;
	}

	private boolean updateAcceNumb(AccessionData accession, JsonNode value) throws RESTApiDataTypeException, RESTApiException {
		if (value != null) {
			// Rename is possible only if accession exists
			if (accession.getAccessionId().getId() == null) {
				throw new RESTApiException("Cannot rename a new accession entry");
			}

			if (!value.isTextual()) {
				throw new RESTApiDataTypeException("newAcceNumb must be a String");
			}

			if (value.isNull()) {
				throw new RESTApiValueException("newAcceNumb cannot be null");
			}

			final String newAcceNumb = value.textValue();
			if (!StringUtils.equals(newAcceNumb, accession.getAccessionName())) {
				accession.setAccessionName(newAcceNumb);
				return true;
			}
		}
		return false;
	}

	private boolean updateOrgCty(AccessionData accession, JsonNode value) throws RESTApiDataTypeException, RESTApiValueException {
		if (value != null) {
			if (!value.isNull() && !value.isTextual()) {
				throw new RESTApiDataTypeException("orgCty must be a String");
			}

			final String orgCty = value.textValue();
			if (!StringUtils.equals(orgCty, accession.getOrigin())) {
				Country country = null;
				if (orgCty != null) {
					country = geoService.getCountry(orgCty);
					if (country == null) {
						// TODO this should become a warning
						// throw new
						// RESTApiValueException("No country with ISO3 code: " +
						// orgCty);
					}
				}
				accession.setCountryOfOrigin(country);
				return true;
			}
		}
		return false;
	}

	private boolean updateUuid(AccessionData accession, JsonNode value) throws RESTApiValueException {
		// We don't allow UUID updates
		/*
		 * if (value != null) { final UUID uuid = value.isNull() ? null :
		 * UUID.fromString(value.textValue());
		 * 
		 * if (accession.getUuid() == null && uuid != null) {
		 * accession.getAccessionId().setUuid(uuid); return true; } }
		 */
		// No change
		return false;
	}

	/**
	 * Inspect incoming JSON and change taxonomy if required
	 * 
	 * @param accession
	 * @param accnJson
	 * 
	 * @return true if taxonomy was modified
	 * @throws RESTApiException
	 */
	private boolean updateTaxonomy(AccessionData accession, JsonNode accnJson) throws RESTApiException {
		if (accession.getTaxonomy() != null && (!accnJson.has(Api1Constants.Accession.GENUS_NEW) && !accnJson.has(Api1Constants.Accession.SPECIES))) {
			if (LOG.isDebugEnabled())
				LOG.debug("Not updating taxonomy without newGenus or species data");

			return false;
		}

		// Do not persist this one, temporary use
		final Taxonomy2 taxonomy = accession.getTaxonomy();
		final Taxonomy2 current = new Taxonomy2(taxonomy);
		// Load JSON values into "current"
		current.setGenus(stringIfProvided(accnJson.get(Api1Constants.Accession.GENUS), current.getGenus()));
		current.setGenus(stringIfProvided(accnJson.get(Api1Constants.Accession.GENUS_NEW), current.getGenus()));
		current.setSpecies(StringUtils.defaultIfBlank(stringIfProvided(accnJson.get(Api1Constants.Accession.SPECIES), current.getSpecies()), "sp."));
		current.setSpAuthor(StringUtils.defaultIfBlank(stringIfProvided(accnJson.get(Api1Constants.Accession.SPAUTHOR), current.getSpAuthor()), ""));
		current.setSubtaxa(StringUtils.defaultIfBlank(stringIfProvided(accnJson.get(Api1Constants.Accession.SUBTAXA), current.getSubtaxa()), ""));
		current.setSubtAuthor(StringUtils.defaultIfBlank(stringIfProvided(accnJson.get(Api1Constants.Accession.SUBTAUTHOR), current.getSubtAuthor()), ""));

		Taxonomy2 ensuredTaxonomy = null;
		try {
			ensuredTaxonomy = taxonomyService.find(current.getGenus(), current.getSpecies(), current.getSpAuthor(), current.getSubtaxa(),
					current.getSubtAuthor());
		} catch (Throwable e) {
			LOG.warn("*** lower(t.genus)=lower(" + current.getGenus() + ") and lower(t.species)=lower(" + current.getSpecies()
					+ ") and lower(t.spauthor)=lower(" + current.getSpAuthor() + ") and lower(t.subtaxa)=lower(" + current.getSubtaxa()
					+ ") and lower(subtauthor)=lower(" + current.getSubtAuthor() + ")");
		}

		if (ensuredTaxonomy == null) {
			LOG.warn("Could not find taxonomy for upsert " + current);
			// throw new PleaseRetryException("Could not find taxonomy " +
			// current.getTaxonName());
			throw new RESTApiException("Could not find taxonomy. Provide GENUS, SPECIES, SPAUTHOR, SUBTAXA and SUBTAUTHOR fields");
		}

		if (!ensuredTaxonomy.sameAs(accession.getTaxonomy())) {
			accession.setTaxonomy(ensuredTaxonomy);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Return
	 * 
	 * @param jsonNode
	 * @param currentValue
	 * @return
	 * @throws RESTApiDataTypeException
	 */
	private String stringIfProvided(JsonNode jsonNode, String currentValue) throws RESTApiException {
		if (jsonNode != null) {
			if (!jsonNode.isNull() && !jsonNode.isTextual()) {
				// We expect a String node
				throw new RESTApiDataTypeException("Not a String");
			}
			if (jsonNode.isNull()) {
				return null;
			}
			return StringUtils.defaultIfBlank(jsonNode.textValue().trim(), currentValue);
		}
		return currentValue;
	}

	private String arrayToString(ArrayNode arr) {
		if (arr == null || arr.isNull()) {
			return null;
		}

		final StringBuffer mcpdArr = new StringBuffer(20);

		for (final JsonNode st : arr) {
			if (st != null && !st.isNull()) {
				if (mcpdArr.length() > 0) {
					mcpdArr.append(";");
				}
				mcpdArr.append(st.asText());
			}
		}

		return StringUtils.defaultIfBlank(mcpdArr.toString(), null);
	}

	/**
	 * Converts textValue to JSON array node if required.
	 * 
	 * @param accnJson
	 * @param key
	 * @return
	 * @throws RESTApiDataTypeException
	 */
	private ArrayNode toMcpdArray(ObjectNode accnJson, String key) throws RESTApiDataTypeException {
		final JsonNode value = accnJson.get(key);

		if (value == null || value.isNull()) {
			return null;
		} else if (value.isArray()) {
			return (ArrayNode) value;
		} else if (value.isTextual()) {
			final ArrayNode arr = accnJson.arrayNode();
			for (final String s : value.textValue().split("[,;]")) {
				if (StringUtils.isBlank(s)) {
					continue;
				}
				arr.add(s.trim());
			}
			return arr;
		} else {
			throw new RESTApiDataTypeException("If provided, '" + key + "' must be an array");
		}
	}

	/**
	 * Convert to {@link AccessionAlias}
	 * 
	 * @param acceNames
	 * @param existingAliases
	 */
	private void updateAccessionAliases(Map<AccessionId, ArrayNode> acceNames, AliasType aliasType, boolean splitInstCode) {
		final List<AccessionAlias> toSave = new ArrayList<AccessionAlias>();
		final List<AccessionAlias> toRemove = new ArrayList<AccessionAlias>();

		for (final AccessionId accession : acceNames.keySet()) {
			final List<AccessionAlias> existingAliases = genesysService.listAccessionAliases(accession);

			final ArrayNode acceName = acceNames.get(accession);
			final List<AccessionAliasJson> aliases = new ArrayList<AccessionAliasJson>();
			if (acceName != null) {
				for (final JsonNode item : acceName) {
					if (item.isTextual() && StringUtils.isNotBlank(item.textValue())) {
						final AccessionAliasJson alias = new AccessionAliasJson();
						final String val = item.textValue().trim();
						if (splitInstCode && val.contains(":")) {
							final String[] s = val.split(":", 2);
							alias.usedBy = s[0].trim();
							alias.name = s[1].trim();
						} else {
							alias.name = val;
						}
						alias.type = aliasType.getId();
						aliases.add(alias);
					}
				}
			}

			// upsert aliases (send in a copy of the existing aliases list)
			upsertAccessionAliases(accession, aliases, aliasType, toRemove, toSave, new ArrayList<AccessionAlias>(existingAliases));
		}

		if (toSave.size() > 0) {
			// LOG.info("Saving aliases count=" + toSave.size());
			genesysService.saveAliases(toSave);
		}
		if (toRemove.size() > 0) {
			LOG.info("Removing aliases count=" + toRemove.size());
			genesysService.removeAliases(toRemove);
		}
	}

	@Override
	@Transactional
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#institute, 'WRITE') or hasPermission(#institute, 'CREATE')")
	public List<AccessionOpResponse> upsertAccessionNames(FaoInstitute institute, List<AccessionNamesJson> batch) throws RESTApiException {
		LOG.info("Batch processing " + batch.size() + " entries for " + institute);

		final boolean useUniqueAcceNumbs = institute.hasUniqueAcceNumbs();
		final List<AccessionOpResponse> upsertResponses = new ArrayList<AccessionOpResponse>();

		final List<AccessionAlias> toSave = new ArrayList<AccessionAlias>();
		final List<AccessionAlias> toRemove = new ArrayList<AccessionAlias>();

		for (final AccessionNamesJson dataJson : batch) {
			AccessionOpResponse upsertResponse = new AccessionOpResponse(dataJson.instCode, dataJson.acceNumb, dataJson.genus);
			upsertResponses.add(upsertResponse);
			UpsertResult upsertResult = null;

			if (!institute.getCode().equals(dataJson.instCode)) {
				throw new RESTApiException("Accession does not belong to instCode=" + institute.getCode() + " acn=" + dataJson);
			}

			Accession accession = null;
			try {
				if (useUniqueAcceNumbs) {
					accession = genesysService.getAccession(institute.getCode(), dataJson.acceNumb);
				} else {
					accession = genesysService.getAccession(institute.getCode(), dataJson.acceNumb, dataJson.genus);
				}
			} catch (NonUniqueAccessionException e) {
				LOG.warn(e.getMessage());
				throw new RESTApiException(e.getMessage());
			}

			if (accession == null) {
				LOG.warn("No such accession " + dataJson);
				upsertResponse.setError("No such asccession " + dataJson);
				continue;
			} else {
				upsertResult = new UpsertResult(UpsertResult.Type.UPDATE);
				upsertResult.setUUID(accession.getUuid());
			}
			// LOG.info("Updating " + dataJson + " with=" + dataJson.aliases);

			final List<AccessionAliasJson> aliases = dataJson.aliases;
			final List<AccessionAlias> existingAliases = genesysService.listAccessionAliases(accession.getAccessionId());
			upsertAccessionAliases(accession.getAccessionId(), aliases, null, toRemove, toSave, existingAliases);

			// Set upsert result
			upsertResponse.setResult(upsertResult);
		}

		if (toSave.size() > 0) {
			LOG.info("Saving aliases for instCode=" + institute.getCode() + " count=" + toSave.size());
			genesysService.saveAliases(toSave);
		}
		if (toRemove.size() > 0) {
			LOG.info("Removing aliases for instCode=" + institute.getCode() + " count=" + toRemove.size());
			genesysService.removeAliases(toRemove);
		}

		return upsertResponses;
	}

	private void upsertAccessionAliases(AccessionId accession, List<AccessionAliasJson> aliases, final AliasType aliasType, List<AccessionAlias> toRemove,
			List<AccessionAlias> toSave, Iterable<AccessionAlias> existingAliases) {

		// Allows us to focus only on a particular alias type
		if (aliasType != null) {
			LOG.debug("Filtering accession aliases by  " + aliasType);
			CollectionUtils.filter(existingAliases, new Predicate<AccessionAlias>() {
				@Override
				public boolean evaluate(AccessionAlias alias) {
					return areEqual(aliasType, alias.getAliasType());
				}
			});
		}

		// Find aliases to remove
		for (final AccessionAlias aa : existingAliases) {
			if (null == CollectionUtils.find(aliases, new Predicate<AccessionAliasJson>() {
				@Override
				public boolean evaluate(AccessionAliasJson alias) {
					return StringUtils.equals(alias.name, aa.getName());
				}
			})) {
				toRemove.add(aa);
			}
		}
		// Add or update
		for (final AccessionAliasJson aa : aliases) {
			AccessionAlias accessionAlias = CollectionUtils.find(existingAliases, new Predicate<AccessionAlias>() {
				@Override
				public boolean evaluate(AccessionAlias alias) {
					return StringUtils.equals(alias.getName(), aa.name);
				}
			});

			if (accessionAlias == null) {
				accessionAlias = new AccessionAlias();
				accessionAlias.setAccession(accession);
			}

			accessionAlias.setName(aa.name);
			accessionAlias.setInstCode(aa.instCode);
			accessionAlias.setUsedBy(aa.usedBy);
			accessionAlias.setAliasType(aliasType != null ? aliasType : AliasType.getType(aa.type));

			toSave.add(accessionAlias);
		}
	}

	@Override
	@Transactional
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#institute, 'DELETE') or hasPermission(#institute, 'MANAGE')")
	public List<AccessionOpResponse> deleteAccessions(FaoInstitute institute, List<AccessionHeaderJson> batch) throws RESTApiException {
		LOG.info("Batch deleting " + batch.size() + " entries for " + institute);

		final List<AccessionOpResponse> upsertResponses = new ArrayList<AccessionOpResponse>();
		final List<Accession> toDelete = new ArrayList<Accession>(batch.size());
		final boolean useUniqueAcceNumbs = institute.hasUniqueAcceNumbs();

		for (final AccessionHeaderJson dataJson : batch) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Loading accession " + dataJson);
			}

			AccessionOpResponse upsertResponse = new AccessionOpResponse(dataJson.instCode, dataJson.acceNumb, dataJson.genus);
			upsertResponses.add(upsertResponse);
			UpsertResult upsertResult;

			if (!institute.getCode().equals(dataJson.instCode)) {
				throw new RESTApiException("Accession does not belong to instCode=" + institute.getCode() + " acn=" + dataJson);
			}

			Accession accession;
			try {
				if (useUniqueAcceNumbs) {
					accession = genesysService.getAccession(institute.getCode(), dataJson.acceNumb);
				} else {
					accession = genesysService.getAccession(institute.getCode(), dataJson.acceNumb, dataJson.genus);
				}
			} catch (NonUniqueAccessionException e) {
				LOG.warn(e.getMessage());
				throw new RESTApiException(e.getMessage());
			}

			if (accession != null) {
				toDelete.add(accession);
				upsertResult = new UpsertResult(UpsertResult.Type.DELETE);
				upsertResult.setUUID(accession.getUuid());
			} else {
				upsertResult = new UpsertResult(UpsertResult.Type.NOOP);
			}

			upsertResponse.setResult(upsertResult);
		}

		if (toDelete.size() > 0) {
			genesysService.removeAccessions(institute, toDelete);
		}

		return upsertResponses;
	}

	@Override
	@Transactional
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#institute, 'DELETE') or hasPermission(#institute, 'MANAGE')")
	public int deleteAccessionsById(FaoInstitute institute, List<Long> batch) {
		LOG.info("Batch deleting " + batch.size() + " entries for " + institute);
		final List<Accession> toDelete = new ArrayList<Accession>(batch.size());

		if (toDelete.size() > 0) {
			genesysService.removeAccessions(institute, toDelete);
		}
		for (final Long accessionId : batch) {

			AccessionData accession;

			accession = genesysService.getAccession(accessionId.longValue());
			if (accession != null) {
				if (!institute.getCode().equals(accession.getInstituteCode())) {
					LOG.warn("Accession does not belong to instCode=" + institute.getCode() + " acn=" + accession.getAccessionId().getId());
				} else if (accession instanceof Accession) {
					toDelete.add((Accession) accession);
				} else {
					LOG.warn("Not appropriate for delete operation: " + accession.getClass());
				}
			}
		}

		if (toDelete.size() > 0) {
			genesysService.removeAccessions(institute, toDelete);
		}
		return toDelete.size();
	}
}
