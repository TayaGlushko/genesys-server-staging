/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.elastic.AccessionDetails;
import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.genesys.Method;
import org.genesys2.server.model.impl.Country;
import org.genesys2.server.model.impl.Crop;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.persistence.domain.AccessionRepository;
import org.genesys2.server.persistence.domain.MethodRepository;
import org.genesys2.server.service.*;
import org.genesys2.server.service.impl.DirectMysqlQuery.MethodResolver;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilter;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilters;
import org.genesys2.server.service.impl.FilterHandler.FilterValue;
import org.genesys2.server.service.impl.FilterHandler.LiteralValueFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.facet.result.Term;
import org.springframework.data.elasticsearch.core.facet.result.TermResult;
import org.springframework.jdbc.core.ArgumentPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class GenesysFilterServiceImpl implements GenesysFilterService {

	private static final Log LOG = LogFactory.getLog(GenesysFilterServiceImpl.class);

	// using this loop to make @Cacheable work
	@Autowired
	private GenesysService genesysService;

	@Autowired
	private MethodRepository methodRepository;

	@Autowired
	private AccessionRepository accessionRepository;

	@Autowired
	private GeoService geoService;

	// @PersistenceContext
	// private EntityManager entityManager;

	private JdbcTemplate jdbcTemplate;

	@Autowired
	private InstituteService instituteService;

	@Autowired
	private TaxonomyService taxonomyService;

	@Autowired
	private CropService cropService;

	@Autowired
	private ElasticService elasticService;

	@Autowired
	public void setDataSource(final DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public Page<Accession> listAccessions(AppliedFilters filters, Pageable pageable) {
		if (LOG.isDebugEnabled()) {
			for (AppliedFilter filter : filters) {
				LOG.debug("Looking at " + filter.toString());
			}
		}

		final DirectMysqlQuery directQuery = new DirectMysqlQuery("accession", "a");
		directQuery.jsonFilter(filters, new MethodResolver() {
			@Override
			public Method getMethod(long methodId) {
				return methodRepository.findOne(methodId);
			}
		});
		directQuery.pageable(pageable);

		// Does this go through the cache?
		final long totalCount = genesysService.countAccessions(filters);
		LOG.debug("Total count: " + totalCount);

		if (totalCount > 0 && pageable.getPageNumber() * pageable.getPageSize() <= totalCount) {
			final List<Long> results = this.jdbcTemplate.queryForList(directQuery.getQuery("distinct a.id"), directQuery.getParameters(), Long.class);
			LOG.debug("Getting accessions " + results.size());

			return new PageImpl<Accession>(results.size() == 0 ? new ArrayList<Accession>() : accessionRepository.listById(results,
			// TODO Consider processing the pageable.getSort
					new Sort("accessionName")),
			// -- TODO
					pageable, totalCount);
		} else {
			return new PageImpl<Accession>(new ArrayList<Accession>(), pageable, totalCount);
		}
	}

	/**
	 * Copy of
	 * {@link GenesysFilterServiceImpl#listAccessions(AppliedFilters, Pageable)

	 */
	@Override
	public Page<AccessionDetails> listAccessionDetails(AppliedFilters filters, Pageable pageable) {
		if (LOG.isDebugEnabled()) {
			for (AppliedFilter filter : filters) {
				LOG.debug("Looking at " + filter.toString());
			}
		}

		final DirectMysqlQuery directQuery = new DirectMysqlQuery("accession", "a");
		directQuery.jsonFilter(filters, new MethodResolver() {
			@Override
			public Method getMethod(long methodId) {
				return methodRepository.findOne(methodId);
			}
		});
		directQuery.pageable(pageable);

		// Does this go through the cache?
		final long totalCount = genesysService.countAccessions(filters);
		LOG.debug("Total count: " + totalCount);

		if (totalCount > 0 && pageable.getPageNumber() * pageable.getPageSize() <= totalCount) {
			final List<Long> results = this.jdbcTemplate.queryForList(directQuery.getQuery("distinct a.id"), directQuery.getParameters(), Long.class);
			LOG.debug("Getting accessions " + results.size());

			if (results.size() == 0) {
				return new PageImpl<AccessionDetails>(new ArrayList<AccessionDetails>(), pageable, totalCount);
			} else {
				return new PageImpl<AccessionDetails>(genesysService.getAccessionDetails(results), pageable, totalCount);
			}
		} else {
			return new PageImpl<AccessionDetails>(new ArrayList<AccessionDetails>(), pageable, totalCount);
		}
	}

	/**
	 * Filtering autocompleter
	 */
	@Override
	public List<LabelValue<String>> autocomplete(String filter, String ac, AppliedFilters filters) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Autocomplete " + filter + " ac=" + ac);
		}

		Crop crop = null;

		{
			String shortName = filters.getFirstLiteralValue(FilterConstants.CROPS, String.class);
			if (shortName != null)
				crop = cropService.getCrop((String) shortName);
		}

		final List<LabelValue<String>> completed = new ArrayList<LabelValue<String>>();
		if (FilterConstants.INSTCODE.equalsIgnoreCase(filter)) {
			final List<FaoInstitute> faoInst = instituteService.autocomplete(ac);
			TermResult termResult = null;
			try {
				termResult = elasticService.termStatisticsAuto(filters, FilterConstants.INSTCODE, 20000);
			} catch (SearchException e) {
				LOG.error("Error occurred during search", e);
			}

			if (termResult != null) {
				for (final FaoInstitute inst : faoInst) {
					String label = inst.getCode() + ", " + inst.getFullName();
					for (Term term: termResult.getTerms()) {
						if (term.getTerm().equals(inst.getCode()) && term.getCount() > 0) {
							label = label.concat(" (" + term.getCount() + ")");
							completed.add(new LabelValue<String>(inst.getCode(), label));
							break;
						}
					}
				}
			}
		} else if (FilterConstants.ORGCTY_ISO3.equalsIgnoreCase(filter)) {
			final List<Country> countries = geoService.autocomplete(ac);
			TermResult termResult = null;
			try {
				termResult = elasticService.termStatisticsAuto(filters, FilterConstants.ORGCTY_ISO3, 20000);
			} catch (SearchException e) {
				LOG.error("Error occurred during search", e);
			}

			if (termResult != null) {
				for (final Country c : countries) {
					String label = c.getCode3() + ", " + c.getName();
					for (Term term: termResult.getTerms()) {
						if (term.getTerm().equals(c.getCode3()) && term.getCount() > 0) {
							label = label.concat(" (" + term.getCount() + ")");
							completed.add(new LabelValue<String>(c.getCode3(), label));
							break;
						}
					}
				}
			}
		} else if (FilterConstants.TAXONOMY_GENUS.equalsIgnoreCase(filter)) {
			final List<String> genera = taxonomyService.autocompleteGenus(ac, crop);
			TermResult termResult = null;
			try {
				termResult = elasticService.termStatisticsAuto(filters, FilterConstants.TAXONOMY_GENUS, 20000);
			} catch (SearchException e) {
				LOG.error("Error occurred during search", e);
			}

			if (termResult != null) {
				for (final String value : genera) {
					String label = value;
					for (Term term: termResult.getTerms()) {
						if (term.getTerm().equals(value) && term.getCount() > 0) {
							label = label.concat(" (" + term.getCount() + ")");
							completed.add(new LabelValue<String>(value, label));
							break;
						}
					}
				}
			}
		} else if (FilterConstants.TAXONOMY_SPECIES.equalsIgnoreCase(filter)) {
			List<String> genus = new ArrayList<>();

			AppliedFilter genusFilter = filters.get(FilterConstants.TAXONOMY_GENUS);
			if (genusFilter != null) {
				Set<FilterValue> elements = genusFilter.getValues();
				for (FilterValue fv : elements)
					genus.add((String) ((LiteralValueFilter) fv).getValue());
			}

			final List<String> species = taxonomyService.autocompleteSpecies(ac, crop, genus);
			TermResult termResult = null;
			try {
				termResult = elasticService.termStatisticsAuto(filters, FilterConstants.TAXONOMY_SPECIES, 20000);
			} catch (SearchException e) {
				LOG.error("Error occurred during search", e);
			}

			if (termResult != null) {
				for (final String value : species) {
					String label = value;
					for (Term term: termResult.getTerms()) {
						if (term.getTerm().equals(value) && term.getCount() > 0) {
							label = label.concat(" (" + term.getCount() + ")");
							completed.add(new LabelValue<String>(value, label));
							break;
						}
					}
				}
			}
		} else if (FilterConstants.TAXONOMY_SUBTAXA.equalsIgnoreCase(filter)) {
			List<String> genus = new ArrayList<>();
			List<String> species = new ArrayList<>();

			AppliedFilter genusFilter = filters.get(FilterConstants.TAXONOMY_GENUS);
			if (genusFilter != null) {
				Set<FilterValue> elements = genusFilter.getValues();
				for (FilterValue fv : elements)
					genus.add((String) ((LiteralValueFilter) fv).getValue());
			}
			AppliedFilter speciesFilter = filters.get(FilterConstants.TAXONOMY_SPECIES);
			if (speciesFilter != null) {
				Set<FilterValue> elements = speciesFilter.getValues();
				for (FilterValue fv : elements)
					species.add((String) ((LiteralValueFilter) fv).getValue());
			}

			final List<String> subtaxa = taxonomyService.autocompleteSubtaxa(ac, crop, genus, species);
			TermResult termResult = null;
			try {
				termResult = elasticService.termStatisticsAuto(filters, FilterConstants.TAXONOMY_SUBTAXA, 20000);
			} catch (SearchException e) {
				LOG.error("Error occurred during search", e);
			}

			if (termResult != null) {
				for (final String value : subtaxa) {
					String label = value;
					for (Term term: termResult.getTerms()) {
						if (term.getTerm().equals(value) && term.getCount() > 0) {
							label = label.concat(" (" + term.getCount() + ")");
							completed.add(new LabelValue<String>(value, label));
							break;
						}
					}
				}
			}
		} else if (FilterConstants.TAXONOMY_SCINAME.equalsIgnoreCase(filter)) {
			final List<String> taxa = taxonomyService.autocompleteTaxonomy(ac);
			TermResult termResult = null;
			try {
				termResult = elasticService.termStatisticsAuto(filters, FilterConstants.TAXONOMY_SCINAME, 20000);
			} catch (SearchException e) {
				LOG.error("Error occurred during search", e);
			}

			if (termResult != null) {
				for (final String taxonomy : taxa) {
					String label = taxonomy;
					for (Term term: termResult.getTerms()) {
						if (term.getTerm().equals(taxonomy) && term.getCount() > 0) {
							label = label.concat(" (" + term.getCount() + ")");
							completed.add(new LabelValue<String>(taxonomy, label));
							break;
						}
					}
				}
			}
		} else {
			throw new RuntimeException("No autocompleter for " + filter);
		}
		return completed;
	}

	public static class LabelValue<T> {
		private final T value;
		private final String label;

		public LabelValue(T value, String label) {
			this.value = value;
			this.label = label;
		}

		public T getValue() {
			return value;
		}

		public String getLabel() {
			return label;
		}
	}

	@Override
	public void listGeo(AppliedFilters filters, Integer limit, RowCallbackHandler rowHandler) {
		listGeoTile(false, filters, limit, -1, 0, 0, rowHandler);
	}

	@Override
	public void listGeoTile(final boolean distinct, AppliedFilters filters, Integer limit, int zoom, int xtile, int ytile, RowCallbackHandler rowHandler) {
		if (LOG.isDebugEnabled()) {
			for (AppliedFilter filter : filters) {
				LOG.debug("Looking at " + filter.toString());
			}
		}

		final DirectMysqlQuery directQuery = new DirectMysqlQuery("accessiongeo", "geo");
		directQuery.filterTile(zoom, xtile, ytile);

		if (!filters.isEmpty() || !distinct) {
			// Join accession table when we have filters or looking at details
			directQuery.innerJoin("accession", "a", "a.id=geo.accessionId");
		}

		if (!filters.isEmpty()) {
			directQuery.join(filters);
			directQuery.filter(filters, new MethodResolver() {
				@Override
				public Method getMethod(long methodId) {
					return methodRepository.findOne(methodId);
				}
			});
		}
		directQuery.limit(limit);

		this.jdbcTemplate.query(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				final PreparedStatement stmt = con.prepareStatement(directQuery.getQuery(distinct ? "distinct geo.longitude, geo.latitude"
						: "a.id, a.acceNumb, a.instCode, geo.longitude, geo.latitude, geo.datum, geo.uncertainty "));
				// Set mysql JConnector to stream results
				stmt.setFetchSize(Integer.MIN_VALUE);
				new ArgumentPreparedStatementSetter(directQuery.getParameters()).setValues(stmt);
				return stmt;
			}
		}, rowHandler);
	}

}
