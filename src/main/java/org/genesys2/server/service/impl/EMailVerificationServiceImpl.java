/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import java.text.MessageFormat;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.exception.UserException;
import org.genesys2.server.model.impl.Article;
import org.genesys2.server.model.impl.User;
import org.genesys2.server.model.impl.VerificationToken;
import org.genesys2.server.service.ContentService;
import org.genesys2.server.service.EMailService;
import org.genesys2.server.service.EMailVerificationService;
import org.genesys2.server.service.PasswordPolicy.PasswordPolicyException;
import org.genesys2.server.service.TokenVerificationService;
import org.genesys2.server.service.TokenVerificationService.NoSuchVerificationTokenException;
import org.genesys2.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class EMailVerificationServiceImpl implements EMailVerificationService {

	private static final Log LOG = LogFactory.getLog(EMailVerificationServiceImpl.class);

	@Autowired
	private TokenVerificationService tokenVerificationService;

	@Autowired
	private EMailService emailService;

	@Autowired
	private UserService userService;

	@Autowired
	private ContentService contentService;

	@Value("${base.url}")
	private String baseUrl;

	@Override
	@Transactional
	public void sendVerificationEmail(User user) {
		// Generate new token
		final VerificationToken verificationToken = tokenVerificationService.generateToken("email-verification", user.getUuid());
		final Article article = contentService.getGlobalArticle("smtp.email-verification", Locale.ENGLISH);

		final String mailSubject = article.getTitle();
		final String mailBody = MessageFormat.format(article.getBody(), baseUrl, verificationToken.getUuid(), user.getEmail(), verificationToken.getKey());

		emailService.sendMail(mailSubject, mailBody, user.getEmail());
	}

	@Override
	@Transactional
	public void sendPasswordResetEmail(User user) {
		// Generate new token
		final VerificationToken verificationToken = tokenVerificationService.generateToken("email-password", user.getUuid());
		final Article article = contentService.getGlobalArticle("smtp.email-password", Locale.ENGLISH);

		final String mailSubject = article.getTitle();
		final String mailBody = MessageFormat.format(article.getBody(), baseUrl, verificationToken.getUuid(), user.getEmail(), verificationToken.getKey());

		emailService.sendMail(mailSubject, mailBody, user.getEmail());
	}

	@Override
	@Transactional
	public void cancelValidation(String tokenUuid) {
		try {
			tokenVerificationService.cancel(tokenUuid);
		} catch (final NoSuchVerificationTokenException e) {
			// Silently cancel exception
		}
	}

	@Override
	@Transactional
	public void validateEMail(String tokenUuid, String key) throws NoSuchVerificationTokenException {
		final VerificationToken consumedToken = tokenVerificationService.consumeToken("email-verification", tokenUuid, key);
		userService.userEmailValidated(consumedToken.getData());
	}

	@Override
	@Transactional(rollbackFor = Throwable.class)
	public void changePassword(String tokenUuid, String key, String password) throws NoSuchVerificationTokenException, PasswordPolicyException {
		final VerificationToken consumedToken = tokenVerificationService.consumeToken("email-password", tokenUuid, key);
		try {
			final User user = userService.getUserByUuid(consumedToken.getData());
			userService.updatePassword(user.getId(), password);

		} catch (final UserException e) {
			LOG.error(e.getMessage(), e);
		}
	}

}
