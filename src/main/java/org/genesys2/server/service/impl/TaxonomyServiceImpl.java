/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.genesys.Taxonomy2;
import org.genesys2.server.model.impl.Crop;
import org.genesys2.server.persistence.domain.Taxonomy2Repository;
import org.genesys2.server.service.CropService;
import org.genesys2.server.service.TaxonomyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class TaxonomyServiceImpl implements TaxonomyService {
	public static final Log LOG = LogFactory.getLog(TaxonomyServiceImpl.class);

	@Autowired
	private Taxonomy2Repository taxonomy2Repository;

	@Autowired
	private CropService cropService;

	@Override
	public Taxonomy2 get(Long id) {
		return taxonomy2Repository.findOne(id);
	}

	@Override
	public List<String> autocompleteGenus(String term, Crop crop) {
		List<String> strings;

		if (crop != null) {
			strings = taxonomy2Repository.autocompleteGenusByCrop(term + "%", crop, new PageRequest(0, 10));
		} else {
			strings = taxonomy2Repository.autocompleteGenus(term + "%", new PageRequest(0, 10));
		}

		return strings;
	}

	@Override
	public List<String> autocompleteSpecies(String term, Crop crop, List<String> genus) {
		List<String> strings;

		if (!genus.isEmpty()) {
			strings = taxonomy2Repository.autocompleteSpeciesByGenus("%" + term + "%", genus, new PageRequest(0, 10));
		} else if (crop != null) {
			strings = taxonomy2Repository.autocompleteSpeciesByCrop("%" + term + "%", crop, new PageRequest(0, 10));
		} else {
			strings = taxonomy2Repository.autocompleteSpecies("%" + term + "%", new PageRequest(0, 10));
		}

		return strings;
	}

	@Override
	public List<String> autocompleteSubtaxa(String term, Crop crop, List<String> genus, List<String> species) {
		List<String> strings;

		if (!genus.isEmpty() && !species.isEmpty()) {
			LOG.debug("Genus=" + genus + " sp=" + species);
			strings = taxonomy2Repository.autocompleteSubtaxaByGenusAndSpecies("%" + term + "%", genus, species, new PageRequest(0, 10));
		} else if (!species.isEmpty()) {
			LOG.debug("sp=" + species);
			strings = taxonomy2Repository.autocompleteSubtaxaBySpecies("%" + term + "%", species, new PageRequest(0, 10));
		} else if (!genus.isEmpty()) {
			LOG.debug("Genus=" + genus);
			strings = taxonomy2Repository.autocompleteSubtaxaByGenus("%" + term + "%", genus, new PageRequest(0, 10));
		} else if (crop != null) {
			strings = taxonomy2Repository.autocompleteSubtaxaByCrop(term + "%", crop, new PageRequest(0, 10));
		} else {
			strings = taxonomy2Repository.autocompleteSubtaxa("%" + term + "%", new PageRequest(0, 10));
		}

		return strings;
	}

	@Override
	public List<String> autocompleteTaxonomy(String term) {
		return taxonomy2Repository.autocompleteTaxonomy("%" + term + "%", new PageRequest(0, 10));
	}

	@Override
	@Cacheable(value = "hibernate.org.genesys2.server.model.impl.Taxonomy2.fullname", key = "#genus + '-' + #species + '-' + #spAuthor + '-' + #subtaxa + '-' + #subtAuthor", unless = "#result == null")
	public Taxonomy2 find(String genus, String species, String spAuthor, String subtaxa, String subtAuthor) {
		return taxonomy2Repository.findByGenusAndSpeciesAndSpAuthorAndSubtaxaAndSubtAuthor(genus, species, spAuthor, subtaxa, subtAuthor);
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public Taxonomy2 internalEnsure(String genus, String species, String spAuthor, String subtaxa, String subtAuthor) throws InterruptedException {
		Long taxSpeciesId = null, taxGenusId = null;

		// Direct species
		if (subtaxa.equals("") && spAuthor.equals("") && subtAuthor.equals("")) {
			if (StringUtils.equals(species, "sp.")) {
				// Self
			} else {
				final Taxonomy2 genusTaxa = internalEnsure(genus, "sp.", "", "", "");
				taxGenusId = genusTaxa.getId();
			}
		} else {
			final Taxonomy2 speciesTaxa = internalEnsure(genus, species, "", "", "");
			taxSpeciesId = speciesTaxa.getId();
			taxGenusId = speciesTaxa.getTaxGenus();
		}

		Taxonomy2 taxonomy = null;
		try {
			taxonomy = find(genus, species, spAuthor, subtaxa, subtAuthor);
		} catch (final Throwable e) {
			LOG.info("Taxonomy not found: " + e.getMessage());
		}

		if (taxonomy != null) {
			return taxonomy;
		} else {
			LOG.info("Adding new taxonomic name: " + genus + " " + species + " " + spAuthor + " " + subtaxa + " " + subtAuthor);
			taxonomy = new Taxonomy2();
			taxonomy.setGenus(genus);
			taxonomy.setSpecies(species);
			taxonomy.setSpAuthor(spAuthor);
			taxonomy.setSubtaxa(subtaxa);
			taxonomy.setSubtAuthor(subtAuthor);
			taxonomy.setTaxGenus(taxGenusId);
			taxonomy.setTaxSpecies(taxSpeciesId);

			try {
				taxonomy = taxonomy2Repository.save(taxonomy);

				if (taxGenusId == null) {
					taxonomy.setTaxGenus(taxonomy.getId());
					taxonomy = taxonomy2Repository.save(taxonomy);
				}
				if (taxSpeciesId == null) {
					taxonomy.setTaxSpecies(taxonomy.getId());
					taxonomy = taxonomy2Repository.save(taxonomy);
				}

				// Update crop taxonomy lists
				cropService.updateCropTaxonomyLists(taxonomy);

				return taxonomy;

			} catch (final Throwable e) {
				LOG.warn("Error " + e.getMessage() + " :" + taxonomy);
				throw new RuntimeException(e.getMessage());
			}
		}

	}

	@Override
	public long getTaxonomy2Id(String genus) {
		return find(genus, "sp.", "", "", "").getId();
	}

	@Override
	public long getTaxonomy2Id(String genus, String species) {
		final Taxonomy2 tax = find(genus, species, "", "", "");
		return tax.getId();
	}

	@Override
	public Taxonomy2 get(String genus, String species) {
		return find(genus, species, "", "", "");
	}

	@Override
	public long countTaxonomy2() {
		return taxonomy2Repository.count();
	}

	@Override
	public Taxonomy2 get(String genus) {
		return find(genus, "sp.", "", "", "");
	}

	@Override
	@Transactional
	public void cleanupTaxonomies() {
		Set<BigInteger> referencedIds = taxonomy2Repository.findTaxonomyReferencedIds();
		Set<Long> allIds = taxonomy2Repository.findTaxonomyIds();
		for (BigInteger integer: referencedIds) {
			allIds.remove(integer.longValue());
		}

		if (allIds.size() > 0) {
			taxonomy2Repository.removeUnusedIds(allIds);
		}
	}
}
