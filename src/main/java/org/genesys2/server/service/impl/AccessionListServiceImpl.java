/**
 * Copyright 2015 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Logger;
import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.genesys.AccessionData;
import org.genesys2.server.model.impl.AccessionList;
import org.genesys2.server.model.impl.User;
import org.genesys2.server.persistence.domain.AccessionListRepository;
import org.genesys2.server.service.AccessionListService;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilters;
import org.genesys2.server.service.worker.ElasticUpdater;
import org.genesys2.spring.SecurityContextUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class AccessionListServiceImpl implements AccessionListService {
	private static final Logger LOG = Logger.getLogger(AccessionListServiceImpl.class);

	@Autowired
	AccessionListRepository accessionListRepository;

	@Autowired
	ElasticUpdater elasticUpdater;

	@Override
	@PostAuthorize("hasRole('ADMINISTRATOR') or hasPermission(returnObject, 'READ')")
	public AccessionList getList(Long id) {
		return accessionListRepository.findOne(id);
	}

	@Override
	@PostAuthorize("hasRole('ADMINISTRATOR') or hasPermission(returnObject, 'READ') or returnObject.shared == true")
	public AccessionList getList(UUID uuid) {
		return accessionListRepository.findByUuid(uuid);
	}

	@SuppressWarnings("unchecked")
	@Override
	@PostFilter("hasRole('ADMINISTRATOR') or hasPermission(filterObject, 'READ') or filterObject.shared == true")
	public List<AccessionList> getLists(List<UUID> uuids) {
		return uuids == null || uuids.isEmpty() ? ListUtils.EMPTY_LIST : accessionListRepository.findByUuids(uuids);
	}

	@Override
	@PreAuthorize("isAuthenticated() and (#accessionList.id==null or hasRole('ADMINISTRATOR') or hasPermission(#accessionList, 'WRITE'))")
	@Transactional
	public void save(AccessionList accessionList) {
		accessionListRepository.save(accessionList);
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	public List<AccessionList> getAll() {
		return accessionListRepository.findAll();
	}

	@Override
	@PreAuthorize("isAuthenticated()")
	public List<AccessionList> getMyLists() {
		final User user = SecurityContextUtil.getCurrentUser();
		return accessionListRepository.listByOwner(user.getId());
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#accessionList, 'WRITE')")
	@Transactional
	public void removeAll(AccessionList accessionList) {
		// Re-index
		elasticUpdater.updateAll(Accession.class, accessionListRepository.getAccessionIds(accessionList).toArray(ArrayUtils.EMPTY_LONG_OBJECT_ARRAY));

		accessionListRepository.removeAll(accessionList);
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#accessionList, 'WRITE')")
	@Transactional
	public void addToList(AccessionList list, AccessionData accession) {
		// Re-index
		elasticUpdater.update(Accession.class, accession.getId());

		accessionListRepository.addOne(list, accession.getAccessionId());
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#accessionList, 'WRITE')")
	@Transactional
	public void addToList(AccessionList acceList, Collection<Long> accessionIds) {
		if (LOG.isDebugEnabled())
			LOG.debug("Adding a bunch " + accessionIds.size());

		accessionListRepository.addAll(acceList, accessionIds);

		// Re-index
		elasticUpdater.updateAll(Accession.class, accessionIds.toArray(ArrayUtils.EMPTY_LONG_OBJECT_ARRAY));
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#accessionList, 'WRITE')")
	@Transactional
	public void setList(AccessionList accessionList, Collection<Long> accessionIds) {
		// Re-index
		elasticUpdater.updateAll(Accession.class, accessionListRepository.getAccessionIds(accessionList).toArray(ArrayUtils.EMPTY_LONG_OBJECT_ARRAY));

		accessionListRepository.removeAll(accessionList);
		accessionListRepository.addAll(accessionList, accessionIds);

		// Re-index
		elasticUpdater.updateAll(Accession.class, accessionIds.toArray(ArrayUtils.EMPTY_LONG_OBJECT_ARRAY));
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#accessionList, 'WRITE')")
	@Transactional
	public void addToList(AccessionList accessionList, AppliedFilters filters) {
		accessionListRepository.addAll(accessionList, filters);

		// Re-index
		elasticUpdater.updateAll(Accession.class, accessionListRepository.getAccessionIds(accessionList).toArray(ArrayUtils.EMPTY_LONG_OBJECT_ARRAY));
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#accessionList, 'DELETE')")
	@Transactional
	public void delete(AccessionList accessionList) {
		// Re-index
		elasticUpdater.updateAll(Accession.class, accessionListRepository.getAccessionIds(accessionList).toArray(ArrayUtils.EMPTY_LONG_OBJECT_ARRAY));

		accessionListRepository.delete(accessionList);
	}

	@Override
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#accessionList, 'DELETE')")
	public Set<Long> getAccessionIds(AccessionList accessionList) {
		return accessionListRepository.getAccessionIds(accessionList);
	}

	@Override
	public int sizeOf(AccessionList list) {
		return accessionListRepository.sizeOf(list);
	}

	/* (non-Javadoc)
	 * @see org.genesys2.server.service.AccessionListService#distinctCount(java.util.List)
	 */
	@Override
	public int distinctCount(List<AccessionList> accessionLists) {
		if (accessionLists.size() == 0)
			return 0;
		return accessionListRepository.distinctCount(accessionLists);
	}

	@Override
	@PreAuthorize("isAuthenticated()")
	public List<AccessionList> findShared() {
		return accessionListRepository.findShared();
	}

	@Override
	@PreAuthorize("isAuthenticated()")
	public UUID getUUIDByTitle(String title) {
		return accessionListRepository.getUUIDByTitle(title);
	}
}
