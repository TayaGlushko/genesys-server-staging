/**
 * Copyright 2016 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.OrFilterBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.QueryStringQueryBuilder.Operator;
import org.elasticsearch.search.sort.SortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.genesys2.server.model.elastic.AccessionDetails;
import org.genesys2.server.model.filters.GenesysFilter;
import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.persistence.domain.GenesysLowlevelRepository;
import org.genesys2.server.service.*;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilter;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilters;
import org.genesys2.server.service.impl.FilterHandler.FilterValue;
import org.genesys2.server.service.impl.FilterHandler.LiteralValueFilter;
import org.genesys2.server.service.impl.FilterHandler.MaxValueFilter;
import org.genesys2.server.service.impl.FilterHandler.MinValueFilter;
import org.genesys2.server.service.impl.FilterHandler.StartsWithFilter;
import org.genesys2.server.service.impl.FilterHandler.ValueRangeFilter;
import org.genesys2.util.NumberUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.FacetedPage;
import org.springframework.data.elasticsearch.core.facet.FacetRequest;
import org.springframework.data.elasticsearch.core.facet.request.TermFacetRequestBuilder;
import org.springframework.data.elasticsearch.core.facet.result.TermResult;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.data.elasticsearch.core.query.UpdateQuery;
import org.springframework.data.elasticsearch.core.query.UpdateQueryBuilder;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Service;

@Service
public class ElasticsearchSearchServiceImpl implements ElasticService, InitializingBean {

	private static final Log LOG = LogFactory.getLog(ElasticsearchSearchServiceImpl.class);

	@Autowired
	private ElasticsearchTemplate elasticsearchTemplate;

	@Autowired
	private ElasticSearchManagementService elasticSearchManagementService;

	@Autowired
	private GenesysService genesysService;

	@Autowired
	private FilterHandler filterHandler;

	private final Map<String, Class<?>> clazzMap;

	@Autowired
	@Qualifier("genesysLowlevelRepositoryCustomImpl")
	private GenesysLowlevelRepository genesysLowlevelRepository;

	public ElasticsearchSearchServiceImpl() {
		clazzMap = new HashMap<String, Class<?>>();
		clazzMap.put(Accession.class.getName(), AccessionDetails.class);
	}

	@Override
	public Page<AccessionDetails> search(String query, Pageable pageable) throws SearchException {
		SearchQuery searchQuery = new NativeSearchQueryBuilder().withIndices(IndexAliasConstants.INDEXALIAS_PASSPORT_READ).withTypes(IndexAliasConstants.PASSPORT_TYPE)
				.withQuery(org.elasticsearch.index.query.QueryBuilders.queryStringQuery(query).defaultOperator(Operator.AND)).withPageable(pageable).build();

		try {
			Page<AccessionDetails> sampleEntities = elasticsearchTemplate.queryForPage(searchQuery, AccessionDetails.class);
			return sampleEntities;
		} catch (Throwable e) {
			throw new SearchException(e.getMessage(), e);
		}
	}

	@Override
	public List<String> autocompleteSearch(String query) throws SearchException {
		SearchQuery searchQuery = new NativeSearchQueryBuilder().withIndices(IndexAliasConstants.INDEXALIAS_PASSPORT_READ).withTypes(IndexAliasConstants.PASSPORT_TYPE)
				.withQuery(org.elasticsearch.index.query.QueryBuilders.queryStringQuery("acceNumb:(" + query + "*)").defaultOperator(Operator.AND))
				.withSort(SortBuilders.fieldSort(FilterConstants.ACCENUMB).order(SortOrder.ASC)).withPageable(new PageRequest(0, 10)).build();

		try {
			Page<AccessionDetails> sampleEntities = elasticsearchTemplate.queryForPage(searchQuery, AccessionDetails.class);
			List<String> sugg = new ArrayList<String>(10);
			for (AccessionDetails ad : sampleEntities) {
				sugg.add(ad.getAcceNumb() + ", " + ad.getTaxonomy().getGenus() + ", " + ad.getInstitute().getCode());
			}
			return sugg;
		} catch (Throwable e) {
			throw new SearchException(e.getMessage(), e);
		}
	}

	@Override
	public Page<AccessionDetails> filter(AppliedFilters appliedFilters, Pageable pageable) throws SearchException {

		AndFilterBuilder filterBuilder = getFilterBuilder(appliedFilters);

		SortBuilder sortBuilder = SortBuilders.fieldSort(FilterConstants.ACCENUMB).order(SortOrder.ASC);
		SearchQuery searchQuery = new NativeSearchQueryBuilder().withIndices(IndexAliasConstants.INDEXALIAS_PASSPORT_READ).withTypes(IndexAliasConstants.PASSPORT_TYPE)
				.withFilter(filterBuilder).withSort(sortBuilder).withPageable(pageable).build();

		try {
			Page<AccessionDetails> sampleEntities = elasticsearchTemplate.queryForPage(searchQuery, AccessionDetails.class);
			return sampleEntities;
		} catch (Throwable e) {
			throw new SearchException(e.getMessage(), e);
		}
	}

	@Override
	public TermResult termStatistics(AppliedFilters appliedFilters, String term, int size) throws SearchException {

		AndFilterBuilder filterBuilder = getFilterBuilder(appliedFilters);

		FacetRequest termFacetRequest;
		if (FilterConstants.DUPLSITE.equals(term)) {
			termFacetRequest = new TermFacetRequestBuilder("f").applyQueryFilter().fields(term).excludeTerms("NOR051").size(size).build();
		} else if (FilterConstants.SGSV.equals(term)) {
			termFacetRequest = new TermFacetRequestBuilder("f").applyQueryFilter().fields(FilterConstants.IN_SGSV).size(size).build();
		} else {
			termFacetRequest = new TermFacetRequestBuilder("f").applyQueryFilter().fields(term).size(size).build();
		}
		SearchQuery searchQuery = new NativeSearchQueryBuilder().withIndices(IndexAliasConstants.INDEXALIAS_PASSPORT_READ).withTypes(IndexAliasConstants.PASSPORT_TYPE)
				.withFilter(filterBuilder).withFacet(termFacetRequest).build();

		try {
			FacetedPage<AccessionDetails> page = elasticsearchTemplate.queryForPage(searchQuery, AccessionDetails.class);
			return (TermResult) page.getFacet("f");

		} catch (Throwable e) {
			throw new SearchException(e.getMessage(), e);
		}
	}

	/**
	 * Runs TermFacet, but will automatically increase size if #otherCount is
	 * more than 10%
	 */
	@Override
	public TermResult termStatisticsAuto(AppliedFilters appliedFilters, String term, int size) throws SearchException {
		TermResult termResult = null;
		int newSize = size;
		do {
			termResult = termStatistics(appliedFilters, term, newSize);

			// Avoid div/0
			if (termResult.getTotalCount() == 0)
				break;

			double otherPerc = (double) termResult.getOtherCount() / (termResult.getMissingCount() + termResult.getTotalCount());
			if (otherPerc < 0.1)
				break;
			newSize += size + Math.max(1, (size / 3));
		} while (newSize < 2 * size);
		return termResult;
	}

	private AndFilterBuilder getFilterBuilder(AppliedFilters appliedFilters) {
		AndFilterBuilder filterBuilder = FilterBuilders.andFilter();

		for (AppliedFilter appliedFilter : appliedFilters) {
			applyFilter(filterBuilder, appliedFilter);
		}

		{
			// Handle HISTORIC: When filter not provided by user, apply
			// historic=false
			if (!appliedFilters.hasFilter(FilterConstants.HISTORIC)) {
				applyFilter(filterBuilder, FilterHandler.NON_HISTORIC_FILTER);
			}
		}

		return filterBuilder;
	}

	private void applyFilter(AndFilterBuilder filterBuilder, AppliedFilter appliedFilter) {
		String key = appliedFilter.getFilterName();

		GenesysFilter genesysFilter = filterHandler.getFilter(key);

		if (genesysFilter == null) {
			LOG.warn("No such filter " + key);
			return;
		}

		// Filter-level OR
		OrFilterBuilder orFilter = FilterBuilders.orFilter();

		// null
		if (appliedFilter.getWithNull()) {
			orFilter.add(FilterBuilders.missingFilter(key));
		}

		Set<FilterValue> filterValues = appliedFilter.getValues();
		if (filterValues != null && !filterValues.isEmpty()) {

			{
				// Handle literals
				Set<Object> literals = new HashSet<Object>();
				for (FilterValue filterValue : filterValues) {
					if (filterValue instanceof FilterHandler.LiteralValueFilter) {
						FilterHandler.LiteralValueFilter literal = (LiteralValueFilter) filterValue;
						literals.add(literal.getValue());
					}
				}

				if (!literals.isEmpty()) {

					if (genesysFilter.isAnalyzed()) {
						// query
						StringBuilder sb = new StringBuilder();
						for (Object val : literals) {
							if (sb.length() > 0)
								sb.append(",");
							if (val instanceof String)
								sb.append("\"" + val + "\"");
							else
								sb.append(val);
						}

						if (FilterConstants.ALIAS.equals(key)) {
							// Nested
							orFilter.add(FilterBuilders.nestedFilter("aliases", QueryBuilders.queryStringQuery("aliases.name" + ":(" + sb.toString() + ")")));
						} else {
							orFilter.add(FilterBuilders.queryFilter(QueryBuilders.queryStringQuery(key + ":(" + sb.toString() + ")")));
						}
					} else {
						// terms

						if (FilterConstants.SGSV.equals(key)) {
							orFilter.add(FilterBuilders.termsFilter(FilterConstants.IN_SGSV, literals).execution("or"));
						} else {
							orFilter.add(FilterBuilders.termsFilter(key, literals).execution("or"));
						}
					}
				}
			}

			{
				// Handle operations
				for (FilterValue filterValue : filterValues) {
					if (filterValue instanceof ValueRangeFilter) {
						ValueRangeFilter range = (ValueRangeFilter) filterValue;
						LOG.debug("Range " + range.getClass() + " " + range);
						orFilter.add(FilterBuilders.rangeFilter(key).from(range.getFrom()).to(range.getTo()));
					} else if (filterValue instanceof MaxValueFilter) {
						MaxValueFilter max = (MaxValueFilter) filterValue;
						LOG.debug("Max " + max);
						orFilter.add(FilterBuilders.rangeFilter(key).to(max.getTo()));
					} else if (filterValue instanceof MinValueFilter) {
						MinValueFilter min = (MinValueFilter) filterValue;
						LOG.debug("Min " + min);
						orFilter.add(FilterBuilders.rangeFilter(key).from(min.getFrom()));
					} else if (filterValue instanceof StartsWithFilter) {
						StartsWithFilter startsWith = (StartsWithFilter) filterValue;
						LOG.debug("startsWith " + startsWith);
						if (genesysFilter.isAnalyzed()) {
							if (FilterConstants.ALIAS.equals(key)) {
								orFilter.add(FilterBuilders.nestedFilter("aliases",
										QueryBuilders.queryStringQuery("aliases.name" + ":" + startsWith.getStartsWith() + "*")));
							} else {
								orFilter.add(FilterBuilders.queryFilter(QueryBuilders.queryStringQuery(key + ":" + startsWith.getStartsWith() + "*")));
							}
						} else {
							orFilter.add(FilterBuilders.prefixFilter(key, startsWith.getStartsWith()));
						}
					}
				}
			}
		}

		if (appliedFilter.isInverse()) {
			filterBuilder.add(FilterBuilders.notFilter(orFilter));
		} else {
			filterBuilder.add(orFilter);
		}

	}

	@Override
	public void updateAll(String className, Collection<Long> ids) {
		if (!clazzMap.containsKey(className)) {
			return;
		}

		if (ids.isEmpty()) {
			LOG.info("Skipping empty updateAll.");
			return;
		}

		if (LOG.isDebugEnabled())
			LOG.debug("Updating " + className + " bulk_size=" + ids.size());

		List<IndexQuery> queries = new ArrayList<IndexQuery>();

		List<AccessionDetails> ads = genesysService.getAccessionDetails(ids);

		for (AccessionDetails ad : ads) {
			if (ad == null)
				continue; // Skip null id

			IndexQuery iq = new IndexQuery();
			iq.setIndexName(IndexAliasConstants.INDEXALIAS_PASSPORT_WRITE);
			iq.setType(IndexAliasConstants.PASSPORT_TYPE);
			iq.setId(String.valueOf(ad.getId()));
			iq.setObject(ad);

			queries.add(iq);
		}

		if (!queries.isEmpty()) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Indexing " + className + " count=" + queries.size() + " of provided objects count=" + ids.size());
			}
			elasticsearchTemplate.bulkIndex(queries);
		}
	}

	@Override
	public void remove(String className, long id) {
		Class<?> clazz2 = clazzMap.get(className);
		if (clazz2 == null) {

			return;
		}
		LOG.info("Removing from index " + clazz2 + " id=" + id);

		if (clazz2.equals(AccessionDetails.class)) {
			elasticsearchTemplate.delete(IndexAliasConstants.INDEXALIAS_PASSPORT_WRITE,
					IndexAliasConstants.PASSPORT_TYPE, String.valueOf(id));
		} else {
			// Default
			elasticsearchTemplate.delete(clazz2, String.valueOf(id));
		}
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		LOG.info("Initializing index");
		elasticsearchTemplate.createIndex(AccessionDetails.class);
		LOG.info("Putting mapping");
		try {
			elasticsearchTemplate.putMapping(AccessionDetails.class);
		} catch (Throwable e) {
			LOG.error("Mapping mismatch. Need to reindex.", e);
		}
		LOG.info("Refreshing");
		elasticsearchTemplate.refresh(AccessionDetails.class, true);

		Map<?, ?> indexMapping = elasticsearchTemplate.getMapping(AccessionDetails.class);

		// Ensure ES index 'genesysarchive'
		if (!elasticsearchTemplate.indexExists(IndexAliasConstants.INDEX_GENESYS_ARCHIVE)) {
			LOG.info("Initializing genesysarchive");
			elasticsearchTemplate.createIndex(IndexAliasConstants.INDEX_GENESYS_ARCHIVE);
			LOG.info("Copying mapping to genesysarchive");
			elasticsearchTemplate.putMapping(IndexAliasConstants.INDEX_GENESYS_ARCHIVE, IndexAliasConstants.PASSPORT_TYPE, indexMapping);
		}

		String newIndex = IndexAliasConstants.INDEX_GENESYS + System.currentTimeMillis();
		if (!elasticSearchManagementService.aliasExists(IndexAliasConstants.INDEXALIAS_PASSPORT_READ)) {
			elasticsearchTemplate.createIndex(newIndex);
			elasticsearchTemplate.putMapping(newIndex, IndexAliasConstants.PASSPORT_TYPE, indexMapping);
			elasticSearchManagementService.realias(IndexAliasConstants.INDEXALIAS_PASSPORT_READ, newIndex);
		}
		if (!elasticSearchManagementService.aliasExists(IndexAliasConstants.INDEXALIAS_PASSPORT_WRITE)) {
			if (!elasticsearchTemplate.indexExists(newIndex)) {
				elasticsearchTemplate.createIndex(newIndex);
				elasticsearchTemplate.putMapping(newIndex, IndexAliasConstants.PASSPORT_TYPE, indexMapping);
			}
			elasticSearchManagementService.realias(IndexAliasConstants.INDEXALIAS_PASSPORT_WRITE, newIndex);
		}
	}

	@Override
	public void regenerateAccessionSequentialNumber() {
		final List<Object[]> batch = new ArrayList<Object[]>();

		genesysLowlevelRepository.listAccessions(null, new RowCallbackHandler() {
			private int counter = 0;

			@Override
			public void processRow(ResultSet rs) throws SQLException {
				long acceId = rs.getLong(1);
				String acceNumb = rs.getString(4);
				counter++;

				Object[] o = new Object[] { acceId, acceNumb, NumberUtils.numericValue(acceNumb) };
				batch.add(o);

				if (batch.size() >= 100) {
					if (counter % 100000 == 0)
						LOG.info("Regenerating sequential numbers " + counter);
					else
						LOG.debug("Regenerating sequential numbers " + counter);

					updateAccessionSequentialNumber(batch);
					batch.clear();
				}
			}
		});

		updateAccessionSequentialNumber(batch);
	}

	/**
	 * Data[] = accessionId, acceNumb and seqNo
	 * 
	 * @param batch
	 */
	private void updateAccessionSequentialNumber(List<Object[]> batch) {
		List<UpdateQuery> queries = new ArrayList<UpdateQuery>(batch.size());

		List<Long> missingDocIds = new ArrayList<Long>(batch.size());

		for (Object[] o : batch) {
			try {

				IndexRequest indexRequest = new IndexRequest();
				indexRequest.index(IndexAliasConstants.INDEXALIAS_PASSPORT_WRITE).type(IndexAliasConstants.PASSPORT_TYPE);
				indexRequest.source("seqNo", o[2]);
				UpdateQuery updateQuery = new UpdateQueryBuilder().withClass(AccessionDetails.class)
						.withIndexName(IndexAliasConstants.INDEXALIAS_PASSPORT_WRITE).withType(IndexAliasConstants.PASSPORT_TYPE)
						.withId(o[0].toString())
						.withIndexRequest(indexRequest).build();
				queries.add(updateQuery);
				// LOG.debug("ES added seqNo to " + o[0].toString());

			} catch (Throwable e) {
				// LOG.error(e.getMessage());
				missingDocIds.add((Long) o[0]);
			}
		}

		if (queries.size() > 0) {
			// LOG.info("Reindexing accession documents: " + queries.size());
			elasticsearchTemplate.bulkUpdate(queries);
		}

		if (missingDocIds.size() > 0) {
			// LOG.info("Reindexing accession documents: " +
			// missingDocIds.size());
			updateAll(Accession.class.getName(), missingDocIds);
		}
	}
}
