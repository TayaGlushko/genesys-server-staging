/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.genesys.Method;
import org.genesys2.server.service.FilterConstants;
import org.genesys2.server.service.MappingService.CoordUtil;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilter;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilters;
import org.genesys2.server.service.impl.FilterHandler.FilterValue;
import org.genesys2.server.service.impl.FilterHandler.LiteralValueFilter;
import org.genesys2.server.service.impl.FilterHandler.MaxValueFilter;
import org.genesys2.server.service.impl.FilterHandler.MinValueFilter;
import org.genesys2.server.service.impl.FilterHandler.StartsWithFilter;
import org.genesys2.server.service.impl.FilterHandler.ValueRangeFilter;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

public class DirectMysqlQuery {
	private static final Log LOG = LogFactory.getLog(DirectMysqlQuery.class);
	private static final String SPACE = " ";

	final Set<String> tables = new HashSet<String>();
	final List<Object> params = new ArrayList<Object>();
	final StringBuffer sb, whereBuffer = new StringBuffer(), sortBuffer = new StringBuffer();

	public DirectMysqlQuery(String baseTable, String baseAlias) {
		sb = new StringBuffer(300);
		sb.append(" from ").append(baseTable).append(SPACE).append(baseAlias);
		tables.add(baseTable);
	}

	public DirectMysqlQuery innerJoin(String table, String alias, String onExpr) {
		if (tables.contains(table)) {
			if (LOG.isDebugEnabled())
				LOG.debug("Table already inner-joined: " + table);
			return this;
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inner-joining " + table + " " + alias + " on " + onExpr);
		}
		sb.append(" inner join ").append(table).append(SPACE);
		tables.add(table);

		if (StringUtils.isNotBlank(alias)) {
			sb.append(alias).append(SPACE);
		}
		sb.append("on ").append(onExpr).append(SPACE);
		return this;
	}

	public DirectMysqlQuery outerJoin(String table, String alias, String onExpr) {
		if (tables.contains(table)) {
			LOG.warn("Table already joined: " + table);
			return this;
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("Leftouter-joining " + table + " " + alias + " on " + onExpr);
		}
		sb.append(" left outer join ").append(table).append(SPACE);
		tables.add(table);

		if (StringUtils.isNotBlank(alias)) {
			sb.append(alias).append(SPACE);
		}
		if (StringUtils.isNotBlank(onExpr)) {
			sb.append("on ").append(onExpr).append(SPACE);
		}
		return this;
	}

	public DirectMysqlQuery jsonFilter(AppliedFilters filters, MethodResolver methodResolver) {
		if (filters == null)
			return this;

		return join(filters).filter(filters, methodResolver);
	}

	protected DirectMysqlQuery join(AppliedFilters filters) {

		if (filters.hasFilter(FilterConstants.LISTS)) {
			innerJoin("accelistitems", "ali", "ali.acceid=a.id");
			innerJoin("accelist", "al", "al.id=ali.listid");
		}

		if (filters.hasFilter(FilterConstants.TAXONOMY_GENUS) || filters.hasFilter(FilterConstants.TAXONOMY_SPECIES)
				|| filters.hasFilter(FilterConstants.TAXONOMY_SUBTAXA) || filters.hasFilter(FilterConstants.TAXONOMY_SCINAME)) {

			innerJoin("taxonomy2", "t", "t.id=a.taxonomyId2");
		}

		if (filters.hasFilter(FilterConstants.CROPS)) {
			innerJoin("crop", null, "crop.id=a.cropId");
		}
		
		// if (filters.hasFilter(FilterConstants.ORGCTY_ISO3)) {
		// innerJoin("country", "cty", "cty.id=a.orgCtyId");
		// }

		if (/* filters.hasFilter(FilterConstants.INSTCODE) || */filters.hasFilter(FilterConstants.INSTITUTE_COUNTRY_ISO3)
				|| filters.hasFilter(FilterConstants.INSTITUTE_NETWORK)) {

			innerJoin("faoinstitute", "fao", "fao.id=a.instituteId");

			if (filters.hasFilter(FilterConstants.INSTITUTE_COUNTRY_ISO3)) {
				innerJoin("country", "faocty", "faocty.id=fao.countryId");
			}

			if (filters.hasFilter(FilterConstants.INSTITUTE_NETWORK)) {
				innerJoin("organizationinstitute", "oi", "oi.instituteId=fao.id");
				innerJoin("organization", "org", "org.id=oi.organizationId");
			}
		}

		if (filters.hasFilter(FilterConstants.GEO_LATITUDE) || filters.hasFilter(FilterConstants.GEO_LONGITUDE)
				|| filters.hasFilter(FilterConstants.GEO_ELEVATION)) {
			innerJoin("accessiongeo", "geo", "geo.accessionId=a.id");
		}

		if (filters.hasFilter(FilterConstants.ALIAS)) {
			innerJoin("accessionalias", "accename", "accename.accessionId=a.id");
		}

		if (filters.hasFilter(FilterConstants.COLLMISSID)) {
			innerJoin("accessioncollect", "col", "col.accessionId=a.id");
		}

		if (filters.hasFilter(FilterConstants.STORAGE)) {
			outerJoin("accessionstorage", "storage", "storage.accessionId=a.id");
		}

		return this;
	}

	protected DirectMysqlQuery filter(AppliedFilters filters, MethodResolver methodResolver) {
		createQuery(whereBuffer, "a.id", filters.get("id"), params);
		{
			// FIXME Are these two still used?
			createQuery(whereBuffer, "t.taxGenus", filters.get("genusId"), params);
			createQuery(whereBuffer, "t.taxSpecies", filters.get("speciesId"), params);
		}
		createQuery(whereBuffer, "al.uuid", filters.get(FilterConstants.LISTS), params);
		createQuery(whereBuffer, "a.acceNumb", filters.get(FilterConstants.ACCENUMB), params);
		createQuery(whereBuffer, "a.seqNo", filters.get(FilterConstants.SEQUENTIAL_NUMBER), params);
		createQuery(whereBuffer, "a.orgCty", filters.get(FilterConstants.ORGCTY_ISO3), params);
		createQuery(whereBuffer, "a.instCode", filters.get(FilterConstants.INSTCODE), params);
		createQuery(whereBuffer, "faocty.code3", filters.get(FilterConstants.INSTITUTE_COUNTRY_ISO3), params);
		createQuery(whereBuffer, "a.inSGSV", filters.get(FilterConstants.SGSV), params);
		createQuery(whereBuffer, "a.mlsStat", filters.get(FilterConstants.MLSSTATUS), params);
		createQuery(whereBuffer, "a.inTrust", filters.get(FilterConstants.ART15), params);
		createQuery(whereBuffer, "a.sampStat", filters.get(FilterConstants.SAMPSTAT), params);
		createQuery(whereBuffer, "a.available", filters.get(FilterConstants.AVAILABLE), params);

		{
			if (filters.hasFilter(FilterConstants.HISTORIC)) {
				createQuery(whereBuffer, "a.historic", filters.get(FilterConstants.HISTORIC), params);
			} else {
				// Handle HISTORIC: When filter not provided by user, apply
				// historic=false
				createQuery(whereBuffer, "a.historic", FilterHandler.NON_HISTORIC_FILTER, params);
			}
		}

		createQuery(whereBuffer, "org.slug", filters.get(FilterConstants.INSTITUTE_NETWORK), params);
		createQuery(whereBuffer, "t.genus", filters.get(FilterConstants.TAXONOMY_GENUS), params);
		createQuery(whereBuffer, "t.species", filters.get(FilterConstants.TAXONOMY_SPECIES), params);
		createQuery(whereBuffer, "t.subtaxa", filters.get(FilterConstants.TAXONOMY_SUBTAXA), params);
		createQuery(whereBuffer, "t.taxonName", filters.get(FilterConstants.TAXONOMY_SCINAME), params);
		createQuery(whereBuffer, "geo.longitude", filters.get(FilterConstants.GEO_LONGITUDE), params);
		createQuery(whereBuffer, "geo.latitude", filters.get(FilterConstants.GEO_LATITUDE), params);
		createQuery(whereBuffer, "geo.elevation", filters.get(FilterConstants.GEO_ELEVATION), params);
		createQuery(whereBuffer, "crop.shortName", filters.get(FilterConstants.CROPS), params);
		createQuery(whereBuffer, "accename.name", filters.get(FilterConstants.ALIAS), params);
		createQuery(whereBuffer, "col.collMissId", filters.get(FilterConstants.COLLMISSID), params);
		createQuery(whereBuffer, "storage.storage", filters.get(FilterConstants.STORAGE), params);

		for (final AppliedFilter methodFilter : filters.methodFilters()) {
			// Handle Genesys Method!
			final long methodId = Long.parseLong(methodFilter.getFilterName().substring(3));
			// Need to validate method
			final Method method = methodResolver.getMethod(methodId);
			if (method != null) {
				final String alias = "gm" + methodId;
				innerJoin("`" + methodId + "`", alias, alias + ".accessionId=a.id");
				createQuery(whereBuffer, alias + ".`" + method.getFieldName() + "`", methodFilter, params);
			} else {
				LOG.warn("No such method with id=" + methodId);
			}
		}

		if (LOG.isDebugEnabled()) {
			LOG.debug("Parameter count: " + params.size());
			LOG.debug("Count query:\n" + sb.toString() + " " + whereBuffer.toString());
		}

		return this;
	}

	public void filterTile(int zoom, int xtile, int ytile) {
		if (zoom >= 0) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("ZOOM=" + zoom);
			}
			final double lonW = CoordUtil.tileToLon(zoom, xtile);
			final double lonE = CoordUtil.tileToLon(zoom, xtile + 1);
			final double diffLon = lonE - lonW;
			final double latN = CoordUtil.tileToLat(zoom, ytile);
			final double latS = CoordUtil.tileToLat(zoom, ytile + 1);
			final double diffLat = latN - latS;

			if (whereBuffer.length() == 0) {
				whereBuffer.append(" where ");
			} else {
				whereBuffer.append(" and ");
			}
			whereBuffer.append(" ((geo.longitude between ? and ?) and (geo.latitude between ? and ?)) ");

			params.add(lonW - zoom * diffLon * .2);
			params.add(lonE + zoom * diffLon * .2);
			params.add(latS - zoom * diffLat * .2);
			params.add(latN + zoom * diffLat * .2);
			if (LOG.isDebugEnabled()) {
				LOG.debug(lonW + " <= lon <= " + lonE + " corr=" + diffLon * .2);
				LOG.debug(latS + " <= lat <= " + latN + " corr=" + diffLat * .2);
			}
		}
	}

	private void createQuery(StringBuffer sb, String dbName, AppliedFilter appliedFilter, List<Object> params) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Handling " + dbName);
		}

		if (appliedFilter != null && appliedFilter.size() > 0) {
			Set<FilterValue> filterValues = appliedFilter.getValues();

			if (LOG.isDebugEnabled()) {
				LOG.debug("Adding " + appliedFilter + " sz=" + appliedFilter.size() + " t=" + appliedFilter.getClass().getSimpleName());
			}

			if (sb.length() == 0) {
				sb.append(" where ");
			} else {
				sb.append(" and ");
			}

			// Opening
			if (appliedFilter.isInverse()) {
				sb.append(" NOT ");
			}
			sb.append(" ( ");

			// A filter value can be (a) explicit value or (b) an operation
			int toHandle = appliedFilter.size();

			// (a) explicit values are handled by =? or by IN (?,?,..)
			int handledCount = handleExplicitValues(sb, dbName, filterValues, params);

			// do we have more?
			if (handledCount > 0 && toHandle > handledCount) {
				sb.append(" OR ");
			}

			int handledCountNull = handleNullValues(sb, dbName, appliedFilter, params);
			handledCount += handledCountNull;

			// do we have more?
			if (handledCountNull > 0 && handledCount > 0 && toHandle > handledCount) {
				sb.append(" OR ");
			}

			// (b) operations are expressed as {"min":12} or {"max":33} or
			// {"range":[3,10]} or {"like":"test"}
			handleOperations(sb, dbName, filterValues, params);

			// closing
			sb.append(" ) ");
		}

	}

	private int handleOperations(StringBuffer sb, String dbName, Set<FilterValue> set, List<Object> params) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Inspecting " + dbName + " ... " + set);
		}

		int counter = 0;
		for (FilterValue filterValue : set) {

			if (filterValue instanceof ValueRangeFilter) {
				ValueRangeFilter vrf = (ValueRangeFilter) filterValue;
				if (LOG.isDebugEnabled()) {
					LOG.debug("Adding array: " + vrf);
				}
				if (counter > 0) {
					sb.append(" or ");
				}
				counter++;
				// must be an array
				sb.append("\n ( ").append(dbName);
				sb.append(" between ? and ? ) ");
				addParam(params, vrf.getFrom());
				addParam(params, vrf.getTo());
			}

			if (filterValue instanceof MinValueFilter) {
				MinValueFilter mvf = (MinValueFilter) filterValue;
				if (LOG.isDebugEnabled()) {
					LOG.debug("Adding min number: " + mvf);
				}
				if (counter > 0) {
					sb.append(" or ");
				}
				counter++;
				// must be an number
				sb.append("\n ( ").append(dbName);
				sb.append(" >= ? ) ");
				addParam(params, mvf.getFrom());
			}

			if (filterValue instanceof MaxValueFilter) {
				MaxValueFilter mvf = (MaxValueFilter) filterValue;
				if (LOG.isDebugEnabled()) {
					LOG.debug("Adding max number: " + mvf);
				}
				if (counter > 0) {
					sb.append(" or ");
				}
				counter++;
				// must be an number
				sb.append("\n ( ").append(dbName);
				sb.append(" <= ? ) ");
				addParam(params, mvf.getTo());
			}

			if (filterValue instanceof StartsWithFilter) {
				StartsWithFilter swf = (StartsWithFilter) filterValue;
				if (LOG.isDebugEnabled()) {
					LOG.debug("Adding LIKE : " + swf);
				}
				if (counter > 0) {
					sb.append(" or ");
				}
				counter++;
				// must be an number
				sb.append("\n ( ").append(dbName);
				sb.append(" LIKE ? ) ");
				addParamLike(params, swf.getStartsWith());

			}
		}

		return counter;
	}

	private int handleNullValues(StringBuffer sb, String dbName, AppliedFilter appliedFilter, List<Object> params) {
		if (appliedFilter.getWithNull()) {
			sb.append("\n ( ").append(dbName);
			sb.append(" is NULL ) ");
			return 1;
		}

		return 0;
	}

	private int handleExplicitValues(StringBuffer sb, String dbName, Set<FilterValue> set, List<Object> params) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Addinfg " + dbName + " ... " + set.size());
		}

		int counter = 0;
		for (FilterValue filterValue : set) {
			if (filterValue instanceof LiteralValueFilter) {
				if (LOG.isDebugEnabled()) {
					LOG.debug("Adding " + filterValue);
				}
				counter++;
				addParam(params, ((LiteralValueFilter) filterValue).getValue());
			}
		}

		if (counter == 0) {
			// Nothing..
		} else if (counter == 1) {
			sb.append("\n ( ").append(dbName);
			sb.append(" = ? ) ");
		} else {
			sb.append("\n ( ").append(dbName);
			sb.append(" IN ( ?");
			for (int i = counter - 1; i > 0; i--) {
				sb.append(",?");
			}
			sb.append(" ) )");
		}

		return counter;
	}

	private void addParam(List<Object> params, Object object) {
		if (object instanceof UUID) {
			params.add(toBytes((UUID) object));
		} else {
			params.add(object);
		}
	}

	private byte[] toBytes(UUID uuid) {
		byte[] b = new byte[16];
		ByteBuffer bb = ByteBuffer.wrap(b);
		bb.order(ByteOrder.BIG_ENDIAN);
		bb.putLong(uuid.getMostSignificantBits());
		bb.putLong(uuid.getLeastSignificantBits());
		return b;
	}

	private void addParamLike(List<Object> params, String string) {
		params.add(string + "%");
	}

	public String getCountQuery(String countWhat) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("select count(" + countWhat + ") " + sb.toString() + " " + whereBuffer.toString());
		}
		return "select count(" + countWhat + ") " + sb.toString() + " " + whereBuffer.toString();
	}

	public Object[] getParameters() {
		return params.toArray();
	}

	public DirectMysqlQuery sort(Sort sort) {
		if (sort == null) {
			return this;
		}

		if (sortBuffer.length() != 0) {
			throw new RuntimeException("sortBuffer is not blank, invalid use of #pageable(Pageable)");
		}

		sortBuffer.append("\n order by ");
		for (final Order o : sort) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Order: " + o);
			}
			// ClassMetadata md =
			// sessionFactory.getClassMetadata(Accession.class);
			// md.
			// EntityType<Accession> x =
			// entityManager.getMetamodel().entity(Accession.class);
			// System.err.println(x.getAttribute(o.getProperty()).getName());
			// sb.append(x.getAttribute(o.getProperty()).getName());
			sortBuffer.append("a.").append(o.getProperty());
			sortBuffer.append(" ").append(o.getDirection());
		}

		return this;
	}

	public DirectMysqlQuery pageable(Pageable pageable) {
		if (sortBuffer.length() != 0) {
			throw new RuntimeException("sortBuffer is not blank, invalid use of #pageable(Pageable)");
		}

		if (pageable == null) {
			return this;
		}

		if (pageable.getSort() != null) {
			sort(pageable.getSort());
		}
		sortBuffer.append(" limit ");
		if (LOG.isDebugEnabled()) {
			LOG.debug("Pageable=" + pageable.getOffset() + " " + pageable.getPageNumber());
		}
		sortBuffer.append(pageable.getPageSize());
		sortBuffer.append(" offset ");
		sortBuffer.append(pageable.getOffset());

		if (LOG.isDebugEnabled()) {
			LOG.debug("Filter query:\n" + sb.toString());
			LOG.debug("Parameter count: " + params.size());
			LOG.debug("Params: " + ArrayUtils.toString(params.toArray()));
		}

		return this;
	}

	public String getQuery(String what) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("select " + what + " " + sb.toString() + " " + whereBuffer.toString() + " " + sortBuffer.toString());
		}
		return "select " + what + " " + sb.toString() + " " + whereBuffer.toString() + " " + sortBuffer.toString();
	}

	public void limit(Integer limit) {
		if (sortBuffer.length() != 0) {
			throw new RuntimeException("sortBuffer is not blank, invalid use of #limit(Integer)");
		}
		if (limit != null) {
			sortBuffer.append(" limit ").append(limit);
		}
	}

	public static interface MethodResolver {
		Method getMethod(long methodId);
	}

}
