/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.service.EasySMTA;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile({ "dev", "cacher" })
@Component
public class EasySMTAMockConnector implements EasySMTA {
	private static final Log LOG = LogFactory.getLog(EasySMTAMockConnector.class);

	@Override
	public EasySMTA.EasySMTAUserData getUserData(String emailAddress) {

		LOG.info("Mock EasySMTA connector for email: " + emailAddress);

		if (RandomUtils.nextBoolean()) {
			LOG.info("Valid mock EasySMTA check");
			final EasySMTAUserData pidData = new EasySMTA.EasySMTAUserData();
			pidData.setPid(RandomStringUtils.randomAlphanumeric(6));
			pidData.setType("in");
			pidData.setTelephone("+00 000 000-0000");
			pidData.setName("Name");
			pidData.setSurname("Surname");
			pidData.setEmail(emailAddress);
			pidData.setOrgName("Organization");
			pidData.setCountry("DEU");
			pidData.setAddress("Address 1");
			pidData.setShipAddress("Address 2");
			pidData.setShipCountry("DEU");
			return pidData;
		} else {
			LOG.info("Invalid mock EasySMTA check");
			return null;
		}
	}
}
