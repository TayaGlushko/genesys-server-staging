/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.filters.AutocompleteFilter;
import org.genesys2.server.model.filters.BasicFilter;
import org.genesys2.server.model.filters.CodedMethodFilter;
import org.genesys2.server.model.filters.GenesysFilter;
import org.genesys2.server.model.filters.GenesysFilter.DataType;
import org.genesys2.server.model.filters.I18nListFilter;
import org.genesys2.server.model.filters.MethodFilter;
import org.genesys2.server.model.filters.ValueName;
import org.genesys2.server.model.genesys.Method;
import org.genesys2.server.model.genesys.TraitCode;
import org.genesys2.server.service.FilterConstants;
import org.genesys2.server.service.TraitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

/**
 * Converts filter requests (usually JSON) to internal data structures
 * 
 * @author matijaobreza
 * 
 */
@Component
public class FilterHandler {
	private static final Log LOG = LogFactory.getLog(FilterHandler.class);

	@Autowired
	private TraitService traitService;
	@Autowired
	private ObjectMapper objectMapper;

	private final ArrayList<GenesysFilter> availableFilters;

	/**
	 * By default we exclude historic records unless the user explicitly
	 * specifies otherwise.
	 */
	public static final AppliedFilter NON_HISTORIC_FILTER = new AppliedFilter().setFilterName(FilterConstants.HISTORIC).addFilterValue(new LiteralValueFilter(false));

	public FilterHandler() {
		this.availableFilters = new ArrayList<GenesysFilter>();

		this.availableFilters.add(new BasicFilter(FilterConstants.CROPS, DataType.STRING));
		this.availableFilters.add(new BasicFilter(FilterConstants.LISTS, DataType.STRING));

		this.availableFilters.add(new I18nListFilter<Integer>(FilterConstants.SAMPSTAT, DataType.NUMERIC).build("accession.sampleStatus", new Integer[] { 100,
				110, 120, 130, 200, 300, 400, 410, 411, 412, 413, 414, 415, 416, 420, 421, 422, 423, 500, 600, 999 }));
		this.availableFilters.add(new AutocompleteFilter(FilterConstants.TAXONOMY_GENUS, "/explore/ac/" + FilterConstants.TAXONOMY_GENUS));
		this.availableFilters.add(new AutocompleteFilter(FilterConstants.TAXONOMY_SPECIES, "/explore/ac/" + FilterConstants.TAXONOMY_SPECIES));
		this.availableFilters.add(new AutocompleteFilter(FilterConstants.TAXONOMY_SUBTAXA, "/explore/ac/" + FilterConstants.TAXONOMY_SUBTAXA));
		this.availableFilters.add(new AutocompleteFilter(FilterConstants.TAXONOMY_SCINAME, "/explore/ac/" + FilterConstants.TAXONOMY_SCINAME));

		this.availableFilters.add(new AutocompleteFilter(FilterConstants.ORGCTY_ISO3, "/explore/ac/" + FilterConstants.ORGCTY_ISO3));
		this.availableFilters.add(new AutocompleteFilter(FilterConstants.INSTITUTE_COUNTRY_ISO3, "/explore/ac/" + FilterConstants.ORGCTY_ISO3));
		this.availableFilters.add(new BasicFilter(FilterConstants.GEO_LATITUDE, DataType.NUMERIC));
		this.availableFilters.add(new BasicFilter(FilterConstants.GEO_LONGITUDE, DataType.NUMERIC));
		this.availableFilters.add(new BasicFilter(FilterConstants.GEO_ELEVATION, DataType.NUMERIC));

		this.availableFilters.add(new BasicFilter(FilterConstants.INSTITUTE_NETWORK, DataType.STRING));
		this.availableFilters.add(new AutocompleteFilter(FilterConstants.INSTCODE, "/explore/ac/" + FilterConstants.INSTCODE));
		this.availableFilters.add(new BasicFilter(FilterConstants.ACCENUMB, DataType.STRING).setAnalyzed(true));
		this.availableFilters.add(new BasicFilter(FilterConstants.SEQUENTIAL_NUMBER, DataType.NUMERIC));
		this.availableFilters.add(new BasicFilter(FilterConstants.ALIAS, DataType.STRING).setAnalyzed(true));
		this.availableFilters.add(new BasicFilter(FilterConstants.SGSV, DataType.BOOLEAN));
		this.availableFilters.add(new BasicFilter(FilterConstants.MLSSTATUS, DataType.BOOLEAN));
		this.availableFilters.add(new BasicFilter(FilterConstants.ART15, DataType.BOOLEAN));
		this.availableFilters.add(new BasicFilter(FilterConstants.AVAILABLE, DataType.BOOLEAN));
		this.availableFilters.add(new BasicFilter(FilterConstants.HISTORIC, DataType.BOOLEAN).allowsNull(false));
		this.availableFilters.add(new BasicFilter(FilterConstants.COLLMISSID, DataType.STRING).setAnalyzed(true));
		this.availableFilters.add(new I18nListFilter<Integer>(FilterConstants.STORAGE, DataType.NUMERIC).build("accession.storage", new Integer[] { 10, 11, 12,
				13, 20, 30, 40, 50, 99 }));
	}

	public List<GenesysFilter> listAvailableFilters() {
		return Collections.unmodifiableList(this.availableFilters);
	}

	public Map<String, GenesysFilter> mapAvailableFilters() {
		Map<String, GenesysFilter> filters = new HashMap<>();
		for (GenesysFilter filter: availableFilters) {
			filters.put(filter.getKey(), filter);
		}

		return filters;
	}

	public GenesysFilter getFilter(String key) {
		if (key.startsWith("gm:")) {
			return getMethodFilter(key);
		}

		for (GenesysFilter f : this.availableFilters) {
			if (f.getKey().equals(key))
				return f;
		}

		return null;
	}

	public List<GenesysFilter> selectFilters(String[] selectedFilters) {
		LOG.debug("Loading filter definitions sel=" + ArrayUtils.toString(selectedFilters));
		final List<GenesysFilter> filters = new ArrayList<GenesysFilter>();

		for (final String selectedFilter : selectedFilters) {
			try {
				final GenesysFilter filter = getFilter(selectedFilter);
				if (filter != null) {
					filters.add(filter);
				}
			} catch (NumberFormatException | NullPointerException e) {
				LOG.warn(e);
			}
		}

		return filters;
	}

	GenesysFilter getMethodFilter(String methodFilterName) {
		long methodId = Long.parseLong(methodFilterName.substring(3));
		final Method method = traitService.getMethod(methodId);
		return method == null ? null : toFilter(method);
	}

	private GenesysFilter toFilter(Method method) {
		DataType dataType = null;

		switch (method.getFieldType()) {
		case 1:
		case 2:
			dataType = DataType.NUMERIC;
			break;
		case 0:
			dataType = DataType.STRING;
		}

		GenesysFilter filter = null;
		if (method.isCoded()) {
			final Map<String, Long> stats = traitService.getMethodStatistics(method);
			final List<ValueName<?>> options = new ArrayList<ValueName<?>>();
			for (final TraitCode traitCode : TraitCode.parseOptions(method.getOptions())) {
				options.add(new ValueName<String>(traitCode.getCode(), traitCode.getValue(), stats.get(traitCode.getCode())));
			}
			filter = new CodedMethodFilter("gm:" + method.getId(), method.getParameter().getTitle(), dataType, options);
		} else {
			filter = new MethodFilter("gm:" + method.getId(), method.getParameter().getTitle(), dataType);
		}
		return filter;
	}

	public static interface FilterValue {
		String getType();
	}

	@JsonSerialize(using = AppliedFilters.Serializer.class)
	@JsonDeserialize(using = AppliedFilters.Deserializer.class)
	public static class AppliedFilters extends ArrayList<AppliedFilter> {
		/**
		 * 
		 */
		private static final long serialVersionUID = -6420230935893961593L;
		private static final ObjectMapper objectMapper = new ObjectMapper();

		@Override
		public String toString() {
			try {
				return objectMapper.writeValueAsString(this);
			} catch (JsonProcessingException e) {
				throw new RuntimeException(e);
			}
		}

		public static class Serializer extends JsonSerializer<AppliedFilters> {
			@Override
			public void serialize(AppliedFilters filters, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
				jgen.writeStartObject();
				for (AppliedFilter filter : filters) {
					jgen.writeArrayFieldStart(filter.getKey());
					for (FilterValue fv : filter.getValues())
						jgen.writeObject(fv);
					if (filter.withNull)
						jgen.writeNull();
					jgen.writeEndArray();
				}
				jgen.writeEndObject();
			}
		}

		public static class Deserializer extends StdDeserializer<AppliedFilters> {
			/**
			 * 
			 */
			private static final long serialVersionUID = -640987417833061931L;

			public Deserializer() {
				super(AppliedFilters.class);
			}

			protected Deserializer(Class<AppliedFilters> vc) {
				super(vc);
			}

			@Override
			public AppliedFilters deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
				if (jp.getCurrentToken() != JsonToken.START_OBJECT) {
					throw new IOException("invalid start marker");
				}
				final AppliedFilters appliedFilters = new AppliedFilters();

				jp.nextToken();
				while (jp.getCurrentToken() == JsonToken.FIELD_NAME) {
					AppliedFilter af = new AppliedFilter();
					appliedFilters.add(af);
					String name = jp.getCurrentName();
					if (name.startsWith("-")) {
						af.setInverse(true);
						name = name.substring(1);
					}
					af.setFilterName(name);

					jp.nextToken();
					if (jp.getCurrentToken() == JsonToken.START_ARRAY) {
						jp.nextToken();

						do {
							if (jp.getCurrentToken() == JsonToken.VALUE_STRING) {
								if (jp.getTextLength() == 36) {
									try {
										af.addFilterValue(new LiteralValueFilter(UUID.fromString(jp.getText())));
									} catch (IllegalArgumentException e) {
										System.err.println("Not an UUID");
										af.addFilterValue(new LiteralValueFilter(jp.getText()));
									}
								} else {
									af.addFilterValue(new LiteralValueFilter(jp.getText()));
								}
							} else if (jp.getCurrentToken() == JsonToken.VALUE_TRUE || jp.getCurrentToken() == JsonToken.VALUE_FALSE) {
								af.addFilterValue(new LiteralValueFilter(jp.getBooleanValue()));
							} else if (jp.getCurrentToken() == JsonToken.VALUE_NUMBER_FLOAT) {
								af.addFilterValue(new LiteralValueFilter(jp.getDoubleValue()));
							} else if (jp.getCurrentToken() == JsonToken.VALUE_NUMBER_INT) {
								af.addFilterValue(new LiteralValueFilter(jp.getLongValue()));
							} else if (jp.getCurrentToken() == JsonToken.VALUE_NULL) {
								af.addFilterValue(null);
							} else if (jp.getCurrentToken() == JsonToken.START_OBJECT) {
								jp.nextToken();
								String op = jp.getCurrentName();
								if ("like".equals(op)) {
									String startsWith = jp.nextTextValue();
									if (startsWith == null || StringUtils.isBlank(startsWith))
										throw new JsonParseException("StartsWithFilter expects a non-blank string", jp.getCurrentLocation());

									af.addFilterValue(new StartsWithFilter(startsWith));
								} else if ("min".equals(op)) {
									Number number1 = null;
									jp.nextToken();
									if (jp.getCurrentToken() == JsonToken.VALUE_NUMBER_FLOAT) {
										number1 = jp.getDoubleValue();
									} else if (jp.getCurrentToken() == JsonToken.VALUE_NUMBER_INT) {
										number1 = jp.getLongValue();
									}

									if (number1 == null)
										throw new JsonParseException("MinValueFilter expects a single numeric value", jp.getCurrentLocation());

									af.addFilterValue(new MinValueFilter(number1));
								} else if ("max".equals(op)) {
									Number number1 = null;
									jp.nextToken();
									if (jp.getCurrentToken() == JsonToken.VALUE_NUMBER_FLOAT) {
										number1 = jp.getDoubleValue();
									} else if (jp.getCurrentToken() == JsonToken.VALUE_NUMBER_INT) {
										number1 = jp.getLongValue();
									}

									if (number1 == null)
										throw new JsonParseException("MaxValueFilter expects a single numeric value", jp.getCurrentLocation());

									af.addFilterValue(new MaxValueFilter(number1));
								} else if ("range".equals(op)) {
									if (jp.nextToken() == JsonToken.START_ARRAY) {
										Number number1 = null, number2 = null;
										jp.nextToken();
										if (jp.getCurrentToken() == JsonToken.VALUE_NUMBER_FLOAT) {
											number1 = jp.getDoubleValue();
										} else if (jp.getCurrentToken() == JsonToken.VALUE_NUMBER_INT) {
											number1 = jp.getLongValue();
										}
										jp.nextToken();
										if (jp.getCurrentToken() == JsonToken.VALUE_NUMBER_FLOAT) {
											number2 = jp.getDoubleValue();
										} else if (jp.getCurrentToken() == JsonToken.VALUE_NUMBER_INT) {
											number2 = jp.getLongValue();
										}
										if (number1 == null || number2 == null || jp.nextToken() != JsonToken.END_ARRAY)
											throw new JsonParseException("ValueRangeFilter expects two numeric values in an array", jp.getCurrentLocation());

										af.addFilterValue(new ValueRangeFilter(number1, number2));
									} else {
										throw new JsonParseException("ValueRangeFilter expects an array of values", jp.getCurrentLocation());
									}
								}

								if (jp.nextToken() != JsonToken.END_OBJECT)
									throw new JsonParseException("Expecting }, got " + jp.getCurrentToken(), jp.getCurrentLocation());
							} else if (jp.getCurrentToken() == JsonToken.END_ARRAY) {
								// Blank array
								break;
							} else {
								throw new JsonParseException("I don't know where I am.", jp.getCurrentLocation());
							}

							jp.nextToken();

						} while (jp.getCurrentToken() != JsonToken.END_ARRAY);
					} else {
						System.err.println(jp.getCurrentToken());
						throw new JsonParseException("Filter values must be provided in an array", jp.getCurrentLocation());
					}

					if (jp.nextToken() == JsonToken.END_OBJECT)
						break;

					// System.err.println(jp.getCurrentToken());
				}
				return appliedFilters;
			}
		}

		/**
		 * Get the first applied filter matching filterName
		 * 
		 * @param filterName
		 * @return
		 */
		public AppliedFilter get(String filterName) {
			for (AppliedFilter af : this) {
				if (af.getFilterName().equals(filterName))
					return af;
			}
			return null;
		}

		/**
		 * Returns true if the filter is listed and has at least one value
		 * specified
		 * 
		 * @param filterName
		 * @return
		 */
		public boolean hasFilter(final String filterName) {
			final AppliedFilter f = get(filterName);
			return f != null && (f.getWithNull() || f.getValues().size() > 0);
		}

		public Collection<AppliedFilter> methodFilters() {
			return CollectionUtils.select(this, new Predicate<AppliedFilter>() {
				@Override
				public boolean evaluate(AppliedFilter filter) {
					return filter.getFilterName().startsWith("gm:");
				}
			});
		}

		/**
		 * Collect filterNames from AppliedFilters in this list
		 * 
		 * @return
		 */
		public String[] getFilterNames() {
			List<String> filterNames = new ArrayList<String>();
			for (AppliedFilter af : this) {
				filterNames.add(af.getFilterName());
			}
			return filterNames.toArray(ArrayUtils.EMPTY_STRING_ARRAY);
		}

		/**
		 * Get first literal filter value
		 * 
		 * @param filterName
		 * @param class1
		 * @return
		 */
		@SuppressWarnings("unchecked")
		public <T> T getFirstLiteralValue(String filterName, Class<T> clazz) {
			AppliedFilter af = get(filterName);
			if (af != null) {
				for (FilterValue fv : af.getValues()) {
					if (fv instanceof LiteralValueFilter) {
						Object obj = ((LiteralValueFilter) fv).getValue();
						if (clazz.isAssignableFrom(obj.getClass()))
							return (T) obj;
					}
				}
			}
			return null;
		}
	}

	public static class AppliedFilter {

		private Set<FilterValue> values = new HashSet<FilterValue>();
		private boolean withNull = false;
		private String filterName;
		private boolean inverse = false;

		public String getFilterName() {
			return this.filterName;
		}

		public AppliedFilter setFilterName(String filterName) {
			this.filterName = filterName;
			return this;
		}

		public AppliedFilter addFilterValue(FilterValue filterValue) {
			if (filterValue == null) {
				this.withNull = true;
			} else {
				values.add(filterValue);
			}

			return this;
		}

		/**
		 * When inverse, selected values are excluded instead of included
		 */
		public boolean isInverse() {
			return inverse;
		}

		/**
		 * When inverse, selected values are excluded instead of included
		 */
		public boolean getInverse() {
			return isInverse();
		}

		/**
		 * Set the filter to be inverse (i.e. excluding) selected values
		 * 
		 * @param inverse
		 */
		public void setInverse(boolean inverse) {
			this.inverse = inverse;
		}

		public boolean getWithNull() {
			return this.withNull;
		}

		public Set<FilterValue> getValues() {
			return values;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((filterName == null) ? 0 : filterName.hashCode());
			result = prime * result + (inverse ? 1231 : 1237);
			result = prime * result + ((values == null) ? 0 : values.hashCode());
			result = prime * result + (withNull ? 1231 : 1237);
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			AppliedFilter other = (AppliedFilter) obj;
			if (filterName == null) {
				if (other.filterName != null)
					return false;
			} else if (!filterName.equals(other.filterName))
				return false;
			if (inverse != other.inverse)
				return false;
			if (values == null) {
				if (other.values != null)
					return false;
			} else if (!values.equals(other.values))
				return false;
			if (withNull != other.withNull)
				return false;
			return true;
		}

		/**
		 * Returns the number of applied values, +1 when null is included
		 * 
		 * @return
		 */
		public int size() {
			if (withNull) {
				return values.size() + 1;
			}
			return values.size();
		}

		/**
		 * Return the filter key used in JSON
		 * 
		 * @return "filterName" or "-filterName" for negative filters
		 */
		public String getKey() {
			if (inverse) {
				return "-" + this.filterName;
			}
			return this.filterName;
		}

		public AppliedFilter addFilterValues(Class<LiteralValueFilter> clazz, Collection<Long> accessionIds) {
			try {
				for (long acceId : accessionIds) {
					values.add(clazz.getDeclaredConstructor(Object.class).newInstance(acceId));
				}
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			}
			return this;
		}
	}

	@JsonSerialize(using = LiteralValueFilter.Serializer.class)
	public static class LiteralValueFilter implements FilterValue {

		public static class Serializer extends JsonSerializer<LiteralValueFilter> {
			@Override
			public void serialize(LiteralValueFilter value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
				jgen.writeObject(value.getValue());
			}
		}

		private Object value;

		public LiteralValueFilter(Object value) {
			this.value = value;
		}

		public Object getValue() {
			return value;
		}

		@Override
		public String toString() {
			return getClass().getSimpleName() + "=" + getValue();
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((value == null) ? 0 : value.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			LiteralValueFilter other = (LiteralValueFilter) obj;
			if (value == null) {
				if (other.value != null)
					return false;
			} else if (!value.equals(other.value))
				return false;
			return true;
		}

		@Override
		public String getType() {
			return "literal";
		}
	}

	@JsonSerialize(using = ValueRangeFilter.Serializer.class)
	public static class ValueRangeFilter implements FilterValue {

		public static class Serializer extends JsonSerializer<ValueRangeFilter> {
			@Override
			public void serialize(ValueRangeFilter value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
				jgen.writeStartObject();
				jgen.writeArrayFieldStart(value.getType());
				jgen.writeObject(value.getFrom());
				jgen.writeObject(value.getTo());
				jgen.writeEndArray();
				jgen.writeEndObject();
			}
		}

		private Number from;
		private Number to;

		public ValueRangeFilter(Number number1, Number number2) {
			if (number1 == null || number2 == null)
				throw new NullPointerException("ValueRangeFilter requires non-null values for range");

			if (number1 instanceof Integer)
				number1 = number1.longValue();
			if (number1 instanceof Float)
				number1 = number1.doubleValue();

			if (number2 instanceof Integer)
				number2 = number2.longValue();
			if (number2 instanceof Float)
				number2 = number2.doubleValue();

			this.from = number1.doubleValue() < number2.doubleValue() ? number1 : number2;
			this.to = number1.doubleValue() < number2.doubleValue() ? number2 : number1;
		}

		@Override
		public String getType() {
			return "range";
		}

		public Number getFrom() {
			return from;
		}

		public Number getTo() {
			return to;
		}

		@Override
		public String toString() {
			return getClass().getSimpleName() + " " + getFrom() + "<=" + getType() + "<=" + getTo();
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((from == null) ? 0 : from.hashCode());
			result = prime * result + ((to == null) ? 0 : to.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			ValueRangeFilter other = (ValueRangeFilter) obj;
			if (from == null) {
				if (other.from != null)
					return false;
			} else if (!from.equals(other.from))
				return false;
			if (to == null) {
				if (other.to != null)
					return false;
			} else if (!to.equals(other.to))
				return false;
			return true;
		}
	}

	@JsonSerialize(using = MaxValueFilter.Serializer.class)
	public static class MaxValueFilter implements FilterValue {
		public static class Serializer extends JsonSerializer<MaxValueFilter> {
			@Override
			public void serialize(MaxValueFilter value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
				jgen.writeStartObject();
				jgen.writeObjectField(value.getType(), value.getTo());
				jgen.writeEndObject();
			}
		}

		private Number to;

		public MaxValueFilter(Number max) {
			if (max instanceof Integer)
				max = max.longValue();
			else if (max instanceof Float)
				max = max.doubleValue();

			this.to = max;
		}

		@Override
		public String getType() {
			return "max";
		}

		public Number getTo() {
			return to;
		}

		@Override
		public String toString() {
			return getClass().getSimpleName() + " " + getType() + ": " + getTo();
		}
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((to == null) ? 0 : to.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			MaxValueFilter other = (MaxValueFilter) obj;
			if (to == null) {
				if (other.to != null)
					return false;
			} else if (!to.equals(other.to))
				return false;
			return true;
		}
	}

	@JsonSerialize(using = MinValueFilter.Serializer.class)
	public static class MinValueFilter implements FilterValue {
		public static class Serializer extends JsonSerializer<MinValueFilter> {
			@Override
			public void serialize(MinValueFilter value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
				jgen.writeStartObject();
				jgen.writeObjectField(value.getType(), value.getFrom());
				jgen.writeEndObject();
			}
		}

		private Number from;

		public MinValueFilter(Number min) {
			if (min instanceof Integer)
				this.from = min.longValue();
			else if (min instanceof Float)
				this.from = min.doubleValue();
			else
				this.from = min;
		}

		@Override
		public String getType() {
			return "min";
		}

		public Number getFrom() {
			return from;
		}


		@Override
		public String toString() {
			return getClass().getSimpleName() + " " + getType() + ": " + getFrom();
		}
		
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((from == null) ? 0 : from.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			MinValueFilter other = (MinValueFilter) obj;
			if (from == null) {
				if (other.from != null)
					return false;
			} else if (!from.equals(other.from))
				return false;
			return true;
		}

	}

	@JsonSerialize(using = StartsWithFilter.Serializer.class)
	public static class StartsWithFilter implements FilterValue {
		public static class Serializer extends JsonSerializer<StartsWithFilter> {
			@Override
			public void serialize(StartsWithFilter value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
				jgen.writeStartObject();
				jgen.writeObjectField(value.getType(), value.getStartsWith());
				jgen.writeEndObject();
			}
		}

		private String startsWith;

		public StartsWithFilter(String startsWith) {
			this.startsWith = startsWith;
		}

		@Override
		public String getType() {
			return "like";
		}

		public String getStartsWith() {
			return startsWith;
		}

		@Override
		public String toString() {
			return getClass().getSimpleName() + " startsWith " + getStartsWith();
		}
				
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((startsWith == null) ? 0 : startsWith.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			StartsWithFilter other = (StartsWithFilter) obj;
			if (startsWith == null) {
				if (other.startsWith != null)
					return false;
			} else if (!startsWith.equals(other.startsWith))
				return false;
			return true;
		}
	}
}
