/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service;

import java.io.IOException;

import org.genesys2.server.service.impl.EasySMTAException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public interface EasySMTA {
	EasySMTAUserData getUserData(String emailAddress) throws EasySMTAException;

	/**
	 * Data returned by Easy-SMTA
	 *
	 * @author matijaobreza
	 */
	@JsonDeserialize(using = Deserializer.class)
	public static class EasySMTAUserData {
		private String pid;
		// / "in" = Individual, "or" = Organization
		private String type;
		private String legalStatus;
		private String name;
		private String surname;
		private String email;
		private String address;
		private String country;
		private String countryName;
		private String telephone;
		private String fax;
		private String orgName;
		private String aoName, aoSurname, aoEmail;
		private String orgAddress, orgCountry, orgCountryName;
		private String aoTelephone, aoFax;
		private String shipAddrFlag;
		private String shipAddress;
		private String shipCountry;
		private String shipTelephone;

		public EasySMTAUserData() {
		}

		@Override
		public String toString() {
			return "ITPGRFA.PID email=" + email + " first=" + name + " last=" + surname + ", org=" + orgName + ", country=" + country;
		}

		public String getPid() {
			return pid;
		}

		public void setPid(String pid) {
			this.pid = pid;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getLegalStatus() {
			return legalStatus;
		}

		public void setLegalStatus(String legalStatus) {
			this.legalStatus = legalStatus;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getSurname() {
			return surname;
		}

		public void setSurname(String surname) {
			this.surname = surname;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getAddress() {
			return address;
		}

		public void setAddress(String address) {
			this.address = address;
		}

		public String getCountry() {
			return country;
		}

		public void setCountry(String country) {
			this.country = country;
		}

		public String getCountryName() {
			return countryName;
		}

		public void setCountryName(String countryName) {
			this.countryName = countryName;
		}

		public String getTelephone() {
			return telephone;
		}

		public void setTelephone(String telephone) {
			this.telephone = telephone;
		}

		public String getFax() {
			return fax;
		}

		public void setFax(String fax) {
			this.fax = fax;
		}

		public String getOrgName() {
			return orgName;
		}

		public void setOrgName(String orgName) {
			this.orgName = orgName;
		}

		public String getAoName() {
			return aoName;
		}

		public void setAoName(String aoName) {
			this.aoName = aoName;
		}

		public String getAoSurname() {
			return aoSurname;
		}

		public void setAoSurname(String aoSurname) {
			this.aoSurname = aoSurname;
		}

		public String getAoEmail() {
			return aoEmail;
		}

		public void setAoEmail(String aoEmail) {
			this.aoEmail = aoEmail;
		}

		public String getOrgAddress() {
			return orgAddress;
		}

		public void setOrgAddress(String orgAddress) {
			this.orgAddress = orgAddress;
		}

		public String getOrgCountry() {
			return orgCountry;
		}

		public void setOrgCountry(String orgCountry) {
			this.orgCountry = orgCountry;
		}

		public String getOrgCountryName() {
			return orgCountryName;
		}

		public void setOrgCountryName(String orgCountryName) {
			this.orgCountryName = orgCountryName;
		}

		public String getAoTelephone() {
			return aoTelephone;
		}

		public void setAoTelephone(String aoTelephone) {
			this.aoTelephone = aoTelephone;
		}

		public String getAoFax() {
			return aoFax;
		}

		public void setAoFax(String aoFax) {
			this.aoFax = aoFax;
		}

		public String getShipAddrFlag() {
			return shipAddrFlag;
		}

		public void setShipAddrFlag(String shipAddrFlag) {
			this.shipAddrFlag = shipAddrFlag;
		}

		public String getShipAddress() {
			return shipAddress;
		}

		public void setShipAddress(String shipAddress) {
			this.shipAddress = shipAddress;
		}

		public String getShipCountry() {
			return shipCountry;
		}

		public void setShipCountry(String shipCountry) {
			this.shipCountry = shipCountry;
		}

		public String getShipTelephone() {
			return shipTelephone;
		}

		public void setShipTelephone(String shipTelephone) {
			this.shipTelephone = shipTelephone;
		}
	}

	public final class Deserializer extends StdDeserializer<EasySMTAUserData> {

		/**
		 *
		 */
		private static final long serialVersionUID = 6520637074244700748L;

		public Deserializer() {
			super(EasySMTAUserData.class);
		}

		protected Deserializer(Class<?> vc) {
			super(vc);
		}

		@Override
		public EasySMTAUserData deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
			if (jp.getCurrentToken() != JsonToken.START_OBJECT) {
				throw new IOException("invalid start marker");
			}
			final EasySMTAUserData pidData = new EasySMTAUserData();
			boolean foundName = false, foundSurname = false;
			boolean foundOrgname = false, foundCountry = false;

			while (jp.nextToken() != JsonToken.END_OBJECT) {
				final String fieldname = jp.getCurrentName();
				jp.nextToken(); // move to next token in string

				if ("pid".equals(fieldname)) {
					pidData.setPid(jp.getText());
				} else if ("type".equals(fieldname)) {
					pidData.setType(jp.getText());
				} else if ("legalStatus".equals(fieldname)) {
					pidData.setLegalStatus(jp.getText());
				} else if ("name".equals(fieldname) || "firstName".equals(fieldname)) {
					if (foundName) {
						throw new JsonMappingException("Field " + fieldname + " is a synonym. Don't provide both.");
					}
					foundName = true;
					pidData.setName(jp.getText());
				} else if ("surname".equals(fieldname) || "lastName".equals(fieldname)) {
					if (foundSurname) {
						throw new JsonMappingException("Field " + fieldname + " is a synonym. Don't provide both.");
					}
					foundSurname = true;
					pidData.setSurname(jp.getText());
				} else if ("email".equals(fieldname)) {
					pidData.setEmail(jp.getText());
				} else if ("address".equals(fieldname)) {
					pidData.setAddress(jp.getText());
				} else if ("country".equals(fieldname) || "countryIsoCode3".equals(fieldname)) {
					if (foundCountry) {
						throw new JsonMappingException("Field " + fieldname + " is a synonym. Don't provide both.");
					}
					foundCountry = true;
					pidData.setCountry(jp.getText());
				} else if ("countryName".equals(fieldname)) {
					pidData.setCountryName(jp.getText());
				} else if ("telephone".equals(fieldname)) {
					pidData.setTelephone(jp.getText());
				} else if ("fax".equals(fieldname)) {
					pidData.setFax(jp.getText());

				} else if ("orgName".equals(fieldname) || "organization".equals(fieldname)) {
					if (foundOrgname) {
						throw new JsonMappingException("Field " + fieldname + " is a synonym. Don't provide both.");
					}
					foundOrgname = true;
					pidData.setOrgName(jp.getText());
				} else if ("orgAddress".equals(fieldname)) {
					pidData.setOrgAddress(jp.getText());
				} else if ("orgCountry".equals(fieldname)) {
					pidData.setOrgCountry(jp.getText());
				} else if ("orgCountryName".equals(fieldname)) {
					pidData.setOrgCountryName(jp.getText());

				} else if ("aoName".equals(fieldname)) {
					pidData.setAoName(jp.getText());
				} else if ("aoSurname".equals(fieldname)) {
					pidData.setAoSurname(jp.getText());
				} else if ("aoEmail".equals(fieldname)) {
					pidData.setAoEmail(jp.getText());
				} else if ("aoTelephone".equals(fieldname)) {
					pidData.setAoTelephone(jp.getText());
				} else if ("aoFax".equals(fieldname)) {
					pidData.setAoFax(jp.getText());

				} else if ("shipAddrFlag".equals(fieldname)) {
					pidData.setShipAddrFlag(jp.getText());
				} else if ("shipAddress".equals(fieldname)) {
					pidData.setShipAddress(jp.getText());
				} else if ("shipCountry".equals(fieldname)) {
					pidData.setShipCountry(jp.getText());
				} else if ("shipTelephone".equals(fieldname)) {
					pidData.setShipTelephone(jp.getText());
				}

				else {
					throw new JsonMappingException("Unsupported EasySMTAUserData field: " + fieldname);
				}
			}
			// Don't close?!
			// jp.close();
			return pidData;
		}
	}
}
