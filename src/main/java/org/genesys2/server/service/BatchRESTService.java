/**
 * Copyright 2015 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service;

import java.util.List;
import java.util.Map;

import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.service.impl.RESTApiException;
import org.genesys2.server.servlet.controller.rest.model.AccessionHeaderJson;
import org.genesys2.server.servlet.controller.rest.model.AccessionNamesJson;

import com.fasterxml.jackson.databind.node.ObjectNode;

public interface BatchRESTService {

	List<AccessionOpResponse> upsertAccessionData(FaoInstitute institute, Map<AccessionHeaderJson, ObjectNode> batch) throws RESTApiException;

	List<AccessionOpResponse> upsertAccessionNames(FaoInstitute institute, List<AccessionNamesJson> batch) throws RESTApiException;

	List<AccessionOpResponse> deleteAccessions(FaoInstitute institute, List<AccessionHeaderJson> batch) throws RESTApiException;

	int deleteAccessionsById(FaoInstitute institute, List<Long> batch);

	void ensureTaxonomies(FaoInstitute institute, Map<AccessionHeaderJson, ObjectNode> batch) throws RESTApiException;
}
