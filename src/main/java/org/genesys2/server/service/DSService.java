package org.genesys2.server.service;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.MappedByteBuffer;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.genesys2.server.model.dataset.DS;
import org.genesys2.server.model.dataset.DSDescriptor;
import org.genesys2.server.model.dataset.DSQualifier;
import org.genesys2.server.model.genesys.AccessionGeo;
import org.genesys2.server.model.impl.Descriptor;
import org.genesys2.server.model.json.WorldclimJson;

public interface DSService {

	void saveDataset(DS ds);

	DSQualifier addQualifier(DS ds, Descriptor d1);
	DSDescriptor addDescriptor(DS ds, Descriptor descriptor);

	DS loadDatasetByUuid(UUID uuid);


	void updateRow(DS ds, Object[] row);
	void updateRows(DS ds, List<Object[]> rows);
	void updateRow(DS ds, Object[] qualifiers, DSDescriptor dsd, Object value);

	void worldclimUpdate(DS dataset, DSDescriptor dsd, Set<Long> ids, MappedByteBuffer buffer, short nullValue, double factor);

	void updateAccessionTileIndex(List<AccessionGeo> toSave);

	WorldclimJson jsonForTile(DS worldClimDataset, Long tileIndex);

	void download(DS ds, OutputStream outputStream) throws IOException;

	void download(DS ds, List<DSDescriptor> dsds, OutputStream outputStream) throws IOException;

	void deleteDataset(DS ds);

	void deleteDescriptor(DSDescriptor dsd);

	void deleteRows(DS ds);
}
