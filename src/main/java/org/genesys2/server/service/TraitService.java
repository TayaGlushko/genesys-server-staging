/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service;

import java.util.List;
import java.util.Map;

import org.genesys2.server.model.genesys.AccessionId;
import org.genesys2.server.model.genesys.Metadata;
import org.genesys2.server.model.genesys.Method;
import org.genesys2.server.model.genesys.Parameter;
import org.genesys2.server.model.genesys.ParameterCategory;
import org.genesys2.server.model.impl.Crop;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;

public interface TraitService {

	Page<Parameter> listTraits(Pageable pageable);

	Page<Parameter> listTraits(Crop crop, Pageable pageable);

	List<Parameter> listTraits();

	Parameter getTrait(long traitId);

	List<Method> getTraitMethods(Parameter trait);

	Method getMethod(long traitId);

	List<Metadata> listMetadataByMethod(Method method);

	List<Method> listMethods(AccessionId accession);

	List<Method> listMethods();

	/**
	 * Add new crop descriptor Property Category
	 *
	 * @param name
	 * @return
	 */
	@PreAuthorize("isAuthenticated()")
	ParameterCategory addCategory(String name, String i18n);

	// TODO implement rdfUri functionality
	// ParameterCategory addCategory(String rdfUri, String name, String i18n);

	/**
	 * Get an existing crop descriptor Property Category by name
	 *
	 * @param name
	 * @return
	 */
	@PreAuthorize("isAuthenticated()")
	ParameterCategory getCategory(String name);

	/**
	 * Add new crop descriptor Property
	 *
	 * @param crop
	 * @param category
	 * @param title
	 * @param i18n
	 * @return
	 */
	@PreAuthorize("isAuthenticated()")
	Parameter addParameter(String rdfUri, Crop crop, String category, String title, String i18n);

	/**
	 *
	 * @param rdfUri
	 *            is the Resource Description Framework (RDF) Uniform Resource
	 *            Identifier (URI) of the Parameter concept
	 * @return
	 */
	@PreAuthorize("isAuthenticated()")
	Parameter getParameter(String rdfUri);

	/**
	 * Get a crop descriptor property by title
	 *
	 * @param crop
	 * @param title
	 * @return
	 */
	@PreAuthorize("isAuthenticated()")
	Parameter getParameter(Crop crop, String title);

	/**
	 * Add new method
	 *
	 * @param description
	 * @param unit
	 * @param fieldName
	 * @param fieldType
	 * @param fieldSize
	 *            required for Strings
	 * @param options
	 * @param range
	 * @param parameter
	 *            Optional, {@link Parameter}
	 * @return
	 */
	@PreAuthorize("isAuthenticated()")
	Method addMethod(String rdfUri, String description, String i18n, String unit, String fieldName, int fieldType, Integer fieldSize, String options,
			String range, Parameter parameter);

	@PreAuthorize("isAuthenticated()")
	Method getMethod(String rdfUri);

	@PreAuthorize("isAuthenticated()")
	Method getMethod(String description, Parameter parameter);

	/**
	 * List user's methods
	 *
	 * @return
	 */
	List<Method> listMyMethods();

	Map<String, Long> getMethodStatistics(Method method);

	List<ParameterCategory> listCategories();

	Map<ParameterCategory, List<Parameter>> mapTraits(Crop crop, List<ParameterCategory> parameterCategories);

	Map<Long, List<Method>> mapMethods(Crop crop);

	Page<Method> listMethods(Pageable pageable);

}
