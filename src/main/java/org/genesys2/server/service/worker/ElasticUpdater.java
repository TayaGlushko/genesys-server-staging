/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.worker;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.elastic.AccessionDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.ElasticsearchException;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.stereotype.Component;

import com.hazelcast.core.IQueue;

/**
 * Component that receives updated or deleted accession IDs and uses a
 * background thread to refresh ES
 * 
 * @author matijaobreza
 */
@Component
public class ElasticUpdater {
	public static final Log LOG = LogFactory.getLog(ElasticUpdater.class);

	@Autowired
	private ElasticsearchTemplate elasticsearchTemplate;

	@Resource
	private IQueue<ElasticNode> elasticRemoveQueue;
	
	@Resource
	private IQueue<ElasticNode> elasticUpdateQueue;

	/**
	 * Schedule objects for removal
	 * 
	 * @param clazz
	 * @param ids
	 */
	public void remove(Class<?> clazz, Long id) {
		ElasticNode node = new ElasticNode(clazz, id);
		elasticUpdateQueue.remove(node);
		elasticRemoveQueue.add(node);
	}

	/**
	 * Schedule objects for removal
	 * 
	 * @param clazz
	 * @param ids
	 */
	public void removeAll(Class<?> clazz, Long... ids) {
		Set<ElasticNode> nodes = makeNodes(clazz, ids);
		if (nodes.isEmpty())
			return;

		elasticUpdateQueue.removeAll(nodes);
		elasticRemoveQueue.addAll(nodes);
	}

	/**
	 * Schedule objects for update
	 * 
	 * @param clazz
	 * @param ids
	 */
	public void update(Class<?> clazz, Long id) {
		ElasticNode node = new ElasticNode(clazz, id);

		elasticRemoveQueue.remove(node);
		// reinsert to end of queue
		elasticUpdateQueue.remove(node);
		elasticUpdateQueue.add(node);
	}

	/**
	 * Schedule objects for update
	 * 
	 * @param clazz
	 * @param ids
	 */
	public void updateAll(Class<?> clazz, Long... ids) {
		Set<ElasticNode> nodes = makeNodes(clazz, ids);
		if (nodes.isEmpty())
			return;

		elasticRemoveQueue.removeAll(nodes);
		// reinsert to end of queue
		elasticUpdateQueue.removeAll(nodes);
		
		
		while (elasticUpdateQueue.size()>5000) {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
			}
		}
		
		elasticUpdateQueue.addAll(nodes);
	}

	/**
	 * Make a Set of ElasticNodes
	 * 
	 * @param clazz
	 * @param ids
	 * @return
	 */
	private Set<ElasticNode> makeNodes(Class<?> clazz, Long[] ids) {
		HashSet<ElasticNode> set = new HashSet<ElasticNode>();
		if (ids != null) {
			for (Long id : ids) {
				set.add(new ElasticNode(clazz, id));
			}
		}
		return set;
	}

	public void clearQueues() {
		elasticRemoveQueue.clear();
		elasticUpdateQueue.clear();
	}

	public static class ElasticNode implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = -7664899162710772530L;
		private final String className;
		private final long id;

		public ElasticNode(final Class<?> clazz, final Long id) {
			this.className = clazz.getName();
			this.id = id;
		}

		public long getId() {
			return id;
		}

		public String getClassName() {
			return className;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((className == null) ? 0 : className.hashCode());
			result = prime * result + (int) (id ^ (id >>> 32));
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			ElasticNode other = (ElasticNode) obj;
			if (className == null) {
				if (other.className != null)
					return false;
			} else if (!className.equals(other.className))
				return false;
			if (id != other.id)
				return false;
			return true;
		}

		@Override
		public String toString() {
			return className + " id=" + id;
		}
	}

	public void archive(final List<AccessionDetails> deletedAccessions) {
		final List<IndexQuery> queries = new ArrayList<>(deletedAccessions.size());
		for (AccessionDetails ad : deletedAccessions) {
			IndexQuery iq = new IndexQuery();
			iq.setObject(ad);
			iq.setIndexName("genesysarchive");
			iq.setType("mcpd");
			queries.add(iq);
		}

		try {
			elasticsearchTemplate.bulkIndex(queries);
		} catch (ElasticsearchException e) {
			LOG.error(e.getMessage());

			Map<String, String> failedDocuments = e.getFailedDocuments();
			for (String key : failedDocuments.keySet()) {
				LOG.warn("ES failed document: " + key + ": " + failedDocuments.get(key));
			}
		}
	}
}
