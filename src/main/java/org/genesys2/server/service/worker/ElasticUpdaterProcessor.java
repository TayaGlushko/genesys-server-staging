package org.genesys2.server.service.worker;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang.time.StopWatch;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.service.ElasticService;
import org.genesys2.server.service.FullTextSearchService;
import org.genesys2.server.service.worker.ElasticUpdater.ElasticNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import com.hazelcast.core.IQueue;

/**
 * ES Processor component uses Spring's @Scheduled annotation to scan queues
 * with 2000ms delay measured from the completion time of each preceding
 * invocation.
 */
@Component
class ElasticUpdaterProcessor {
	public static final Log LOG = LogFactory.getLog(ElasticUpdaterProcessor.class);

	private static final int BATCH_SIZE = 100;

	@Autowired
	private ElasticService elasticService;

	@Autowired
	private FullTextSearchService fullTextSearchService;

	@Autowired
	private ThreadPoolTaskExecutor executor;

	@Resource
	private IQueue<ElasticNode> elasticRemoveQueue;

	@Resource
	private IQueue<ElasticNode> elasticUpdateQueue;

	private HashMap<String, Set<Long>> buckets = new HashMap<String, Set<Long>>();

	@Scheduled(fixedDelay = 2000)
	public void processQueues() {
		// First remove
		{
			if (LOG.isTraceEnabled()) {
				LOG.trace("Scanning ES remove queue");
			}

			int i = 0;
			ElasticNode toRemove = null;
			do {
				toRemove = elasticRemoveQueue.poll();
				i++;

				if (toRemove != null) {
					removeNode(toRemove);
				}

				if (LOG.isInfoEnabled() && (i % 100 == 0)) {
					LOG.info("Queue size remove=" + elasticRemoveQueue.size() + " deleted=" + i);
				}

			} while (toRemove != null);
		}

		// Then update
		{
			if (LOG.isTraceEnabled()) {
				LOG.trace("Scanning ES update queue");
			}

			int i = 0;
			StopWatch stopWatch = new StopWatch();
			stopWatch.start();

			ElasticNode toUpdate = null;
			do {
				toUpdate = elasticUpdateQueue.poll();
				i++;

				if (toUpdate != null) {
					updateNode(toUpdate);
				}

				if (LOG.isInfoEnabled() && (i % 500 == 0)) {
					LOG.info("Queue size update=" + elasticUpdateQueue.size() + " indexed=" + i + " rate=" + (1000.0 * i / (stopWatch.getTime()))
							+ " records/s");
				}

			} while (toUpdate != null);
		}

		if (!buckets.isEmpty()) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Queue size remove=" + elasticRemoveQueue.size() + " update=" + elasticUpdateQueue.size());
			}

			for (String className : buckets.keySet()) {
				Set<Long> bucket = buckets.get(className);
				executeUpdate(className, bucket);
			}
			buckets.clear();
		}
	}

	/**
	 * Schedule a parallel update
	 * 
	 * @param className
	 * @param bucket
	 */
	private void executeUpdate(final String className, final Set<Long> bucket) {
		final HashSet<Long> copy = new HashSet<>(bucket);
		bucket.clear();
		executor.execute(new Runnable() {
			@Override
			public void run() {
				if (LOG.isTraceEnabled()) {
					LOG.trace("Reindexing " + className + " size=" + copy.size());
				}

				elasticService.updateAll(className, copy);
				fullTextSearchService.updateAll(className, copy);

				if (LOG.isTraceEnabled()) {
					LOG.trace("Reindexing done.");
				}
			}
		});
	}

	private void updateNode(ElasticNode toUpdate) {
		if (toUpdate == null)
			return;

		String className = toUpdate.getClassName();

		Set<Long> bucket = buckets.get(className);
		if (bucket == null) {
			buckets.put(className, bucket = new HashSet<Long>());
		}
		bucket.add(toUpdate.getId());
		if (bucket.size() >= BATCH_SIZE) {
			executeUpdate(className, bucket);
		}
	}

	private void removeNode(ElasticNode toRemove) {
		if (toRemove == null)
			return;

		elasticService.remove(toRemove.getClassName(), toRemove.getId());
		fullTextSearchService.remove(toRemove.getClassName(), toRemove.getId());
	}
}