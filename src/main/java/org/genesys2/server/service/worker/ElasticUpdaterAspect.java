/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.worker;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.genesys2.server.model.elastic.AccessionDetails;
import org.genesys2.server.model.elastic.FullTextDocument;
import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.genesys.AccessionRelated;
import org.genesys2.server.model.genesys.SvalbardData;
import org.genesys2.server.model.impl.*;
import org.genesys2.server.service.GenesysService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Aspects that trigger updates of ES
 */
@Aspect
@Component
public class ElasticUpdaterAspect {
	public static final Log LOG = LogFactory.getLog(ElasticUpdaterAspect.class);

	@Autowired
	private ElasticUpdater elasticUpdater;

	@Autowired
	private GenesysService genesysService;

	/**
	 * Handle updates to Accession and related objects
	 * 
	 * @param joinPoint
	 * @param result
	 * @return
	 * @throws Throwable
	 */
	@AfterReturning(value = "execution(* org.genesys2.server.service.GenesysService.save*(..))", returning = "result")
	public Object afterSave(final JoinPoint joinPoint, final Object result) throws Throwable {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Returning from " + joinPoint.toLongString());
		}
		if (result != null) {
			if (LOG.isDebugEnabled()) {
				LOG.info("Result " + result.getClass());
			}
			if (result instanceof Collection<?>) {
				Collection<?> list = (Collection<?>) result;
				if (list.isEmpty())
					return result;

				for (Object o : list) {
					if (LOG.isDebugEnabled()) {
						LOG.debug("Indexing object " + o);
					}
					if (o == null)
						continue;
					if (o instanceof Accession) {
						elasticUpdater.update(Accession.class, ((Accession) o).getId());
					}
					if (o instanceof AccessionRelated) {
						elasticUpdater.update(Accession.class, ((AccessionRelated) o).getAccession().getId());
					}
					if (o instanceof SvalbardData) {
						elasticUpdater.update(Accession.class, ((SvalbardData) o).getId());
					}
				}

			}
		}
		return result;
	}

	/**
	 * Handle removal of accession related objects
	 * 
	 * @param joinPoint
	 * @param result
	 * @return
	 * @throws Throwable
	 */
	@AfterReturning(value = "execution(* org.genesys2.server.service.GenesysService.remove*(..))", returning = "result")
	public Object afterRemove(final JoinPoint joinPoint, final Object result) throws Throwable {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Returning from " + joinPoint.toLongString());
			LOG.debug("Result " + result);
		}
		if (result instanceof Collection<?>) {
			Collection<?> list = (Collection<?>) result;
			if (list.isEmpty())
				return result;

			for (Object o : list) {
				if (o == null)
					continue;
				// if (o instanceof Accession) {
				// elasticUpdater.remove(Accession.class, ((Accession)
				// o).getId());
				// }
				if (o instanceof AccessionRelated) {
					elasticUpdater.update(Accession.class, ((AccessionRelated) o).getAccession().getId());
				}
				if (o instanceof SvalbardData) {
					elasticUpdater.update(Accession.class, ((SvalbardData) o).getId());
				}
			}

		}
		return result;
	}

	/**
	 * Handle accession removal
	 * 
	 * @param joinPoint
	 * @param institute
	 * @param accessions
	 * @return
	 * @throws Throwable
	 */
	@Around(value = "execution(* org.genesys2.server.service.GenesysService.removeAccessions(..)) && args(institute, accessions)")
	public Object aroundRemoveAccession(final ProceedingJoinPoint joinPoint, final FaoInstitute institute, final Collection<Accession> accessions)
			throws Throwable {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Returning from " + joinPoint.toLongString());
		}

		Collection<Long> accessionIds = new ArrayList<Long>(accessions.size());
		for (Accession a : accessions) {
			if (a != null && a.getId() != null)
				accessionIds.add(a.getId());
		}
		// Need to load AccessionDetails before deleting
		List<AccessionDetails> deletedAccessions = genesysService.getAccessionDetails(accessionIds);

		try {
			Object res = joinPoint.proceed();
			// This will not trigger if an exception occurs
			elasticUpdater.archive(deletedAccessions);

			// Delete all
			for (AccessionDetails accn : deletedAccessions) {
				if (accn == null)
					continue;

				elasticUpdater.remove(Accession.class, accn.getId());
			}

			return res;
		} finally {

		}
	}

	/**
	 * Updates to CropTaxonomy trigger reindexing of accessions
	 */
	@AfterReturning(value = "execution(* org.genesys2.server.persistence.domain.CropTaxonomyRepository.save(..))", returning = "result")
	public Object afterCropTaxonomySave(final JoinPoint joinPoint, final Object result) throws Throwable {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Returning from " + joinPoint.toLongString());
			LOG.debug("Result " + result);
		}

		// Find all accessions with taxonomies in the list
		if (result instanceof CropTaxonomy) {
			CropTaxonomy ct = (CropTaxonomy) result;
			elasticUpdater.updateAll(Accession.class, genesysService.listAccessionsIds(ct.getTaxonomy()).toArray(ArrayUtils.EMPTY_LONG_OBJECT_ARRAY));

		} else if (result instanceof Collection<?>) {
			Collection<CropTaxonomy> list = (Collection<CropTaxonomy>) result;
			if (list.isEmpty())
				return result;

			for (CropTaxonomy ct : list) {
				if (ct == null)
					continue;
				elasticUpdater.updateAll(Accession.class, genesysService.listAccessionsIds(ct.getTaxonomy()).toArray(ArrayUtils.EMPTY_LONG_OBJECT_ARRAY));
			}
		}

		return result;
	}

	/**
	 * Updates to CropTaxonomy trigger reindexing of accessions
	 */
	@Around(value = "execution(* org.genesys2.server.persistence.domain.CropTaxonomyRepository.delete(..)) && args(cropTaxonomies)")
	public Object aroundCropTaxonomyDelete(final ProceedingJoinPoint joinPoint, final List<CropTaxonomy> cropTaxonomies) throws Throwable {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Around " + joinPoint.toLongString());
		}

		Set<Long> accessionsWithCT = new HashSet<Long>();

		// Find all accessions with taxonomies in the list
		if (cropTaxonomies != null && !cropTaxonomies.isEmpty()) {
			for (CropTaxonomy ct : cropTaxonomies) {
				if (ct == null)
					continue;
				accessionsWithCT.addAll(genesysService.listAccessionsIds(ct.getTaxonomy()));
			}
		}

		try {
			Object res = joinPoint.proceed();

			// Update accessions that had the Taxonomy before deletion
			elasticUpdater.updateAll(Accession.class, accessionsWithCT.toArray(ArrayUtils.EMPTY_LONG_OBJECT_ARRAY));

			return res;
		} finally {

		}
	}

	/**
	 * Handle updates to Article
	 *
	 * @param joinPoint
	 * @param result
	 * @return
	 * @throws Throwable
	 */
	@AfterReturning(value = "execution(* org.genesys2.server.persistence.domain.ArticleRepository.save*(..))", returning = "result")
	public Object afterSaveArticle(final JoinPoint joinPoint, final Object result) throws Throwable {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Returning from " + joinPoint.toLongString());
		}
		if (result != null) {
			if (LOG.isDebugEnabled()) {
				LOG.info("Result " + result.getClass());
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("Indexing object " + result);
			}
			elasticUpdater.update(Article.class, ((Article) result).getId());
		}
		return result;
	}

	/**
	 * Handle updates to ActivityPost
	 *
	 * @param joinPoint
	 * @param result
	 * @return
	 * @throws Throwable
	 */
	@AfterReturning(value = "execution(* org.genesys2.server.persistence.domain.ActivityPostRepository.save*(..))", returning = "result")
	public Object afterSavePost(final JoinPoint joinPoint, final Object result) throws Throwable {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Returning from " + joinPoint.toLongString());
		}
		if (result != null) {
			if (LOG.isDebugEnabled()) {
				LOG.info("Result " + result.getClass());
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("Indexing object " + result);
			}
			elasticUpdater.update(ActivityPost.class, ((ActivityPost) result).getId());
		}
		return result;
	}

	/**
	 * Handle updates to Country
	 *
	 * @param joinPoint
	 * @param result
	 * @return
	 * @throws Throwable
	 */
	@AfterReturning(value = "execution(* org.genesys2.server.persistence.domain.CountryRepository.save*(..))", returning = "result")
	public Object afterSaveCountry(final JoinPoint joinPoint, final Object result) throws Throwable {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Returning from " + joinPoint.toLongString());
		}
		if (result != null) {
			if (LOG.isDebugEnabled()) {
				LOG.info("Result " + result.getClass());
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("Indexing object " + result);
			}
			elasticUpdater.update(Country.class, ((Country) result).getId());
		}
		return result;
	}

	/**
	 * Handle updates to FaoInstitute
	 *
	 * @param joinPoint
	 * @param result
	 * @return
	 * @throws Throwable
	 */
	@AfterReturning(value = "execution(* org.genesys2.server.persistence.domain.FaoInstituteRepository.save*(..))", returning = "result")
	public Object afterSaveInstitute(final JoinPoint joinPoint, final Object result) throws Throwable {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Returning from " + joinPoint.toLongString());
		}
		if (result != null) {
			if (LOG.isDebugEnabled()) {
				LOG.info("Result " + result.getClass());
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("Indexing object " + result);
			}
			elasticUpdater.update(FaoInstitute.class, ((FaoInstitute) result).getId());
		}
		return result;
	}

	/**
	 * Handle removal of Article
	 *
	 * @param joinPoint
	 * @return
	 * @throws Throwable
	 */
	@Before(value = "execution(* org.genesys2.server.persistence.domain.ArticleRepository.delete*(..))")
	public void beforeRemoveArticle(final JoinPoint joinPoint) throws Throwable {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Returning from " + joinPoint.toLongString());
			LOG.debug("Id=" + (long) joinPoint.getArgs()[0]);
		}
		elasticUpdater.remove(Article.class, (Long) joinPoint.getArgs()[0]);
	}

	/**
	 * Handle removal of ActivityPost
	 *
	 * @param joinPoint
	 * @return
	 * @throws Throwable
	 */
	@Before(value = "execution(* org.genesys2.server.persistence.domain.ActivityPostRepository.delete*(..))")
	public void beforeRemoveActivityPost(final JoinPoint joinPoint) throws Throwable {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Returning from " + joinPoint.toLongString());
			LOG.debug("Id=" + (long) joinPoint.getArgs()[0]);
		}
		elasticUpdater.remove(FullTextDocument.class, (Long) joinPoint.getArgs()[0]);
	}

	/**
	 * Handle removal of Country
	 *
	 * @param joinPoint
	 * @return
	 * @throws Throwable
	 */
	@Before(value = "execution(* org.genesys2.server.persistence.domain.CountryRepository.delete*(..))")
	public void beforeRemoveCountry(final JoinPoint joinPoint) throws Throwable {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Returning from " + joinPoint.toLongString());
			LOG.debug("Id=" + (long) joinPoint.getArgs()[0]);
		}
		elasticUpdater.remove(Country.class, (Long) joinPoint.getArgs()[0]);
	}

	/**
	 * Handle removal of FaoInstitute
	 *
	 * @param joinPoint
	 * @return
	 * @throws Throwable
	 */
	@Before(value = "execution(* org.genesys2.server.persistence.domain.FaoInstituteRepository.delete*(..))")
	public void beforeRemoveFaoInstitute(final JoinPoint joinPoint) throws Throwable {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Returning from " + joinPoint.toLongString());
			LOG.debug("Id=" + (long) joinPoint.getArgs()[0]);
		}
		elasticUpdater.remove(FaoInstitute.class, (Long) joinPoint.getArgs()[0]);
	}
}
