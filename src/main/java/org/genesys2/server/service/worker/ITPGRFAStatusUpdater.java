/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.worker;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.StopWatch;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.genesys2.server.model.impl.Country;
import org.genesys2.server.model.impl.ITPGRFAStatus;
import org.genesys2.server.service.GeoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import com.opencsv.CSVReader;

/**
 * Update country ITPGRFA status by fetching data from
 * {@link ITPGRFAStatusUpdater#ITPGRFA_STATUS_URL}.
 *
 * @author Matija Obreza
 */
@Component
public class ITPGRFAStatusUpdater {

	public static String ITPGRFA_STATUS_URL = "http://www.planttreaty.org/list_countries/csv/countries.csv";

	public static final Log LOG = LogFactory.getLog(ITPGRFAStatusUpdater.class);

	@Autowired
	private GeoService geoService;

	@Autowired
	private TaskExecutor taskExecutor;

	@Autowired
	private HttpClientBuilder httpClientBuilder;

	private static final int BATCH_SIZE = 20;

	private static final String[] CSV_HEADERS = { "Country ISO CODE", "Country", "FAO Region 1", "FAO Region 2", "CP", "Name of NFP", "Membership", "by",
			"Income", "Development" };

	protected static final int COLUMN_ISOCODE3 = 0;
	protected static final int COLUMN_CONTRACTING_PARTY = 4;
	protected static final int COLUMN_NAME_OF_NFP = 5;
	protected static final int COLUMN_MEMBERSHIP = 6;
	protected static final int COLUMN_MEMBERSHIP_BY = 7;

	/**
	 * Update local {@link ITPGRFAStatus} entries with data from CSV
	 *
	 * @throws IOException
	 */
	public void downloadAndUpdate() throws IOException {
		InputStream itpgrfaCSVStream = null;

		final HttpGet httpget = new HttpGet(ITPGRFA_STATUS_URL);
		HttpResponse response = null;
		final CloseableHttpClient httpclient = httpClientBuilder.build();
		try {
			response = httpclient.execute(httpget);

			// Get hold of the response entity
			final HttpEntity entity = response.getEntity();
			if (entity == null) {
				LOG.warn("No HttpEntity in response, bailing out");
				return;
			}
			LOG.debug(entity.getContentType() + " " + entity.getContentLength());

			// If the response does not enclose an entity, there is no
			// need to bother about connection release
			if (entity != null) {
				itpgrfaCSVStream = new BufferedInputStream(entity.getContent());
			}

			updateFromStream(itpgrfaCSVStream);
		
		} catch (final ClientProtocolException e) {
			LOG.error(e.getMessage(), e);
			throw new IOException(e);
		} catch (final IOException e) {
			LOG.error(e);
			throw e;
		} finally {
			IOUtils.closeQuietly(itpgrfaCSVStream);
			IOUtils.closeQuietly(httpclient);
		}
	}

	private void updateFromStream(InputStream instream) throws IOException {
		final CSVReader reader = new CSVReader(new BufferedReader(new InputStreamReader(instream)), ',', '"', '\\', 0, false, true);

		try {
			final List<String[]> batch = new ArrayList<String[]>(BATCH_SIZE);

			// Get headers
			String[] line = reader.readNext();
			LOG.warn("Got headers: " + ArrayUtils.toString(line));
			if (CSV_HEADERS.length > line.length) {
				reader.close();
				throw new IOException("CSV header count mismatch. Found: " + ArrayUtils.toString(line));
			}

			for (int i = CSV_HEADERS.length - 1; i >= 0; i--) {
				if (!line[i].equals(CSV_HEADERS[i])) {
					reader.close();
					throw new IOException("CSV header mismatch, found '" + line[i] + "' instead of '" + CSV_HEADERS[i] + "'");
				}
			}

			// Timer
			final StopWatch stopWatch = new StopWatch();
			stopWatch.start();

			while ((line = reader.readNext()) != null) {
				for (int i = 0; i < line.length; i++) {
					if (line[i].equals("null") || StringUtils.isBlank(line[i])) {
						line[i] = null;
					}
				}

				if (line[COLUMN_ISOCODE3] == null) {
					LOG.warn("Country ISO CODE 3 is null, skipping line" + ArrayUtils.toString(line, "NULL"));
					continue;
				}

				if (LOG.isDebugEnabled()) {
					LOG.debug(">>> " + ArrayUtils.toString(line, "NULL"));
				}

				batch.add(line);

				if (batch.size() >= BATCH_SIZE) {
					workIt(batch);
					batch.clear();
				}
			}

			if (batch.size() > 0) {
				LOG.debug("Have items in the batch after loop.");
				workIt(batch);
				batch.clear();
			}

			stopWatch.stop();
			LOG.info("Done importing ITPGRFA status in " + stopWatch.getTime() + "ms");

		} finally {
			IOUtils.closeQuietly(reader);
		}

	}

	private void workIt(final List<String[]> batch) {

		// Need copy!
		final List<String[]> batchCopy = new ArrayList<String[]>(batch);

		taskExecutor.execute(new Runnable() {
			@Override
			public void run() {
				for (final String[] line : batchCopy) {
					if (LOG.isDebugEnabled()) {
						LOG.debug("Working on " + ArrayUtils.toString(line, "NULL"));
					}
					updateCountry(line[COLUMN_ISOCODE3], line[COLUMN_CONTRACTING_PARTY], line[COLUMN_NAME_OF_NFP], line[COLUMN_MEMBERSHIP],
							line[COLUMN_MEMBERSHIP_BY]);
				}
			}
		});
	}

	protected void updateCountry(String countryIso3, String contractingParty, String nameOfNFP, String membership, String membershipBy) {
		final Country country = geoService.getCountry(countryIso3);

		if (country == null) {
			LOG.error("No country with name=" + countryIso3);
			return;
		}

		geoService.updateITPGRFA(country, contractingParty, membership, membershipBy, nameOfNFP);
	}
}
