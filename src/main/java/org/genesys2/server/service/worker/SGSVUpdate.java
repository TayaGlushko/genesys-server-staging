/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.worker;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.genesys.SvalbardData;
import org.genesys2.server.persistence.domain.SvalbardRepository;
import org.genesys2.server.service.GenesysService;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.orm.hibernate3.HibernateOptimisticLockingFailureException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import com.opencsv.CSVReader;

@Component
public class SGSVUpdate {
	private static final String SGSV_DOWNLOAD_URL = "http://www.nordgen.org/sgsv/download.php?file=/scope/sgsv/files/sgsv_templates.tab";

	static final String[] SGSV_HEADERS = { "sgsv_id", "institute_code", "deposit_box_number", "collection_name", "accession_number", "full_scientific_name",
			"country_of_collection_or_source", "number_of_seeds", "regeneration_month_and_year", "other_accession_designations", "provider_institute_code",
			"accession_url", "country_code", "country_name", "continent_name", "seeds", "genus", "species_epithet", "species", "taxon_name", "date_of_deposit",
			"date_of_dataset", "sgsv_template_id", "box_id", "sgsv_taxon_id", "taxon_authority", "infraspesific_epithet", "vernacular_name", "itis_tsn",
			"sgsv_genus_id", "accession_name" };

	public static final Log LOG = LogFactory.getLog(SGSVUpdate.class);
	private static final int BATCH_SIZE = 50;

	@Autowired
	private HttpClientBuilder httpClientBuilder;

	@Autowired
	private TaskExecutor taskExecutor;

	@Autowired
	private GenesysService genesysService;

	@Autowired
	private SvalbardRepository svalbardRepository;

	@PreAuthorize("hasRole('ADMINISTRATOR')")
	public void updateSGSV() {
		LOG.warn("Importing SGSV data from " + SGSV_DOWNLOAD_URL);

		final HttpGet httpget = new HttpGet(SGSV_DOWNLOAD_URL);
		HttpResponse response = null;
		final CloseableHttpClient httpclient = httpClientBuilder.build();
		try {
			response = httpclient.execute(httpget);

			LOG.debug(response.getStatusLine());

			// Get hold of the response entity
			final HttpEntity entity = response.getEntity();

			for (final Header header : response.getAllHeaders()) {
				LOG.debug(header);
			}

			LOG.debug(entity.getContentType() + " " + entity.getContentLength());

			importSGSVStream(entity.getContent(), SGSV_DOWNLOAD_URL);
		} catch (final Throwable e) {
			LOG.error(e, e);
		} finally {
			IOUtils.closeQuietly(httpclient);
		}
	}

	void importSGSVStream(final InputStream str, final String source) throws IOException {

		int counter = 0;
		CSVReader reader = null;
		try {
			reader = new CSVReader(new BufferedReader(new InputStreamReader(str)), '\t', '"', false);

			final String[] headers = reader.readNext();
			if (!validHeaders(headers)) {
				return;
			}

			final List<String[]> bulk = new ArrayList<String[]>(BATCH_SIZE);

			String[] line = null;
			while ((line = reader.readNext()) != null) {
				if (counter % 1000 == 0) {
					LOG.info(counter + ": " + ArrayUtils.toString(line));
				}

				// Clean up
				for (int i = 0; i < line.length; i++) {
					line[i] = line[i].trim();
					if (line[i].equals("null") || line[i].equals("<null>") || StringUtils.isBlank(line[i])) {
						line[i] = null;
					}
				}

				if (line.length > SGSV_HEADERS.length) {
					LOG.warn("Funny line length " + Arrays.toString(line));
				}

				bulk.add(line);

				counter++;

				if (counter % BATCH_SIZE == 0) {
					workIt(bulk);
					bulk.clear();
				}
			}

			workIt(bulk);
			bulk.clear();

		} catch (final Throwable e) {
			LOG.error(e.getMessage(), e);
			throw new IOException(e);
		} finally {
			IOUtils.closeQuietly(reader);
		}

		LOG.info("Done importing SGSV data. Imported: " + counter);
	}

	boolean validHeaders(String[] headers) {
		if (headers == null) {
			LOG.warn("null headers received");
			return false;
		}
		LOG.debug("Headers: " + headers.length);
		if (headers.length != SGSV_HEADERS.length) {
			LOG.warn("Expected 30 headers, got " + headers.length);
			return false;
		}
		for (int i = 0; i < SGSV_HEADERS.length; i++) {
			if (!StringUtils.equals(headers[i], SGSV_HEADERS[i])) {
				LOG.warn("SGSV template header mismatch pos=" + i + " expected=" + SGSV_HEADERS[i] + " found=" + headers[i]);
				return false;
			}
		}
		return true;
	}

	void workIt(final List<String[]> bulk) {
		// Need copy!
		final ArrayList<String[]> bulkCopy = new ArrayList<String[]>(bulk);
		LOG.trace("Queueing job size=" + bulkCopy.size());

		taskExecutor.execute(new Runnable() {

			@Override
			public void run() {
				final List<SGSVEntry> accns = bulkToList(bulkCopy);
				if (LOG.isTraceEnabled())
					LOG.trace("Got SGVS entries size=" + accns.size());

				for (int retry = 5; retry > 0; retry--) {
					if (retry < 5) {
						LOG.warn("Persistence attempt " + (5 - retry));
					}

					try {
						updateSvalbards(accns);

						// EXIT
						return;

					} catch (final HibernateOptimisticLockingFailureException e) {
						LOG.warn("Failed to save data, will retry. " + e.getMessage());
						continue;
					}
				}

				LOG.warn("Persistence retries failed!");
			}
		});

		bulk.clear();
	}

	List<SGSVEntry> bulkToList(ArrayList<String[]> bulkCopy) {
		final List<SGSVEntry> entries = new ArrayList<SGSVEntry>(bulkCopy.size());
		// Extract INSTCODE and ACCENUMB
		for (final String[] entry : bulkCopy) {
			try {
				entries.add(new SGSVEntry(entry));
			} catch (ArrayIndexOutOfBoundsException | NumberFormatException e) {
				LOG.warn("Invalid entry: " + ArrayUtils.toString(entry, "NULL"));
			}
		}
		return entries;
	}

	void updateSvalbards(List<SGSVEntry> accns) {
		// LOG.info("Updating SGSV size="+ accns.size());

		// Find accessions based on holdingInstitute and
		// accession
		// name
		final List<Accession> accessions = genesysService.listAccessionsSGSV(accns);

		// Must be same order
		if (accessions.size() != accns.size()) {
			LOG.error("Accession lists don't match");
			return;
		}

		// Check if they match (Genus, OrigCty) for now
		final List<Accession> matching = new ArrayList<Accession>(accessions.size());
		final List<SvalbardData> svalbards = new ArrayList<SvalbardData>(accessions.size());

		for (int i = accns.size() - 1; i >= 0; i--) {
			boolean updateAccession = false;
			final Accession accn = accessions.get(i);
			final SGSVEntry entry = accns.get(i);

			if (accn == null) {
				// LOG.info("No such accession in the database. Skipping " +
				// entry);
				continue;
			} else {
				if (LOG.isTraceEnabled())
					LOG.trace("Updating " + accn);
			}

			// NOTE Removed acceNumb check because of aliases
			// if (accn.getAccessionName().equalsIgnoreCase(entry.acceNumb) &&
			// accn.getInstituteCode().equals(entry.instCode)) {

			{
				if (!Boolean.TRUE.equals(accn.getInSvalbard())) {
					accn.setInSvalbard(true);
					updateAccession = true;
				}

				SvalbardData svalbardData = svalbardRepository.findOne(accn.getId());
				if (svalbardData == null) {
					svalbardData = new SvalbardData();
				}
				svalbardData.setId(accn.getId());
				svalbardData.setBoxNumber(entry.boxNo);
				svalbardData.setDepositDate(entry.depositDate);
				svalbardData.setQuantity(entry.quantity);
				svalbardData.setTaxonomy(entry.fullTaxa);
				svalbardData.setUnitId(entry.unitId);
				if (StringUtils.isNotBlank(entry.acceUrl)) {
					svalbardData.setAcceUrl(entry.acceUrl);
				}

				svalbards.add(svalbardData);
				if (updateAccession) {
					matching.add(accn);
				}
			}

		}

		// Save data
		if (matching.size() > 0) {
			LOG.info("Setting inSGSV=true for accessions size=" + matching.size());
			genesysService.setInSvalbard(matching);
		}
		if (svalbards.size() > 0) {
			try {
				LOG.info("Saving svalbards size=" + svalbards.size());
				genesysService.saveSvalbards(svalbards);
			} catch (final ConstraintViolationException e) {
				LOG.warn(e.getMessage());
				for (final Accession a : matching) {
					LOG.warn("\t" + a);
				}
			}
		}
	}
}
