/**
 * Copyright 2015 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.worker;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.MappedByteBuffer;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.genesys.worldclim.WorldClimUtil;
import org.genesys.worldclim.grid.generic.GenericGridFile;
import org.genesys.worldclim.grid.generic.GenericGridZipFile;
import org.genesys.worldclim.grid.generic.Header;
import org.genesys2.server.aspect.AsAdmin;
import org.genesys2.server.model.dataset.DS;
import org.genesys2.server.model.dataset.DSDescriptor;
import org.genesys2.server.model.dataset.DSQualifier;
import org.genesys2.server.model.genesys.AccessionGeo;
import org.genesys2.server.model.impl.Descriptor;
import org.genesys2.server.persistence.domain.AccessionGeoRepository;
import org.genesys2.server.persistence.domain.GenesysLowlevelRepository;
import org.genesys2.server.service.DSService;
import org.genesys2.server.service.DescriptorService;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilters;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Download data from WorldClim.org and update local cache.
 * {@link WorldClimUpdaterImpl#WORLDCLIM_ORG}.
 *
 * @author Matija Obreza
 */
@Component
public class WorldClimUpdater implements InitializingBean {
	public static final Log LOG = LogFactory.getLog(WorldClimUpdater.class);

	public static final UUID WORLDCLIM_DATASET = UUID.fromString("BC84433B-A626-4BDF-97D3-DB36D79499C6");

	public static String WORLDCLIM_ORG = "http://www.worldclim.org";
	public static String WORLDCLIM_ORG_FILEBASE = "http://biogeo.ucdavis.edu/data/climate/worldclim/1_4/grid/cur/";

	// http://biogeo.ucdavis.edu/data/climate/worldclim/1_4/grid/cur/tmin_2-5m_bil.zip
	// http://biogeo.ucdavis.edu/data/climate/worldclim/1_4/grid/cur/tmax_2-5m_bil.zip
	// http://biogeo.ucdavis.edu/data/climate/worldclim/1_4/grid/cur/tmean_2-5m_bil.zip
	// http://biogeo.ucdavis.edu/data/climate/worldclim/1_4/grid/cur/prec_2-5m_bil.zip
	// http://biogeo.ucdavis.edu/data/climate/worldclim/1_4/grid/cur/bio_2-5m_bil.zip
	// http://biogeo.ucdavis.edu/data/climate/worldclim/1_4/grid/cur/alt_2-5m_bil.zip

	@Autowired
	private HttpClientBuilder httpClientBuilder;

	@Value("${download.files.dir}")
	private String downloadDir;

	private File worldClimDir;

	@Autowired
	private DSService dsService;

	@Autowired
	private DescriptorService descriptorService;

	@Autowired
    @Qualifier("genesysLowlevelRepositoryCustomImpl")
	private GenesysLowlevelRepository genesysLowlevel;

	@Autowired
	private AccessionGeoRepository accessionGeoRepository;

	@Override
	public void afterPropertiesSet() throws Exception {
		this.worldClimDir = new File(downloadDir, "worldclim");
		this.worldClimDir.mkdirs();
	}

	@AsAdmin
	public void downloadAll() {
		try {
			downloadAndExtract(worldClimDir, "alt_2-5m_bil.zip");
			downloadAndExtract(worldClimDir, "prec_2-5m_bil.zip");
			downloadAndExtract(worldClimDir, "tmin_2-5m_bil.zip");
			downloadAndExtract(worldClimDir, "tmax_2-5m_bil.zip");
			downloadAndExtract(worldClimDir, "tmean_2-5m_bil.zip");
			downloadAndExtract(worldClimDir, "bio_2-5m_bil.zip");
		} catch (IOException e) {
			LOG.error("Failed to download and import", e);
		}
	}

	/**
	 * Ensure that local copy of WorldClim data exists and then update
	 * 'variableName' for all accessions with lat/lng
	 * 
	 * @param variableName
	 * @throws IOException
	 */
	public void update(String variableName) throws IOException {
		if (!haveData(variableName)) {
			LOG.warn("Missing .bil and .hdr files for " + variableName);
			downloadAndExtract(worldClimDir, getFileName(variableName));
		}

		DS dataset = dsService.loadDatasetByUuid(WORLDCLIM_DATASET);
		if (dataset == null) {
			// Ensure dataset
			dataset = createWorldClimDataset();
			Descriptor foo = descriptorService.getDescriptor("tileIndex");
			if (foo == null) {
				foo = createDescriptor(dataset, "tileIndex");
			}
			dsService.addQualifier(dataset, foo);
		} else {
		}

		Descriptor descriptor = descriptorService.getDescriptor(variableName);
		DSDescriptor worldclimDescriptor = null;
		if (descriptor == null) {
			descriptor = createDescriptor(dataset, variableName);
			worldclimDescriptor = dsService.addDescriptor(dataset, descriptor);
			LOG.info("Created worldclimDescriptor " + variableName);
		} else {
			for (DSDescriptor dsd : dataset.getDescriptors()) {
				if (dsd.getDescriptor().getCode().equals(variableName)) {
					LOG.debug("Found worldclimDescriptor " + variableName);
					worldclimDescriptor = dsd;
					break;
				}
			}
			if (worldclimDescriptor == null)
				worldclimDescriptor = dsService.addDescriptor(dataset, descriptor);
		}

		LOG.info("Using worldClim " + worldclimDescriptor);

		GenericGridFile ggf = new GenericGridFile(worldClimDir, variableName);
		Header header = ggf.readHeader();

		short nullValue = (short) header.getNoDataValue();
		MappedByteBuffer buffer = ggf.mapDataBuffer();

		double factor = 1.0;
		if ("bio3".equals(variableName)) {
			factor = .01f;
		} else if ("bio4".equals(variableName)) {
			factor = .01f;
		} else if (variableName.matches("(tmean|tmax|tmin)\\d{1,2}")) {
			factor = .1f;
		}

		Set<Long> tileIndexSet = getExistingTileIndexes();
		if (tileIndexSet.isEmpty())
			tileIndexSet = generateTileIndexes();
		
		List<Long> tileIndexes = new ArrayList<Long>(tileIndexSet);
		int batchSize = 1000;
		for (int fromIndex = 0; fromIndex < tileIndexes.size(); fromIndex += batchSize) {
			HashSet<Long> ids = new HashSet<Long>(tileIndexes.subList(fromIndex, Math.min(fromIndex + batchSize, tileIndexes.size())));
			LOG.info("Processing tileIndexes: " + fromIndex + " of " + tileIndexes.size());
			dsService.worldclimUpdate(dataset, worldclimDescriptor, ids, buffer, nullValue, factor);
		}
		
		LOG.info("Done processing " + variableName);
	}

	private Set<Long> generateTileIndexes() throws JsonParseException, JsonMappingException, IOException {
		// read accessions with lat/lng and update in batch
		final List<Long> accessionIds = new ArrayList<Long>(20000);
		ObjectMapper mapper = new ObjectMapper();
		AppliedFilters filters = mapper.readValue("{\"-geo.longitude\":[null]}", AppliedFilters.class);

		genesysLowlevel.listAccessionIds(filters, null, new RowCallbackHandler() {
			@Override
			public void processRow(ResultSet rs) throws SQLException {
				accessionIds.add(rs.getLong(1));
			}
		});

		LOG.info("Retrieved accession ids for filter " + filters.toString() + ": " + accessionIds.size());

		final Set<Long> tileIndexSet = new HashSet<Long>(20000);

		int batchSize = 50;

		for (int fromIndex = 0; fromIndex < accessionIds.size(); fromIndex += batchSize) {
			HashSet<Long> ids = new HashSet<Long>(accessionIds.subList(fromIndex, Math.min(fromIndex + batchSize, accessionIds.size())));
			LOG.info("Processing position: " + fromIndex + " of " + accessionIds.size());
			List<AccessionGeo> acGeo = accessionGeoRepository.findForAccessions(ids);
			List<AccessionGeo> toSave = new ArrayList<AccessionGeo>();
			for (AccessionGeo geo : acGeo) {
				Double lat = geo.getLatitude();
				Double lon = geo.getLongitude();

				if (lat != null && lon != null) {
					Long tileIndex = WorldClimUtil.getTileIndex(5, lon, lat);
					if (tileIndex != null) {
						tileIndexSet.add(tileIndex);
						if (geo.getTileIndex() == null) {
							geo.setTileIndex(tileIndex);
							toSave.add(geo);
						}
					}
				}
			}
			dsService.updateAccessionTileIndex(toSave);
		}
		return tileIndexSet;
	}

	private Set<Long> getExistingTileIndexes() {
		final Set<Long> tileIndexSet = new HashSet<Long>(20000);

		tileIndexSet.addAll(accessionGeoRepository.getTileIndexes());
		return tileIndexSet;
	}

	private Descriptor createDescriptor(DS dataset, String variableName) {
		Descriptor descriptor = new Descriptor();
		descriptor.setCode(variableName);
		descriptor.setTitle("WorldClim " + variableName);
		descriptorService.saveDescriptor(descriptor);
		LOG.info("Created descriptor " + descriptor);
		return descriptor;
	}

	private DS createWorldClimDataset() {
		DS dataset = new DS();
		// dataset.setDescription("Genesys accessions linked with worldclim.org data. Maintained automatically by Genesys");
		// dataset.setName("WorldClim.org Data");
		// dataset.setSource("worldclim.org + genesys-pgr.org");
		// dataset.setUploadDate(new Date());
		dataset.setUuid(WORLDCLIM_DATASET);
		dataset.setQualifiers(new ArrayList<DSQualifier>());
		dataset.setDescriptors(new ArrayList<DSDescriptor>());
		dsService.saveDataset(dataset);
		LOG.info("Created dataset " + WORLDCLIM_DATASET);
		return dataset;
	}

	// TODO Move to worldclim!
	private String getFileName(String variableName) throws FileNotFoundException {
		if ("alt".equalsIgnoreCase(variableName)) {
			return "alt_2-5m_bil.zip";
		} else if (variableName.matches("prec\\d{1,2}")) {
			return "prec_2-5m_bil.zip";
		} else if (variableName.matches("tmax\\d{1,2}")) {
			return "tmax_2-5m_bil.zip";
		} else if (variableName.matches("tmin\\d{1,2}")) {
			return "tmin_2-5m_bil.zip";
		} else if (variableName.matches("tmean\\d{1,2}")) {
			return "tmean_2-5m_bil.zip";
		} else if (variableName.matches("bio\\d{1,2}")) {
			return "bio_2-5m_bil.zip";
		} else
			throw new FileNotFoundException("No worldclim file for " + variableName);
	}

	private boolean haveData(String variableName) {
		GenericGridFile ggf = new GenericGridFile(worldClimDir, variableName);
		return ggf.getHeaderFile().exists() && ggf.getDataFile().exists();
	}

	/**
	 * Download WorldClim file to a temporary file
	 * 
	 * @param fileName
	 * @return resulting file
	 * @throws IOException
	 */
	public File download(String fileName) throws IOException {
		InputStream istream = null;
		OutputStream ostream = null;

		File tempFile = File.createTempFile("worldclim", ".zip");

		final CloseableHttpClient httpclient = httpClientBuilder.build();
		final HttpGet httpget = new HttpGet(WORLDCLIM_ORG_FILEBASE + fileName);
		HttpResponse response = null;
		try {
			response = httpclient.execute(httpget);

			// Get hold of the response entity
			final HttpEntity entity = response.getEntity();
			LOG.debug(entity.getContentType() + " " + entity.getContentLength());

			istream = new BufferedInputStream(entity.getContent());
			ostream = new BufferedOutputStream(new FileOutputStream(tempFile));
			LOG.info("Downloading " + fileName + " " + entity.getContentLength());
			IOUtils.copy(istream, ostream);
			ostream.flush();
			LOG.info("Retrieved " + fileName);

		} catch (final ClientProtocolException e) {
			LOG.error(e.getMessage(), e);
			tempFile.delete();
			throw new IOException(e);
		} finally {
			IOUtils.closeQuietly(istream);
			IOUtils.closeQuietly(ostream);
			IOUtils.closeQuietly(httpclient);
		}

		return tempFile;
	}

	/**
	 * Download WorldClim file and extract to destination folder.
	 * 
	 * @param destination
	 * @param fileName
	 * @return
	 * @throws IOException
	 */
	public List<File> downloadAndExtract(File destination, String fileName) throws IOException {
		File tempFile = download(fileName);
		try {
			List<File> files = GenericGridZipFile.unzip(tempFile, destination);
			for (File file : files) {
				LOG.info("Extracted " + file.getCanonicalPath());
			}
			return files;
		} finally {
			tempFile.delete();
		}
	}

}
