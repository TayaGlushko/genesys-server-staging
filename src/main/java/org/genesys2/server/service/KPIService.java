/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.genesys2.server.model.kpi.Dimension;
import org.genesys2.server.model.kpi.Execution;
import org.genesys2.server.model.kpi.ExecutionRun;
import org.genesys2.server.model.kpi.KPIParameter;
import org.genesys2.server.model.kpi.Observation;
import org.springframework.data.domain.Pageable;

public interface KPIService {

	long getSingleResult(String paQuery, Object... params);

	KPIParameter save(KPIParameter parameter);

	KPIParameter getParameter(String name);

	KPIParameter getParameter(long id);

	KPIParameter delete(KPIParameter parameter);

	Dimension<?> save(Dimension<?> dimension);

	Dimension<?> getDimension(long id);

	Dimension<?> delete(Dimension<?> dimension);

	Set<?> getValues(Dimension<?> loadedJpa);

	Execution save(Execution execution);

	Execution getExecution(long id);
	Execution getExecution(String executionName);
	
	Execution delete(Execution execution);

	List<Observation> execute(Execution execution);

	List<Observation> save(Execution execution, List<Observation> observations);

	List<KPIParameter> listParameters();

	List<Dimension<?>> listDimensions();

	List<Execution> listExecutions();

	List<Observation> listObservations(ExecutionRun executionRun, Map<String, String> dimensionFilters, Pageable page);

	ExecutionRun findLastExecutionRun(Execution execution);

	void deleteObservations(ExecutionRun executionRun);

	List<ExecutionRun> listExecutionRuns(Execution execution, Pageable pageable);

	ExecutionRun getExecutionRun(long runId);

	List<Observation> listObservations(Execution execution, long dimensionKeyId, Pageable pageable);

}
