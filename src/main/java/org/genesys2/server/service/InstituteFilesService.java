/**
 * Copyright 2016 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service;

import java.io.IOException;
import java.util.UUID;

import org.genesys2.server.filerepository.InvalidRepositoryFileDataException;
import org.genesys2.server.filerepository.InvalidRepositoryPathException;
import org.genesys2.server.filerepository.NoSuchRepositoryFileException;
import org.genesys2.server.filerepository.model.ImageGallery;
import org.genesys2.server.filerepository.model.RepositoryImage;
import org.genesys2.server.filerepository.model.RepositoryImageData;
import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.impl.FaoInstitute;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Handle file repository operations for {@link FaoInstitute}.
 */
public interface InstituteFilesService {

	ImageGallery loadImageGallery(FaoInstitute institute, Accession accession);

	RepositoryImage getImage(FaoInstitute institute, Accession accession, UUID uuid) throws NoSuchRepositoryFileException;

	RepositoryImage updateImageMetadata(FaoInstitute institute, Accession accession, UUID uuid, RepositoryImageData repositoryImage) throws NoSuchRepositoryFileException;

	byte[] getFileBytes(FaoInstitute institute, Accession accession, RepositoryImage repositoryImage) throws NoSuchRepositoryFileException;

	Page<ImageGallery> listImageGalleries(FaoInstitute institute, Pageable pageable);

	ImageGallery createImageGallery(FaoInstitute institute, Accession accession);

	RepositoryImage addImage(FaoInstitute institute, Accession accession, String originalFilename, String contentType, byte[] bytes) throws InvalidRepositoryPathException, InvalidRepositoryFileDataException, IOException;

	RepositoryImage updateImage(FaoInstitute institute, Accession accession, RepositoryImage repositoryImage, String contentType, byte[] bytes) throws NoSuchRepositoryFileException, IOException;

	RepositoryImage deleteImage(FaoInstitute institute, Accession accession, RepositoryImage repositoryImage) throws NoSuchRepositoryFileException, IOException;


}
