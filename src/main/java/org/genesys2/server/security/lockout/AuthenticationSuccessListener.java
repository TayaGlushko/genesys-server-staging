/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.security.lockout;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.security.AuthUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;

/**
 * Log successful login attempt and notify {@link AccountLockoutManager}.
 *
 * @author Matija Obreza, matija.obreza@croptrust.org
 */
@Component
public class AuthenticationSuccessListener implements ApplicationListener<AuthenticationSuccessEvent> {
	private final Log _log = LogFactory.getLog(getClass());

	@Autowired
	private AccountLockoutManager lockoutManager;

	@Override
	public void onApplicationEvent(AuthenticationSuccessEvent event) {
		final Object principal = event.getAuthentication().getPrincipal();

		String userName = null;
		if (principal instanceof AuthUserDetails) {
			userName = ((AuthUserDetails) principal).getUser().getEmail();
		} else if (principal instanceof org.springframework.security.core.userdetails.User) {
			userName = ((org.springframework.security.core.userdetails.User) principal).getUsername();
		}

		final Object details = event.getAuthentication().getDetails();
		if (details != null && details instanceof WebAuthenticationDetails) {
			final WebAuthenticationDetails wad = (WebAuthenticationDetails) details;
			// This can be picked up by fail2ban http://www.fail2ban.org/
			_log.info("Successful login attempt for username=" + userName + " from IP=" + wad.getRemoteAddress());
		}

		lockoutManager.handleSuccessfulLogin(userName);
	}
}
