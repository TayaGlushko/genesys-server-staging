/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.genesys2.server.exception.NotUniqueUserException;
import org.genesys2.server.exception.UserException;
import org.genesys2.server.model.impl.LoginType;
import org.genesys2.server.model.impl.User;
import org.genesys2.server.service.ContentService;
import org.genesys2.server.service.EMailVerificationService;
import org.genesys2.server.service.PasswordPolicy.PasswordPolicyException;
import org.genesys2.server.service.TeamService;
import org.genesys2.server.service.TokenVerificationService.NoSuchVerificationTokenException;
import org.genesys2.server.service.UserService;
import org.genesys2.spring.ResourceNotFoundException;
import org.genesys2.util.ReCaptchaUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/profile")
public class UserProfileController extends BaseController {

	@Autowired
	private UserService userService;

	@Autowired
	private TeamService teamService;

	@Autowired
	private EMailVerificationService emailVerificationService;

	@Autowired
	private ContentService contentService;

	@Value("${captcha.siteKey}")
	private String captchaSiteKey;

	@Value("${captcha.privateKey}")
	private String captchaPrivateKey;

	@RequestMapping
	@PreAuthorize("isAuthenticated()")
	public String welcome(ModelMap model) {
		final User user = userService.getMe();
		return "redirect:/profile/" + user.getUuid();
	}

	@RequestMapping("/list")
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	public String list(ModelMap model, @RequestParam(value = "page", defaultValue = "1") int page) {
		model.addAttribute("pagedData", userService.listUsers(new PageRequest(page - 1, 50, new Sort("name"))));
		return "/user/index";
	}

	@RequestMapping("/{uuid:.+}/vetted-user")
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	public String addRoleVettedUser(@PathVariable("uuid") String uuid) {
		userService.addVettedUserRole(uuid);
		return "redirect:/profile/" + uuid;
	}

	@RequestMapping("/{uuid:.+}")
	@PreAuthorize("isAuthenticated()")
	public String someProfile(ModelMap model, @PathVariable("uuid") String uuid) {
		final User user = userService.getUserByUuid(uuid);
		if (user == null) {
			throw new ResourceNotFoundException();
		}

		model.addAttribute("user", user);
		model.addAttribute("teams", teamService.listUserTeams(user));

		return "/user/profile";
	}

	@RequestMapping("/{uuid:.+}/edit")
	@PreAuthorize("hasRole('ADMINISTRATOR') || principal.user.uuid == #uuid")
	public String edit(ModelMap model, @PathVariable("uuid") String uuid) {
		someProfile(model, uuid);
		model.addAttribute("availableRoles", userService.listAvailableRoles());
		return "/user/edit";
	}

	@RequestMapping(value = "/{uuid}/send", method = RequestMethod.GET)
	@PreAuthorize("hasRole('ADMINISTRATOR') || principal.user.uuid == #uuid")
	public String sendEmail(ModelMap model, @PathVariable("uuid") String uuid) {

		final User user = userService.getUserByUuid(uuid);
		emailVerificationService.sendVerificationEmail(user);

		return "redirect:/profile/" + user.getUuid();
	}

	@RequestMapping(value = "/{tokenUuid:.+}/cancel", method = RequestMethod.GET)
	public String cancelValidation(ModelMap model, @PathVariable("tokenUuid") String tokenUuid) {
		emailVerificationService.cancelValidation(tokenUuid);
		return "redirect:/";
	}

	@RequestMapping(value = "/{tokenUuid:.+}/validate", method = RequestMethod.GET)
	public String validateEmail(ModelMap model, @PathVariable("tokenUuid") String tokenUuid) {
		model.addAttribute("tokenUuid", tokenUuid);
		return "/user/validateemail";
	}

	@RequestMapping(value = "/{tokenUuid:.+}/validate", method = RequestMethod.POST)
	public String validateEmail2(ModelMap model, @PathVariable("tokenUuid") String tokenUuid, @RequestParam(value = "key", required = true) String key) {
		try {
			emailVerificationService.validateEMail(tokenUuid, key);
			return "redirect:/profile";
		} catch (final NoSuchVerificationTokenException e) {
			// Not valid
			model.addAttribute("tokenUuid", tokenUuid);
			model.addAttribute("error", "error");
			return "/user/validateemail";
		}
	}

	@RequestMapping(value = "/forgot-password")
	public String forgotPassword(ModelMap model) {
		model.addAttribute("captchaSiteKey", captchaSiteKey);
		model.addAttribute("blurp", contentService.getGlobalArticle("user.reset-password-instructions", getLocale()));
		return "/user/email";
	}

	@RequestMapping(value = "/password/reset", method = RequestMethod.POST)
	public String resetPassword(ModelMap model, HttpServletRequest req, @RequestParam(value = "g-recaptcha-response", required = false) String response, @RequestParam("email") String email,
			RedirectAttributes redirectAttributes) throws IOException {

		// Validate the reCAPTCHA
		if (!ReCaptchaUtil.isValid(response, req.getRemoteAddr(), captchaPrivateKey)) {
			_logger.warn("Invalid captcha.");
			redirectAttributes.addFlashAttribute("error", "errors.badCaptcha");
			return "redirect:/profile/forgot-password";
		}

		try {
			final User user = userService.getUserByEmail(email);

			if (user != null && user.getLoginType() == LoginType.GOOGLE) {
				_logger.warn("Password for users with login type GOOGLE can't be reset!");
				redirectAttributes.addFlashAttribute("error", "errors.reset-password.invalid-login-type");
				return "redirect:/profile/forgot-password";
			}

			if (user != null) {
				emailVerificationService.sendPasswordResetEmail(user);
			}

			return "redirect:/content/user.password-reset-email-sent";
		} catch (UsernameNotFoundException e) {
			redirectAttributes.addFlashAttribute("error", "errors.no-such-user");
			return "redirect:/profile/forgot-password";
		}
	}

	@RequestMapping(value = "/{tokenUuid:.+}/pwdreset", method = RequestMethod.GET)
	public String passwordReset(ModelMap model, @PathVariable("tokenUuid") String tokenUuid) {
		model.addAttribute("captchaSiteKey", captchaSiteKey);
		model.addAttribute("tokenUuid", tokenUuid);
		return "/user/password";
	}

	@RequestMapping(value = "/{tokenUuid:.+}/pwdreset", method = RequestMethod.POST)
	public String updatePassword(ModelMap model, @PathVariable("tokenUuid") String tokenUuid, HttpServletRequest req, @RequestParam(value = "g-recaptcha-response", required = false) String response,
			@RequestParam(value = "key", required = true) String key, @RequestParam("password") String password, RedirectAttributes redirectAttributes) throws IOException {

		// Validate the reCAPTCHA
		if (!ReCaptchaUtil.isValid(response, req.getRemoteAddr(), captchaPrivateKey)) {
			model.addAttribute("key", key);
			model.addAttribute("error", "errors.badCaptcha");
			return passwordReset(model, tokenUuid);
		}

		try {
			emailVerificationService.changePassword(tokenUuid, key, password);
			return "redirect:/content/user.password-reset";
		} catch (final NoSuchVerificationTokenException e) {
			model.addAttribute("key", key);
			model.addAttribute("error", "verification.invalid-key");
			return passwordReset(model, tokenUuid);
		} catch (PasswordPolicyException e) {
			model.addAttribute("key", key);
			model.addAttribute("error", e.getMessage());
			return passwordReset(model, tokenUuid);
		}
	}

	@RequestMapping(value = "/{uuid:.+}/update", method = { RequestMethod.POST })
	@PreAuthorize("hasRole('ADMINISTRATOR') || principal.user.uuid == #uuid")
	public String update(ModelMap model, @PathVariable("uuid") String uuid, @RequestParam("name") String name,
				@RequestParam("email") String email, @RequestParam(value = "currentPwd", required = false) String currentPwd,
				@RequestParam(value = "pwd1", required = false) String pwd1, @RequestParam(value = "pwd2", required = false) String pwd2,
				RedirectAttributes redirectAttributes) {
		final User user = userService.getUserByUuid(uuid);
		if (user == null) {
			throw new ResourceNotFoundException();
		}

		try {
			userService.updateData(user.getId(), name, email);
		} catch (NotUniqueUserException e) {
			redirectAttributes.addFlashAttribute("emailError", "User with e-mail address " + e.getEmail() + " already exists");
			return "redirect:/profile/" + user.getUuid() + "/edit";
		} catch (UserException e) {
			redirectAttributes.addFlashAttribute("emailError", "E-mail address is incorrect");
			return "redirect:/profile/" + user.getUuid() + "/edit";
		}

		if (StringUtils.isNotBlank(pwd1) || StringUtils.isNotBlank(pwd2)) {

			if (user.getLoginType() == LoginType.GOOGLE) {
				redirectAttributes.addFlashAttribute("emailError", "Password for users with login type GOOGLE can't be set!");
				return "redirect:/profile/" + user.getUuid() + "/edit";
			}

			if (StringUtils.isBlank(currentPwd) || !userService.checkPasswordsMatch(currentPwd, user.getPassword())) {
				redirectAttributes.addFlashAttribute("emailError", "Current password is incorrect");
				return "redirect:/profile/" + user.getUuid() + "/edit";
			}

			if (pwd1.equals(pwd2)) {
				try {
					_logger.info("Updating password for " + user);
					userService.updatePassword(user.getId(), pwd1);
					_logger.warn("Password updated for " + user);
				} catch (final UserException e) {
					redirectAttributes.addFlashAttribute("emailError", e.getMessage());
					_logger.error(e.getMessage(), e);
					return "redirect:/profile/" + user.getUuid() + "/edit";
				} catch (PasswordPolicyException e) {
					redirectAttributes.addFlashAttribute("emailError", e.getMessage());
					_logger.error(e.getMessage());
					return "redirect:/profile/" + user.getUuid() + "/edit";
				}
			} else {
				_logger.warn("Passwords didn't match for " + user);
				redirectAttributes.addFlashAttribute("emailError", "Passwords didn't match for " + user);
				return "redirect:/profile/" + user.getUuid() + "/edit";
			}
		}

		return "redirect:/profile/" + user.getUuid();
	}

	@RequestMapping(value = "/{uuid:.+}/update-roles", method = { RequestMethod.POST })
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	public String updateRoles(ModelMap model, @PathVariable("uuid") String uuid, @RequestParam("role") List<String> selectedRoles) {
		final User user = userService.getUserByUuid(uuid);
		if (user == null) {
			throw new ResourceNotFoundException();
		}

		userService.updateRoles(user, selectedRoles);
		return "redirect:/profile/" + user.getUuid();
	}

}
