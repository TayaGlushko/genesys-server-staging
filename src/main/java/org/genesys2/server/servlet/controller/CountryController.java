/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.genesys2.server.model.impl.Country;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.service.ContentService;
import org.genesys2.server.service.FilterConstants;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.GeoRegionService;
import org.genesys2.server.service.GeoService;
import org.genesys2.server.service.InstituteService;
import org.genesys2.server.service.impl.FilterHandler;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilter;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilters;
import org.genesys2.spring.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/geo")
public class CountryController extends BaseController {

	@Autowired
	private GeoService geoService;

	@Autowired
	private InstituteService instituteService;

	@Autowired
	private GenesysService genesysService;

	@Autowired
	private ContentService contentService;

	@Autowired
	private GeoRegionService geoRegionService;

	@RequestMapping
	public String view(ModelMap model) {
		List<Country> countries = geoService.listActive(getLocale());
		model.addAttribute("countries", countries);
		
		List<String> firstLetters = countries.stream().map(country -> country.getName(getLocale()).substring(0, 1)).distinct().collect(Collectors.toList());
		model.addAttribute("firstLetters", firstLetters);
		
		return "/country/index";
	}

	@RequestMapping("/{country}")
	public String view(ModelMap model, @PathVariable(value = "country") String countryStr) {
		_logger.debug("Viewing country " + countryStr);

		if (!countryStr.toUpperCase().equals(countryStr)) {
			return "redirect:/geo/" + countryStr.toUpperCase();
		}

		final Country country = geoService.getCountry(countryStr);
		if (country == null) {
			throw new ResourceNotFoundException();
		}
		model.addAttribute("country", country);
		model.addAttribute("region", geoRegionService.getRegion(country));
		model.addAttribute("itpgrfa", geoService.getITPGRFAStatus(country));

		model.addAttribute("blurp", contentService.getArticle(country, "blurp", getLocale()));

		// All institutes
		model.addAttribute("faoInstitutes", instituteService.listByCountry(country));
		// Active ones
		model.addAttribute("genesysInstitutes", instituteService.listByCountryActive(country));

		model.addAttribute("countByOrigin", genesysService.countByOrigin(country));
		model.addAttribute("countByLocation", genesysService.countByLocation(country));

		return "/country/details";
	}

	@RequestMapping("/{country}/map")
	public String map(ModelMap model, @PathVariable(value = "country") String countryStr) {
		view(model, countryStr);

		@SuppressWarnings("unchecked")
		final List<FaoInstitute> institutes = (List<FaoInstitute>) model.get("faoInstitutes");

		model.addAttribute("jsonInstitutes", geoService.toJson(institutes).toString());

		return "/country/map";
	}

	@PreAuthorize("hasRole('ADMINISTRATOR')")
	@RequestMapping("/{country}/edit")
	public String edit(ModelMap model, @PathVariable(value = "country") String countryStr) {
		view(model, countryStr);
		return "/country/edit";
	}

	@PreAuthorize("hasRole('ADMINISTRATOR')")
	@RequestMapping("/{country}/update")
	public String update(ModelMap model, @PathVariable(value = "country") String countryStr, @RequestParam("blurp") String blurp) {
		_logger.debug("Editing country " + countryStr);
		final Country country = geoService.getCountry(countryStr);
		if (country == null) {
			throw new ResourceNotFoundException();
		}

		geoService.updateBlurp(country, blurp, getLocale());

		return "redirect:/geo/" + country.getCode3();
	}

	@RequestMapping("/{country}/data")
	public String viewData(ModelMap model, @PathVariable(value = "country") String countryStr,
			@RequestParam(value = "page", required = false, defaultValue = "1") int page) {
		_logger.debug("Viewing country " + countryStr);
		final Country country = geoService.getCountry(countryStr);
		if (country == null) {
			throw new ResourceNotFoundException();
		}

		model.addAttribute("filter", "{\"" + FilterConstants.ORGCTY_ISO3 + "\":[\"" + country.getCode3() + "\"]}");

		return "redirect:/explore";
	}

	@RequestMapping("/{country}/overview")
	public String viewData(HttpServletRequest request, @PathVariable(value = "country") String countryStr) throws UnsupportedEncodingException {
		_logger.debug("Viewing country " + countryStr);
		final Country country = geoService.getCountry(countryStr);
		if (country == null) {
			throw new ResourceNotFoundException();
		}

		AppliedFilters appliedFilters = new AppliedFilters();
		appliedFilters.add(new AppliedFilter().setFilterName(FilterConstants.ORGCTY_ISO3).addFilterValue(
				new FilterHandler.LiteralValueFilter(country.getCode3())));

		return "forward:/explore/overview?filter=" + URLEncoder.encode(appliedFilters.toString(), "UTF8");
	}
}
