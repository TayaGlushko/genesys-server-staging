/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import org.genesys2.server.model.impl.Team;
import org.genesys2.server.model.impl.User;
import org.genesys2.server.service.ContentService;
import org.genesys2.server.service.InstituteService;
import org.genesys2.server.service.TeamService;
import org.genesys2.server.service.UserService;
import org.genesys2.spring.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@Scope("request")
@RequestMapping("/team")
public class TeamController extends BaseController {

	@Autowired
	private InstituteService instituteService;

	@Autowired
	private ContentService contentService;

	@Autowired
	private TeamService teamService;

	@Autowired
	private UserService userService;

	@RequestMapping("")
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	public String viewAll(ModelMap model, @RequestParam(value = "page", required = false, defaultValue = "1") int page) {
		model.addAttribute("pagedData", teamService.listTeams(new PageRequest(page - 1, 50, new Sort("name"))));
		return "/team/index";
	}

	@RequestMapping("/{teamUuid}")
	public String viewTeam(ModelMap model, @PathVariable(value = "teamUuid") String uuid) {
		final Team team = teamService.getTeam(uuid);
		if (team == null) {
			throw new ResourceNotFoundException();
		}
		model.addAttribute("team", team);
		model.addAttribute("teammembers", teamService.getMembers(team));
		model.addAttribute("blurp", contentService.getArticle(team, "blurp", getLocale()));

		return "/team/details";
	}

	@RequestMapping("/{teamUuid}/addMember")
	public String addTeamMember(@PathVariable("teamUuid") String uuid, @RequestParam("email") String email, ModelMap model) {
		final Team team = teamService.getTeam(uuid);
		if (team == null) {
			throw new ResourceNotFoundException();
		}

		User user = null;
		try {
			user = userService.getUserByEmail(email);
		} catch (final UsernameNotFoundException e) {
			model.addAttribute("team", team);
			model.addAttribute("teammembers", teamService.getMembers(team));
			model.addAttribute("blurp", contentService.getArticle(team, "blurp", getLocale()));
			model.addAttribute("error", "error");
			return "/team/details";
		}

		teamService.addTeamMember(uuid, user);

		return "redirect:/team/" + uuid;
	}

	@PreAuthorize("hasRole('ADMINISTRATOR')")
	@RequestMapping("/{teamUuid}/{uuid}/deleteMember")
	public String deleteTeamMember(@PathVariable("teamUuid") String teamUuid, @PathVariable("uuid") String uuid) {

		final User user = userService.getUserByUuid(uuid);
		teamService.removeTeamMember(teamUuid, user);

		return "redirect:/team/" + teamUuid;
	}

	@RequestMapping("/{teamUuid}/edit")
	public String editTeamInformation(@PathVariable("teamUuid") String teamUuid, ModelMap model) {
		final Team team = teamService.getTeam(teamUuid);
		if (team == null) {
			throw new ResourceNotFoundException();
		}
		model.addAttribute("team", team);
		return "/team/edit";
	}

	@RequestMapping("/{teamUuid}/update")
	public String updateTeamInformation(@PathVariable("teamUuid") String teamUuid, @RequestParam("teamName") String teamName) {

		teamService.updateTeamInformation(teamUuid, teamName);

		return "redirect:/team/" + teamUuid;
	}
}
