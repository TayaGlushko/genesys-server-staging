/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller.rest.model;

import java.text.MessageFormat;

import org.genesys2.server.model.impl.AccessionIdentifier3;

public class AccessionHeaderJson implements AccessionIdentifier3 {
	public String instCode;
	public String acceNumb;
	public String genus;

	@Override
	public String getHoldingInstitute() {
		return instCode;
	}

	@Override
	public String getAccessionName() {
		return acceNumb;
	}

	@Override
	public String getGenus() {
		return genus;
	}

	@Override
	public String toString() {
		return MessageFormat.format("AID3 instCode={0} acceNumb={1} genus={2}", instCode, acceNumb, genus);
	}
}
