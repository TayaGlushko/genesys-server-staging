/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.genesys2.server.model.UserRole;
import org.genesys2.server.model.impl.LoginType;
import org.genesys2.server.model.impl.User;
import org.genesys2.server.service.ContentService;
import org.genesys2.server.service.CropService;
import org.genesys2.server.service.EMailVerificationService;
import org.genesys2.server.service.OrganizationService;
import org.genesys2.server.service.PasswordPolicy.PasswordPolicyException;
import org.genesys2.server.service.StatisticsService;
import org.genesys2.server.service.UserService;
import org.genesys2.server.servlet.filter.LocaleURLFilter;
import org.genesys2.util.ReCaptchaUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Controller which simply handles *.html requests
 */
@Controller
public class HtmlController extends BaseController {

	@Autowired
	private UserService userService;

	@Autowired
	private CropService cropService;

	@Autowired
	private Validator validator;

	@Autowired
	private EMailVerificationService emailVerificationService;

	@Autowired
	private ContentService contentService;

	@Value("${captcha.siteKey}")
	private String captchaSiteKey;

	@Value("${captcha.privateKey}")
	private String captchaPrivateKey;

	@Autowired
	private StatisticsService statisticsService;

	@Autowired
	private OrganizationService organizationService;

	@RequestMapping("/")
	public String index(HttpServletRequest request) {

		Locale requestLocale = request.getLocale();
		Locale requestedLocale = (Locale) request.getAttribute(LocaleURLFilter.REQUEST_LOCALE_ATTR);
		if (_logger.isDebugEnabled()) {
			_logger.debug("Request locale: " + requestLocale + " requested: " + requestedLocale);
		}

		if (requestedLocale == null && requestLocale != null) {
			_logger.info("Redirecting to request locale: " + requestLocale.getLanguage());
			return "redirect:/" + requestLocale.getLanguage().toLowerCase() + "/welcome";
		}

		return "redirect:/welcome";
	}

	@RequestMapping(value = "welcome")
	public String welcome(ModelMap model) {
		model.addAttribute("cropList", cropService.list(getLocale()));
		model.addAttribute("lastNews", contentService.lastNews());
		model.addAttribute("welcomeBlurp", contentService.getGlobalArticle("welcome", getLocale()));
		model.addAttribute("sideBlurp", contentService.getGlobalArticle("sideBlurp", getLocale()));

		model.addAttribute("numberOfCountries", statisticsService.numberOfCountries());
		model.addAttribute("numberOfInstitutes", statisticsService.numberOfInstitutes());
		model.addAttribute("numberOfAccessions", statisticsService.numberOfAccessions());
		model.addAttribute("numberOfActiveAccessions", statisticsService.numberOfActiveAccessions());
		model.addAttribute("numberOfHistoricAccessions", statisticsService.numberOfHistoricAccessions());

		model.addAttribute("organizations", organizationService.list(new PageRequest(0, 5)));

		return "/index";
	}

	@RequestMapping(value = "login")
	public String login() {
		return "/login";
	}

	@RequestMapping(value = "registration")
	public String registration(ModelMap model) {
		model.addAttribute("captchaSiteKey", captchaSiteKey);
		model.addAttribute("blurp", contentService.getGlobalArticle("registration", getLocale()));
		return "/registration";
	}

	@RequestMapping(value = "new-user")
	public String addUser(@ModelAttribute User user, BindingResult bindingResult, HttpServletRequest req,
			@RequestParam(value = "g-recaptcha-response", required = false) String response, RedirectAttributes redirectAttributes) throws IOException {
		
		user.getRoles().add(UserRole.USER);
		validator.validate(user, bindingResult);

		// Validate the reCAPTCHA
		if (!ReCaptchaUtil.isValid(response, req.getRemoteAddr(), captchaPrivateKey)) {
			_logger.warn("Invalid captcha.");
			redirectAttributes.addFlashAttribute("error", "errors.badCaptcha");
			return "redirect:/registration.html";
		}

		try {
			if (!bindingResult.hasErrors()) {
				if (!userService.exists(user.getEmail())) {
					final User newUser = userService.createAccount(user.getEmail(), user.getPassword(), user.getName(), LoginType.PASSWORD);

					emailVerificationService.sendVerificationEmail(newUser);

					return "redirect:/content/account-created";
				} else {
					redirectAttributes.addFlashAttribute("error", "registration.user-exists");
				}
			} else {
				redirectAttributes.addFlashAttribute("error", "New account form has errors: " + bindingResult.getErrorCount());
				_logger.warn("New account form has errors: " + bindingResult.getErrorCount());
			}
		} catch (final PasswordPolicyException e) {
			redirectAttributes.addFlashAttribute("error", e.getMessage());
		} catch (final Exception e) {
			_logger.error(e.getMessage(), e);
			// simpleExceptionHandler(e);
			redirectAttributes.addFlashAttribute("error", e.getMessage());
		}

		return "redirect:/registration.html";
	}

	@RequestMapping("/access-denied")
	public void accessDenied() {
		throw new AccessDeniedException("Spring Security denied access to the resource.");
	}

	@RequestMapping(value = "/errors/{code}")
	public String errorHandler(@PathVariable("code") int code) {
		return "/errors/error";
	}
}
