/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import java.util.List;

import org.genesys2.server.model.oauth.OAuthClientDetails;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

public abstract class ProfileController<T, ID> extends BaseController {

	private final String viewPrefix = getViewPrefix();

	/**
	 * Get details record for T with given identifier
	 *
	 * @param identifier
	 * @return
	 */
	protected abstract T getDetails(ID identifier);

	/**
	 * Return list of T entries to display on index page.
	 *
	 * Example: "/admin/oauth"
	 *
	 * @return
	 */
	protected abstract List<OAuthClientDetails> list();

	/**
	 * Get prefix to JSP views
	 *
	 * @return
	 */
	protected abstract String getViewPrefix();

	/**
	 * List controllers
	 *
	 * @return
	 */
	@RequestMapping(value = "")
	public String index(ModelMap model) {
		model.addAttribute("items", list());
		return viewPrefix + "/index";
	}

	/**
	 * View client details
	 *
	 * @param model
	 * @param clientId
	 * @return
	 */
	@RequestMapping(value = "/{id}")
	public final String details(ModelMap model, @PathVariable("id") ID identifier) {
		final T item = getDetails(identifier);

		if (item == null) {
			_logger.warn("No such item " + identifier);
			return "redirect:/admin/oauth/" + identifier + "/edit";
		}

		model.addAttribute("item", item);
		return viewPrefix + "/details";
	}

	/**
	 * Overridable method to add extra items to the profile
	 *
	 * @param item
	 */
	public void addDetails(ModelMap model, T item) {
		// NOOP
	}

	/**
	 * Edit client details
	 *
	 * @param model
	 * @param clientId
	 * @return
	 */
	@RequestMapping(value = "/{id}/edit")
	public String edit(ModelMap model, @PathVariable("id") ID identifier) {
		final T item = getDetails(identifier);

		if (item == null) {
			return viewPrefix + "/edit";
		}

		model.addAttribute("item", item);
		return viewPrefix + "/edit";
	}

	/**
	 * Edit client details
	 *
	 * @param model
	 * @param clientId
	 * @return
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String update(ModelMap model, @RequestParam(required = false, defaultValue = "", value = "id") ID identifier) {
		final T item = getDetails(identifier);

		if (item == null) {

			return viewPrefix + "/edit";
		}

		model.addAttribute("client", item);
		return "redirect:.";
	}
}
