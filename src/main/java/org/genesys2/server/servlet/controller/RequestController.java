/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import java.io.IOException;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.genesys2.server.model.impl.User;
import org.genesys2.server.service.ContentService;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.RequestService;
import org.genesys2.server.service.RequestService.RequestException;
import org.genesys2.server.service.RequestService.RequestInfo;
import org.genesys2.server.service.TokenVerificationService.NoSuchVerificationTokenException;
import org.genesys2.server.service.impl.EasySMTAException;
import org.genesys2.spring.SecurityContextUtil;
import org.genesys2.util.ReCaptchaUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Controller to manage request processing.
 * 
 * @author matijaobreza
 */
@Controller
@Scope("request")
@RequestMapping("/request")
public class RequestController extends BaseController {

	@Autowired
	private SelectionBean selectionBean;

	@Autowired
	private GenesysService genesysService;

	@Autowired
	private ContentService contentService;
	
	@Value("${captcha.siteKey}")
	private String captchaSiteKey;

	@Value("${captcha.privateKey}")
	private String captchaPrivateKey;

	@Autowired
	private RequestService requestService;

	/**
	 * Give information about the request process
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping
	public String view(ModelMap model, @RequestParam(value = "page", required = false, defaultValue = "1") int page) {
		model.addAttribute("blurp", contentService.getGlobalArticle("request-intro", getLocale()));

		model.addAttribute("totalCount", selectionBean.size());
		model.addAttribute("availableCount", genesysService.countAvailableForDistribution(selectionBean.copy()));
		model.addAttribute("pagedData", genesysService.listAccessions(selectionBean.copy(), new PageRequest(page - 1, 50, new Sort("accessionName"))));

		return "/request/index";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/start")
	public String start(ModelMap model) {
		final User currentUser = SecurityContextUtil.getCurrentUser();
		model.addAttribute("blurp", contentService.getGlobalArticle("request-personal", getLocale()));
		model.addAttribute("captchaSiteKey", captchaSiteKey);

		if (!model.containsAttribute("requestEmail") && currentUser != null) {
			model.addAttribute("requestEmail", currentUser.getEmail());
		}
		return "/request/personal";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/submit")
	public String submit(ModelMap model, HttpServletRequest req, @RequestParam(value = "email", defaultValue = "", required = true) String requestEmail,
			@RequestParam(value = "g-recaptcha-response", required = false) String response, @RequestParam(value = "notes") String notes,
			@RequestParam(value = "purpose") int purposeType, @RequestParam(value = "smta", required = false) Boolean preacceptSMTA) throws IOException {

		final User currentUser = SecurityContextUtil.getCurrentUser();
		model.addAttribute("notes", notes);
		model.addAttribute("requestEmail", requestEmail);

		InternetAddress internetAddress;
		try {
			internetAddress = new InternetAddress(requestEmail, true);
		} catch (final AddressException e1) {
			_logger.warn("Email not valid. " + e1.getMessage());
			return start(model);
		}

		// Validate the reCAPTCHA
		if (currentUser == null && !ReCaptchaUtil.isValid(response, req.getRemoteAddr(), captchaPrivateKey)) {
			_logger.warn("Recaptcha not valid. Stopping here.");
			return start(model);
		}

		if (preacceptSMTA == null || preacceptSMTA == false) {
			model.addAttribute("smta", false);
			return start(model);
		}

		final RequestInfo requestInfo = new RequestInfo();
		requestInfo.setEmail(internetAddress.toString());
		requestInfo.setPreacceptSMTA(preacceptSMTA);
		requestInfo.setPurposeType(purposeType);
		requestInfo.setNotes(StringUtils.defaultIfBlank(notes, null));

		if (StringUtils.isBlank(requestInfo.getEmail())) {
			_logger.warn("No email address was specified for request. Stopping here.");
			return start(model);
		}

		try {
			// send email to registered email address, wait for
			// response
			requestService.initiateRequest(requestInfo, selectionBean.copy());
		} catch (final RequestException e) {
			_logger.warn(e.getMessage());
			model.addAttribute("error", e);
			return start(model);
		}

		// Whatever the response is, we render the same message
		model.addAttribute("blurp", contentService.getGlobalArticle("request-received", getLocale()));
		return "/request/received";
	}

	@RequestMapping(value = "/{tokenUuid:.+}/validate", method = RequestMethod.GET)
	public String validateClientRequest(ModelMap model, @PathVariable("tokenUuid") String tokenUuid,
			@RequestParam(value = "key", required = false) String key) {
		if (!model.containsAttribute("blurp")) {
			model.addAttribute("blurp", contentService.getGlobalArticle("request-validate", getLocale()));
		}
		model.addAttribute("tokenUuid", tokenUuid);
		model.addAttribute("key", key);
		return "/request/validaterequest";
	}

	@RequestMapping(value = "/{tokenUuid:.+}/validate", method = RequestMethod.POST)
	public String validateClientRequest2(ModelMap model, @PathVariable("tokenUuid") String tokenUuid,
			@RequestParam(value = "key", required = true) String key) {

		try {
			requestService.validateClientRequest(tokenUuid, key);
		} catch (final RequestService.NoPidException e) {
			_logger.error(e.getMessage());

			model.addAttribute("blurp", contentService.getGlobalArticle("request-validate-no-pid", getLocale()));
			return validateClientRequest(model, tokenUuid, key);

		} catch (final RequestException e) {
			_logger.error(e.getMessage(), e);
		} catch (final NoSuchVerificationTokenException e) {
			_logger.error("Verification token is not valid");
			model.addAttribute("error", e);
			return validateClientRequest(model, tokenUuid, null);
		} catch (EasySMTAException e) {
			_logger.error("Error connecting to EasySMTA:  " + e.getMessage());
			model.addAttribute("error", e);
			return validateClientRequest(model, tokenUuid, null);
		}

		return "redirect:/content/request-validated";
	}

	/**
	 * Genebank confirms receipt of request
	 * 
	 * @param model
	 * @param tokenUuid
	 * @return
	 */
	@RequestMapping(value = "/{tokenUuid:.+}/confirm", method = RequestMethod.GET)
	public String confirmReceipt(ModelMap model, @PathVariable("tokenUuid") String tokenUuid, @RequestParam(value = "key", required = false) String key) {
		if (!model.containsAttribute("blurp")) {
			model.addAttribute("blurp", contentService.getGlobalArticle("request-confirm-receipt", getLocale()));
		}

		model.addAttribute("tokenUuid", tokenUuid);
		model.addAttribute("key", key);
		return "/request/confirmrequest";
	}

	@RequestMapping(value = "/{tokenUuid:.+}/confirm", method = RequestMethod.POST)
	public String confirmReceipt2(ModelMap model, @PathVariable("tokenUuid") String tokenUuid, @RequestParam(value = "key", required = true) String key) {

		try {
			requestService.validateReceipt(tokenUuid, key);
		} catch (final NoSuchVerificationTokenException e) {
			_logger.error("Verification token is not valid");

			model.addAttribute("error", e);
			return confirmReceipt(model, tokenUuid, null);
		}

		return "redirect:/content/request-confirmed";
	}
}
