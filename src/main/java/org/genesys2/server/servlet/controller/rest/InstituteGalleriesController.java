/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller.rest;

import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.filerepository.InvalidRepositoryFileDataException;
import org.genesys2.server.filerepository.InvalidRepositoryPathException;
import org.genesys2.server.filerepository.NoSuchRepositoryFileException;
import org.genesys2.server.filerepository.model.ImageGallery;
import org.genesys2.server.filerepository.model.RepositoryImage;
import org.genesys2.server.filerepository.model.RepositoryImageData;
import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.InstituteFilesService;
import org.genesys2.server.service.InstituteService;
import org.genesys2.server.service.impl.NonUniqueAccessionException;
import org.genesys2.spring.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Allows for management of institute-owned files and respective metadata.
 */
@Controller
@PreAuthorize("isAuthenticated()")
@RequestMapping(value = { InstituteGalleriesController.CONTROLLER_URL })
public class InstituteGalleriesController extends RestController {

	public static final Log LOG = LogFactory.getLog(InstituteGalleriesController.class);

	protected static final String CONTROLLER_URL = "/api/v0/img";

	@Autowired
	private InstituteService instituteService;

	@Autowired
	private GenesysService genesysService;

	@Autowired
	private InstituteFilesService instituteFilesService;

	/**
	 * List existing image galleries for INSTCODE
	 */
	@RequestMapping(value = "/{instCode}/_galleries", method = { RequestMethod.GET }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody Page<ImageGallery> listAccessionGalleries(@PathVariable("instCode") final String instCode, @RequestParam(name = "page", required = false, defaultValue = "1") final int page) {

		final FaoInstitute institute = this.instituteService.findInstitute(instCode);
		if (institute == null) {
			throw new ResourceNotFoundException();
		}

		LOG.info("Listing galleries for institute=" + institute);

		// TODO no images!
		return instituteFilesService.listImageGalleries(institute, new PageRequest(page, 50));
	}

	/**
	 * List UUIDs of images in the accession gallery for INSTCODE/acn/ACCENUMB
	 *
	 * @throws NonUniqueAccessionException
	 */
	@RequestMapping(value = "/{instCode}/acn/{acceNumb:.+}", method = { RequestMethod.GET }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody List<UUID> listAccessionGallery(@PathVariable("instCode") final String instCode, @PathVariable("acceNumb") final String acceNumb) throws NonUniqueAccessionException {

		final FaoInstitute institute = this.instituteService.findInstitute(instCode);
		final Accession accession = this.genesysService.getAccession(instCode, acceNumb);
		if (institute == null || accession == null) {
			throw new ResourceNotFoundException();
		}

		LOG.info("Listing image uuids for gallery institute=" + institute + " accn=" + accession);

		final ImageGallery imageGallery = this.instituteFilesService.loadImageGallery(institute, accession);
		if (imageGallery == null) {
			throw new ResourceNotFoundException("No image gallery");
		}

		final List<UUID> uuids = imageGallery.getImages().stream().map(image -> image.getUuid()).collect(Collectors.toList());
		if (LOG.isDebugEnabled()) {
			LOG.debug("Gallery images=" + uuids);
		}

		return uuids;
	}

	/**
	 * Update image metadata
	 *
	 * @throws NonUniqueAccessionException
	 */
	@RequestMapping(value = "/{instCode}/acn/{acceNumb:.+}/{uuid}/_metadata", method = { RequestMethod.PUT }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody RepositoryImage updateImageMetadata(final HttpServletRequest request, final HttpServletResponse response, @PathVariable("instCode") final String instCode,
			@PathVariable("acceNumb") final String acceNumb, @PathVariable("uuid") final UUID uuid, @RequestBody final RepositoryImageData imageData)
			throws IOException, NonUniqueAccessionException {

		final FaoInstitute institute = this.instituteService.findInstitute(instCode);
		final Accession accession = this.genesysService.getAccession(instCode, acceNumb);
		if (institute == null || accession == null) {
			throw new ResourceNotFoundException();
		}

		LOG.info("Updating image metadata in gallery institute=" + institute + " accn=" + accession + " uuid=" + uuid);

		try {
			final RepositoryImage existingRepositoryImage = this.instituteFilesService.getImage(institute, accession, uuid);

			if (existingRepositoryImage == null) {
				throw new ResourceNotFoundException("No such thing");
			}

			return this.instituteFilesService.updateImageMetadata(institute, accession, existingRepositoryImage.getUuid(), imageData);

		} catch (final NoSuchRepositoryFileException e) {
			LOG.warn("404 - No such repository file ", e);
			throw new ResourceNotFoundException("No such thing");
		}
	}

	/**
	 * Get image metadata.
	 *
	 * @throws IOException
	 * @throws NonUniqueAccessionException
	 */
	@RequestMapping(value = "/{instCode}/acn/{acceNumb:.+}/{uuid}/_metadata", method = { RequestMethod.GET }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody RepositoryImage getImageMetadata(final HttpServletRequest request, final HttpServletResponse response, @PathVariable("instCode") final String instCode,
			@PathVariable("acceNumb") final String acceNumb, @PathVariable("uuid") final UUID uuid) throws IOException, NonUniqueAccessionException {

		final FaoInstitute institute = this.instituteService.findInstitute(instCode);
		final Accession accession = this.genesysService.getAccession(instCode, acceNumb);
		if (institute == null || accession == null) {
			throw new ResourceNotFoundException();
		}

		LOG.info("Getting image metadata from gallery institute=" + institute + " accn=" + accession + " uuid=" + uuid);

		try {
			final RepositoryImage repositoryImage = this.instituteFilesService.getImage(institute, accession, uuid);

			if (repositoryImage == null) {
				throw new ResourceNotFoundException("No such thing");
			}

			return repositoryImage;

		} catch (final NoSuchRepositoryFileException e) {
			LOG.warn("404 - No such repository file ", e);
			throw new ResourceNotFoundException("No such thing");
		}
	}

	/**
	 * Get image bytes.
	 *
	 * @throws IOException
	 * @throws NonUniqueAccessionException
	 */
	@RequestMapping(value = "/{instCode}/acn/{acceNumb:.+}/{uuid}", method = { RequestMethod.GET })
	public void getImage(final HttpServletRequest request, final HttpServletResponse response, @PathVariable("instCode") final String instCode, @PathVariable("acceNumb") final String acceNumb,
			@PathVariable("uuid") final UUID uuid) throws IOException, NonUniqueAccessionException {

		final FaoInstitute institute = this.instituteService.findInstitute(instCode);
		final Accession accession = this.genesysService.getAccession(instCode, acceNumb);
		if (institute == null || accession == null) {
			throw new ResourceNotFoundException();
		}

		LOG.info("Getting image from gallery institute=" + institute + " accn=" + accession + " uuid=" + uuid);

		try {
			final RepositoryImage repositoryImage = this.instituteFilesService.getImage(institute, accession, uuid);

			if (repositoryImage == null) {
				throw new ResourceNotFoundException("No such thing");
			}

			final byte[] data = this.instituteFilesService.getFileBytes(institute, accession, repositoryImage);

			response.setContentType(repositoryImage.getContentType());
			response.addHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", repositoryImage.getOriginalFilename()));

			response.setContentLength(data.length);
			response.getOutputStream().write(data);
			response.flushBuffer();

		} catch (final NoSuchRepositoryFileException e) {
			LOG.warn("404 - No such repository file ", e);
			throw new ResourceNotFoundException("No such thing");
		}
	}

	/**
	 * Delete image from accession gallery
	 *
	 * @throws IOException
	 * @throws NonUniqueAccessionException
	 */
	@RequestMapping(value = "/{instCode}/acn/{acceNumb:.+}/{uuid}", method = { RequestMethod.DELETE })
	public void deleteImage(final HttpServletRequest request, final HttpServletResponse response, @PathVariable("instCode") final String instCode, @PathVariable("acceNumb") final String acceNumb,
			@PathVariable("uuid") final UUID uuid) throws IOException, NonUniqueAccessionException {

		final FaoInstitute institute = this.instituteService.findInstitute(instCode);
		final Accession accession = this.genesysService.getAccession(instCode, acceNumb);
		if (institute == null || accession == null) {
			throw new ResourceNotFoundException();
		}

		LOG.info("Deleting image from gallery institute=" + institute + " accn=" + accession + " uuid=" + uuid);

		try {
			final RepositoryImage repositoryImage = this.instituteFilesService.getImage(institute, accession, uuid);

			if (repositoryImage == null) {
				throw new ResourceNotFoundException("No such thing");
			}

			this.instituteFilesService.deleteImage(institute, accession, repositoryImage);

		} catch (final NoSuchRepositoryFileException e) {
			LOG.warn("404 - No such repository file ", e);
			throw new ResourceNotFoundException("No such thing");
		}
	}

	/**
	 * Add or update image bytes. Content-Type is extracted from request headers, the request body represents the bytes.
	 * 
	 * @param originalFilename
	 *
	 * @throws NonUniqueAccessionException
	 * @throws InvalidRepositoryFileDataException
	 * @throws InvalidRepositoryPathException
	 */
	@RequestMapping(value = "/{instCode}/acn/{acceNumb:.+}/", method = { RequestMethod.PUT, RequestMethod.POST }, consumes = { "image/*" }, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody RepositoryImage addImage(final HttpServletRequest request, final HttpServletResponse response, @PathVariable("instCode") final String instCode,
			@PathVariable("acceNumb") final String acceNumb, @RequestParam(name = "originalFilename", required = true) String originalFilename, HttpEntity<byte[]> requestEntity)
			throws IOException, NonUniqueAccessionException, InvalidRepositoryPathException, InvalidRepositoryFileDataException {

		// originalFilename could not be part of the path (and @PathVariable)
		// because the wrong content-negotiating strategy kicked in.

		final FaoInstitute institute = this.instituteService.findInstitute(instCode);
		final Accession accession = this.genesysService.getAccession(instCode, acceNumb);
		if (institute == null || accession == null) {
			throw new ResourceNotFoundException();
		}

		LOG.info("Adding image to gallery institute=" + institute + " accn=" + accession);

		// Create gallery if missing
		ImageGallery imageGallery = this.instituteFilesService.loadImageGallery(institute, accession);
		if (imageGallery == null) {
			LOG.info("Adding image gallery for institute=" + institute + " accn=" + accession);
			imageGallery = this.instituteFilesService.createImageGallery(institute, accession);
		}

		return instituteFilesService.addImage(institute, accession, originalFilename, requestEntity.getHeaders().getContentType().toString(), requestEntity.getBody());
	}

}
