/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.filter;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public class LocaleWrappedServletResponse extends HttpServletResponseWrapper {
	private static final Logger LOG = Logger.getLogger(LocaleWrappedServletResponse.class);

	private String prefix;
	private LocaleURLMatcher localeUrlMatcher;
	private String defaultLanguagePrefix;

	public LocaleWrappedServletResponse(HttpServletResponse response, LocaleURLMatcher localeUrlMatcher, String urlLanguage, String defaultLanguage) {
		super(response);
		this.localeUrlMatcher = localeUrlMatcher;
		this.prefix = updatePrefix(urlLanguage);
		this.defaultLanguagePrefix = "/" + defaultLanguage + "/";
	}

	private boolean isExcluded(String url) {
		// Exclude querystring-only urls
		return url.startsWith("?") || localeUrlMatcher.isExcluded(url);
	}

	@Override
	public String encodeURL(String url) {
		if (isExcluded(url)) {
			if (url.startsWith(defaultLanguagePrefix)) {
				return super.encodeURL(url.substring(defaultLanguagePrefix.length() - 1));
			}
			return super.encodeURL(url);
		} else {
			String encodedURL = prefix + super.encodeURL(url);
			if (LOG.isDebugEnabled()) {
				LOG.debug("encodeURL " + url + " to " + encodedURL);
			}
			return encodedURL;
		}
	}

	@Override
	@Deprecated
	public String encodeUrl(String url) {
		if (isExcluded(url)) {
			return super.encodeUrl(url);
		} else {
			String encodedURL = prefix + super.encodeUrl(url);
			if (LOG.isDebugEnabled()) {
				LOG.debug("encodeUrl " + url + " to " + encodedURL);
			}
			return encodedURL;
		}
	}

	@Override
	public String encodeRedirectURL(String url) {
		if (isExcluded(url)) {
			return super.encodeRedirectURL(url);
		} else {
			String encodedURL = prefix + super.encodeRedirectURL(url);
			if (LOG.isDebugEnabled()) {
				LOG.debug("encodeRedirectURL " + url + " to " + encodedURL);
			}
			return encodedURL;
		}
	}

	@Override
	@Deprecated
	public String encodeRedirectUrl(String url) {
		if (isExcluded(url)) {
			return super.encodeRedirectUrl(url);
		} else {
			String encodedURL = prefix + super.encodeRedirectUrl(url);
			if (LOG.isDebugEnabled()) {
				LOG.debug("encodeRedirectUrl " + url + " to " + encodedURL);
			}
			return encodedURL;
		}
	}

	private String updatePrefix(String language) {
		if (StringUtils.isBlank(language)) {
			return "";
		} else {
			return "/" + language;
		}
	}
}
