/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.spring.config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.spring.DefaultRolesPrefixPostProcessor;
import org.genesys2.transifex.client.TransifexService;
import org.genesys2.transifex.client.TransifexServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;

@Configuration
@Import({ SpringProperties.class, SpringCommonConfig.class, SpringAclConfig.class, SpringSchedulerConfig.class, SpringDataBaseConfig.class,
		SpringMailConfig.class, SpringSecurityOauthConfig.class, SpringCacheConfig.class, ElasticsearchConfig.class, FileRepositoryConfig.class })
@ImportResource({ "classpath:/spring/spring-security.xml" })
public class ApplicationConfig {
	public static final Log LOG = LogFactory.getLog(ApplicationConfig.class);

	@Bean
	public TransifexService transifexService() {
		return new TransifexServiceImpl();
	}

	@Bean
	public DefaultRolesPrefixPostProcessor defaultRolesPrefixPostProcessor(){
		return new DefaultRolesPrefixPostProcessor();
	}

}
