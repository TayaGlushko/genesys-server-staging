/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.spring.config;

import java.util.Properties;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IExecutorService;
import com.hazelcast.core.IMap;
import com.hazelcast.core.IQueue;
import com.hazelcast.spring.cache.HazelcastCacheManager;
import com.hazelcast.web.WebFilter;

import org.genesys2.server.security.lockout.AccountLockoutManager.AttemptStatistics;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@EnableCaching
@Import({ HazelcastConfig.class })
public class SpringCacheConfig {
	@Value("${hazelcast.instanceName}")
	protected String instanceName = "genesys";

	@Value("${base.cookie-domain}")
	private String cookieDomain;

	@Value("${base.cookie-secure}")
	private String cookieSecure;

	@Value("${base.cookie-http-only}")
	private String cookieHttpOnly;

	@Bean
	public HazelcastCacheManager cacheManager(HazelcastInstance hazelcastInstance) {
		HazelcastCacheManager cm = new HazelcastCacheManager(hazelcastInstance);
		return cm;
	}

	@Bean
	public IMap<Object, Object> tileserverMap(HazelcastInstance hazelcast) {
		IMap<Object, Object> x = hazelcast.getMap("tileserver");
		return x;
	}

	@Bean
	public IQueue<Object> elasticRemoveQueue(HazelcastInstance hazelcast) {
		return hazelcast.getQueue("es-remove");
	}

	@Bean
	public IQueue<Object> elasticUpdateQueue(HazelcastInstance hazelcast) {
		return hazelcast.getQueue("es-update");
	}

	@Bean
	public IExecutorService distributedExecutor(HazelcastInstance hazelcast) {
		IExecutorService executorService = hazelcast.getExecutorService("hazel-exec");
		return executorService;
	}
	
	@Bean
	public IMap<String, AttemptStatistics> accountLockoutMap(HazelcastInstance hazelcast) {
		IMap<String, AttemptStatistics> x = hazelcast.getMap("accountLocks");
		return x;
	}

	@Bean
	public WebFilter hazelcastWebFilter() {
		return new WebFilter(filterProps());
	}

	private Properties filterProps() {
		final Properties properties = new Properties();
		properties.setProperty("use-client", "false");
		properties.setProperty("map-name", "jetty-sessions");
		properties.setProperty("sticky-session", "false");
		properties.setProperty("cookie-name", "hz-session-id");
		properties.setProperty("cookie-domain", cookieDomain);
		properties.setProperty("cookie-secure", cookieSecure);
		properties.setProperty("cookie-http-only", cookieHttpOnly);
		properties.setProperty("debug", "true");
		properties.setProperty("instance-name", instanceName);
		properties.setProperty("shutdown-on-destroy", "true");
		return properties;
	}
}
