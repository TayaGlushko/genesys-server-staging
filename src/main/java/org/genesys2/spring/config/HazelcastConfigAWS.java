/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.spring.config;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.hazelcast.config.AwsConfig;
import com.hazelcast.config.Config;
import com.hazelcast.config.EvictionPolicy;
import com.hazelcast.config.ExecutorConfig;
import com.hazelcast.config.GroupConfig;
import com.hazelcast.config.JoinConfig;
import com.hazelcast.config.MapConfig;
import com.hazelcast.config.MaxSizeConfig;
import com.hazelcast.config.MaxSizeConfig.MaxSizePolicy;
import com.hazelcast.config.NetworkConfig;
import com.hazelcast.config.QueueConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ManagedContext;

@Configuration
@Profile("aws")
public class HazelcastConfigAWS extends HazelcastConfig {
	@Value("${hazelcast.aws.region}")
	private String region;
	@Value("${hazelcast.aws.access-key}")
	private String accessKey;
	@Value("${hazelcast.aws.secret-key}")
	private String secretKey;
	@Value("${hazelcast.aws.security-group}")
	private String securityGroupName;

	@Bean
	public HazelcastInstance hazelcast(ManagedContext managedContext) {
		Config cfg = new Config();
		cfg.setManagedContext(managedContext);
		cfg.setInstanceName(instanceName);

		GroupConfig groupConfig = cfg.getGroupConfig();
		groupConfig.setName(name);
		groupConfig.setPassword(password);

		cfg.setProperty("hazelcast.merge.first.run.delay.seconds", "5");
		cfg.setProperty("hazelcast.merge.next.run.delay.seconds", "5");
		cfg.setProperty("hazelcast.logging.type", "log4j");
		cfg.setProperty("hazelcast.icmp.enabled", "true");

		NetworkConfig network = cfg.getNetworkConfig();
		network.setPort(hazelPort);
		network.setPortAutoIncrement(false);

		if (StringUtils.isNotBlank(hazelPublicAddress)) {
			System.out.println("Hazelcast: Using public address " + hazelPublicAddress);
			network.setPublicAddress(hazelPublicAddress);
		}
		
		if (StringUtils.isNotBlank(hazelInterfaces)) {
			for (String hazelInterface : hazelInterfaces.split(",")) {
				System.out.println("Hazelcast: Adding single interface " + hazelInterface);
				network.getInterfaces().addInterface(hazelInterface);
			}
		}

		JoinConfig join = network.getJoin();
		join.getMulticastConfig().setEnabled(false);

		AwsConfig awsConfig = join.getAwsConfig();
		awsConfig.setEnabled(true);
		awsConfig.setRegion(region);
		awsConfig.setAccessKey(accessKey);
		awsConfig.setSecretKey(secretKey);
		awsConfig.setConnectionTimeoutSeconds(20);
		awsConfig.setSecurityGroupName(securityGroupName);

		MapConfig defaultMapConfig = new MapConfig();
		defaultMapConfig.setName("default");
		// defaultMapConfig.setTimeToLiveSeconds(0);
		defaultMapConfig.setEvictionPolicy(EvictionPolicy.LFU);
		// defaultMapConfig.setMaxIdleSeconds();
		MaxSizeConfig defaultMaxSizeConfig = new MaxSizeConfig();
		defaultMaxSizeConfig.setSize(defaultCacheSize);
		defaultMaxSizeConfig.setMaxSizePolicy(MaxSizePolicy.PER_NODE);
		defaultMapConfig.setMaxSizeConfig(defaultMaxSizeConfig);
		cfg.addMapConfig(defaultMapConfig);

		MapConfig tileserverMapConfig = new MapConfig();
		tileserverMapConfig.setName("tileserver");
		tileserverMapConfig.setTimeToLiveSeconds(tileserverTTL);
		tileserverMapConfig.setEvictionPolicy(tileserverEvictionPolicy);
		tileserverMapConfig.setMaxIdleSeconds(tileserverMaxIdle);
		MaxSizeConfig tileserverMaxSizeConfig = new MaxSizeConfig();
		tileserverMaxSizeConfig.setSize(tileserverMaxSize);
		tileserverMaxSizeConfig.setMaxSizePolicy(MaxSizePolicy.PER_NODE);
		tileserverMapConfig.setMaxSizeConfig(tileserverMaxSizeConfig);
		cfg.addMapConfig(tileserverMapConfig);

		ExecutorConfig execConfig = new ExecutorConfig();
		execConfig.setName("hazel-exec");
		execConfig.setPoolSize(4);
		execConfig.setQueueCapacity(2);
		execConfig.setStatisticsEnabled(true);
		cfg.addExecutorConfig(execConfig);

		QueueConfig queueConfig = new QueueConfig();
		queueConfig.setName("elasticsearchQueue");
		queueConfig.setMaxSize(100);
		cfg.addQueueConfig(queueConfig);

		HazelcastInstance instance = Hazelcast.newHazelcastInstance(cfg);
		return instance;
	}

}
