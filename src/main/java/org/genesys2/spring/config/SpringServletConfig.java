/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.spring.config;

import java.util.Locale;
import java.util.Properties;
import java.util.Set;

import javax.annotation.Resource;

import org.genesys2.spring.AddStuffInterceptor;
import org.genesys2.spring.RequestAttributeLocaleResolver;
import org.genesys2.spring.RequestTrackingInterceptor;
import org.genesys2.spring.validation.oval.spring.SpringOvalValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.validation.Validator;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;
import org.springframework.web.servlet.theme.CookieThemeResolver;
import org.springframework.web.servlet.theme.ThemeChangeInterceptor;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Import({ SpringProperties.class })
@EnableWebMvc
@EnableAspectJAutoProxy
@ComponentScan(basePackages = { "org.genesys2.server.servlet.filter", "org.genesys2.server.servlet.controller" })
@Configuration
public class SpringServletConfig extends WebMvcConfigurerAdapter {

	@Value("${theme.defaultThemeName}")
	private String defaultThemeName;

	@Autowired
	private AddStuffInterceptor addStuffInterceptor;

	@Autowired(required = false)
	private RequestTrackingInterceptor requestTrackingInterceptor;

	@Resource
	private Set<String> supportedLocales;

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		super.addResourceHandlers(registry);
		registry.addResourceHandler("/html/**").addResourceLocations("/html/");
	}

	@Bean
	public ViewResolver viewResolver() {
		final InternalResourceViewResolver resolver = new NewGUIViewResolver();
		resolver.setPrefix("/WEB-INF/jsp");
		resolver.setSuffix(".jsp");
		resolver.setExposeContextBeansAsAttributes(true);
		resolver.setExposedContextBeanNames(new String[] { "props", "jspHelper" });
		resolver.setRedirectHttp10Compatible(false);
		resolver.setRequestContextAttribute("requestContext");
		return resolver;
	}

	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		final ThemeChangeInterceptor themeChangeInterceptor = new ThemeChangeInterceptor();
		themeChangeInterceptor.setParamName("theme");

		registry.addInterceptor(themeChangeInterceptor);
		if (requestTrackingInterceptor != null) {
			registry.addInterceptor(requestTrackingInterceptor);
		}
		registry.addInterceptor(addStuffInterceptor);
	}

	@Scope("singleton")
	@Bean
	@Override
	public Validator getValidator() {
		final SpringOvalValidator validator = new SpringOvalValidator();
		validator.setValidator(new net.sf.oval.Validator());
		return validator;
	}

	@Bean
	public SimpleMappingExceptionResolver simpleMappingExceptionResolver() {
		final Properties properties = new Properties();
		properties.setProperty("javax.servlet.UnavailableException", "/404");

		final SimpleMappingExceptionResolver resolver = new SimpleMappingExceptionResolver();
		resolver.setExceptionMappings(properties);
		return resolver;
	}

	@Bean
	public ResourceBundleMessageSource messageSource() {
		final ResourceBundleMessageSource source = new ResourceBundleMessageSource();
		source.setBasename("content/language");
		source.setDefaultEncoding("UTF-8");
		source.setUseCodeAsDefaultMessage(true);
		return source;
	}

	@Bean
	public RequestAttributeLocaleResolver localeResolver() {
		final RequestAttributeLocaleResolver resolver = new RequestAttributeLocaleResolver();
		resolver.setDefaultLocale(new Locale("en"));
		resolver.setSupportedLocales(supportedLocales);
		return resolver;
	}

	@Bean
	public CookieThemeResolver themeResolver() {
		final CookieThemeResolver cookieThemeResolver = new CookieThemeResolver();
		cookieThemeResolver.setDefaultThemeName(defaultThemeName);
		return cookieThemeResolver;
	}
	
	@Override
	public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
		// Trying to be tooooo smart. Ignore path-based extension in content negotiation.
		configurer.favorPathExtension(false);
	}
	
//	/**
//	 * http://stackoverflow.com/questions/16332092/spring-mvc-pathvariable-with-dot-is-getting-truncated
//	 */
//	@Override
//	public void configurePathMatch(PathMatchConfigurer configurer) {
//		configurer.setUseSuffixPatternMatch(false);
//	}
}