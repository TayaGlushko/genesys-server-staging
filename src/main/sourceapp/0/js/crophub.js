/**
 * Copyright 2015 Global Crop Diversity Trust 
 * 
 * jQuery $ and $.ajax() required.
 */
'use strict';

var GenesysMaps = {
  LatLng : function(lat, lng) {
    this.lat = lat;
    this.lng = lng;
    this.noWrap = true;
  },

  loaded : false,
  queue : [],
  loadedMaps : [],
  defaultOptions : {
    zoom : 5,
    markerTitle: null,
    center : [ 0, 0 ]
  },

  map : function(language, doms, mapOptions, callback) {
    var mapOpt = $.extend({}, this.defaultOptions, mapOptions);
    doms.each(function(idx, el) {
      if ($.inArray(el, this.loadedMaps) < 0) {
        GenesysMaps.queue.push({
          element : el,
          mapOptions : $.extend({}, mapOpt),
          callback : callback
        });
      }
    });
    this.onLoad();
  },

  onLoad : function() {
    var config = this.queue.shift();
    while (config !== null && config !== undefined) {
      var mapOptions = config.mapOptions;
      var map = L.map(config.element, mapOptions).setView(mapOptions.center, 4);
      L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}', {
    	  attribution: 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ',
    	  maxZoom: 16
      }).addTo(map);
      if (mapOptions.markerTitle !== null) {
        var marker = L.marker(mapOptions.center).addTo(map);
        marker.bindPopup(mapOptions.markerTitle).openPopup();
      }
      if (config.callback !== null && config.callback !== undefined) {
        config.callback(config.element, map);
      }
      config = this.queue.shift();
    }
  },
  boundingBox : function(latLngArray) {
    var sw = new GenesysMaps.LatLng(180, 180);
    var ne = new GenesysMaps.LatLng(-180, -180);

    latLngArray.forEach(function(latLng) {
      sw.lat = Math.min(sw.lat, latLng.lat);
      sw.lng = Math.min(sw.lng, latLng.lng);
      ne.lat = Math.max(ne.lat, latLng.lat);
      ne.lng = Math.max(ne.lng, latLng.lng);
    });

    return [ [ sw.lat, sw.lng ], [ ne.lat, ne.lng ] ];
  }
};

GenesysMaps.BoundingBox = function() {
  this.south = 90;
  this.north = -90;
  this.east = -180;
  this.west = 180;
};

GenesysMaps.BoundingBox.prototype.add = function(pos) {
  this.east = Math.max(this.east, pos[0]);
  this.west = Math.min(this.west, pos[0]);
  this.north = Math.max(this.north, pos[1]);
  this.south = Math.min(this.south, pos[1]);
};

GenesysMaps.BoundingBox.prototype.getBounds = function() {
  return [ [ this.west, this.south ], [ this.east, this.north ] ];
};

var GenesysChart = {

  histogram : function(placeholder, data) {
    $.plot(placeholder, [ {
      data : data,
      color : '#88ba42'
    } ], {
      series : {
        bars : {
          show : true,
          barWidth : 0.4,
          fill : 1,
          lineWidth : 0,
          align : 'center'
        }
      },
      xaxis : {
        tickLength : 0
      }
    });
  },

  chart : function(placeholder, url, options, labelCallback, clickCallback) {

    var defaultOptions = {
      series : {
        pie : {
          show : true,
          label : {
            show : false
          }
        }
      },
      grid : {
        hoverable : true,
        clickable : true
      },
      legend : {
        show : false
      }
    };

    var opts = $.extend(true, defaultOptions, options);

    $.ajax(url, {
      success : function(data) {

        $.plot($(placeholder), GenesysChart.makeDataForFlot(data, labelCallback), opts);

        $('<div class="tooltip-chart"></div>').css({
          position : 'absolute',
          display : 'none',
          border : '1px solid #929292',
          padding : '2px',
          'background-color' : '#929292',
          opacity : 0.80
        }).appendTo(placeholder);

        $(placeholder).bind('plothover', function(event, pos, obj) {

          if (obj !== null) {

            var height = $(placeholder).height();
            var width = $(placeholder).width();

            var radius = obj.series.pie.label.radius + 99;
            var halfAngle = ((obj.series.startAngle + obj.series.angle) + obj.series.startAngle) / 2;

            var x = width / 2 + Math.round(Math.cos(halfAngle) * radius);
            var y = height / 2 + Math.round(Math.sin(halfAngle) * radius);

            var str = obj.series.label + ' (' + obj.datapoint[1][0][1] + ')';
            $($(this).find('.tooltip-chart')).html(str).css({
              top : y,
              left : x
            }).show();
          } else {
            $($(this).find('.tooltip-chart')).hide();
          }
        });

        $(placeholder).bind('plotclick', function(event, pos, obj) {
          if (obj !== null && obj.series.labelSource !== null && clickCallback !== null) {
            return clickCallback(obj.series.labelSource);
          }
        });
      },
      error : function(error) {
        console.log(error);
      }
    });
  },

  makeDataForFlot : function(data, labelCallback) {
    var dataFlot = [];
    $.each(data, function(idx, statistic) {
      var label;

      if (statistic[0] !== null && labelCallback !== null) {
        label = labelCallback(statistic[0]);
      } else if (typeof statistic[0] === 'string') {
        label = statistic[0];
      }

      dataFlot[idx] = {
        labelSource : statistic[0],
        label : label,
        data : statistic[1]
      };
    });
    return dataFlot;
  }
};

var GenesysFilterUtil = {
  // Enable autocompleters
  registerAutocomplete : function(container, jsonData) {
    $(container).find('.autocomplete-filter').each(function() {
      var t = $(this);
      console.log('Autocomplete ' + t);
      t.autocomplete({
        delay : 200,
        minLength : 3,
        messages : {
          noResults : '',
          results : function() {
          }
        },
        source : function(request, response) {
          $.get(t.attr('x-source'), {
            term : request.term,
            jsonFilter : JSON.stringify(jsonData)
          }, function(data) {
            response(data);
          });
        }
      });
    });
  },
  // replace chars
  setCharAt : function(str, index, chr) {
    if (index > str.length - 1) {
      return str;
    }
    return str.substr(0, index) + chr + str.substr(index + 1);
  },
  // form 'get' request
  submitJson : function(url, jsonData) {
    var filter = JSON.stringify(jsonData);
    url = url + '?filter=' + filter;

    window.location.href = encodeURI(url);
  },
  // check that value exist in json array
  existInJson : function(input, key, jsonData) {
    var array = jsonData[key];

    for (var i = array.length - 1; i >= 0; i--) {
      if (JSON.stringify(array[i]) === JSON.stringify(input)) {
        return true;
      }
    }
    return false;
  },

  // form json array
  collectData : function(key, value, jsonData) {
    if (value === 'null') {
      value = null;
    }

    var jsonValue = [];
    var exist = jsonData[key] !== null && jsonData[key] !== undefined;

    if (exist) {
      jsonValue = jsonData[key];
    }

    jsonValue.push(value);
    return jsonValue;
  },

  // add in html value of filter
  appendHtml : function(key, value, jsonValue, element) {
    var normKey = GenesysFilterUtil.normKey(key);

    if (value === null) {
      value = 'null';
    }
    if (jsonValue === null) {
      jsonValue = 'null';
    }

    if (typeof jsonValue === 'object' && jsonValue !== null) {
      jsonValue = JSON.stringify(jsonValue);
    }

    var div = '<div class="filtval complex" x-key="' + normKey + jsonValue + '" i-key="' + key + '">' + value + '</div>';
    $(element).parents('.filter-block').find('.filter-values').append(div);
  },
  // remove value from json array
  removeValue : function(value, key, jsonData) {
    var array = jsonData[key];

    try {
      value = JSON.parse(value);

      if (typeof value === 'number') {
        value = String(value);
        array.splice(array.indexOf(value), 1);
        return false;
      }

      if (typeof value === 'boolean') {
        array.splice(array.indexOf(value), 1);
        return false;
      }

      if (typeof value === 'object') {
        $.each(array, function(i) {
          if (JSON.stringify(array[i]) === JSON.stringify(value)) {
            array.splice(i, 1);
            return false;
          }
        });
      }

    } catch (stringValue) {
      array.splice(array.indexOf(value), 1);
    }

  },
  // add new filter
  appendFilter : function(filter, filterId, jsonData) {

    if ($(filterId).length !== 0) {
      console.log('Filter already loaded');
      GenesysFilterUtil.showFilter(filterId);
      return;
    }

    $.ajax('/' + document.documentElement.lang + '/additional-filter', {
      type : 'GET',
      data : 'filter=' + filter,

      success : function(data) {
        jsonData[filter] = [];
        $('#allfilters').append(data);
        GenesysFilterUtil.registerAutocomplete('#allfilters', jsonData);
        GenesysFilterUtil.showFilter(filterId);
      },
      error : function(error) {
        console.log(error);
      }
    });
  },
  showFilter : function(filterEl) {
    var fe = $(filterEl);
    fe.toggleClass('edit-filter');
    fe.find('.filter-values').parent().toggleClass('col-lg-9').toggleClass('col-lg-4');
  },
  normKey : function(key) {
    return key.replace(/\./g, '-').replace(/:/g, '_');
  }
};

var GenesysFilter = {
  normKey : GenesysFilterUtil.normKey,
  // add filter with autocomplete or exact field
  filterAutocomplete : function(element, jsonData) {

    var key = $(element).attr('i-key');
    var normKey = GenesysFilter.normKey(key);

    var inputId = '#' + normKey + '_input';
    var value = $(element).parent().find(inputId).val();

    if (value === '') {
      return;
    }

    if (!GenesysFilterUtil.existInJson(value, key, jsonData)) {
      GenesysFilterUtil.appendHtml(key, value, value, element);
      jsonData[key] = GenesysFilterUtil.collectData(key, value, jsonData);
      $(inputId).val('');
    }
    return jsonData;
  },
  // add filter with two input text fields for enter values of range
  filterRange : function(element, jsonData,i18n) {
    var key = $(element).attr('i-key');
    var normKey = GenesysFilter.normKey(key);
    var inputId1 = '#' + normKey + '_input_1';
    var inputId2 = '#' + normKey + '_input_2';

    var inputValue1 = $(inputId1).val();
    var inputValue2 = $(inputId2).val();

    var value = null;

    var range = {};

    var rangeValue = [];

    var jsonValue = [];

    var exist = jsonData[key] !== null;
    if (exist) {
      jsonValue = jsonData[key];
    }

    // var notNull = inputValue1 !== '' && inputValue2 !== '';

    if (!isNaN(inputValue1) && !isNaN(inputValue2)) {
      if (inputValue1 === inputValue2) {

        if (inputValue1 === '') {
          // Ignore blank
          return;
        }
        jsonValue.push(parseFloat(inputValue1));
        value = inputValue1;
      } else if (inputValue1 !== '' && inputValue2 === '') {

        range.min = parseFloat(inputValue1);
        jsonValue.push(range);
        value = i18n.moreThan + inputValue1;
      } else if (inputValue1 === '' && inputValue2 !== '') {

        range.max = parseFloat(inputValue2);
        jsonValue.push(range);
        value = i18n.lessThan + inputValue2;
      } else if (inputValue1 !== '' && inputValue2 !== '') {

        rangeValue.push(parseFloat(inputValue1));
        rangeValue.push(parseFloat(inputValue2));

        range.range = rangeValue;
        jsonValue.push(range);
        value = i18n.between + inputValue1 + i18n.varEnd + inputValue2;
      }

      GenesysFilterUtil.appendHtml(key, value, range, element);

      jsonData[key] = jsonValue;
      $(inputId1).val('');
      $(inputId2).val('');
      return jsonData;
    }
  },
  // add filter with few checkboxes
  filterList : function(element, jsonData, textValue) {
    var key = $(element).attr('i-key');
    var normKey = GenesysFilter.normKey(key);
    var option = $(element).val();

    var inputId = '#' + normKey + option + '_input';
    var value = $(element).parent().find(inputId).val();

    var checked = $(element).parent().find(inputId).is(':checked');

    if (checked && !GenesysFilterUtil.existInJson(value, key, jsonData)) {
      jsonData[key] = GenesysFilterUtil.collectData(key, value, jsonData);
      GenesysFilterUtil.appendHtml(key, textValue, value, element);

    } else {
      var xkey = normKey + value;
      GenesysFilterUtil.removeValue(value, key, jsonData);
      $('div[x-key=' + xkey + ']').remove();
    }
  },
  // add filter with three checkboxes true/false/null values
  filterBoolean : function(element, jsonData,i18n) {
    var value = $(element).val();
    var textValue;
    if (value === 'null') {
      value = null;
      textValue = i18n.varNull;
    }
    if (value === 'true') {
      value = true;
      textValue = i18n.varTrue;
    }
    if (value === 'false') {
      value = false;
      textValue = i18n.varFalse;
    }

    var key = $(element).attr('i-key');
    var normKey = GenesysFilter.normKey(key);

    var checked = $(element).is(':checked');

    if (checked && !GenesysFilterUtil.existInJson(value, key, jsonData)) {
      GenesysFilterUtil.appendHtml(key, textValue, value, element);
      jsonData[key] = GenesysFilterUtil.collectData(key, value, jsonData);

    } else {
      var xkey = normKey + value;
      GenesysFilterUtil.removeValue(value, key, jsonData);
      $('div[x-key=' + xkey + ']').remove();
    }
  },

  // add filter with options for crop
  filterCrop : function(element, jsonData) {
    var value = element.val();
    if (value === 'null' || value === '') {
      value = null;
    }
    var key = $(element).attr('i-key');
    delete jsonData[key];
    // Clear previous crop selection
    element.parent().parent().parent().parent().find('.complex').remove();

    if (value !== null) {
      GenesysFilterUtil.appendHtml(key, value, value, element);
      jsonData[key] = GenesysFilterUtil.collectData(key, value, jsonData);
      $('#menu2').show();
    } else {
      $('#menu2').hide();
    }
  },

  filterLike : function(element, jsonData, i18n) {
    var key = $(element).attr('i-key');
    var normKey = GenesysFilter.normKey(key);

    var inputId = '#' + normKey + '_input';
    var value = $(element).parent().find(inputId).val();
    if (value === '') {
      return;
    }

    var likeArray = {};
    likeArray.like = value;
    var likeValue = i18n.like + value;

    if (!GenesysFilterUtil.existInJson(likeArray, key, jsonData)) {
      GenesysFilterUtil.appendHtml(key, likeValue, likeArray, element);
      jsonData[key] = GenesysFilterUtil.collectData(key, likeArray, jsonData);
      $(inputId).val('');
    }
    return jsonData;
  }

};

$(document).ready(function() {
  $('body').on('click', '.get_widget', function() {
    var clientId = $(this).val();
    $.ajax('/' + document.documentElement.lang + '/get_widget', {
      type : 'GET',
      data : 'clientId=' + clientId,
      success : function(data) {
        $('.modal-content').empty();
        $('.modal-content').append(data);
      },
      error : function(error) {
        console.log(error);
      }
    });
  });

  var sc = $('#selcounter');

  $.ajax({
    url : '/sel/json/selection',
    type : 'GET',
    success : function(data) {
      sc.html(data.length);
      sc.attr('x-size', data.length);
      if (data.length > 0) {
        for (var i = data.length - 1; i >= 0; i--) {
          $('.sel[x-aid=' + data[i] + ']').addClass('picked');
        }
      }
    }
  });

  $('body').on('click', '.sel[x-aid]', function(event) {
    event.preventDefault();
    var foo = $(this);
    var aid = foo.attr('x-aid');
    var action = foo.hasClass('picked') ? 'remove' : 'add';

    $.ajax({
      url : '/sel/json',
      type : 'POST',
      dataType : 'json',
      contentType : 'application/json; charset=utf-8',
      data : JSON.stringify({
        'action' : action,
        'id' : aid
      }),
      beforeSend : function() {
        if (action === 'add') {
          foo.addClass('picked');
        } else {
          foo.removeClass('picked');
        }
      },
      success : function(data) {
        console.log(data);
        var sc = $('#selcounter');
        sc.html(data.count);
        /*
         * if (data.count===0) { sc.hide(); } else { sc.show(); }
         */
      },
      error : function(error) {
        if (action === 'add') {
          foo.removeClass('picked');
        } else {
          foo.addClass('picked');
        }
        console.log('Error:');
        console.log(error);
      }
    });
  });

});

$(document).ready(function() {
  $('#news a').click(function(e) {
    e.preventDefault();
    $(this).tab('show');
  });
});

// show mobile menu
$('.mobile-menu-show').click(function() {
  $('.mobile-header .header-top .mobile-menu-show').css('display', 'none');
  $('.mobile-header .header-top .mobile-menu-hide').css('display', 'inline-block');
  $('.logo-container').css('display', 'none');
  $('.mobile-header #search').show();
  $('#left-col').show();
  $('#nav-main').show();
});

$('.mobile-menu-hide').click(function() {
  $('.mobile-header .header-top .mobile-menu-show').css('display', 'inline-block');
  $('.mobile-header .header-top .mobile-menu-hide').css('display', 'none');
  $('.logo-container').css('display', 'block');
  $('.mobile-header #search').hide();
  $('#left-col').hide();
  $('#nav-main').hide();
});

// CSRF protection
$(function() {
  var token = $('meta[name="_csrf"]').attr('content');
  var header = $('meta[name="_csrf_header"]').attr('content');
  $(document).ajaxSend(function(e, xhr /* , options */) {
    xhr.setRequestHeader(header, token);
  });
});

/**
 * Tweet
 */
$('.twitter-share-button').click(function(e) {
  e.preventDefault();
  var w=window.open($(this).attr('href'), 'shareWindow', 'width=600,height=300,scrollbars=yes,toolbars=no');
  w.focus();
});

$('.twitter-follow-button').click(function(e) {
  e.preventDefault();
  var w=window.open($(this).attr('href'), 'shareWindow', 'width=600,height=300,scrollbars=yes,toolbars=no');
  w.focus();
});

/**
 * Share on LinkedIn 
 */
$('.linkedin-share-button').click(function(e) {
  e.preventDefault();
  var w=window.open($(this).attr('href'), 'shareWindow', 'width=600,height=300,scrollbars=yes,toolbars=no');
  w.focus();
});

$('[x-href]').click(function(e) {
  e.preventDefault();
  var url=$(this).attr('x-href');
  console.log('X-Url: ' + url);
  window.location=url;
});

//
// function enableSessionWarning(secondsBeforeExpiry) {
// if ($('#session_expired_popup').length === 0) {
// return;
// }
//
// $(function() {
// $('#session_expired_popup').dialog({
// dialogClass : 'no-close',
// autoOpen : false,
// modal : true,
// closeOnEscape : false,
// show : {
// duration : 0
// }
// });
// });
//
// setTimeout(function() {
// $('#session_expired_popup').dialog('open');
// setTimeout(function() {
// $('#session_expired_popup').dialog('close');
// }, 60 * 1000);
// }, ((secondsBeforeExpiry - 65) * 1000));
// }
