/**
 * Copyright 2015 Global Crop Diversity Trust
 * jQuery $ and $.ajax() required.
 */

'use strict';

var GenesysPGR = function(baseUrl, clientId) {
  var defaultOptions = {
    startAt: 1,
    maxRecords: 50,
    success: function() {},
    error: function() {}
  };
  
  return {
    clientId: clientId,
    baseUrl: baseUrl,
    clientSecret: null,
    defaultOptions: defaultOptions,
    
    getUrl : function(apiCall) {
      return this.baseUrl + '/webapi' + apiCall + '?client_id=' + this.clientId + (this.clientSecret!==null ? '&client_secret=' + this.clientSecret : '');
    },
    
    listAccessions : function (filter, opts) {
      var o = $.extend({}, GenesysPGR.defaultOptions, opts); 
      var json={filter: JSON.stringify(filter), startAt: Math.max(1, o.startAt), maxRecords: o.maxRecords };

      $.ajax(this.getUrl('/v0/acn/filter'), {
        dataType: 'json',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(json),

        success: function (accessions) {
          o.success(accessions);
        },

        error: function (errorAsync) {
          o.error(errorAsync);
        }
      });
    },
    
    overview : function (filter, opts) {
      var o = $.extend({}, GenesysPGR.defaultOptions, opts); 

      $.ajax(this.getUrl('/v0/acn/overview'), {
        dataType: 'json',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(filter),

        success: function (accessions) {
          o.success(accessions);
        },

        error: function (errorAsync) {
          o.error(errorAsync);
        }
      });
    }
  };
};
	